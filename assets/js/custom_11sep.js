$(document).ready(function(){
    $('#location-modal ul a').click(function(){
       var thisText = $(this).text();
       $('.form_group.location').find('span').text(thisText);
       $(this).parents('#location-modal').removeClass('open');
       $('#location-modal .modal-body').removeClass('slideInDown');
       $('#location-modal ul a').removeClass('active');
       $(this).addClass('active');
    })
    

    
    

    $('.form_group.location').click(function(){
        $('#location-modal').addClass('open');
        $('#location-modal .modal-body').addClass('slideInDown');
    })

    $('.modal-body .close').click(function(){
        $(this).parents('#location-modal').removeClass('open');
        $('#location-modal .modal-body').removeClass('slideInDown');
    })

    $('.client_slider').owlCarousel({
        loop:true,
        margin:70,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:2
            }
        }
    })

    $('.similar-products').owlCarousel({
        loop:true,
        margin:15,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })

    $('.brought-products').owlCarousel({
        loop:true,
        margin:15,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    $('#gotop').click(function(){
        $('body, html').animate(
            {
                scrollTop: 0,
            }
        )
    });

    



    $('.tabblocks li a').click(function(){
        var t = $(this).attr('id');
    
        if($(this).hasClass('active')){

        }
        else
        {
            $('.tabblocks li a').removeClass('active');
            $('.tab-con').removeClass('show-tab-container');
            $(this).addClass('active');
            $(t).addClass('show-tab-container');
        }
   
  });



    $('.gallery a').click(function(event){
        event.preventDefault();
        var largeImage = $(this).attr('data-full');
        $('.selected').removeClass();
        $(this).addClass('selected');
        $('.full img').hide();
        $('.full img').attr('src', largeImage);
        $('.full img').fadeIn();
  
  
      }); // closing the listening on a click
      $('.full img').on('click', function(){
        var modalImage = $(this).attr('src');
        $.fancybox.open(modalImage);
      });

      $('.faq_container li span').click(function(){
          if($(this).hasClass('active')){
            $(this).next().slideUp();
            $(this).removeClass('active')
          }
          else
          {
              //$('.faq_container li span').removeClass('active');
              $(this).next().slideDown();
              $(this).addClass('active')
          }
      })

      

      $('.cart_wrapper .cart_block .left .cartBox .pro_remove_add .remove').click(function(){
        var checkstr =  confirm('Are you sure you want to delete this?');
          
          if(checkstr == true){
            $(this).parents('.cartBox').remove();
          }else{
          return false;
          }
      })

      $('.sebmunu-click').click(function(){
          $(this).toggleClass('active');
         $(this).next().slideToggle();

         if($(this).hasClass('active')){
             $(this).removeClass('lnr-chevron-right').addClass('lnr-chevron-down');
         }
         else
         {
            $(this).removeClass('lnr-chevron-down').addClass('lnr-chevron-right');
         }
      })


      $('.menubars').click(function(){
          if($(this).hasClass('active')){
            $('.left-block').animate({left:'0'}, 300)
            $('.right-block').removeClass('mrg0');
            $('.right-block .header').animate({left:'240px'}, 300);
            $(this).removeClass('active');   
          }
          else
          {
            $('.left-block').animate({left:'-240px'}, 300);
            $('.right-block').addClass('mrg0');
            $('.right-block .header').animate({left:'0'}, 300);
            $(this).addClass('active');
          }
      })

     

     


    $('.tablinks li a').click(function(event){
          event.preventDefault();
        var tabb = $(this).attr('href');
    
        if($(this).hasClass('active')){

        }
        else
        {
            $('.tablinks li a').removeClass('active');
            $('.tabcon').removeClass('show-tab-container');
            $(this).addClass('active');
            $(tabb).addClass('show-tab-container');
        }
   
    });

    $('#rightSugTabsLink li a').click(function(event){
        event.preventDefault();
        var tabb = $(this).attr('href');
  
      if($(this).hasClass('active')){

      }
      else
      {
          $('#rightSugTabsLink li a').removeClass('active');
          $('.tabcon-right').removeClass('show-tab-container');
          $(this).addClass('active');
          $(tabb).addClass('show-tab-container');
      }
 
  });

  $('.top_clock').click(function(){
    if($(this).hasClass('active')){
        $(this).next().slideUp();
        $(this).removeClass('active');
    }else
    {  
        $('.rewards').hide();
        $('.top_clock').removeClass('active');
        $(this).next().slideDown();
        $(this).addClass('active')
    }
})



$('.rewards').click(function(event){
    event.stopPropagation();
});


function mainBody() {
    var winWidth = $(window).width();
    if(winWidth <= 991) {
        $('.left-block').css('left', '-240px');
        $('.right-block').css('margin-left', '0');
        $('.right-block .header').css('left', '0');
        $('.menubars').addClass('active');

        $('.menubars').click(function(){
        if($(this).hasClass('active')){
            $('.right-block').css('margin-left', '0');
        }else{
            $('.right-block').css('margin-left', '240px');
        }
        })
    }
}

mainBody();

$(window).resize(function(){
    mainBody();
})

// CODE FOR HEALTH PACKAGE

 $('.date-list').owlCarousel({
        loop:true,
		margin:10,
		nav:true,
		navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:5
			}
		}
       
    })

    $('.bkap_wrap .close').click(function(){
        $('.bkap_wrap').hide();
    })

    

    $('.bookapoinment').click(function(){
        $(this).parent().next().show();
    })

    $('.sendBtn').click(function(){
        $('#popUpModal').addClass('open');
    })

    $('button.close').click(function(){
        $('#popUpModal').removeClass('open');
    })
     



    
})

AOS.init();











