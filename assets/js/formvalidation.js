/**
 * File : Form Validation for fronted
 * 
 * This file contain the validation of edit user form
 * 
 * @author Santosh Kumar
 */
 
 
 
	 /**
	  * This function  use for buyer Registration for fronted
	 * 
	 * @author Santosh Kumar
		Date :22 Aug 2018
	 */
	 function formValidator(e)
	{

		var regex = /^[a-zA-Z]+$/;
		var nm=document.forms["form_main"]["name"].value;
		var em = document.forms["form_main"]["email"].value;
		var atpos = em.indexOf("@");
		var dotpos = em.lastIndexOf(".");
		var mn = document.forms["form_main"]["mobile"].value;
		var mobNumLen = document.forms["form_main"]["mobile"].value.length; 
		if( nm==""  &&  em=="" &&  mn=="")
		{
		c_error.textContent = "ALL Field must be fill Properly";
		document.form_main.email.focus();
		return false;	
		}

		if(!nm.match(/^[a-zA-Z]+$/))
		{
		name_error.textContent = "Name Field must be alphabets";
		document.form_main.name.focus() ;
		return false;
		} 
			if(em == null || em=="")
			{

			email_error.textContent = "Please  Enter  the Email ID";
			document.form_main.email.focus();
			return false;
			} 
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=em.length)
		{

		email_error.textContent = "Please  Enter  Vailid  Email ID ";
		document.form_main.email.focus();
		return false;
		}

		if(mn == "" || mn == null || isNaN(mn) ||
		mn.length < 10 || mn.length >10 )
		{
		mob_error.textContent = "Please  Enter  Vailid  Mobile Number";
		document.form_main.mobile.focus() ;
		return false;
		} 
	  return true ;

	}

	/**
	  * This function  use for buyer login for fronted
	 * 
	 * @author Santosh Kumar
		Date :22 Aug 2018
	 */

	 function loginValidator(e)
	{

		var em = document.forms["form_main"]["email"].value;
		var atpos = em.indexOf("@");
		var dotpos = em.lastIndexOf(".");
		var ps=document.forms["form_main"]["password"].value;

		if( em==null &&  em=="")
		{
		c_error.textContent = "Please Enter User Name and Password";
		return false;	
		}


		if(em == null || em=="")
		{
		
			email_error.textContent = "Please Enter  Email ID";
			
			return false;
		} 
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=em.length)
		{
			
			email_error.textContent = "Please Enter Valid Email ID";
			document.form_main.email.focus();
			return false;
		}


		if(ps == null || ps == "" )
		{
			password_error.textContent = "Plese Enter Password";
			return false;
		} 


	   return true ;

    }
	
	
	/**
	  * This function  use for buyer Reset Password for fronted
	 * 
	 * @author Santosh Kumar
		Date :22 Aug 2018
	 */
	function forgotValidator(e)
	{
		
		var em = document.forms["form_main"]["email"].value;
		var atpos = em.indexOf("@");
		var dotpos = em.lastIndexOf(".");
				if(em == null || em=="")
		{
		
			email_error.textContent = "Please Enter  Email ID";
			
			return false;
		} 
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=em.length)
		{
			
			email_error.textContent = "Please Fill  all  field";
			
			return false;
		}
		return true
	}
	
		function password_Validator(e)
	{
		
		var pass = document.forms["form_main"]["password"].value;
		
				if(pss == null || ps=="")
		{
		
			password_error.textContent = "Please Enter The Password";
			
			return false;
		} 
		
		return true
	}
	
	
	
	/**
	  * This function  use for buyer change Password for fronted
	 * 
	 * @author Santosh Kumar
		Date :22 Aug 2018
	 */
	
	 function validatePassword() {
		
		
		var currentPassword = document.getElementById("currentPassword").value;
        var newPassword = document.getElementById("newPassword").value;
        var confirmPassword = document.getElementById("confirmPassword").value;
		
		
		
		 if (currentPassword =="" && newPassword =="" && confirmPassword =="" ) {
			
            alert("Please Enter  all Field ");
            return false;
        }
		
		
		 if (currentPassword =="" ) {
			
            alert("Please Enter  Old  Password ");
            return false;
        }
		
		 if (newPassword =="" ) {
			
            alert("Please Enter  New  Password ");
            return false;
        }
		
        if (newPassword != confirmPassword) {
			
              alert("new password and  confirmPassword Not Match  ");
            return false;
        }
		  
		
        return true;
    }
	
	
	/**
	  * This function  use for admin  addNewChemistShops   redio button on select show hide text box
	 * 
	 * @author Santosh Kumar
		Date :29 Aug 2018
	 */
	
	
	  $(function () {
        $("input[name='chkPassPort']").click(function () {
            if ($("#chkYes").is(":checked")) {
                $("#dvPassport").show();
            } else {
                $("#dvPassport").hide();
            }
        });
		
		
		 $("input[name='chkPassPort1']").click(function () {
            if ($("#chkYes").is(":checked")) {
                $("#dvPassport1").show();
            } else {
                $("#dvPassport1").hide();
            }
        });
    });
	