/**
 * File : addUser.js
 * 

 * 
 * Using validation plugin : jquery.validate.js
 * 
*/

 
	/* validation for user */
	jQuery(document).ready(function(){
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		rules:{
			fname :{ required : true },
			email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
			password : { required : true },
			cpassword : {required : true, equalTo: "#password"},
			mobile : { required : true, digits : true },
			role : { required : true, selected : true}
		},
		messages:{
			fname :{ required : "This field is required" },
			email : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
			password : { required : "This field is required" },
			cpassword : {required : "This field is required", equalTo: "Please enter same password" },
			mobile : { required : "This field is required", digits : "Please enter numbers only" },
			role : { required : "This field is required", selected : "Please select atleast one option" }			
		}
		
		
	
		
		
		
		

	});
	
	
	
	
	
	
	/*validation for adding mastercode */	
		
		var addHospital= $("#addHospital");
	
	 var validator = addHospital.validate({
		
		rules:{
			organisation :{ required : true },
			//address : { required : true },
			location : { required : true },
			state : {required : true, selected : true},
			district : { required : true, selected : true},
			city : { required : true, selected : true},
			timefrom : { required : true, selected : true},
			timeto : { required : true, selected : true},
		
			
			
		},
		messages:{
			organisation :{ required : "This field is required" },
			//email : { required : "This field is required", email : "Please enter valid email address"},
			
			//address :{ required : "This field is required" },
			location :{ required : "This field is required" },
			state : { required : "This field is required" },
			district : { required : "This field is required" },
			city : { required : "This field is required"},
			timefrom : { required : "This field is required"},
			timeto : { required : "This field is required"},
		
		}
	
		});
		
	
	
	
	
	
	
	
		
    /*validation for adding hospital doctors */	
		
	var addHospitalDoctors = $("#addHospitalDoctors");
	
	var validator = addHospitalDoctors.validate({
		
		rules:{
			mastercode :{ required : true, selected : true },
			doctorname : { required : true },
			specialization : { required : true },
			charges : {required : true},
			discountoffered : { required : true},
			discountdisplayed : { required : true},
			opinionmail1 : { required : true},
			opinionviamailcharge : { required : true},
			opinionvideo : { required : true},
			opinionviavideocharge : {required : true},
			relship_manager : {required : true},
			
		
			
			
		},
		messages:{
			mastercode :{ required : "This field is required" },
			doctorname :{ required : "This field is required" },
			specialization :{ required : "This field is required" },
			charges : { required : "This field is required" },
			discountoffered : { required : "This field is required" },
			discountdisplayed : { required : "This field is required"},
			opinionmail1 : { required : "This field is required" },
			opinionviamailcharge : { required : "This field is required" },
			
			opinionvideo   : { required : "This field is required"},	
			opinionviavideocharge :{ required : "This field is required" },
			relship_manager :{ required : "This field is required" },
					
		
		}
	
		});
		
	
	
	
	    /*validation for addHospitalReferredDoctors */	
		
	var addHospitalReferredDoctors = $("#addHospitalReferredDoctors");
	
	var validator = addHospitalReferredDoctors.validate({
		
		rules:{
			mastercode :{ required : true, selected : true },
			doctorname : { required : true },
			specialization : { required : true },
			charges : {required : true},
			discountoffered : { required : true},
			discountdisplayed : { required : true},
			opinionmail1 : { required : true},
			opinionviamailcharge : { required : true},
			opinionvideo : { required : true},
			opinionviavideocharge : {required : true},
			relship_manager : {required : true},
			addressof_visitclinic : {required : true},
		    experience : {required : true},
			charges : {required : true},
			
			
		},
		messages:{
			mastercode :{ required : "This field is required" },
			doctorname :{ required : "This field is required" },
			specialization :{ required : "This field is required" },
			charges : { required : "This field is required" },
			discountoffered : { required : "This field is required" },
			discountdisplayed : { required : "This field is required"},
			opinionmail1 : { required : "This field is required" },
			opinionviamailcharge : { required : "This field is required" },
			
			opinionvideo   : { required : "This field is required"},	
			opinionviavideocharge :{ required : "This field is required" },
			relship_manager :{ required : "This field is required" },
			addressof_visitclinic :{ required : "This field is required" },	
            experience :{ required : "This field is required" },
            charges :{ required : "This field is required" },			
	
		}
	
		});
	
		
		
		/*validation for Health Checkup Package  */	
	var healthcheckuppackageForm = $("#healthcheckuppackage");
	
	var validator = healthcheckuppackageForm.validate({
		
		rules:{
			packagename :{ required : true },
			cost : { required : true },
			discount : { required : true }
			
		
			
			
		},
		messages:{
			packagename :{ required : "This field is required" },
			cost :{ required : "This field is required" },
			discount :{ required : "This field is required" }
			
		
		}
	
		});
		
		
		
			/*validation for Health Checkup Test  */	
	var healthcheckuptestForm = $("#healthcheckuptest");
	
	var validator = healthcheckuptestForm.validate({
		
		rules:{
			testname :{ required : true },
			cost : { required : true },
			discount : { required : true }
			
		
			
			
		},
		messages:{
			testname :{ required : "This field is required" },
			cost :{ required : "This field is required" },
			discount :{ required : "This field is required" }
			
		
		}
	
		});
		
		
	
		
		
		
		
		
				/*validation for Health Checkup Test Parameter  */	
	var healthcheckuptestparameter = $("#healthcheckuptestparameter");
	
	var validator = healthcheckuptestparameter.validate({
		
		rules:{
			parameter :{ required : true }
		
			
		
			
			
		},
		messages:{
			parameter :{ required : "This field is required" }
			
			
		
		}
	
		});
		
		
		
		
		
		/*validation for  edit diagnostic */	
		
		
	var editHospitalDiaganosticForm= $("#editHospitalDiaganosticForm");
	var validator = editHospitalDiaganosticForm.validate({
		rules:{
			registrationNo :{ required : true },
			discountofferedtest :{ required : true },
			discountofferedtestdisplayed :{ required : true },
			discountofferedhealthcheckup :{ required : true },
			discountofferedhealthcheckupdisplayed :{ required : true },
			timefrom :{ required : true },
			timeto :{ required : true }
			
			
			
			
		},
		messages:{
			registrationNo :{ required : "Please Enter Registration No." },
			discountofferedtest :{ required : "Please Enter Discount Offered On Test " },
			discountofferedtestdisplayed :{ required : "Please Enter Discount Offered On Test Displayed" },
		    discountofferedhealthcheckup :{ required : "Please Enter Discount Offered  On Health Checkup" },
			discountofferedhealthcheckupdisplayed :{ required : "Please Enter Discount Offered On Health Checkup Displayed" },
			timefrom :{ required : "Please Select timefrom" },
			timeto :{ required : "Please Select timeto" }
			
		
		}
	
		});
		
		
		
		var addHospitalDiaganosticForm= $("#addHospitalDiaganosticForm");
	    var validator = addHospitalDiaganosticForm.validate({
		rules:{
			mastercode :{ required : true },
			registrationNo :{ required : true },
			discountofferedtest :{ required : true },
			discountofferedtestdisplayed :{ required : true },
			discountofferedhealthcheckup :{ required : true },
			discountofferedhealthcheckupdisplayed :{ required : true },
			timefrom :{ required : true },
			timeto :{ required : true },
			photograph :{ required : true }
			
			
			
		},
		messages:{
			mastercode :{ required : "Please Select Master code." },
			registrationNo :{ required : "Please Enter Registration No." },
			discountofferedtest :{ required : "Please Enter Discount Offered On Test " },
			discountofferedtestdisplayed :{ required : "Please Enter Discount Offered On Test Displayed" },
		    discountofferedhealthcheckup :{ required : "Please Enter Discount Offered  On Health Checkup" },
			discountofferedhealthcheckupdisplayed :{ required : "Please Enter Discount Offered On Health Checkup Displayed" },
			timefrom :{ required : "Please Select From Time" },
			timeto :{ required : "Please Select to Time" },
			photograph :{ required : "Please Select photograph" }
			
		
		}
	
		});
		
		
		
		
/*validation for  edit chemist Shop */	
		
		
	var editHospitalChemistShopForm = $("#editHospitalChemistShopForm");
	var validator=editHospitalChemistShopForm.validate({
		rules:{
			registrationn_no:{ required : true },
			offeron_ayurvedic_medicines:{ required : true },
			displayon_ayurvedic_medicines:{ required : true },
			offeredon_homeopathy_medicines:{ required : true },
			displayon_homeopathy_medicines:{ required : true },
			offeredon_allopathy_medicines:{ required : true },
			displayedon_allopathy_medicines:{ required : true },
			offeredon_unani_medicines:{ required : true },
			displayon_unani_medicines:{ required : true },
			
			dis_off_onconsumables:{ required : true },
			dis_tobe_diplay_onconsumables:{ required : true },
			dis_off_onfmcg:{ required : true },
			dis_tobe_display_onemcg:{ required : true },
			dis_off_onspecific_brand:{ required : true },
			dis_tobe_display_onspecific_brand:{ required : true },
			
			timefrom:{ required : true },
			timeto:{ required : true }
			
			
        },
		messages:{
			registrationn_no:{ required : "Please Enter Registration No."},
			offeron_ayurvedic_medicines:{ required : "Please Enter    Ayurvedic Offered on Medicines"},
			displayon_ayurvedic_medicines:{ required : "Please Enter  Ayurvedic Displayed on Medicines"},
			offeredon_homeopathy_medicines:{ required : "Please Enter  Homeopathy Offered on Medicines"},
			displayon_homeopathy_medicines:{ required : "Please Enter  Homeopathy Displayed on Medicines"},
			offeredon_allopathy_medicines:{ required : "Please Enter    Allopathy Offered on Medicines"},
			displayedon_allopathy_medicines:{ required : "Please Enter  Allopathy Displayed on Medicines"},
			offeredon_unani_medicines:{ required : "Please Enter  unani Offered on Medicines"},
			displayon_unani_medicines:{ required : "Please Enter  unani Displayed on Medicines"},
			
			dis_off_onconsumables:{ required : "Please Enter  Discount on consumables"},
			dis_tobe_diplay_onconsumables:{ required : "Please Enter  diplay Discount on consumables"},
			dis_off_onfmcg:{ required : "Please Enter Discount on FMCG "},
			dis_tobe_display_onemcg:{ required : "Please Enter Discount  Displayed on FMCG "},
			dis_off_onspecific_brand:{ required : "Please Enter  Discount on Specific Brand"},
			dis_tobe_display_onspecific_brand:{ required : "Please Enter  Discount Displayed on Specific Brand"},
			timefrom:{ required : "Please Select timefrom"},
			timeto:{ required : "Please Select timeto"}
			
		
			
		
		}
	
		});
		
		
		
	/*validation for addDocSpecialization  */	
	var adddoctorSpecialization = $("#adddoctorSpecialization");
	
	var validator = adddoctorSpecialization.validate({
		
		rules:{
			specialization :{ required : true }
		
			
		
			
			
		},
		messages:{
			specialization :{ required : "Please Enter The Specialization " }
			
			
		
		}
	
		});
		
			
	
	
		/*validation for adddoctorQualification  */	
	var adddoctorQualification = $("#adddoctorQualification");
	
	var validator = adddoctorQualification.validate({
		
		rules:{
			qualification :{ required : true }
		
			
		},
		messages:{
			qualification :{ required : "Please Enter The Qualification" }
			
			
		
		}
	
		});


        /*validation for addCorporatePackages  */
        var addCorporatePackages = $("#addCorporatePackages");
        var validator = addCorporatePackages.validate({
            rules:{
                healthpackage :{ required : true },
                corporateclient :{ required : true },
                startdate :{ required : true },
                enddate :{ required : true },
                dataImport :{ required : true }
            },
            messages:{
                healthpackage :{ required : "Please Enter The healthpackage" },
                corporateclient :{ required : "Please Enter The corporateclient" },
                startdate :{ required : "Please Select Service Start Date" },
                enddate :{ required : "Please Select Service End Date" },
                dataImport :{ required : "Please Select .csv File" }
            }
        });
		
		
		
		
	
   /*validation for Health Checkup linking  */	
	var healthcheckuptestlinkingForm = $("#healthcheckuptestlinking");
	
	var validator = healthcheckuptestlinkingForm.validate({
		
		rules:{
			
			healthpackage:{ required : true, selected : true },
			vendor_decided_name:{ required : true },
			facility2:{ required : true, selected : true },
		    mastercode:{ required : true, selected : true },
			subcode:{ required : true, selected : true },
		
			
			
		},
		messages:{
			
			healthpackage:{ required : "This field is required" },
		    vendor_decided_name :{ required : "This field is required" },
		    facility2:{ required : "This field is required" },
			mastercode:{ required : "This field is required" },
			subcode:{ required : "This field is required" },
		
		}
	
		});
	
	
		
		
		
		
		
		
	
});
