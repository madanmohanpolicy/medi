/** delete user */
jQuery(document).ready(function(){
	
	
	jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "admin/deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	/*delete mastercode*/
	jQuery(document).on("click", ".deleteHospital", function(){
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteHospital",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Hospital successfully deleted"); }
				else if(data.status = false) { alert("Hospital deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	
		/*delete HealthPAckage Linking*/
	jQuery(document).on("click", ".deleteHealthPackageLinking", function(){
	
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteHealthPackageLinking",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this health Package Linking ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("HealthPackage Linking successfully deleted"); }
				else if(data.status = false) { alert("HealthPackage LInking deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	/*delete hospital doctore */
	
	jQuery(document).on("click", ".deleteHospitalDoctor", function(){
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteHospitalDoctor",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this doctor  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Hospital Doctor successfully deleted"); }
				else if(data.status = false) { alert("Hospital Doctor deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	/*delete Medical Practitioner*/
	
	jQuery(document).on("click", ".deleteMedicalPractitioner", function(){
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteMedicalPractitioner",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Medical Practitioner doctor  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Medical Practitioner Doctor successfully deleted"); }
				else if(data.status = false) { alert("Medical Practitioner  Doctor deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	/*delete MEdical Health Package*/
	
	jQuery(document).on("click", ".deleteHealthPackage", function(){
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteHealthPackage",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Medical Health Package  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Medical HEalth PAckage successfully deleted"); }
				else if(data.status = false) { alert("Medical Health  Package deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
			/*delete Medical Health Test*/
	
	jQuery(document).on("click", ".deleteHealthTest", function(){
		
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteHealthTest",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Medical Health Test  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Medical Health Test successfully deleted"); }
				else if(data.status = false) { alert("Medical Health  Test deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	/* Delete Hospital  Doctor schedule */
	
	
	jQuery(document).on("click", ".deleteDoctorSchedule", function(){
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteDoctorSchedule",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Doctor Schedule  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Doctor Schedule successfully deleted"); }
				else if(data.status = false) { alert("Doctor Schedule deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	/* Delete Hospital  Doctor schedule */
	
	
	jQuery(document).on("click", ".deletepackageschedule", function(){
		
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteHealthPackageSchedule",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this  Schedule  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert(" Schedule successfully deleted"); }
				else if(data.status = false) { alert(" Schedule deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	
	
	
	
	
	
	/* Delete Health Checkup Test Parameter */
	
	
	
	jQuery(document).on("click", ".deleteHealthTestParameter", function(){
		var id = $(this).data("id"),
	
			hitURL = baseURL + "admin/deleteHealthTestParameter",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this HealthCheckup Parameter  ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
			
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Test Parameter successfully deleted"); }
				else if(data.status = false) { alert("Test Parameter deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	

	jQuery(document).on("click", ".searchList", function(){
		
	});
	/* code for filling of district dropdown on selection of state */
	
	$('#state').change(function(){
	

  var state_id = $('#state').val();

  if(state_id != '')
  { 
	  
   $.ajax({
    url: baseURL+"admin/state/"+state_id,
    method:"POST",
    data:{state_id:state_id},
    success:function(data)
    {
		
		
		
		
     $('#district').html(data);

     
    }
   });
  }
 else {
	 
	 
	 $('#district').html('<option value="">Select District</option>');
 }
 });
 
 
 /* code for filling of city dropdown on selection of district */
 
 
 
	$('#district').change(function(){
	

  var district_id = $('#district').val();

  if(district_id != '')
  { 

   $.ajax({
    url: baseURL+"admin/city/"+district_id,
    method:"POST",
    data:{district_id:district_id},
    success:function(data)
    {
		
		
		
		
     $('#city').html(data);

     
    }
   });
  }
 else {
	 
	 
	 $('#city').html('<option value="">Select City</option>');
 }
 });
 
 
 
 /* code for filling of pincode dropdown on selection of city */
 
 
 
	$('#city').change(function(){
	

  var city_id = $('#city').val();

  if(city_id != '')
  { 
	
   $.ajax({
    url: baseURL+"admin/pincode/"+city_id,
    method:"POST",
    data:{city_id:city_id},
    success:function(data)
    {
		
	
		
		
     $('#pincode').html(data);

     
    }
   });
  }
 else {
	 
	 
	 $('#pincode').html('<option value="">Select Pincode</option>');
 }
 });
 
 
 
 
 /* code for filling of Hospital/Diaganostic Center dropdown on selection of Type */
 
 
 
	$('#facility').change(function(){
		
	

  var facility = $('#facility').val();

  if(facility != '')
  { 

   $.ajax({
    url: baseURL+"admin/facilityname/"+facility,
    method:"POST",
    data:{facility:facility},
    success:function(data)
    {
		
		
		
		$("#facilityname").multiselect('destroy');
     $('#facilityname').html(data);
$("#facilityname").multiselect('destroy');
     	$('#facilityname').multiselect({
		includeSelectAllOption: true,
	  nonSelectedText: 'Select ',
	  enableFiltering: true,
	
       showCheckbox       : true, 
	  enableCaseInsensitiveFiltering: true,
	  buttonWidth:'100%'
	 });
	
	 
	 
	 


    }
   });
  }
 else {
	 
	
	 $('#facilityname').html('<option value="">Select </option>');
 }
 });
 
 
 /**
     * ##########################################################################
	* All These  below functions are  Added  By Santosh Kumar Verma 
	* ############################################################################
	*/
 
 
   /**
	* This function is used to delete Hospital Diaganostic  
	* create by Santosh Kumar 29 Aug 2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".deleteHospitalDiaganostic", function(){
		
		
		var id = $(this).data("id");
	
		   var id = $(this).data("id"),
	  
			hitURL = baseURL + "admin/deleteHospitalDiaganostic",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Hospital Diaganostic ?");
		
		if(confirmation)
		{
			
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Hospital Diaganostic successfully deleted"); }
				else if(data.status = false) { alert("Hospital Diaganostic deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	


    /**
	* This function is used to delete  Diaganostic  
	* create by Santosh Kumar 5 September  2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".deleteDiaganostic", function(){
		
		
		var id = $(this).data("id");
	
		   var id = $(this).data("id"),
	  
			hitURL = baseURL + "admin/deleteDiaganostic",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this  Diaganostic ?");
		
		if(confirmation)
		{
			
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Diaganostic successfully deleted"); }
				else if(data.status = false) { alert("Diaganostic deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	/**
	* This function is used to delete  hospital Chemist Shops 
	* create by Santosh Kumar 5 September  2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".deletehospitalChemistShop", function(){
		
		
		var id = $(this).data("id");
	
		   var id = $(this).data("id"),
	  
			hitURL = baseURL + "admin/deletehospitalChemistShop",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this hospital Chemist Shop ?");
		
		if(confirmation)
		{
			
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert(" Hospital Chemist Shop successfully deleted"); }
				else if(data.status = false) { alert(" Hospital Chemist Shop deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	
	/**
	* This function is used to delete  customer  
	* create by Santosh Kumar 10 September  2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".deleteCustomer", function(){
		
		
		var id = $(this).data("id");
	
		   var id = $(this).data("id"),
	  
			hitURL = baseURL + "admin/deleteCustomer",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this  Customer ?");
		
		if(confirmation)
		{
			
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
				
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Customer successfully deleted"); }
				else if(data.status = false) { alert("Customer deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	
	
	/**
	* This function is used to change customer status 
	* create by Santosh Kumar 10 September  2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".chnageCustomerStatus", function(){
		
		var st = $(this).data("status"); 
		
		var id = $(this).data("id");
		
		
		
		
		   var id = $(this).data("id"),

	  
			hitURL = baseURL + "admin/chnageCustomerStatus",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to change Customer status?");
		
		if(confirmation)
		{
			
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {
				"id":id,"status":st
			} 
			}).done(function(data){
				
				console.log(data);
			     //currentRow.parents('tr').load();
				 //$(this).parents('tr').load();
				  //$(this).closest('tr')load();
				 if(data.status = true) { alert("Customer  status  change successfully"); }
				else if(data.status = false) { alert("Customer status  change failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	/**
	* This function is used to delete  Doctor Specialization  
	* create by Santosh Kumar 10 September  2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".deleteDoctorSpecialization", function(){
		
		
		var id = $(this).data("id");
	
		   var id = $(this).data("id"),
	  
			hitURL = baseURL + "admin/deleteDoctorSpecialization",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Doctor Specialization?");
		
		if(confirmation)
		{
		
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
			
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Doctor Specialization successfully deleted"); }
				else if(data.status = false) { alert("Doctor Specialization deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	

	
	
	/**
	* This function is used to delete  Doctor Qualification  
	* create by Santosh Kumar 10 September  2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".deleteDoctorQualification", function(){
		
		
		var id = $(this).data("id");
	
		   var id = $(this).data("id"),
	  
			hitURL = baseURL + "admin/deleteDoctorQualification",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Doctor Qualification?");
		
		if(confirmation)
		{
		
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
			
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Doctor Qualification successfully deleted"); }
				else if(data.status = false) { alert("Doctor Qualification deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	
	/**
	* This function is used to delete  Referred Doctors Schedule   
	* create by Santosh Kumar 21 September  2018
	* @return boolean $result : TRUE / FALSE
	*/
	
	jQuery(document).on("click", ".deleteReferredDoctorSchedule", function(){
		
		
		var id = $(this).data("id");
	
		   var id = $(this).data("id"),
	  
			hitURL = baseURL + "admin/deleteReferredDoctorSchedule",
			currentRow = $(this);
			
		var confirmation = confirm("Are you sure to delete this Referred Doctor Schedule?");
		
		if(confirmation)
		{
		
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : {id : id} 
			}).done(function(data){
			
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Referred Doctor Schedule successfully deleted"); }
				else if(data.status = false) { alert("Referred Doctor Schedule deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
//end	
	
	
	
	
 /* Code for Hospital/addHealthPackageLinking */
  
$('#facility2').change(function(){

  var facility2 = $('#facility2').val();

  if(facility2 != '')
  { 

   $.ajax({
    url: baseURL+"admin/facilityname2/"+facility2,
    method:"POST",
    data:{facility:facility2},
	
    success:function(data)
    {
		
    
	 $("#mastercode").chosen('destroy');
	 $('#mastercode').html(data);
    $(".livesearch").chosen();	
	//alert(data); 
	
	  
	
    }
   });
  }
 else {
	 
	
  $('#mastercode').html('<option value="">Select</option>');
 }
 });
 
 //end
 
 
 
 
  /* Code for Hospital/add HealthPackageLinking subcode */
  
$('#mastercode').change(function(){

  var mastercode = $('#mastercode').val();
  //alert(mastercode);

  if(mastercode != '')
  { 

   $.ajax({
    url: baseURL+"admin/diasubcode/"+mastercode,
    method:"POST",
    data:{mastercode:mastercode},
	
    success:function(data)
    {
		
		
	 $("#subcode").chosen('destroy');
	 $('#subcode').html(data);
    $(".livesearch").chosen();	
	//alert(data); 
	
	  
	
    }
   });
  }
 else {
	 
	
  $('#subcode').html('<option value="">Select</option>');
 }
 });
 
 //end
 
	
	
	
    /**
     * ##########################################################################
	* End 
	* ############################################################################
	*/	

 
 
	
	
});

