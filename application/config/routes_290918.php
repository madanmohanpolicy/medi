<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = "home";
$route['404_override'] = 'error';

$route['pdetails'] = "pdetails/index";
$route['cart'] = "cart/index";
$route['mediwheel/home'] = "home";
$route['translate_uri_dashes'] = FALSE;
defined('BASEPATH') OR exit('No direct script access allowed');

/*********** USER DEFINED ROUTES *******************/

$route['admin'] = 'login/loginMe';
$route['admin/login'] = 'login/loginMe';
$route['admin/loginMe'] = 'login/loginMe';
$route['admin/dashboard'] = 'user';
$route['admin/logout'] = 'user/logout';
$route['admin/userListing'] = 'user/userListing';
$route['admin/hospitalListing']='hospital/hospitalListing';
$route['admin/userListing/(:num)'] = "user/userListing/$1";
$route['admin/addNew'] = "user/addNew";
//$route['addNew'] = "hospital/addNew";
$route['admin/addNewUser'] = "user/addNewUser";
$route['admin/addNewHospital'] = "hospital/addNewHospital";
$route['admin/hospitalListing']="hospital/hospitalListing";
$route['admin/editOld'] = "user/editOld";
$route['admin/editOldHospital/(:num)'] = "hospital/editOldHospital/$1";
$route['admin/editHospital/(:num)'] = "hospital/editHospital/$1";
$route['admin/editHospitalDoctor/(:num)'] = "hospital/editHospitalDoctor/$1";
$route['admin/editHospitalDoctors/(:num)'] = "hospital/editHospitalDoctors/$1";
$route['admin/editMedicalPractitionerDoctor/(:num)'] = "hospital/editMedicalPractitionerDoctor/$1";

$route['admin/editOld/(:num)'] = "user/editOld/$1";
$route['admin/hospitalDoctorsListing']="hospital/hospitalDoctorsListing";
$route['admin/addNewHospitalDoctor']="hospital/addNewHospitalDoctor";
$route['admin/addNewHospitalDoctors']="hospital/addNewHospitalDoctors";
$route['admin/medicalPractitioner']="hospital/medicalPractitioner";
$route['admin/addNewmedicalPractitioner']="hospital/addNewmedicalPractitioner";
$route['admin/addNewmedicalPractitioners']="hospital/addNewmedicalPractitioners";
$route['admin/editUser(:num)'] = "user/editUser/$1";
$route['admin/editUser'] = "user/editUser";
$route['admin/deleteUser'] = "user/deleteUser";
$route['admin/deleteHospital']="hospital/deleteHospital";
$route['admin/deleteHospitalDoctor']="hospital/deleteHospitalDoctor";
$route['admin/deleteMedicalPractitioner']="hospital/deleteMedicalPractitioner";

$route['admin/loadChangePass'] = "user/loadChangePass";
$route['admin/changePassword'] = "user/changePassword";
$route['admin/pageNotFound'] = "user/pageNotFound";
$route['admin/checkEmailExists'] = "user/checkEmailExists";
$route['admin/login-history'] = "user/loginHistoy";
$route['admin/login-history/(:num)'] = "user/loginHistoy/$1";
$route['admin/login-history/(:num)/(:num)'] = "user/loginHistoy/$1/$2";

$route['admin/forgotPassword'] = "login/forgotPassword";
$route['admin/resetPasswordUser'] = "login/resetPasswordUser";
$route['admin/resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['admin/resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['admin/resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['admin/createPasswordUser'] = "login/createPasswordUser";
$route['admin/state/(:num)']="hospital/district/$1";
$route['admin/city/(:num)']="hospital/city/$1";
$route['admin/pincode/(:num)']="hospital/pincode/$1";
$route['admin/hospitalDiaganosticListing']="hospital/hospitalDiaganosticListing";
$route['admin/addNewHospitalDiaganostic']="hospital/addNewHospitalDiaganostic";
$route['admin/addNewHospitalDiaganostics']="hospital/addNewHospitalDiaganostics";
$route['admin/addHealthcheckuppackages']="hospital/addHealthcheckuppackages";
$route['admin/addHealthCheckupPackage']="hospital/addHealthCheckupPackage";
$route['admin/healthChekupPackageListing']="hospital/healthChekupPackageListing";
$route['admin/healthChekupPackageListing/(:num)']="hospital/healthChekupPackageListing/$1";
$route['admin/editHealthChekupPackage/(:num)']="hospital/editHealthChekupPackage/$1";
$route['admin/editHealthPackages/(:num)']="hospital/editHealthPackages/$1";
$route['admin/deleteHealthPackage']="hospital/deleteHealthPackage";

$route['admin/healthCheckupTest']="hospital/healthCheckupTest";
$route['admin/addHealthcheckupTests']="hospital/addHealthcheckupTests";
$route['admin/addHealthCheckupTest']="hospital/addHealthCheckupTest";
$route['admin/editHealthChekupTest/(:num)']="hospital/editHealthChekupTest/$1";
$route['admin/editHealthChekupTests/(:num)']="hospital/editHealthChekupTests/$1";
$route['admin/deleteHealthTest']="hospital/deleteHealthTest";
$route['admin/addDoctorsSchedule/(:num)']="hospital/addDoctorsSchedule/$1";
$route['admin/scheduleListing']="hospital/scheduleListing";


$route['admin/scheduleListing/(:num)']="hospital/scheduleListing/$1";
$route['admin/addDoctorSchedule/(:num)']="hospital/addDoctorSchedule/$1";
$route['admin/deleteDoctorSchedule']="hospital/deleteDoctorSchedule";
$route['admin/deleteHealthTestParameter']="hospital/deleteHealthTestParameter";
$route['admin/deleteHealthPackageLinking']="hospital/deleteHealthPackageLinking";


$route['admin/testParameter/(:num)']="hospital/testParameter/$1";
$route['admin/addHealthCheckupTestParameter/(:num)']="hospital/addHealthCheckupTestParameter/$1";
$route['admin/addHealthCheckupTestParameters/(:num)']="hospital/addHealthCheckupTestParameters/$1";
$route['admin/editHealthChekupTestParameter/(:num)']="hospital/editHealthChekupTestParameter/$1";
$route['admin/editHealthChekupTestParameters/(:num)']="hospital/editHealthChekupTestParameters/$1";
$route['admin/testParameters/(:num)']="hospital/testParameter/$1";
$route['admin/healthChekupPackageLinking']="hospital/healthChekupPackageLinking";
$route['admin/addHealthPackageLinking']="hospital/addHealthPackageLinking";
$route['admin/facilityname/(:num)']="hospital/facilityname/$1";
$route['admin/addHealthcheckupPackageLinking']="hospital/addHealthcheckupPackageLinking";
$route['admin/editHealthChekupPackageLinking/(:num)']="hospital/editHealthChekupPackageLinking/$1";
$route['admin/editHealthPackageLinking/(:num)']="hospital/editHealthPackageLinking/$1";
$route['admin/healthChekupPackageLinking/(:num)']="hospital/healthChekupPackageLinking/$1";
$route['admin/healthPackageLinking/(:num)']="hospital/healthPackageLinking/$1";
$route['admin/healthPackageSchedule/(:num)']="hospital/healthPackageSchedule/$1";
$route['admin/addHealthPackageSchedule/(:num)']="hospital/addHealthPackageSchedule/$1";
$route['admin/addHealthCheckupPackageSchedule/(:num)']="hospital/addHealthCheckupPackageSchedule/$1";
$route['admin/editHealthPackageSchedule/(:num)']="hospital/editHealthPackageSchedule/$1";
$route['admin/editHealthCheckupPackageSchedule/(:num)']="hospital/editHealthCheckupPackageSchedule/$1";
$route['admin/deleteHealthPackageSchedule']="hospital/deleteHealthPackageSchedule";
$route['admin/editDoctorSchedule/(:num)']="hospital/editDoctorSchedule/$1";
$route['admin/editDoctorscheduleInfo/(:num)']="hospital/editDoctorscheduleInfo/$1";
$route['admin/healthPackageBookingList']="hospital/healthPackageBookingList";


########## Add bellow route are  Added by Santosh  Kumar Verma ######
#######for hospital ChemistShops##############
$route['admin/addNewChemistShop']="hospital/addNewChemistShop";
$route['admin/addHospitalChemistShop']="hospital/addHospitalChemistShop";
$route['admin/hospitalChemistShopsListing/(:num)']="hospital/hospitalChemistShopsListing/$1";
$route['admin/hospitalChemistShopsListing']="hospital/hospitalChemistShopsListing";
$route['admin/edithospitalChemistShop/(:num)']="hospital/edithospitalChemistShop/$1";
$route['admin/deletehospitalChemistShop']="hospital/deletehospitalChemistShop";
$route['admin/updateHospitalChemistShop/(:num)']="hospital/updateHospitalChemistShop/$1";
#######for hospital  Diaganostic##############
$route['admin/hospitalDiaganosticListing/(:num)']="hospital/hospitalDiaganosticListing/$1";
$route['admin/editHospitalDiaganostic/(:num)']="hospital/editHospitalDiaganostic/$1";
$route['admin/updateHospitalDiaganostic/(:num)']="hospital/updateHospitalDiaganostic/$1";
$route['admin/deleteHospitalDiaganostic']="hospital/deleteHospitalDiaganostic";

#######for hospital  Diaganostic##############
$route['admin/addNewHospitalDiaganosticform']="hospital/addNewHospitalDiaganosticform";
$route['admin/diaganosticListing']="diaganostic/diaganosticListing";
$route['admin/diaganosticListing/(:num)']="diaganostic/diaganosticListing/$1";
$route['admin/addNewDiaganosticform']="diaganostic/addNewDiaganosticform";
$route['admin/addNewDiaganostic']="diaganostic/addNewDiaganostic";
$route['admin/editDiaganostic/(:num)']="diaganostic/editDiaganostic/$1";
$route['admin/updateDiaganostic/(:num)']="diaganostic/updateDiaganostic/$1";
$route['admin/deleteDiaganostic']="diaganostic/deleteDiaganostic";
$route['admin/hospitalListing/(:num)']='hospital/hospitalListing/$1';













#######for 1MG  customer ##############

$route['admin/customerListing']="customer/customerListing";
$route['admin/customeDetail']="customer/customeDetail";
$route['admin/customerDetail/(:num)']="customer/customerDetail/$1";
$route['admin/customerListing/(:num)']="customer/customerListing/$1";
$route['admin/deleteCustomer']="customer/deleteCustomer";
$route['admin/chnageCustomerStatus']="customer/chnageCustomerStatus";
$route['admin/chnageCustomerStatus']="customer/chnageCustomerStatus";
$route['admin/chnageCustomerStatus/(:num)/(:num)']="customer/chnageCustomerStatus/$1/$2";
##cart
$route['admin/cartListing']="customer/cartListing";





#######for Doctors Specialization ##############

$route['admin/doctorSpecializationListing']="doctor/doctorSpecializationListing";
$route['admin/addDocSpecializationform']="doctor/addDocSpecializationform";
$route['admin/addDocSpecialization']="doctor/addDocSpecialization";
$route['admin/editdoctorSpecialization/(:num)']="doctor/editdoctorSpecialization/$1";
$route['admin/updateDoctorSpecialization']="doctor/updateDoctorSpecialization";
$route['admin/deleteDoctorSpecialization']="doctor/deleteDoctorSpecialization";
#Doctors Qualification
$route['admin/doctorQualificationListing']="doctor/doctorQualificationListing";
$route['admin/addDocQualificationform']="doctor/addDocQualificationform";
$route['admin/addDocQualification']="doctor/addDocQualification";
$route['admin/editdoctorQualification/(:num)']="doctor/editdoctorQualification/$1";
$route['admin/updateDoctorQualification']="doctor/updateDoctorQualification";
$route['admin/deleteDoctorQualification']="doctor/deleteDoctorQualification";
//end

//###Referred Doctors SchedulediaganosticListing

$route['admin/referredDoctorsScheduleListing/(:num)']="hospital/referredDoctorsScheduleListing/$1";
$route['admin/editReferredDoctorSchedule/(:num)']="hospital/editReferredDoctorSchedule/$1";
$route['admin/editReferredDoctorscheduleInfo/(:num)']="hospital/editReferredDoctorscheduleInfo/$1";
$route['admin/addReferredDoctorSchedule/(:num)']="hospital/addReferredDoctorSchedule/$1";
$route['admin/addReferredDoctorsSchedule/(:num)']="hospital/addReferredDoctorsSchedule/$1";
$route['admin/deleteReferredDoctorSchedule']="hospital/deleteReferredDoctorSchedule";


/*************Assign Health Package to Corporate Routing Start Here**************************/

$route['admin/assignHealthPackageToCorporate']="corporatePackage/assignPackageToCorporate";
$route['admin/assignHealthPackageToCorporatePostData']="corporatePackage/assignHealthPackageToCorporatePostData";


/*************Assign Health Package to Corporate Routing End Here**************************/


//add By Sanrosh

$route['admin/facilityname2/(:num)']="hospital/facilityname2/$1";


//$route['admin/diasubcode']="hospital/diasubcode";
$route['admin/diasubcode/(:num)']="hospital/diasubcode/$1";


$route['admin/addmoreDiasubcode/(:num)']="hospital/addmoreDiasubcode/$1";
//$route['admin/addmoreDiasubcode/(:num)']="hospital/addmoreDiasubcode/$1";
$route['admin/addmoreDiasubcode/(:num)/(:num)']="hospital/addmoreDiasubcode/$1/$2";























