<?php
    if(!defined('BASEPATH')) exit('No direct script access allowed');
    require APPPATH . '/libraries/BaseController.php';
    /**
	* Class : Customer (customerController)
	* customer Class to control all operations.
	*created by santosh 10 September 2018
	*/
	
    class Customer extends BaseController
    {
		/**
		* This is default constructor of the class
		*/
		  public function __construct()
		{
			parent::__construct();
			$this->load->model('customer_model');
			$this->load->helper('url');
			$this->isLoggedIn();   
		}
		/**
		* This function used to load the first 
		*/
		 public function index()
		{
			$this->global['pageTitle'] = 'Mediwheel : Customer';
			
		   // $this->loadViews("dashboard", $this->global, NULL , NULL);
		   
		}
			
		
		/**
		* This is use for  customer listing 
		create BY Santosh Kumar Verma 4 September
		*/
		function customerListing()
		{
		   if($this->isAdmin() == TRUE)
			{
			  $this->loadThis();
			}
			else
			{  
		    $searchText = $this->security->xss_clean($this->input->post('searchText'));
			$data['searchText'] = $searchText;
		
			$this->load->library('pagination');
		
			$count = $this->customer_model->customerListingCount($searchText);
			
			$returns = $this->paginationCompress ( "admin/customerListing/", $count, 10,3);
		
			$data['diaganosticRecords'] = $this->customer_model->customerListing($searchText, $returns["page"], $returns["segment"]);
		
			$this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
		
			$this->loadViews("customerListing", $this->global, $data, NULL);
			   
			  
			}
		}
		
		/**
		* This is use for  customer details 
		create BY Santosh Kumar Verma 10 September
		*/
		
		function customerDetail($id=null)
		{
		   if($this->isAdmin() == TRUE)
			{
			  $this->loadThis();
			}
			else
			{  
					$this->load->model('customer_model');
					
					$id =$id; 
					$data['customerInfo'] = $this->customer_model->getcustomerInfo($id);
					$this->global['pageTitle'] = 'Mediwheel : Customer info';
					$this->loadViews("customerdetail", $this->global, $data, NULL);
			   
			  
			}
		}
	
		/**
		* This function is used to delete Customer
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
		
		function deleteCustomer($id=null)
		{
		
			
			$this->load->model('customer_model');
			$id = $this->input->post('id');

			$customer = array('isDeleted'=>1);

			$result = $this->customer_model->deleteCustomer($id, $customer);

			if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
			else { echo(json_encode(array('status'=>FALSE))); }

		}	
			
		/**
		* This function is used to change  Customer status
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/	
	    function  chnageCustomerStatus($id=null)
		{
			
			
			$this->load->model('customer_model');
			$this->load->helper('url');
			
			$id    =  $this->uri->segment(3);
			$status =  $this->uri->segment(4);
			
			if($status=='0')
			{
			$status ='1';	
			}else{
			$status ='0';	
			}
			
		    //$id = $this->input->post('id');
			//$status = $this->input->post('status');
			$customer = array('status'=>$status);

			 $result = $this->customer_model->CustomerStatus($id, $customer); 
				
					
				redirect('admin/customerListing');
				
				
			
			if ($result) {
            				
			 				
			//echo(json_encode(array('status'=>TRUE)));
             redirect("admin/customerListing", 'refresh');   
           
			}
			else { echo(json_encode(array('status'=>FALSE))); }
			//echo $this->db->last_query();
		}
		
		
		/**
		* This is use for  Cart listing 
		  create BY Santosh Kumar Verma 11 September
		*/
		function cartListing()
		{
		   if($this->isAdmin() == TRUE)
			{
			  $this->loadThis();
			}
			else
			{  
		    $searchText = $this->security->xss_clean($this->input->post('searchText'));
			$data['searchText'] = $searchText;
		
			$this->load->library('pagination');
		
			$count = $this->customer_model->cartListingCount($searchText);
			
			$returns = $this->paginationCompress ( "admin/customerListing/", $count, 10,3);
		
			$data['cartRecords'] = $this->customer_model->cartListing($searchText, $returns["page"], $returns["segment"]);
		
			$this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
		
			$this->loadViews("cartListing", $this->global, $data, NULL);
			   
			  
			}
		}
		
    }	