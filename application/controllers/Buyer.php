<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer extends CI_Controller
 {

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');
				//$this->load->library('mail');


	}
	
	/**
	* This function used to open the foem for sigining up user.
	* Its default funtion of this controller.
	* @param comments goes here
	*/
	public function index()
	{
		$data = array(
			  'page_title' => 'Mediwheel- Your Welness Partner',
			  
			  );
		$this->load->view('front_pages/includes/header',$data);
		
		if(isset($this->session->userdata['is_logged_in'])){
				$this->load->view('front_pages/profile');
				
				}else{
				$this->load->view("front_pages/register.php");
				}
		
		$this->load->view('front_pages/includes/footer',$data);
	}
	
	/**
	* This function used to open the form to verify the mobile
	* @param comments goes here
	*/
	public function verifymob()
	{
		$data = array(
			  'page_title' => 'Mediwheel- Verify Your Mobile', 
			  );
		
		$this->load->view('front_pages/includes/header',$data);		
		$this->load->view("front_pages/verifymob.php");		
		$this->load->view('front_pages/includes/footer',$data);
	}
	
	/**
	* This function used to open the form for login of user.
	* It funtion calls once user click on login or asked to login
	* Created by Madan on 21st Aug, 2018
	* Updated by Santosh on 22nd Aug 2018 for validation.
	* @param comments goes here
	*/	
	public function register_buyer()
	{	
		
		
		$name= $this->input->get('name');
		$email= $this->input->get('email');
		$mobile= $this->input->get('mobile');
		$otp = rand(10000,99999);
		$password = $otp;	
		$message = '';
		$password_encrypt = md5($otp);
		
		if($name != '' && $email != '' && $mobile != '' )
		{			
			$name = preg_replace('/\s+/', '%20', $name);
			 $url =  base_url()."websignup/?name=$name&email=$email&mobile=$mobile&password=$password_encrypt&signup=1";
			$response = $this->execute_webservices($url);
			$response = json_decode($response);
			 
			 //echo "<pre>aaa";
			//print_r($response);
			//die;
			
			if($response->error == 0)
			{
				// write the code for sending email to user. Send all users details in email with password.
				$name = preg_replace('/%20/', ' ', $name);
				$to = $email;
				$sub ="Mediwheel.in : User Registration";
$mail_content ="
Dear $name,

Thanks for doing registration with us. 
Your login id is your email and one time pin is : $otp
After Login, Please change your password.

Thanks,
Mediwheel Team";
				$this->sendMailAFterRegistration($to,$sub,$mail_content); 
				$message = $response->message;
				// send otp to user on mobile 
				
				$mobile_message = $this->sendOTP_AFterRegistration($mobile,$otp);
				$data = array(
					'front_user_otp' => $otp,
					'front_user_mobile' => $mobile
					);
				$this->session->set_userdata($data);
		
			}
			else
			{
				if($response->message != '')
				{
					$message = $response->message;
				}
				else
				{
					$message = "Some error occured. Please try again";
				}
			}
			
		}
		else
		{
			$message = 'Not Valid Data';
		}
		//echo $message;
		//die;
		$this->session->set_flashdata('success_msg', $message);
		if($message == "User Registered Successfully")
		{	
			redirect('buyer/verifymob');
		}
		else
		{
			redirect('buyer');
		}
    }
	
	/*
		This function is used to send mail to user's once registration is done.
	*/
	function sendMailAFterRegistration($to,$sub,$message)
	{
		$this->load->library('email');
		$this->email->from('noreply@mediwheel.in', 'Mediwheel Admin');		
		$this->email->to($to);
		$this->email->bcc('madan.me@gmail.com');
		$this->email->subject($sub);
		$this->email->message($message);
		$this->email->send();
	}
	
	/*
		This function is used to send otp to user's once registration is done.
	*/
	function sendOTP_AFterRegistration($mobile,$otp)
	{
		$message_mobile = "";		
		$message_mobile = "Dear User, Thanks for registration with us. Your one time pin is $otp";
		$message_mobile = preg_replace('/\s+/', '%20', $message_mobile);
		$url = "http://india.ibulksms.com/sendurlcomma.aspx?user=20063297&pwd=nprpu4&senderid=MEDSAV&mobileno=$mobile&msgtext=$message_mobile&smstype=0";
		$message_mobile = $this->execute_webservices($url);
		if($message_mobile)
		{
			//$this->session->userdata['is_logged_in'])
		}
		return $message_mobile;
	}
	
	
	
	/**
	* This function used to check the otp which is entered by user
	* this funtion gets called once user submit the OTP to verify mobile number
	* Created by Madan on 17th Sept, 2018		
	*/	
	public function verifymobile()
	{	
		$otp= $this->input->post('mobile');	 
		$mobile= $this->input->post('front_user_mobile');		
		$message = '';
		
		//echo $this->session->userdata['front_user_otp'];
		//echo "otp".$otp;
		//die;
		if($otp != '')
		{	
			if($otp == $this->session->userdata['front_user_otp'])
			{				
				$message = "Mobile Verified";
				 $data = array(
					'is_mobile_verified' => 1
            );				
			$result =$this->load->model('buyer_model');
			$result = $this->buyer_model->verifyMobile($mobile ,$data );
			}
			else
			{				
				$message = "Invalid OTP! Please enter correct OTP";				
			}
			
		}
		else
		{
			$message = "Invalid OTP! Please enter correct OTP";	
		}
		//echo $message;
		//die;
		$this->session->set_flashdata('success_msg', $message);		
		redirect('buyer/verifymob');
		
    }
	
	
	
	
	/**
	* This function used to run the Curl
	* @param comments goes here
	*/
	public function execute_webservices($url)
	{
		//$url = "http://localhost/mediwheel/websignup/?name=www&mobile=9958469872&email=madan.me@gmail.com&signup=1";
		//echo  $url;
		$response = '';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",  
		CURLOPT_HTTPHEADER => array(
		"accept: application/vnd.healthkartplus.v7+json",		
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		

		curl_close($curl);

		return $response;
	}
	
	/**
	* This function used to send mail when user takes packages.
	*/
	function sendmail()
	{
		//$email= $this->input->post('email');
		//$to = "madan.me@gmail.com";
		//$to = $email;
		//$subject = "My subject";
		//$txt = "Hello world!";
		//$headers = "From: noreply@mediwheel.in" . "\r\n" ;
		//mail($to,$subject,$txt,$headers);
		
		$email= $this->input->post('email');
		$mobile= $this->input->post('mobile');
		$this->load->library('email');
		$this->email->from('noreply@mediwheel.in', 'No Reply');
		$this->email->to($email);
		$this->email->bcc('rajinder@medsave.in');
		$this->email->bcc('anuj@medsave.in');
		$this->email->bcc('madan@policywheel.com');
$msg='
Dear User,

Thanks for appointment with us. Our team will call you shortly.
	
Thanks,
Mediwheel.in Team';
		$this->email->subject('Health Checkup Intimation');
		$this->email->message($msg);
		if($this->email->send())
		{
			echo "mail sent";
		}
		else
		{
			echo "mail not sent";
		}
		// send message to mobile
		$message_mobile = "";
		$message_mobile = "Dear User, Thanks for appointment with us. Our team will call you shortly, Mediwheel.in Team";
		$message_mobile = preg_replace('/\s+/', '%20', $message_mobile);
		$url = "http://india.ibulksms.com/sendurlcomma.aspx?user=20063297&pwd=nprpu4&senderid=MEDSAV&mobileno=$mobile&msgtext=$message_mobile&smstype=0";
		print_r($this->execute_webservices($url));
		redirect("http://mediwheel.in/health-pakackge-demo/thanks.html?mail=1");
	}
	
	/**
	* This function used to open the form for login of user.
	* It funtion calls once user click on login or asked to login
	* Created by Madan on 21st Aug, 2018
	* Updated by Santosh on 22nd Aug 2018 for validation.
	* @param comments goes here
	*/
	public function login_view()
	{
		$this->load->library('session');
		if($this->input->get('packageId')&&$this->input->get('appointmentdate')&&$this->input->get('appointmenttime')){
			
		
			$healthPackageBookDate=$this->input->get('appointmentdate');
			$mdi=$this->input->get('mdi');
			$id_p=$this->input->get('id_p');
			$id_d=$this->input->get('id_d');
			$packageId=$this->input->get('packageId');
			$bookingTime=$this->input->get('appointmenttime');
			  $packagebookinginfo=array(
					'health_package_booking_date' => $healthPackageBookDate,
					'health_package_id' =>$packageId,
					'health_package_booking_time'=>$bookingTime,
					'mdi'=>$mdi,
					'id_p'=>$id_p,
					'id_d'=>$id_d
					);
					
			
		}
		
		         
					
					
					
					
					else{
						
						
		             $packagebookinginfo=array(
					'health_package_booking_date' =>'',
					'health_package_id' =>'',
					'health_package_booking_time'=>''
					);
					
						
					}
					$this->session->set_userdata($packagebookinginfo);
		$data = array(
			  'page_title' => 'Mediwheel - Login',			  
			  );	  
			  
		$this->load->view('front_pages/includes/header',$data);
		//$this->load->view('front_pages/login',$data);
		
		if(isset($this->session->userdata['is_logged_in'])){
		$this->load->view('front_pages/user_profile');
		//readdir('buyer/');
		}else{
		//$this->load->view('front_pages/login_package');
		$this->load->view('front_pages/login');
		}
		
		$this->load->view('front_pages/includes/footer',$data);
	}
	
	/**
	* This function used to open the form for login of user.
	* It funtion calls once user click on login or asked to login
	* Created by Madan on 21st Aug, 2018
	* Updated by Santosh on 22nd Aug 2018 for validation.
	* @param comments goes here
	*/
	public function login_view2()
	{
		$data = array(
			  'page_title' => 'Mediwheel - Login',			  
			  );	  
		if(isset($this->session->userdata['is_logged_in']))
		{
			session_destroy();
			redirect("buyer/login_view2/");
		}
		$this->load->view('front_pages/includes/header',$data);		
		$this->load->view('front_pages/login_package');		
		$this->load->view('front_pages/includes/footer',$data);
	}
	
	/**
	* This function used to buyer profile
	* @param comments goes here
	*/
	public function  buyer_profile()
	{
		// get email and password entered by user
		if(!isset($this->session->userdata['is_logged_in']))
		{
			$email= $this->input->post('email');
			$password= $this->input->post('password');		
		
			//Check user's credentials by running the webservices
			$url =  base_url()."weblogin/?email=$email&password=$password&login=1";
			$response = $this->execute_webservices($url);
			$response = json_decode($response);
				 
			//echo "<pre>aaa";
			//print_r($response);
			//die;
			
			// if error response is not 0, means its true, we will send user to their login page.
				if($response->error != "0")
				{
					$this->session->set_flashdata('success_msg', "Invalid Credentials");
					redirect('buyer/login_view');
				}
				else
				{

					//echo "sss";
					//die;
					//$response->data;
					//echo "<pre>";
					//print_r($response);
					
					//checking the cart
					$cart = "";
					$this->load->model('cart_model');
					$cart = $this->cart_model->getCart($response->data->front_id);
					//echo "<pre>";
					//print_r($cart);
					//die;
					$data = array(
					'front_id' => $response->data->front_id,
					'front_name' => $response->data->front_name,
					'front_email' => $response->data->front_email,
					'front_mobile' => $response->data->front_mobile,
					'is_logged_in' => true,
					'cart_data' => $cart['total_row']
					);
					$this->session->set_userdata($data);
					
					// check from where the user is coming, and redirect accordingly.
					if(isset($this->session->userdata['front_goto_page']))
					{
						redirect($this->session->userdata['front_goto_page']);
					}
					else
					{
						redirect("buyer/profile");
					}
					
					
				}
					
		}
		
					//echo "<pre>";
					//print_r($this->session);
					//die;
					$data = array(		
					'page_title' => 'Mediwheel - profile',							  
					);
					//echo "<pre>";
					//print_r($this->data);
					//die;
					
					//$this->load->view('front_pages/includes/header',$data);
					//$this->load->view('front_pages/user_profile',$data);
					//$this->load->view('front_pages/includes/footer',$data);
		
        
		
	
	}

	
	public function  profile()
	{
		     $data = array(
		
			  'page_title' => 'Mediwheel - profile',
             );
			$this->load->view('front_pages/includes/header',$data);

				if(isset($this->session->userdata['is_logged_in'])){
				$this->load->view('front_pages/user_profile',$data);				
				}else{
				$this->load->view('front_pages/login_view');
				}	
			$this->load->view('front_pages/includes/footer',$data);	
	}
	
	/**
	* This function used to buyer profile
	* @param comments goes here
	*/
	public function editprofile()
	{        
	          
		      $id =  $_GET['id'];
			  $this->load->model('buyer_model');
		      $results = $this->buyer_model->getBuyer($id);
			 //print_r($result);
		      $data = array(
			  'page_title' => 'Mediwheel - edit profile',
			  'results'=>$results,
			  
			  );
					$this->load->view('front_pages/includes/header1',$data);
					$this->load->view('front_pages/editprofile',$data);
					$this->load->view('front_pages/includes/footer',$data);
				
        
		
	
	}
	
	public function updateProfile()
	{
		    $id = $this->input->post('id');
            $name = $this->input->post('name');
            $email = $this->input->post('email');
			$mobile = $this->input->post('mobile');
			
			 $data = array(

			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'mobile' => $this->input->post('mobile')
                );
				
				
			$result =$this->load->model('buyer_model');
			$result = $this->buyer_model->updateBuyer($id ,$data );
			
            if($result)
			{
		
			redirect('buyer/profile');
			}
          
	
	}


	/**
	* This function used to change buyer password view page
	* Created by Santosh on 22st Aug, 2018
	* Updated by Madan on 17th Sept 2018
	* @param comments goes here
	*/
	public function password_reset_view()
	{ 
		$data = array(
		'page_title' => 'Mediwheel - Forgot Password',			  
		);
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view('front_pages/forgot_password',$data);
		$this->load->view('front_pages/includes/footer',$data); 
	}
	
	/**
	* This function used to call for changing the password.
	* Created by Madan on 18th Sept, 2018
	* @param comments goes here
	*/
	 public function password_reset()
	 {		 
				
		$email= $this->input->post('email');		
		$message = '';
		if($email != '')
		{
			$this->load->model('buyer_model');			
			$results = $this->buyer_model->checkUserEmail($email);
			if(count($results) > 0 )
			{
				// need to send email that we have sent you the link in your registered email to reset.
				
				//Load email library 
					 $this->load->library('email');					 
					 $to_email = $email;					 
					 $url_val = md5($to_email.time().rand(2,5));
					 $link    =  base_url()."buyer/reset_password/$url_val"; 					 
					 $this->buyer_model->update_password_url_val($url_val,$to_email);
					 
					 // Sending email 
					 $this->email->from('noreply@mediwheel.in', 'Mediwheel Admin');					 
					 $this->email->to($to_email);
					 $this->email->bcc('madan.me@gmail.com');
					 $this->email->subject('Mediwheel Admin : Reset your password');					
					 $message = "Hi,\n\nPlease click on the below link to reset the password.\n\n$link \n\nThanks,\nMediwheel Team";					 
					 $this->email->message($message);					 
					//Send mail 
				if($this->email->send()) 
				{
					$this->session->set_flashdata('success_msg', "We have sent you the \"password reset link \" to your registered email id. Requesting you to please reset your password by clicking on reset button.");					
				}
				else
				{
					 $this->session->set_flashdata('success_msg', "There is some problem. Please try after sometime.");					 
				}						
			}
			else
			{
				$this->session->set_flashdata('success_msg', "Entered email id is not registered with us");				
			}
		}
		else
		{
			$this->session->set_flashdata('success_msg', "Please enter your email id");			
		}
			redirect("buyer/password_reset_view");			
	 }
	 
	 
	 /**
	* This function used to  reset  password  buyer user
	* Created by Madan on 18th sept, 2018
	* @param comments goes here
	*/
	 public function  reset_password($url_val='')
     {
		   $this->load->helper('url'); 
		   $this->load->model('buyer_model');
		   $data = '';
		   $msg_success = '';
		   $msg = '';
		   $url_posted_val = '';
		   $id_val = '';
		   $npassword = '';
		   $cpassword = '';
		   $id = '';
		   $to_email = '';
		   $from_email = '';
		// checking if form is submitted 
		if ($this->input->post('reset_form')) 
		{
			//print_r("sdddddddddddddd");
				$url_posted_val = $this->input->post('url_val');
				$npassword = trim($this->input->post('npassword'));
				$cpassword = trim($this->input->post('cpassword'));
				
				if($npassword != '' and $npassword == $cpassword and strlen($npassword) >5 and strlen($npassword) < 30)
				{
					// checking if there is valid user email
					$id_val = $this->buyer_model->check_valid_forgot_password($url_posted_val);
					if ($id_val) 
					{
						/* echo "<pre>";
						 print_r($id_val);
						 echo "</pre>";
						*/ 
						//Update Password
						$id = $id_val[0]->id;						
						$this->buyer_model->update_password_val($npassword,$id);


						$to_email = $id_val[0]->email;					
						//Load email library 
						$this->load->library('email');	
						// Sending email 
						$this->email->from('noreply@mediwheel.in', 'Mediwheel Admin');					 
						$this->email->to($to_email);
						$this->email->bcc('madan.me@gmail.com');
						$this->email->subject('Mediwheel Admin : Reset your password');					
						$message = "Hi,\n\nYou have reset your password. \n\nThanks,\nMediwheel Team";					 
						$this->email->message($message);
						
						//Send mail 
						 if($this->email->send())
						 {							 
							$this->session->set_flashdata('success_msg', "Your password has been reset.");						  
						 }
						 else 
						 {
							 $this->session->set_flashdata('success_msg', "There is some problem. Please try after sometime.");	
						 }
						 
						
					}
					else
					{
						$this->session->set_flashdata('success_msg', "There is some problem. Please try after sometime or contact admin");	
					}
				}
				else
				{
					$this->session->set_flashdata('success_msg', "There is some problem. Please try after sometime or contact adminn");
					
				}	
					$url_val = $url_posted_val;
		}				
		
		$data = array(
		'page_title' => 'Mediwheel - Reset Password',
		'url_val' => $url_val,		
		);
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view('front_pages/reset_password',$data);
		$this->load->view('front_pages/includes/footer',$data);    
     }
	 
	 
	 
	
	 
    
	 /**
	* This function used to   buyer  logout
	* Created by Madan on 31st Aug, 2018
	* @param comments goes here
	*/
	
	public function logout()
	{
	    $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value)
		{
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
			$this->session->sess_destroy();
			redirect('buyer/login_view');
	}
	
	 /**
	* This function used to   buyer change pass  after loged in
	* Created by Santosh on 27st Aug, 2018
	* @param comments goes here
	*/
	public function changepwd()
	{
	
		$this->load->library('session');
		
		$npassword= $this->input->post('npassword');
		$opassword= $this->input->post('opassword');
		$cpassword = $this->input->post('cpassword');
			
		$this->load->model('buyer_model');
	    $id = $this->session->userdata('front_id');
		$results = $this->buyer_model->getBuyer($id);
		if(count($results) > 0 && $npassword == $cpassword)
		{
			$old_pass = $results['password'];
		
		//echo "<pre>aaa";
		///print_r($results);
		//die;
			
		$data = array(
			  'password' => md5($npassword),			  
			  );
		
			if ( md5($opassword) == $results['password']) 
			{
			 //echo "ok"; die;
			 $results = $this->buyer_model->updatePassword($id,$data);  	
			 $message = "Password Changed Successfully";
				
			}else{
			
			$message = "Password Couldn't changed. Please try again";
			
			}
		}
	     $this->session->set_flashdata('success_msg', $message);
		 redirect('buyer/login_view');
		//$results = $this->buyer_model->getBuyer($id);
			//print_r($result);
	// $result = $this->buyer_model->buyer_login($email,$password);	
      
	}
	
	function signup1mgAfterCheckout()
	{
		  

		  $name = "Madan";
		  $email = "madan@policywheel.com";
		  $password = "Medi987654";
		  $phone = "9958469872";
		  
		  $curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://stagapi.1mg.com/webservices/users/signup",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n$email\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"name\"\r\n\r\n$name Mohan\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"password\"\r\n\r\n$password\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"phone_number\"\r\n\r\n$phone\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cart_oos\"\r\n\r\ntrue\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
		CURLOPT_HTTPHEADER => array(
		"accept: application/vnd.healthkartplus.v7+json",
		"cache-control: no-cache",
		"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
		"hkp-platform: HealthKartPlus-9.0.0-Android",
		"postman-token: 847fbb91-7792-da6f-df7e-e44532926a68"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			echo "<pre>";
		 // print_r($response);
		  print_r(json_decode($response));
			}
	}
	
	
	
	function getOTP_AfterSignUp()
	{
		  

		  $name = "Madan";
		  $email = "madan@policywheel.com";
		  $password = "Medi987654";
		  $phone = "9958469872";
		  
		  $curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://stagapi.1mg.com/webservices/users/token/create",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data;  name=\"username\"\r\n\r\n$phone\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"isRetry\"\r\n\r\ntrue\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
		CURLOPT_HTTPHEADER => array(
		"accept: application/vnd.healthkartplus.v7+json",
		"cache-control: no-cache",
		"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
		"hkp-platform: HealthKartPlus-9.0.0-Android",
		"postman-token: 847fbb91-7792-da6f-df7e-e44532926a68"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			echo "<pre>";
		 // print_r($response);
		  print_r(json_decode($response));
			}
	}
	
	
	
	function verifyOTP($phone,$token)
	{
		  

		  
		  $token = "581099";
		  $phone = "9958469872";
		  
			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://stagapi.1mg.com/webservices/users/token/verify",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"verification_token\"\r\n\r\n$token\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"username\"\r\n\r\n$phone\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"return_token\"\r\n\r\ntrue\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
			CURLOPT_HTTPHEADER => array(
			"accept: application/vnd.healthkartplus.v7+json",
			"cache-control: no-cache",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			"hkp-platform: HealthKartPlus-9.0.0-Android",
			"postman-token: 3b99726d-3039-399b-7d64-8a33b7ee01cb",
			"x-api-key: 1mg_client_access_key"
			),
			));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			echo "<pre>";
		 // print_r($response);
		  print_r(json_decode($response));
			}
	}
	
	
	
	/*response from postman after verifying OTP
	
		{
    "status": 0,
    "context": {
        "discount": 0,
        "contact_number": null,
        "platformHeader": "HealthKartPlus-9.0.0-Android",
        "storeCode": null,
        "canRenderHTMLHeader": null,
        "emailId": null,
        "city": "",
        "pinCode": "",
        "login": false
    },
    "errors": null,
    "message": null,
    "op": "login",
    "header": {
        "cartCount": 1,
        "REDIRECT_URL": "http://api.1mg.com/",
        "authToken": "ccbf1948-74cd-4ed8-946f-3e90083c0cc4"
    },
    "hasMore": false,
    "result": {
        "op": "login",
        "profession": null,
        "errors": null,
        "totalRecordCount": 0,
        "number": "9958469872",
        "otp_required": false,
        "result": null,
        "emailAddress": "madan@policywheel.com",
        "username": "Madan Mohan",
        "cartCount": 1,
        "roles": [
            "ROLE_USER"
        ],
        "status": 0,
        "context": null,
        "authToken": "ccbf1948-74cd-4ed8-946f-3e90083c0cc4",
        "message": null,
        "uid": "1fb88f5c-e598-4fa3-8f10-69bfb976ec5c",
        "1mgCash": 50,
        "affiliate_params": null,
        "header": {},
        "hasMore": false
    },
    "totalRecordCount": 0
}
	
	*/
		//function getCart($phone,$token)
		function getCart()
		{
		  
			
		  
			$auth_token = "ccbf1948-74cd-4ed8-946f-3e90083c0cc4";
			$phone = "9958469872";

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://stagapi.1mg.com/webservices/cart?cart_oos=true&city=New%20Delhi",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "qty=4&cart_oos=true&productId=67577&city=Gurgaon",
			CURLOPT_HTTPHEADER => array(
			"accept: application/vnd.healthkartplus.v7+json",
			"authorization: $auth_token",
			"cache-control: no-cache",
			"hkp-platform: HealthKartPlus-9.0.0-Android",
			"postman-token: b31953c6-6e2c-f1e3-19c5-69585d986088",
			"x-1mglabs-platform: HealthKartPlus-9.0.0-Android",
			"x-api-key: 1mg_client_access_key"
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			echo "cURL Error #:" . $err;
			} else {
			echo "<pre>";
			// print_r($response);
			print_r(json_decode($response));
			}
		}	
	
	
	/* 1mg response if cart is having value.
	stdClass Object
(
    [result] => stdClass Object
        (
            [offerDiscount] => 0
            [patientName] => Not Provided
            [actualAmnt] => 868.91
            [billNumber] => 
            [cashback_info] => stdClass Object
                (
                    [total_amount_with_cashback] => 783
                    [cashback_info_message] => 
                    [cashback_usable] => 50
                    [1mg_cash_promo] => 50
                    [cashback_redeemable_message] => You can pay 20% of the total amount of your order using 1mgCash.
                    [cashback_availed_message] => 
                    [cashback_discount] => 0
                    [cashback_redeemable_percent] => 20
                )

            [formatOrderDate] => September 10, 2018
            [waiting_for_rx_notification] => 
            [requestedItem] => 
            [shippingAmt] => 0
            [email] => madan@policywheel.com
            [totalSavings] => 35.91
            [min_order_amount] => 1
            [attachments] => Array
                (
                )

            [couponCode] => 
            [phoneNumber] => 
            [saving] => 4.13
            [app_download_widget] => 
            [status] => In Cart
            [deliveryMsg] => 
            [totalAmt] => 833
            [discountedMrp] => 833
            [couponDescription] => 
            [prescription_option] => upload
            [doctorName] => Not Provided
            [isOrderSplit] => 
            [prescription] => Array
                (
                )

            [orderId] => MWZiODhmNWMtZTU5OC00ZmEzLThmMTAtNjliZmI5NzZlYzVjMTUzNjU4NTE5Ng
            [userId] => 1fb88f5c-e598-4fa3-8f10-69bfb976ec5c
            [partialCart] => 
            [orderAmount] => 833
            [discount] => 35.91
            [new_user] => 
            [orderIdAndEmailAddressHash] => 
            [coupon_error] => 
            [cart_msg] => 
            [offerId] => 
            [best_coupon] => stdClass Object
                (
                    [coupon_code] => NEWTEST
                    [cashback_value] => 416.5
                    [description] => Coupon for new user
                    [coupon_message] => Use NEWTEST coupon to earn 1mgCash  ₹ 416.5
                    [discount_value] => 0
                )

            [cancelReason] => 
            [orderDate] => 1536585196000
            [address] => 
            [orderItemGroups] => Array
                (
                    [0] => stdClass Object
                        (
                            [iconUrl] => 
                            [discountMessage] => 
                            [title] => Items Not Requiring Prescription (1)
                            [orderItems] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [sellingUnit] => 4
                                            [packForm] => Capsule
                                            [vendorDiscountPercent] => 0
                                            [productGroupBrandName] => Uprise-D3
                                            [unitCount] => 7
                                            [generics] => Array
                                                (
                                                )

                                            [offer_message] => 
                                            [trimmed] => 
                                            [isPrescriptionRequired] => 
                                            [productSlug] => otc67577
                                            [digitizeNote] => 
                                            [originalPrice] => 0
                                            [unitPrice] => 31.03
                                            [manuName] => Alkem Laboratories Ltd
                                            [actualPrice] => 868.91
                                            [prescriptionQuantity] => 
                                            [couponDiscountErrorMessage] => 
                                            [couponDiscount] => 
                                            [formatForm] => strips
                                            [couponDiscountPercent] => 
                                            [status] => 
                                            [prtype] => OTC
                                            [orderLineId] => 
                                            [available] => 1
                                            [productName] => Uprise-D3 60K Capsule
                                            [url] => /otc/product/uprise-d3-60k-capsule/otc67577
                                            [orderId] => MWZiODhmNWMtZTU5OC00ZmEzLThmMTAtNjliZmI5NzZlYzVjMTUzNjU4NTE5Ng
                                            [drug_form] => Capsule
                                            [productQuantity] => 28
                                            [pSize] => 4 capsules
                                            [pid] => 67577
                                            [itemNote] => 
                                            [discountPerc] => 4
                                            [packSizeLabel] => strip of 4 capsules
                                            [vendorPrice] => 
                                            [item_price] => 119
                                            [parent_sku_id] => 
                                            [freebie] => 
                                            [vendorStock] => 
                                            [vendorAccumulatedStock] => 
                                            [transformed_image_urls] => Array
                                                (
                                                )

                                            [vendorBidDto] => 
                                            [digitizeMatch] => 
                                            [unitsInPack] => 4
                                            [order_limit_rule] => 
                                            [productId] => 67577
                                            [offeredPrice] => 833
                                            [form] => strip
                                        )

                                )

                            [subTitle] => 
                        )

                )

            [patientNames] => Array
                (
                )

            [oos_count] => 0
            [upSellMessage] => 
            [doctorNames] => Array
                (
                )

            [prescriptionReq] => 
        )

    [errors] => 
    [totalRecordCount] => 1
    [context] => stdClass Object
        (
            [emailId] => 
            [pinCode] => 
            [canRenderHTMLHeader] => 
            [discount] => 0
            [platformHeader] => HealthKartPlus-9.0.0-Android
            [login] => 
            [city] => New Delhi
            [contact_number] => 
            [storeCode] => 
        )

    [hasMore] => 
    [op] => get order summary
    [status] => 0
    [message] => 
    [header] => stdClass Object
        (
            [orderMinValue] => 
        )

)
	
	*/
	
	function test()
	{
		$name = "Madan%20Mohan%20mmmm aaa";
		echo $name = preg_replace('/%20/', ' ', $name);
	}
	
	
	function placeorder ()
	{
		$auth_token = "ccbf1948-74cd-4ed8-946f-3e90083c0cc4";

$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://stagapi.1mg.com/webservices/orders/current/place?device=Android",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "PUT",
				  CURLOPT_POSTFIELDS => "rx_queued=false&payment_mode=COD&cashback_availed=0.0",
				  CURLOPT_HTTPHEADER => array(
					"accept: application/vnd.healthkartplus.v7+json",
					"authorization: $auth_token",
					"cache-control: no-cache",
					"content-type: application/x-www-form-urlencoded",
					"hkp-platform: HealthKartPlus-9.0.0-Android",
					"postman-token: a6e65e5c-c364-3824-09a4-2d323d21a64c",
					"x-1mglabs-platform: HealthKartPlus-9.0.0-Android",
					"x-api-key: 1mg_client_access_key"
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
				  echo "cURL Error #:" . $err;
				} else {
				  echo $response;
				}
	}

 }

?>
