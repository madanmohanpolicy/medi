<?php
    if(!defined('BASEPATH')) exit('No direct script access allowed');
    require APPPATH . '/libraries/BaseController.php';
    /**
	* Class : Doctor ( Doctor  Controller)
	* Doctor Class to control all Doctor Specialization and Doctor Qualification operations.
	*created by santosh  Date: 14 September 2018
	*/
	
    class Doctor extends BaseController
    {
		/**
		* This is default constructor of the class
		*/
		  public function __construct()
		{
			parent::__construct();
			$this->load->model('doctor_model');
			$this->load->helper('url');
			$this->isLoggedIn();   
		}
		/**
		* This function used to load the first 
		*/
		 public function index()
		{
			$this->global['pageTitle'] = 'Mediwheel : Customer';
			
		  
		   
		}
			
		
		/**
		* This is use for  doctor Specialization listing 
		 create BY Santosh Kumar Verma 14 September
		*/
		function doctorSpecializationListing()
		{
		   if($this->isAdmin() == TRUE)
			{
			  $this->loadThis();
			}
			else
			{  
		    $searchText = $this->security->xss_clean($this->input->post('searchText'));
			$data['searchText'] = $searchText;
		
			$this->load->library('pagination');
		
			$count = $this->doctor_model->doctorSpecializationCount($searchText);
			
			$returns = $this->paginationCompress ( "admin/docSpecializationListing/", $count, 10,3);
		
			$data['diaganosticRecords'] = $this->doctor_model->docSpecializationListing($searchText, $returns["page"], $returns["segment"]);
		
			$this->global['pageTitle'] = 'Mediwheel : Doctor Listing';
		
			$this->loadViews("admin/doctorSpecializationListing", $this->global, $data, NULL);
			   
			  
			}
		}
		
		
		/**
		* This  function for Doctor Specialization Add Form
		 create BY Santosh Kumar Verma 14 September
		*/
		 
		function addDocSpecializationform()
        {
			if($this->isAdmin() == TRUE)
			{
			$this->loadThis();
			}
			else
			{
			$this->load->model('doctor_model');
			$this->global['pageTitle'] = 'Mediwheel : Add New Doctor Specialization';

			$this->loadViews("admin/addDocSpecialization", $this->global,  NULL);
			}
		}
				
		/**
		* This  function for Add Doctor Specialization  
		 create BY Santosh Kumar Verma 14 September
		*/		
		
		function addDocSpecialization()
		{
			
				
			if($this->isAdmin() == TRUE)
			{
		
			$this->loadThis();
			}
			else
			{
			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('specialization');
			
			$specialization = $this->security->xss_clean($this->input->post('specialization'));
			$this->load->model('doctor_model');
			
			 $docInfo = array('specialization'=>$specialization);
			$result = $this->doctor_model->addDocSpecializationdetail($docInfo);
			
			if($result > 0)
			{
				$this->session->set_flashdata('success', 'Doctor Specialization  Added successfully');
			}
			else
			{
				$this->session->set_flashdata('error', 'Doctor Specialization Added failed');
			}
			
			redirect('admin/addDocSpecializationform');
			
			}
	    }
	
	
		/**
		* This  function for edit form for Doctor Specialization  
		 create BY Santosh Kumar Verma 14 September
		*/		
		
		function editdoctorSpecialization($id=null)
		{
			if($id == null)
			{
			
				redirect('admin/healthCheckupTest');
			}
			else{
			
			
					$data['doctorSpecializationInfo'] = $this->doctor_model->getdoctorSpecializationInfo($id);
			   
					$this->global['pageTitle'] = 'Mediwheel : Edit HealthCheckupTest';
				
					$this->loadViews("admin/editdoctorSpecialization", $this->global, $data, NULL);
			    }
        }
	


		/**
		* This function is used to update Doctor Specialization
		* create by Santosh Kumar  Deate :14 September
		* @return boolean $result : TRUE / FALSE
		*/			
		public function updateDoctorSpecialization($id=null)
		{
			
			
			if($this->isAdmin() == TRUE)
			{
				$this->loadThis();
			}
			else
			{
				$this->load->library('form_validation');
				
				$id = $this->input->post('id');
				$specialization    = $this->security->xss_clean($this->input->post('specialization'));
				
				$doctorSpecializationInfo = array('specialization'=> $specialization);
				$result = $this->doctor_model->updatedocSpecializationInfo($doctorSpecializationInfo, $id);
				 
					
					if($result == true)
					{
					   $this->session->set_flashdata('success', 'Doctor Specialization Updated Successfully.');
					   
						redirect('admin/editdoctorSpecialization/'.$id);	
					}
					else
						{
							$this->session->set_flashdata('error', 'Doctor Specialization updated failed');
						}
					redirect('admin/editdoctorSpecialization/'.$id);		
				  
				 
			}
			
					
		}

    	/**
		* This function is used to delete Doctor Specialization
		* create by Santosh Kumar Date:14 September
		* @return boolean $result : TRUE / FALSE
		*/
		
		function deleteDoctorSpecialization($id=null)
		{
	
	
			
		     $this->load->model('doctor_model');
			$id = $this->input->post('id');
       
			$Doctor = array('isDeleted'=>1);
            
			$result = $this->doctor_model->deleteDocSpecialization($id, $Doctor);

			if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
			else { echo(json_encode(array('status'=>FALSE))); }

		}
		
		
		
		
		 ##################################################################################################
		 #                      These All bellow these  function  use  for   Doctor Qualification          # 
		 ##################################################################################################
		
	   /**
		* This is use for  doctor Specialization listing 
		 create BY Santosh Kumar Verma 14 September
		*/
		function doctorQualificationListing()
		{
		   if($this->isAdmin() == TRUE)
			{
			  $this->loadThis();
			}
			else
			{  
		    $searchText = $this->security->xss_clean($this->input->post('searchText'));
			$data['searchText'] = $searchText;
		
			$this->load->library('pagination');
		
			$count = $this->doctor_model->doctorQualificationCount($searchText);
			
			$returns = $this->paginationCompress ( "admin/docQualificationListing/", $count, 10,3);
		
			$data['docqualificationRecords'] = $this->doctor_model->docQualificationListing($searchText, $returns["page"], $returns["segment"]);
		
			$this->global['pageTitle'] = 'Mediwheel : Doctor Listing';
		
			$this->loadViews("admin/doctorQualificationListing", $this->global, $data, NULL);
			   
			  
			}
		}
		
		
		/**
		* This  function for Doctor Qualification Add Form
		 create BY Santosh Kumar Verma 14 September
		*/
		 
		function addDocQualificationform()
        {
			if($this->isAdmin() == TRUE)
			{
			$this->loadThis();
			}
			else
			{
			$this->load->model('doctor_model');
			$this->global['pageTitle'] = 'Mediwheel : Add New Doctor Qualification';

			$this->loadViews("admin/addDocQualification", $this->global,  NULL);
			}
		}
				
		/**
		* This  function for Add Doctor Qualification  
		 create BY Santosh Kumar Verma 14 September
		*/		
		
		function addDocQualification()
		{
			
				
			if($this->isAdmin() == TRUE)
			{
		
			$this->loadThis();
			}
			else
			{
			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('qualification');
			
			$qualification= $this->security->xss_clean($this->input->post('qualification'));
			$this->load->model('doctor_model');
			
			 $docInfo = array('qualification'=>$qualification);
			$result = $this->doctor_model->addDocQualificationdetail($docInfo);
			
			if($result > 0)
			{
				$this->session->set_flashdata('success', 'Doctor Qualification   Added Successfully');
			}
			else
			{
				$this->session->set_flashdata('error', 'Doctor Qualification Added  Failed');
			}
			
			redirect('admin/addDocQualificationform');
			
			}
	    }
	
	
		/**
		* This  function for edit form for Doctor Qualification   
		 create BY Santosh Kumar Verma 14 September
		*/		
		
		function editdoctorQualification ($id=null)
		{
			if($id == null)
			{
			
				redirect('admin/healthCheckupTest');
			}
			else{
			
			
					$data['doctorQualificationInfo'] = $this->doctor_model->getdoctorQualificationInfo($id);
			   
					$this->global['pageTitle'] = 'Mediwheel : Edit HealthCheckupTest';
				
					$this->loadViews("admin/editdoctorQualification", $this->global, $data, NULL);
			    }
        }
	


		/**
		* This function is used to update Doctor Specialization
		* create by Santosh Kumar  Deate :14 September
		* @return boolean $result : TRUE / FALSE
		*/			
		public function updateDoctorQualification($id=null)
		{
			
			
			if($this->isAdmin() == TRUE)
			{
				$this->loadThis();
			}
			else
			{
				$this->load->library('form_validation');
				
				$id = $this->input->post('id');
				$qualification     = $this->security->xss_clean($this->input->post('qualification'));
				
				$doctorQualificationInfo= array('qualification'=> $qualification );
				$result = $this->doctor_model->updatedocQualificationInfo($doctorQualificationInfo, $id);
				 
					
					if($result == true)
					{
					   $this->session->set_flashdata('success', 'Doctor Qualification  Updated Successfully.');
					   
						redirect('admin/editdoctorQualification/'.$id);	
					}
					else
						{
							$this->session->set_flashdata('error', 'Doctor Qualification  Updated failed');
						}
					redirect('admin/editdoctorQualification/'.$id);		
				  
				 
			}
			
					
		}

    	/**
		* This function is used to delete Doctor Qualification 
		* create by Santosh Kumar Date:14 September
		* @return boolean $result : TRUE / FALSE
		*/
		
		function deleteDoctorQualification($id=null)
		{
	
	
			
		     $this->load->model('doctor_model');
			$id = $this->input->post('id');
       
			$Doctor = array('isDeleted'=>1);
            
			$result = $this->doctor_model->deleteDocQualification($id, $Doctor);

			if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
			else { echo(json_encode(array('status'=>FALSE))); }

		}
			
		
		
    }	