<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/*
	*  Putting all default things which are required under the constructor
	*/
	function __construct()
	{

		parent::__construct();
		
        $this->load->model('home_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation'); 
		//$this->load->model('User_model');
    

    }
	
	
	 
	
	
	public function index()
	{		 		 
		 $data 	 = array(
          'page_title' => 'Mediwheel- My Dashboard',
          );
		
		//$this->load->view('front_pages/includes/header',$data);
		$this->load->view('front_pages/dashboard',$data);
		//$this->load->view('front_pages/includes/footer',$data);
	}
}
