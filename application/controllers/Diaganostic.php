<?php

     if(!defined('BASEPATH')) exit('No direct script access allowed');

	 require APPPATH . '/libraries/BaseController.php';


	/**
	* Class : Diaganostic (DiaganosticController)
	* Diaganostic Class to control all operations.
	*created by santosh 28 Aug 2018
	*/
    class Diaganostic extends BaseController
    {
		/**
		* This is default constructor of the class
		*/
		public function __construct()
		{
        parent::__construct();
        $this->load->model('diaganostic_model');
        $this->isLoggedIn();   
		}
    
		/**
		* This function used to load the first screen of the user
		*/
		public function index()
		{
		
        $this->global['pageTitle'] = 'Mediwheel : Dashboard';
        
        //$this->loadViews("diaganosticListing", $this->global, NULL , NULL);
		}
		
		
		/**
		* This is use for  Diaganostic listing 
		create BY Santosh Kumar Verma 4 September
		*/
		function diaganosticListing()
		{
		if($this->isAdmin() == TRUE)
		{
		  $this->loadThis();
		}else
			{        
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				$data['searchText'] = $searchText;
            
				$this->load->library('pagination');
            
				$count = $this->diaganostic_model->diagnosticListingCount($searchText);
				
				$returns = $this->paginationCompress ( "admin/hospitalDoctorsListing/", $count, 10,3);
            
				$data['diaganosticRecords'] = $this->diaganostic_model->diaganosticListing($searchText, $returns["page"], $returns["segment"]);
            
				$this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
				$this->loadViews("diaganosticListing", $this->global, $data, NULL);
				
				
			}
		}
	
	
	
	
	   	function addNewDiaganosticform() 
			{
	
	
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('diaganostic_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New Hospital Diaganostic';

				$this->loadViews("addNewDiaganostic", $this->global,  NULL);
				
				}
	
	
			}
	
	
	   /**
		* This is use for add Diaganostic  
		create BY Santosh Kumar Verma 4 September
		*/
		function addNewDiaganostic()
		{
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{  
				
                $masterCode 				    = $this->security->xss_clean($this->input->post('mastercode'));
				$mastercode 				    = $this->security->xss_clean($this->input->post('mastercode'));
			    $diaganosticSubCode 			= $this->security->xss_clean($this->input->post('diaganosticSubCode'));
			  
			    $registrationNo				    = $this->security->xss_clean($this->input->post('registrationNo'));
                $imaging 						= $this->security->xss_clean($this->input->post('imaging'));
                $nabl 							= $this->input->post('nabl');
				$lab 							= $this->security->xss_clean($this->input->post('lab'));
				
				$xray 							= $this->security->xss_clean($this->input->post('xray'));
				$digitalxray_offer 				= $this->security->xss_clean($this->input->post('digitalxray_offer'));
				$digitalxray_display 			= $this->security->xss_clean($this->input->post('digitalxray_display'));
				
				$ultrasound 					= $this->input->post('ultrasound');
				$ultrasound_offer 				= $this->input->post('ultrasound_offer');
				$ultrasound_display 			= $this->input->post('ultrasound_display');
				
				$ecg 							= $this->input->post('ecg');
				$ecg_offer 						= $this->input->post('ecg_offer');
				$ecg_display 					= $this->input->post('ecg_display');
				
				$trademill 						= $this->input->post('trademill');
				$trademill_offer 				= $this->input->post('trademill_offer');
				$trademill_display 				= $this->input->post('trademill_display');
				
				$twoDeco 						= $this->input->post('eco');
				$eco_offer 						= $this->input->post('eco_offer');
				$eco_display 					= $this->input->post('eco_display');
				
				$ct 							= $this->security->xss_clean($this->input->post('ct'));
				$ct_offer 						= $this->security->xss_clean($this->input->post('ct_offer'));
				$ct_display 					= $this->security->xss_clean($this->input->post('ct_display'));
				
			    $mri 							= $this->security->xss_clean($this->input->post('mri'));
				$mri_offer 						= $this->security->xss_clean($this->input->post('mri_offer'));
				$mri_display 					= $this->security->xss_clean($this->input->post('mri_display'));
				
				$petct 							= $this->security->xss_clean($this->input->post('petct'));
				$petct_offer 					= $this->security->xss_clean($this->input->post('petct_offer'));
				$petct_display 					= $this->security->xss_clean($this->input->post('petct_display'));
				
				$receptionarea 					= $this->security->xss_clean($this->input->post('receptionarea'));
				$receptionarea_offer 			= $this->security->xss_clean($this->input->post('receptionarea_offer'));
				$receptionarea_display 			= $this->security->xss_clean($this->input->post('receptionarea_display'));
				
				$waitingarea 					= $this->security->xss_clean($this->input->post('waitingarea'));
				$waitingarea_offer 				= $this->security->xss_clean($this->input->post('waitingarea_offer'));
			    $waitingarea_display 			= $this->security->xss_clean($this->input->post('waitingarea_display')); 

				$homecollection 				= $this->security->xss_clean($this->input->post('homecollection'));
				$hcollect_charge 				= $this->security->xss_clean($this->input->post('hcollect_charge'));
				$hcollect_time 				    = $this->security->xss_clean($this->input->post('hcollect_time'));
				
				
				$parkingspace 					= $this->security->xss_clean($this->input->post('parkingspace'));
				$parkingspace_offer 			= $this->security->xss_clean($this->input->post('parkingspace_offer'));
				$parkingspace_display 			= $this->security->xss_clean($this->input->post('parkingspace_display'));
				
				$discountofferedtest 			= $this->security->xss_clean($this->input->post('discountofferedtest'));
				$discountofferedtestdisplayed = $this->security->xss_clean($this->input->post('discountofferedtestdisplayed'));
				 $discountofferedhealthcheckup   = $this->security->xss_clean($this->input->post('discountofferedhealthcheckup'));
			   $discountofferedhealthcheckupdisplayed = $this->security->xss_clean($this->input->post('discountofferedhealthcheckupdisplayed'));
			   
				$time_24_hours = $this->security->xss_clean($this->input->post('chk_time'));
				
				$timefrom = $this->security->xss_clean($this->input->post('timefrom'));
				$timeto = $this->security->xss_clean($this->input->post('timeto'));
				
				$hard_copy = $this->security->xss_clean($this->input->post('hard_copy'));
				$softcopy_onmail = $this->security->xss_clean($this->input->post('softcopy_onmail'));
				
				
				
				$sundayopen 						= $this->security->xss_clean($this->input->post('sundayopen'));
				$sun_timefrom 						= $this->security->xss_clean($this->input->post('sun_timefrom'));
				$suntimeto                          = $this->security->xss_clean($this->input->post('suntimeto'));
				if($sundayopen=='0')
				{
				$sun_timefrom =''; 	
				$suntimeto =''; 
				}
				
				$xray_film                          = $this->security->xss_clean($this->input->post('xray_film'));
				$discount_availableon_package       = $this->security->xss_clean($this->input->post('discount_availableon_package'));
				$reportsupload                       = $this->security->xss_clean($this->input->post('reportsupload'));
				$discountavailableonindividualtests = $this->security->xss_clean($this->input->post('discountavailableonindividualtests'));
				
				$username = $this->diaganostic_model->getUserName($this->session->userId);
				
				
				
				//for Address Details
				 $location = $this->security->xss_clean($this->input->post('location'));
				 $address = $this->security->xss_clean($this->input->post('address'));
				 $district = $this->input->post('district');
				 $city = $this->input->post('city');
			     $state = $this->input->post('state');

				$pincode = $this->input->post('pincode');
				$pincode1 = $this->input->post('pincode1');
				$pincodevalue = $this->diaganostic_model->getPincode($pincode);
				if(isset($pincodevalue[0])){
				$pincode=$pincodevalue[0]->pincode;
				}
				else {
					$pincode='';
				}
				
				if($pincode1!=''){
					$pincodeother=$pincode1;
				}
				else{
					$pincodeother='';
					
				}
				
				
				
				  
			   
				$target_dir 					= "uploads/diaganostic/";
				$target_file 					= $target_dir . basename($_FILES["photograph"]["name"]);
				$uploadOk 						= 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				// Check if image file is a actual image or fake image
			if(isset($_POST["submit"]))
			{
				$check = getimagesize($_FILES["photograph"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				} else {
       
				$uploadOk = 0;
				}
			}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["photograph"]["size"] > 9000000) {
    
			    $this->session->set_flashdata('error', 'File is too large!');
				$uploadOk = 0;	
				redirect('admin/addNewDiaganosticform');		
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
   
				$this->session->set_flashdata('error', 'Invalid  file type!');
				$uploadOk = 0;	
				redirect('admin/addNewDiaganosticform');	
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
				} 
				}
				 
				 if($imaging =='0')
				 {
				 $xray        ='0';
				 $digitalxray_offe='';
				 $digitalxray_display ='';
				  
				 
				 $ultrasound  ='';
				 $ultrasound_offer='';
				 $ultrasound_display='';
				 
				 $ecg  ='0';
				 $ecg_offer='';
				 $ecg_display='';
				 
				 
				 $ct  ='0';
				 $ct_offer='';
				 $ct_display='';
				 
				 $mri  ='0';
				 $mri_offer='';
				 $mri_display='';
				 
				 $petct  ='0';
				 $petct_offer='';
				 $petct_display='';
				 }
				 if($xray=='0')
				 {
				 $digitalxray_offer ='';
				 $digitalxray_offer ='';
				 }
				 
				if($time_24_hours=='1')
				 {
				 $timefrom ='0';
				 $timeto ='0';
				 } 
				 
				 
				 if($ultrasound=='0')
				 {
				 $ultrasound_offer ='';
				 $ultrasound_display ='';
				 }
				 
				 if($ecg=='0')
				 {
				 $ecg_offer ='';
				 $ecg_display ='';
				 }
				 
				 if($trademill=='0')
				 {
				 $trademill_offer ='';
				 $trademill_display ='';
				 }
				 
				 if($twoDeco=='0')
				 {
				 $eco_offer ='';
				 $eco_display ='';
				 }
				 if($ct=='0')
				 {
				 $ct_offer ='';
				 $ct_display ='';
				 }
				 if($mri=='0')
				 {
				 $mri_offer ='';
				 $mri_display ='';
				 }
				  if($petct=='0')
				 {
				 $petct_offer ='';
				 $petct_display ='';
				 }
				 
				  if($receptionarea=='0')
				 {
				 $receptionarea_offer ='';
				 $receptionarea_display ='';
				 }
				 
				  if($waitingarea=='0')
				 {
				 $waitingarea_offer ='';
				 $waitingarea_display ='';
				 }
				 
				  if($homecollection=='0')
				 {
				 $ultrasound_offer ='';
				 $ultrasound_display ='';
				 }
				 
				 
				$photopath=base_url().$target_file;		 
				$this->load->model('diaganostic_model');
                $lastdiaid = $this->diaganostic_model->lastdiaid();
				$lastdiaid= $lastdiaid[0]->id;
				$lastdiaid=$lastdiaid+1;
				
				$diaSubCode=$masterCode."DIA".$lastdiaid;
				$diaganosticsInfo = array(
				'mastercode'=>$masterCode,
				'diaganosticSubCode'=>$diaSubCode,
				'registrationNo'=>$registrationNo,
				'imaging'=>$imaging,
				'nabl'=>$nabl,
				'lab'=>$lab,
				'digitalXray'=> $xray,
				'digitalxray_offer'=>$digitalxray_offer,
				'digitalxray_display'=>$digitalxray_display,
				
				'ultrasound'=>$ultrasound,
				'ultrasound_offer'=>$ultrasound_offer,
				'ultrasound_display'=>$ultrasound_display,
				
				'ecg'=>$ecg,
				 'ecg_offer'=>$ecg_offer,
				 'ecg_display'=>$ecg_display,
				 
				 'trademill'=>$trademill,
				 'trademill_offer'=>$trademill_offer,
				 'trademill_display'=>$trademill_display,

				 'twoDeco'=>$twoDeco,
				 'eco_offer'=>$eco_offer,
				 'eco_display'=>$eco_display,
				 
				 'ct'=>$ct,
				 'ct_offer'=>$ct_offer,
				 'ct_display'=>$ct_display,
				 
				 'mri'=>$mri,
				 'mri_offer'=>$mri_offer,
				 'mri_display'=>$mri_display,
				 
				 'petct'=>$petct,
				 'petct_offer'=>$petct_offer,
				 'petct_display'=>$petct_display,
				 
				 'receptionArea'=>$receptionarea,
				 'receptionarea_offer'=>$receptionarea_offer,
				 'receptionarea_display'=>$receptionarea_display,
				 
				 'waitingArea'=>$waitingarea,
				 'waitingarea_offer'=>$waitingarea_offer,
				 'waitingarea_display'=>$waitingarea_display,
				 
				 'homecollection'=>$homecollection,
				 'hcollect_charge'=>$hcollect_charge,
				 'hcollect_time'=>$hcollect_time,
				 
				 'parkingSpace'=>$parkingspace,
				 'parkingspace_offer'=>$parkingspace_offer,
				 'parkingspace_display'=>$parkingspace_display,
				 
				 'discountofferedtest'=>$discountofferedtest,
				 'discountDisplayedTest'=>$discountofferedtestdisplayed,
				 'discountOfferdHealthCheckup'=>$discountofferedhealthcheckup,
				 'discountDisplayedHealthCheckupe'=>$discountofferedhealthcheckupdisplayed,
				 'time_24_hours'=>$time_24_hours,
				 'timefrom'=>$timefrom,
				 'timeto'=>$timeto,
				 'hard_copy'=>$hard_copy,
				 'softcopy_onmail'=>$softcopy_onmail,
				 
				 'sundayopen'=>$sundayopen,
				 'sun_timefrom'=>$sun_timefrom,
				 'suntimeto'=>$suntimeto,
				 'reportsupload'=>$reportsupload,
				 'discount_availableon_package'=>$discount_availableon_package,
				 'discountavailableonindividualtests'=>$discountavailableonindividualtests,
				 'xray_film '=>$xray_film,
				 'photograph'=>$photopath,
				 
				'location'=>$location, 
				'address'=> $address,
				 'district'=>$district,
				 'city'=>$city,
				 'state'=>$state,
				 'pincode'=>$pincode,
				 'pincodeother'=>$pincode1,
				 'createdBy'=>$username[0]->name,
				 'createdDate'=>date('Y-m-d H:i:s')
				 
				 
				 );
			
				
				$this->load->model('diaganostic_model');
			    $results = $this->diaganostic_model->addNewDiaganostics($diaganosticsInfo);
				
				
				
				
				$contact = $this->security->xss_clean($this->input->post('contact'));
				$mobile = $this->security->xss_clean($this->input->post('mobile'));
				$landline = $this->security->xss_clean($this->input->post('landline'));
				$email = $this->security->xss_clean($this->input->post('email'));
				
				 $cnum=sizeof($contact);
				$lastid= $this->diaganostic_model->lastid();
				$lastid= $lastid['0']->id;
				$resultsubcode_details= $this->diaganostic_model->lastsubcode_details($lastid);
				//print_r($resultsubcode_details);die;
				
			    
				for($i=0; $i< $cnum; $i++ )
				{
				$diaganosticscontactInfo = array(
				'dia_id'=>$resultsubcode_details['0']->id,
				'mastercode'=>$resultsubcode_details['0']->mastercode,
				'diaganosticSubCode'=>$resultsubcode_details['0']->diaganosticSubCode,
				'contactname'=>$contact[$i],
				'email'=>$email[$i],
				'landline'=>$landline[$i],
				'mobile'=>$mobile[$i],
				);
				
				$result = $this->diaganostic_model->addNewDiaganosticscontactdetails($diaganosticscontactInfo);
				
				}
				
                
                if($results > 0)
                {
                    $this->session->set_flashdata('success', 'New Diaganostic Center  created successfully');
					redirect('admin/addNewDiaganosticform');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital Diaganostics  Centercreation failed');
                }
                
               // redirect('admin/hospitalDiaganosticListing');
				//}
            
				}

		}
		
    
			
		/**
		* This function is used to delete HospitalDiaganostic
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
		function deleteDiaganostic($id=null)
		{
		
					
			$this->load->model('diaganostic_model');
			$id = $this->input->post('id');

			$diaganostic = array('isDeleted'=>1);

			$result = $this->diaganostic_model->deleteDiaganostic($id, $diaganostic);

			if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
			else { echo(json_encode(array('status'=>FALSE))); }

		}
		
	
		/**
		* This function is used for edit  Diaganostic
		* created by Santosh Kumar  5 September 2018
		*/
		
		function editDiaganostic($id=null)
		{
		
		  $this->load->library('form_validation');
		

			if($id == null)
			{
				redirect('admin/healthCheckupPackageListing');
			}
			else {
				
			   	
			$data['HospitalDiaganosticInfo'] = $this->diaganostic_model->getDiaganosticInfo($id);
			
			//print_r($data['HospitalDiaganosticInfo']);
			
			 $subcode =   $data['HospitalDiaganosticInfo'][0]->diaganosticSubCode;
			
			$data['HospitalDiaganosticcontactInfo'] = $this->diaganostic_model->getDiaganostic_contactInfo($subcode);
			
			$this->global['pageTitle'] = 'Mediwheel : Edit Hospital Diaganostic';
			
			$this->loadViews("editDiaganostic", $this->global, $data, NULL);
			}
		
		
		}
	
	
	
		/**
		* This function is used to update  Diaganostic
		* create by Santosh Kumar 5 September 
		* @return boolean $result : TRUE / FALSE
		*/			
	public function updateDiaganostic($id=null)
	{
			
			
				if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			
			
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
			
			 $diaid = $this->input->post('id');
            
                $mastercode           			  = $this->security->xss_clean($this->input->post('mastercode'));
				$diaganosticSubCode           	  = $this->security->xss_clean($this->input->post('diaganosticSubCode'));
                $registrationNo                   = $this->input->post('registrationNo');
				$imaging                 		  = $this->input->post('imaging');
				$nabl                 			  = $this->input->post('nabl');
				$lab                 			  = $this->input->post('lab');
				
				$digitalXray                	  = $this->input->post('xray');
				$digitalxray_offer                = $this->input->post('digitalxray_offer');
				$digitalxray_display              = $this->input->post('digitalxray_display');
				
				
				$ultrasound               	      = $this->input->post('ultrasound');
				$ultrasound_offer                 = $this->input->post('ultrasound_offer');
				$ultrasound_display               = $this->input->post('ultrasound_display');
				
				$ecg                       		  = $this->input->post('ecg');
				$ecg_offer                        = $this->input->post('ecg_offer');
				$ecg_display                      = $this->input->post('ecg_display');
				
				$trademill                		  = $this->input->post('trademill');
				$trademill_offer                  = $this->input->post('trademill_offer');
				$trademill_display                = $this->input->post('trademill_display');
				
				$twoDeco                 		  = $this->input->post('eco');
				$eco_offer                 		  = $this->input->post('eco_offer');
				$eco_display                      = $this->input->post('eco_display');
				
				$ct                 			  = $this->input->post('ct');
				$ct_offer                 		  = $this->input->post('ct_offer');
				$ct_display                 	  = $this->input->post('ct_display');
				
				$mri                 			  = $this->input->post('mri');
				$mri_offer                 		  = $this->input->post('mri_offer');
				$mri_display                 	  = $this->input->post('mri_display');
				
				$petCt	                 		  = $this->input->post('petct');
				$petct_offer	                  = $this->input->post('petct_offer');
				$petct_display	                  = $this->input->post('petct_display');
				
				//$timings                 		  = $this->input->post('timings');
				$receptionarea                	  = $this->input->post('receptionarea');
				$receptionarea_offer              = $this->input->post('receptionarea_offer');
				$receptionarea_display            = $this->input->post('receptionarea_display');
				
				$waitingarea                 	  = $this->input->post('waitingarea');
				$waitingarea_offer                = $this->input->post('waitingarea_offer');
				$waitingarea_display              = $this->input->post('waitingarea_display');
				
				$parkingspace                 	  = $this->input->post('parkingspace');
				$parkingspace_offer               = $this->input->post('parkingspace_offer');
				$parkingspace_display             = $this->input->post('parkingspace_display');
				
				$timefrom                 	      = $this->input->post('timefrom');
				$timeto                 	      = $this->input->post('timeto');
				$time_24_hours                 	  = $this->input->post('chk_time');
				
				
				$homecollection                   = $this->input->post('homecollection');
				$hcollect_charge                  = $this->input->post('hcollect_charge');
				$hcollect_time                 	  = $this->input->post('hcollect_time');
				
				$hard_copy                 	      = $this->input->post('hard_copy');
				$softcopy_onmail                  = $this->input->post('softcopy_onmail');
				
				$photograph                       = $this->input->post('photograph');
				$discountOfferedTest              = $this->input->post('discountofferedtest');
				$discountDisplayedTest            = $this->input->post('discountofferedtestdisplayed');
				$discountOfferdHealthCheckup      = $this->input->post('discountofferedhealthcheckup');
				$discountDisplayedHealthCheckupe  = $this->input->post('discountofferedhealthcheckupdisplayed');
				
				
				$sundayopen  					= $this->input->post('sundayopen');
				$sun_timefrom  					= $this->input->post('sun_timefrom');
				$suntimeto  					= $this->input->post('suntimeto');
				
				if($sundayopen=='0')
				{
					$sun_timefrom ='';
					$suntimeto ='';
				}
				$reportsupload  				= $this->input->post('reportsupload');
				$discount_availableon_package   = $this->input->post('discount_availableon_package');
				$discountavailableonindividualtests  = $this->input->post('discountavailableonindividualtests');
				$xray_film                       = $this->input->post('xray_film');
				
				
			//for Address
			     $location 						= $this->security->xss_clean($this->input->post('location'));
				 $address 						= $this->security->xss_clean($this->input->post('address'));
				 $district					    = $this->input->post('district');
				 $city                          = $this->input->post('city');
			     $state                         = $this->input->post('state');
			     $pincode                       = $this->input->post('pincode');
				 $pincode1                      = $this->input->post('pincode1');
				 
				 if($pincode!=''){
					
					$pincode=$pincode;
				}
				else{
					
					if($pincode1!=''){
						$pincode=$pincode1;
					}
				}
				
				
			/*image  section */
				
			    $filename                  = $_FILES["photograph"]["name"];	
			    $target_dir 					= "uploads/diaganostic/";
				$target_file 					= $target_dir . basename($_FILES["photograph"]["name"]);
				$uploadOk 						= 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				
		    // Check if image file is a actual image or fake image
			if(isset($_POST["submit"]))
			{
				$check = getimagesize($_FILES["photograph"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				} else {
       
				$uploadOk = 0;
				$uploadOk = 0;
				}
			}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["photograph"]["size"] > 9000000) {
    
				$uploadOk = 0;
				  $this->session->set_flashdata('error', 'File is too large!');	
				redirect('admin/editDiaganostic/'.$diaid);	
				}
				// Allow certain file formats
				
				if($filename !="")
				{
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" )
				{
   
				$uploadOk = 0;
				$this->session->set_flashdata('error', 'Invalid  File Type');	
			    
				redirect('admin/editDiaganostic/'.$diaid);	
				}
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
				} 
				}
               
           		/* end image  section */
				if($imaging =='0')
				 {
				/*
				 $digitalXray  ='0';
				 $ultrasound  ='0';
				 $ecg  ='0';
				 $trademill  ='0';
				 $twoDeco  ='0';
				 $ct  ='0';
				 $mri  ='0';
				 $petCt  ='0';
				 */
				 
				
				 $digitalxray_offer ='';
				 $digitalxray_display ='';
				 
				 $ultrasound_offer ='';
				 $ultrasound_display ='';
				 
				 $ecg_offer ='';
				 $ecg_display ='';
				 
				 $ct_offer ='';
				 $ct_display ='';
				 
				 $mri_offer ='';
				 $mri_display ='';
				 
				 $petct_offer ='';
				 $petct_display ='';
				 
				 }
				 
				 
				 if($time_24_hours=='1')
				 {
			     $timefrom  ='';
                 $timeto    =''; 
				 }
				 
				 
				 if($digitalXray=='0')
				 {
				 $digitalxray_offer ='';
				 $digitalxray_offer ='';
				 }
				 
				
				 if($ultrasound=='0')
				 {
				 $ultrasound_offer ='';
				 $ultrasound_display ='';
				 }
				 
				 if($ecg=='0')
				 {
				 $ecg_offer ='';
				 $ecg_display ='';
				 }
				 
				 if($trademill=='0')
				 {
				 $trademill_offer ='';
				 $trademill_display ='';
				 }
				 
				 if($twoDeco=='0')
				 {
				 $eco_offer ='';
				 $eco_display ='';
				 }
				 if($ct=='0')
				 {
				 $ct_offer ='';
				 $ct_display ='';
				 }
				 if($mri=='0')
				 {
				 $mri_offer ='';
				 $mri_display ='';
				 }
				  if($petct=='0')
				 {
				 $petct_offer ='';
				 $petct_display ='';
				 }
				 
				  if($receptionarea=='0')
				 {
				 $receptionarea_offer ='';
				 $receptionarea_display ='';
				 }
				 
				  if($waitingarea=='0')
				 {
				 $waitingarea_offer ='';
				 $waitingarea_display ='';
				 }
				 
				  if($parkingspace=='0')
				 {
				 $parkingspace_offer ='';
				 $parkingspace_display ='';
				 }
				 
				 if($homecollection=='0')
				 {
				 $ultrasound_offer ='';
				 $ultrasound_display ='';
				 }
				 
				
                $photopath=base_url().$target_file;	
				
				if($filename=="")
				{
				echo  $photopath = $this->input->post('hidphotograph');
				
                }
				
                $hospitalDiaganosticInfo = array(
				'mastercode'=>             $mastercode,
				//'diaganosticSubCode'=>   $diaganosticSubCode,
				'registrationNo'=>         $registrationNo,
				'imaging'=>                $imaging,
				'nabl'=>                   $nabl,
				'lab'=>                    $lab,
				
				'digitalXray'=>            $digitalXray,
				'digitalxray_offer'=>      $digitalxray_offer,
				'digitalxray_display'=>    $digitalxray_display,
				
				'ultrasound'=>             $ultrasound,
				'ultrasound_offer'=>       $ultrasound_offer,
				'ultrasound_display'=>     $ultrasound_display,
				
				'ecg'=>                    $ecg,
				'ecg_offer'=>              $ecg_offer,
				'ecg_display'=>            $ecg_display,
				
				'trademill'=>              $trademill,
				'trademill_offer'=>        $trademill_offer,
				'trademill_display'=>      $trademill_display,
				
				'twoDeco'=>                $twoDeco,
				'eco_offer'=>              $eco_offer,
				'eco_display'=>            $eco_display,
				
				'ct'=>                     $ct,
				'ct_offer'=>               $ct_offer,
				'ct_display'=>             $ct_display,
				
				'mri'=>                    $mri,
				'mri_offer'=>              $mri_offer,
				'mri_display'=>            $mri_display,
				
				'petCt'=>                  $petCt,
				'petct_offer'=>            $petct_offer,
				'petct_display'=>          $petct_display,
				
                'receptionArea'=>          $receptionarea,
				'receptionarea_offer'=>    $receptionarea_offer,
				'receptionarea_display'=>  $receptionarea_display,
				
				'waitingArea'=>            $waitingarea,
				'waitingarea_offer'=>      $waitingarea_offer,
				'waitingarea_display'=>    $waitingarea_display,
				
				'parkingSpace'=>           $parkingspace,
				'parkingspace_offer'=>     $parkingspace_offer,
				'parkingspace_display'=>   $parkingspace_display,
				
				'homecollection'=>         $homecollection,
				'hcollect_charge'=>        $hcollect_charge,
				'hcollect_time'=>          $hcollect_time,
				
				'time_24_hours'=>          $time_24_hours,
				'timefrom'=>               $timefrom,
				'timeto'=>                 $timeto,
				
				'hard_copy'=>              $hard_copy,
				'softcopy_onmail'=>        $softcopy_onmail,
				
				'photograph'=>             $photopath,
				'discountOfferedTest'=>    $discountOfferedTest,
				'discountDisplayedTest'=>  $discountDisplayedTest,
				'discountOfferdHealthCheckup'=>$discountOfferdHealthCheckup,
				'discountDisplayedHealthCheckupe'=>$discountDisplayedHealthCheckupe ,
				
				'sundayopen'=>$sundayopen,
				'sun_timefrom'=>$sun_timefrom,
				'suntimeto'=>$suntimeto,
				'reportsupload'=>$reportsupload,
				'discount_availableon_package'=>$discount_availableon_package,
				'discountavailableonindividualtests'=>$discountavailableonindividualtests,
				'xray_film'=>$xray_film,
				
				
				'location'=>$location,
				'address'=> $address,
				'district'=>$district,
				'city'=>$city,
				'state'=>$state,
				'pincode'=>$pincode,
				);
              
			  
			 
			  
               $result = $this->diaganostic_model->updateDiaganostic($hospitalDiaganosticInfo, $id);
			   
			   
			   // Diacontact details 
			   
			   $contact= $this->security->xss_clean($this->input->post('contact'));
			   $mobile = $this->security->xss_clean($this->input->post('mobile'));
			   $landline = $this->security->xss_clean($this->input->post('landline'));
			   $email = $this->security->xss_clean($this->input->post('email'));
			  
			   $oldid = $this->security->xss_clean($this->input->post('oldid'));
			   $contactid = $this->security->xss_clean($this->input->post('contactid'));
			   $cmastercode = $this->security->xss_clean($this->input->post('cmastercode'));
               $cdiaganosticSubCode = $this->security->xss_clean($this->input->post('cdiaganosticSubCode'));				
			   
				
			   $cnum=sizeof($contact);
			   
			   
			   for($i=0; $i< $cnum; $i++ )
				{
				$diaganosticscontactInfo = array(
				'contactname'=>$contact[$i],
				'email'=>$email[$i],
				'landline'=>$landline[$i],
				'mobile'=>$mobile[$i],
				);
				
				$diaganosticscontactInfo2 = array(
				'mastercode'=>$mastercode,
				'diaganosticSubCode'=>$diaganosticSubCode,
				'contactname'=>$contact[$i],
				'email'=>$email[$i],
				'landline'=>$landline[$i],
				'mobile'=>$mobile[$i],
				);
				
			   	
					
				   if($oldid[$i]!=''){
					  $cid=$contactid[$i];
				        $results = $this->diaganostic_model->updateDiaganosticscontactdetails($diaganosticscontactInfo,$cid);
				       }else{
						   
						$results = $this->diaganostic_model->addNewDiaganosticscontactdetails2($diaganosticscontactInfo2);
					   }
				}
			  
              
                if($result == true)
                {
                   $this->session->set_flashdata('success', ' Diaganostic Centre Updated successfully.');
				   
				    redirect('admin/editDiaganostic/'.$diaid);	
                }
                else
                {
                    $this->session->set_flashdata('error', 'Diaganostic Centre  updated failed');
                }
                
               //  redirect('admin/hospitalDiaganosticListing');
			  
			   redirect('admin/editDiaganostic/'.$diaid);		
			  
			 
        }
			
					
	}
		
		
		
		/**
		* This function is used to change  chnage Diaganostic Center Subcode Status status
		* create by Santosh Kumar Verma
		* @return boolean $result : TRUE / FALSE
		*/	
	    function  chnageDiaganosticCenterSubcodeStatus($id=null)
		{
			
			
			$this->load->model('diaganostic_model');
			$this->load->helper('url');
			
			$id    =  $this->uri->segment(3);
			$status =  $this->uri->segment(4);
			
			if($status=='0')
			{
			$status ='1';	
			}else{
			$status ='0';	
			}
			
		    //$id = $this->input->post('id');
			//$status = $this->input->post('status');
			$status = array('status'=>$status);

			 $result = $this->diaganostic_model->DiaganosticCenterSubcodeStatus($id, $status); 
				
					
				redirect('admin/diaganosticListing');
				
				
			
			if ($result) {
            				
			 				
			//echo(json_encode(array('status'=>TRUE)));
             redirect("admin/diaganosticListing", 'refresh');   
           
			}
			else { echo(json_encode(array('status'=>FALSE))); }
			//echo $this->db->last_query();
		}
			
	

	
			
			
			
			
	
		
    }	