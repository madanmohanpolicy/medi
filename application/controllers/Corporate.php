<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Corporate extends CI_Controller
 {

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');
				//$this->load->library('mail');


	}
	
	public function index()
	{
		$cdata = array(
			  'page_title' => 'Mediwheel- Your Welness Partner',
			  
			  );
		
		$secretkey=$this->input->get('skey');
	
			$this->load->model('corporate_model');		
           	
			$results = $this->corporate_model->getMobile($secretkey);
		
	if($results){
	
		
		$mobile=$results[0]->mobile;
	
		$otp = rand(10000,99999);
		$this->sendOTP_Corporate($mobile,$otp);
	$data = array(
					'front_corporate_user_otp' => $otp,
					'front_corporate_user_mobile' => $mobile,
					'front_corporate_user_secretkey' =>$secretkey
					);
					
					$this->session->set_userdata($data);
	           
	
	}
	else{
		$data = array(
					'front_corporate_user_otp' => '',
					'front_corporate_user_mobile' => '',
					'front_corporate_user_secretkey' =>''
					);
	}
	
	$this->load->view('front_pages/includes/header',$cdata);
		
		
				$this->load->view('front_pages/corporate_user_password_reset',$data);
				
				
		
		$this->load->view('front_pages/includes/footer',$data);
	
	}
	
	
	public function createPassword(){
		$newpass=$this->input->post('npassword');
		$scrtkey=$this->input->post('skey');
		
	
		$confirmpass=$this->input->post('cpassword');
		$Otp=$this->input->post('otp');
			$data = array(
			  'page_title' => 'Mediwheel- Your Welness Partner',
			  
			  );
		if($newpass===$confirmpass){
			if($Otp==$this->session->front_corporate_user_otp){
				  $corporateuserinfo = array();
              
                $corporateuserinfo = array('password'=>md5($newpass), 'status'=>1,'is_mobile_verified'=>1);
				$this->load->model('corporate_model');		
			$results = $this->corporate_model->createCorporatePassword($corporateuserinfo,$scrtkey);
			
			$resultsnew = $this->corporate_model->getCorporateUserInfo($scrtkey);
						// Sending email 
						
						
						/*
						$this->load->library('email');	
						$this->email->from('noreply@mediwheel.in', 'Mediwheel Admin');					 
						$this->email->to($to_email);
						$this->email->bcc('madan.me@gmail.com');
						$this->email->subject('Mediwheel Admin : password Created');					
						$message = "Hi,\n\nYou have created your password. \n\nThanks,\nMediwheel Team";					 
						$this->email->message($message);*/
						
			       if($resultsnew){
					   $to_email=$resultsnew[0]->email;
						 $sessionArray = array('front_id'=>$resultsnew[0]->id,                    
                                        'front_name'=>$resultsnew[0]->name,
                                        'front_email'=>$resultsnew[0]->email,
                                        'front_mobile'=>$resultsnew[0]->mobile,
										'front_is_corporate_user'=>1,
										 'front_corporate_name'=>$resultsnew[0]->corporate_name,
										'is_logged_in'=>1,
										
                                );
                
                $this->session->set_userdata($sessionArray);
			redirect('health');
				   }
			}
		else {
			echo "OTP was wrong ";
			
		}
			
		}
		else{
			
			echo "New password and Confirem Passwored was not same";
		}
		//echo $newpass.'-'.$confirmpass.'-'.$Otp;
		//$this->session->corporate_city='4005';
		
	
	}
	function sendOTP_Corporate($mobile,$otp)
	{
		$message_mobile = "";		
		$message_mobile = "Dear User,  Your OTP is $otp";
		$message_mobile = preg_replace('/\s+/', '%20', $message_mobile);
		$url = "http://india.ibulksms.com/sendurlcomma.aspx?user=20063297&pwd=nprpu4&senderid=MEDSAV&mobileno=$mobile&msgtext=$message_mobile&smstype=0";
		$message_mobile = $this->execute_webservices($url);
		if($message_mobile)
		{
			//$this->session->userdata['is_logged_in'])
		}
		return $message_mobile;
	}
	
	
	public function execute_webservices($url)
	{
		//$url = "http://localhost/mediwheel/websignup/?name=www&mobile=9958469872&email=madan.me@gmail.com&signup=1";
		//echo  $url;
		$response = '';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",  
		CURLOPT_HTTPHEADER => array(
		"accept: application/vnd.healthkartplus.v7+json",		
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return $response;
	}
	
 }