<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/*
	*  Putting all default things which are required under the constructor
	*/
	function __construct()
	{

		parent::__construct();
		
        $this->load->model('home_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation'); 
		//$this->load->model('User_model');
    

    }
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
		 $cities =	$this->home_model->getMetroCity();
		 
		 //echo "<pre>";
		// print_r($cities);
		 
		 $data 	 = array(
          'page_title' => 'Mediwheel- Your Welness Partner',
          'cities'     => $cities,
          );
		
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view('front_pages/home',$data);
		$this->load->view('front_pages/includes/footer',$data);
	}
	
	public function about()
	{		 		 
		 $data 	 = array(
          'page_title' => 'Mediwheel- Your Welness Partner',
          );
		
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view('front_pages/about',$data);
		$this->load->view('front_pages/includes/footer',$data);
	}
}
