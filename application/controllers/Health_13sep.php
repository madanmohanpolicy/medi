<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Health extends CI_Controller
	{

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');
				//$this->load->library('mail');


	}
	
	/**
	* This function used to open the foem for sigining up user.
	* Its default funtion of this controller.
	* @param comments goes here
	*/
	public function index()
	{
		 $this->load->model('healthpackage_model');
		 
        
		$data['healthPackageRecords'] = $this->healthpackage_model->healthPackageListing();
		
		
		
		//$this->session->userdata['is_logged_in'])
		
		$data['page_title'] = 'Mediwheel : Health Packages';
		$this->load->view('front_pages/includes/header',$data);		
		$this->load->view('front_pages/health',$data,NULL);	
		$this->load->view('front_pages/includes/footer',$data);
	}
	
	function booking(){
		
	$packagename=$this->input->get('package');
	$packageid=$this->input->get('packageId');
	$organisation=$this->input->get('organisation');
	$organisationid=$this->input->get('organisationid');
	$cost=$this->input->get('cost');
	$discount=$this->input->get('discount');
	$costafterdiscount=$this->input->get('costafterdiscount');
	$appointmentdate=$this->input->get('appointmentdate');
	
	$appointmenttime=$this->input->get('appointmenttime');
	$userId=$this->session->front_id;
	$userName=$this->session->front_name;
	$userEmail=$this->session->front_email;
	$userMobile=$this->session->front_mobile;
	
	$this->load->model('healthpackage_model');
	 $rec= $this->healthpackage_model->ifBooked($packageid,$organisationid,$appointmentdate,$appointmenttime);
		
		if($rec>0){
			echo "This Time Slot is Already Booked ";
		}
		else{
		     $bookingid = $this->healthpackage_model->lastbookid();
	
				$bookid=$bookingid[0]->id;

				$bookingId='bkg'.(1000000+$bookid+1);
			;
			 $bookinginfo=array('booking_id'=>$bookingId,'user_id'=>$userId,'user_name'=>$userName,'user_email'=>$userEmail,'user_mobile'=>$userMobile,'hospital_id'=>$organisationid,'hospital_name'=>$organisation,'cost'=>$cost,'discount'=>$discount,'cost_after_discount'=>$costafterdiscount,'package_id'=>$packageid,'package_name'=>$packagename,'appointment_date'=>$appointmentdate,'appointment_time'=>$appointmenttime);
			 
			
			  $this->load->model('healthpackage_model');
                $result = $this->healthpackage_model->addNewBooking($bookinginfo);
                
                if($result > 0)
                {
                    echo " Booking Successfully Completed";
                }
                else
                {
                    echo "There is some Problem in Booking";
                }
			 
			 }
		//redirect("health/");
		
	}
	
	
	
	

    }

?>
