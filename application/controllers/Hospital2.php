<?php

        if(!defined('BASEPATH')) exit('No direct script access allowed');

		require APPPATH . '/libraries/BaseController.php';

		/**
		* Class : Hospital (HospitalController)
		* Hospital Class to control all operations.
		**/
		class Hospital extends BaseController
		{
			
		/**
		* This is default constructor of the class
		*/
		public function __construct()
		{
        parent::__construct();
        $this->load->model('hospital_model');
        $this->isLoggedIn();   
		}
    
		/**
		* This function used to load the first screen of the user
		*/
		public function index()
		{
		
        $this->global['pageTitle'] = 'Mediwheel : Dashboard';
        
        $this->loadViews("hospitalListing", $this->global, NULL , NULL);
		}
    
		/**
		* This function is used to load the mastercode list
		*/
		function hospitalListing()
		{
       
               
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->hospital_model->hospitalListingCount($searchText);

			$returns = $this->paginationCompress ( "admin/hospitalListing/", $count, 15,3 );
            
            $data['hospitalRecords'] = $this->hospital_model->hospitalListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
            $this->loadViews("hospitalListing", $this->global, $data, NULL);
        }


		/**
		* This function is used to load the add new form FOR MASTERCODE
		*/
	 
		function addNew()
		{
			
			
        if($this->isAdmin() == TRUE)
        {
			
            $this->loadThis();
        }
        else
        {
            $this->load->model('hospital_model');
            //$data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Mediwheel : Add New Hospital';

            $this->loadViews("addNewHospital", $this->global,  NULL);
        }
		}

    
   
    
		/**
		* This function is used to add new mastercode to the system
		*/
		function addNewHospital()
		{
	
        if($this->isAdmin() == TRUE)
        {
			
            $this->loadThis();
        }
        else
        {
		
            $this->load->library('form_validation');
            
           $this->form_validation->set_rules('organisation','organisation','trim|required|max_length[128]');
           // $this->form_validation->set_rules('type','type','Type|required');
          ///  $this->form_validation->set_rules('address','address','required');
			//$this->form_validation->set_rules('location','location','required');
			///$this->form_validation->set_rules('state','state','required');
			///$this->form_validation->set_rules('district','district','required');
			///$this->form_validation->set_rules('city','city','required');
			
			//$this->form_validation->set_rules('contact','contact','required|min_length[10]');
			//$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
			//$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
           
        
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $organisation = ucwords(strtolower($this->security->xss_clean($this->input->post('organisation'))));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $type1 = $this->input->post('type');
				
                 $location = $this->security->xss_clean($this->input->post('location'));
				 $address = $this->security->xss_clean($this->input->post('address'));
				 $district = $this->input->post('district');
				 $city = $this->input->post('city');
			     $state = $this->input->post('state');
				 $nabh=$this->input->post('nabh');
				 $nabl=$this->input->post('nabl');
				
			     $pincode = $this->input->post('pincode');
				 $pincode1 = $this->input->post('pincode1');
				 $pincodevalue = $this->hospital_model->getPincode($pincode);
			if(isset($pincodevalue[0])){
			$pincode=$pincodevalue[0]->pincode;
			}
			else {
				$pincode='';
			}
				
				
				
					
					if($pincode1!=''){
						$pincodeother=$pincode1;
					}
					else{
						$pincodeother='';
						
					}
				
					
				 $contact = $this->security->xss_clean($this->input->post('contact'));
			
				 $contact=implode(',',$contact);
				   
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
				$mobile=implode(',',$mobile);
				 $landline = $this->security->xss_clean($this->input->post('landline'));
				$landline=implode(',',$landline);
                $email = $this->security->xss_clean($this->input->post('email'));
				$email=implode(',',$email);
				 $this->load->model('hospital_model');
                $resultid = $this->hospital_model->lastid();
				//print_r($resultid );
				
				if($type1=='1'){
				$type='CH';
				$docid = $this->hospital_model->lastmastercodeid($type);
	
				$id=$docid[0]->id;

				$mastercode='CH'.(1000000+$id+1);
			
}

				if($type1=='2'){
	
				$type='DIA';
				$docid = $this->hospital_model->lastmastercodeid($type);
	
				$id=$docid[0]->id;
				$mastercode='DIA'.(1000000+$id+1);
				}
				if($type1=='3'){
				$type='HA';
				$docid = $this->hospital_model->lastmastercodeid($type);
	
				$id=$docid[0]->id;
				$mastercode='HA'.(1000000+$id+1);
				}
				if($type1=='4'){
				$type='CP';
				$docid = $this->hospital_model->lastmastercodeid($type);
	
				$id=$docid[0]->id;
	
				$mastercode='CP'.(1000000+$id+1);
				}
				if($type1=='5'){
				$type='MP';
				$docid = $this->hospital_model->lastmastercodeid($type);
	
				$id=$docid[0]->id;
				$mastercode='MP'.(1000000+$id+1);
				}
				if($type1=='6'){
				
				$type='OT';
				$docid = $this->hospital_model->lastmastercodeid($type);
	
				$id=$docid[0]->id;
				$mastercode='OT'.(1000000+$id+1);
				}
		
			
                $hospitalInfo = array('mastercode'=>$mastercode,'organisation'=>$organisation, 'type'=>$type1,'nabh_accredited'=>$nabh,'nabl_accredited'=>$nabl,'location'=>$location, 'address'=> $address,
                                    'district'=>$district,'city'=>$city,'state'=>$state, 'pincode'=>$pincode,'pincodeother'=>$pincode1, 'contactname'=>$contact,'landline'=>$landline,'mobile'=>$mobile,'email'=>$email);
									 

                $this->load->model('hospital_model');
                $result = $this->hospital_model->addNewHospital($hospitalInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Hospital created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('admin/hospitalListing');
				
				}
				}
		}
    
				/**
				* This function is used load mastercode edit information
				* @param number $userId : Optional : This is user id
				*/
				function editOldHospital($id = NULL)
				{

        
            if($id == NULL)
            {
			
                redirect('admin/hospitalListing');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['hospitalInfo'] = $this->hospital_model->getHospitalInfo($id);
            
            $this->global['pageTitle'] = 'Mediwheel : Edit Hospital';
            
            $this->loadViews("editOldHospital", $this->global, $data, NULL);
       
			}
	
	
			function editHospitalDoctor($id = NULL)
			{

        
            if($id == NULL)
            {
			
                redirect('admin/hospitalDoctorsListing');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['hospitalDoctorInfo'] = $this->hospital_model->getHospitalDoctorInfo($id);
            
            $this->global['pageTitle'] = 'Mediwheel : Edit Hospital';
            
            $this->loadViews("editHospitalDoctor", $this->global, $data, NULL);
       
			}
	
	
			function editMedicalPractitionerDoctor($id = NULL)
			{

        
            if($id == NULL)
            {
			
                redirect('admin/medicalPractitioner');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['medicalPractitionerInfo'] = $this->hospital_model->getHospitalDoctorInfo($id);
            
            $this->global['pageTitle'] = 'Mediwheel : Edit Hospital';
            
            $this->loadViews("editMedicalPractitionerDoctor", $this->global, $data, NULL);
       
			}
	
	
    
    
			/**
			* This function is used to edit the mastercode information
			*/
			function editHospital($id = null)
			{ 
 
        
            $this->load->library('form_validation');
             
             

            $this->form_validation->set_rules('organisation','organisation','trim|required|max_length[128]');
            //$this->form_validation->set_rules('type','type','Type|required');
            //$this->form_validation->set_rules('address','address','required');
			//$this->form_validation->set_rules('location','location','required');
			//$this->form_validation->set_rules('state','state','required');
			//$this->form_validation->set_rules('district','district','required');
			//$this->form_validation->set_rules('city','city','required');
			
			//$this->form_validation->set_rules('contact','contact','required|min_length[10]');
			//$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
			//$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
                   if($this->form_validation->run() == FALSE)
            { 
               $this->editOldHospital($id);
           }
           else
            {
                 $organisation = ucwords(strtolower($this->security->xss_clean($this->input->post('organisation'))));
				
				
                //$email = $this->security->xss_clean($this->input->post('email'));
                $type = $this->input->post('type');
				
                 $location = $this->security->xss_clean($this->input->post('location'));
				 $address = $this->security->xss_clean($this->input->post('address'));
				 $district = $this->input->post('district');
				 $city = $this->input->post('city');
			     $state = $this->input->post('state');
			     $pincode = $this->input->post('pincode');
				 $pincode1 = $this->input->post('pincode1');
				// $pincodevalue = $this->hospital_model->getPincode($pincode);
			//$pincode=$pincodevalue[0]->pincode;
			
				if($pincode!=''){
					
					$pincode=$pincode;
				}
				else{
					
					if($pincode1!=''){
						$pincode=$pincode1;
					}
				}
					
				 $contact= $this->security->xss_clean($this->input->post('contact'));
				 
				 $contact=implode(',',$contact);
				 
				 $nabh=$this->input->post('nabh');
				 
				 $nabl=$this->input->post('nabl');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
				$mobile=implode(',',$mobile);
				$landline = $this->security->xss_clean($this->input->post('landline'));
				$landline=implode(',',$landline);
                $email = $this->security->xss_clean($this->input->post('email'));
				$email=implode(',',$email);
				 $this->load->model('hospital_model');
                $resultid = $this->hospital_model->lastid();
                $hospitalInfo = array();
              
                $hospitalInfo = array('organisation'=>$organisation, 'type'=>$type,'nabh_accredited'=>$nabh,'nabl_accredited'=>$nabl,'location'=>$location, 'address'=> $address,
                                    'district'=>$district,'city'=>$city,'state'=>$state, 'pincode'=>$pincode, 'contactname'=>$contact,'landline'=>$landline,'mobile'=>$mobile,'email'=>$email);

                
                $result = $this->hospital_model->editHospital($hospitalInfo, $id);
              
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Hospital updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital updation failed');
                }
                
                redirect('admin/hospitalListing');
				}
				}
		
		
		
		
				function editHospitalDoctors($id = null)
				{ 
 
        
				$this->load->library('form_validation');
             
             
				//$this->load->library('form_validation');
            
				// $this->form_validation->set_rules('mastercode','mastercode','trim|required|max_length[128]');
				//$this->form_validation->set_rules('doctorname','doctorname','Type|required');
				// $this->form_validation->set_rules('specialization','specialization','required');
				//$this->form_validation->set_rules('qualification','qualification','required');
				//$this->form_validation->set_rules('license','license','required');
				//$this->form_validation->set_rules('experience','experience','required');
				//$this->form_validation->set_rules('charges','charges','required');
			
				//$this->form_validation->set_rules('discountoffered','discountoffered');
				////$this->form_validation->set_rules('discountdisplayed','discountdisplayed');
				//$this->form_validation->set_rules('opinionmail','opinionmail');
				///$this->form_validation->set_rules('opinionviamailcharge','opinionviamailcharge');
				//$this->form_validation->set_rules('opinionvideo','opinionvideo');
				//$this->form_validation->set_rules('opinionviavideocharge','opinionviavideocharge');
           
           
        
				// if($this->form_validation->run() == FALSE)
				//{
               //$this->addNewHospitalDoctors();
				// }
				//
				//else{
                $masterCode=$this->security->xss_clean($this->input->post('mastercode'));
		
				$doctortype=$this->input->post('doctortype');
		
                $doctorName = ucwords(strtolower($this->security->xss_clean($this->input->post('doctorname'))));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $specialization = $this->input->post('specialization');
				
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
				 $qualification = $this->security->xss_clean($this->input->post('qualification'));
				 $license = $this->input->post('license');
				 $experience = $this->input->post('experience');
			     $charges = $this->input->post('charges');
			     $discountoffered = $this->input->post('discountoffered');
				 $discountdisplayed = $this->security->xss_clean($this->input->post('discountdisplayed'));
			
				
				   
                $opinionmail = $this->security->xss_clean($this->input->post('opinionmail'));
				
				$opinionviamailcharge = $this->security->xss_clean($this->input->post('opinionviamailcharge'));
			
                $opinionvideo = $this->security->xss_clean($this->input->post('opinionvideo'));
				 $opinionviavideocharge = $this->security->xss_clean($this->input->post('opinionviavideocharge'));
				  $mCode=  substr($masterCode,0,2);
				
				 if(  $mCode=='CP'){
					 $target_dir = "uploads/hospitalDoctors/";
				 }
				 if(  $mCode=='MP'){
				
				$target_dir = "uploads/medicalPractitioners/";
				 }
				
				$target_file = $target_dir . basename($_FILES["photograph"]["name"]);
		$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES["photograph"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				}
				else {
       
				$uploadOk = 0;
				}
				}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["photograph"]["size"] > 500000) {
    
				$uploadOk = 0;
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
   
				$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
				} 
				}
				$photograph1=  $this->input->post('photograph1');
		
				 if($_FILES["photograph"]["name"]=='')
				 {
					$target_file=$photograph1; 
				
					$photopath=$target_file;	
			
					
				 
				 }
				if($_FILES["photograph"]["name"]!=''){
					
				$photopath=base_url().$target_file;		 
				
				}
			


                $doctorsInfo = array('doctorName'=>$doctorName, 'specialization'=>$specialization,'remarks'=>$remarks, 'qualification'=> $qualification,
                                    'license'=>$license,'experience'=>$experience,'consultationCharges'=>$charges, 'discountOffered'=>$discountoffered, 'discountDisplayed'=>$discountdisplayed,'opinionViaMail'=>$opinionmail,'opinionViaMailCharges'=>$opinionviamailcharge,'opinionViaVideoConferencing'=>$opinionvideo,'opinionViaVideoConferencingCharges'=>$opinionviavideocharge,'photograph'=>$photopath);
                $result = $this->hospital_model->editHospitalDoctors($doctorsInfo, $id);
              
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Hospital updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital updation failed');
                }
                if($doctortype=='hospital'){
                redirect('admin/hospitalDoctorsListing');
				
				}
				if($doctortype=='medicalpractitioner'){
                redirect('admin/medicalPractitioner');
				}
        
		
				}
		
		
		
		
		
		
		
  

				/**
				* This function is used to delete the mastercode using Id
				* @return boolean $result : TRUE / FALSE
				*/
				function deleteHospital()
				{
       
				$id = $this->input->post('id');
		
				$hospitalInfo = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteHospital($id, $hospitalInfo);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
	            /**
				* This function is used to delete the Hospital Doctors using Id
				* @return boolean $result : TRUE / FALSE
				*/
				function deleteHospitalDoctor()
				{
       
				$id = $this->input->post('id');
		
				$hospitalDoctorInfo = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteHospitalDoctor($id, $hospitalDoctorInfo);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
				
				
				
				
				/**
				* This function is used to delete the Medical Health Package using Id
				* @return boolean $result : TRUE / FALSE
				*/
				function deleteHealthPackage()
				{
       
				$id = $this->input->post('id');
		
				$healthPackage = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteHealthPackage($id, $healthPackage);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
				
				
				
				/**
				* This function is used to delete the Medical Health Test using Id
				* @return boolean $result : TRUE / FALSE
				*/
				function deleteHealthTest($id=null)
				{
       
				$id = $this->input->post('id');
		
				$healthTest = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteHealthTest($id, $healthTest);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
				
				
				
				
				
				
				
				/**
				* This function is used to delete the Medical Practitioner using Id
				* @return boolean $result : TRUE / FALSE
				*/
				function deleteMedicalPractitioner()
				{
       
				$id = $this->input->post('id');
		
				$medicalPractitionerInfo = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteMedicalPractitionerDoctor($id, $medicalPractitionerInfo);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
	
                
   
   

				/**
				* Page not found : error 404
				*/
				function pageNotFound()
				{
				$this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
				$this->loadViews("404", $this->global, NULL, NULL);
				}
	
	
				/*function for District dropdown */
	
				function district(){
	
				$id= '';
				$id= $this->input->post('state_id');

				if($id)
				{
				$data= $this->hospital_model->fetch_district($id);
				
				$str = "";
				$str = '<select class="form-control required" id="district" name="district">';
				$str.= '<option value="0">Select District</option>';
				//print_r( $data);
				for($i=0; $i<count($data); $i++)
				{
					
					$str.= '<option value='.$data[$i]->id.'>'.$data[$i]->district.'</option>';
				}
				
				$str.= "</Select>";
				
				}
				echo $str;

				}
				/*function for City dropdown */
				function city(){
		
	
				$id= '';
				$id= $this->input->post('district_id');

				if($id)
				{
				$data= $this->hospital_model->fetch_city($id);
			
				$str = "";
				
				$str = '<select class="form-control required" id="city" name="city">';
				$str.= '<option value="0">Select City</option>';
				print_r( $data);
				for($i=0; $i<count($data); $i++)
				{
					$str.= '<option value='.$data[$i]->id.'>'.$data[$i]->city.'</option>';
				}
				
				$str.= "</Select>";
				
				}
				echo $str;

				}

				/*function for pincode dropdown */
		
				function pincode(){
	
				$id= '';
				$id= $this->input->post('city_id');

				if($id)
				{
				$data= $this->hospital_model->fetch_pincode($id);
				
				$str = "";
				
				$str = '<select class="form-control required" id="pincode" name="pincode">';
				$str.= '<option value="0">Select Pincode</option>';
				//print_r( $data);
				for($i=0; $i<count($data); $i++)
				{
					$str.= '<option value='.$data[$i]->id.'>'.$data[$i]->pincode.'</option>';
				}
				
				$str.= "</Select>";
				
				}
				echo $str;

				}
		
		
		
		         /**** Function for Hospital Doctors Listing ***/


				function hospitalDoctorsListing() {
	
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				$data['searchText'] = $searchText;
            
				$this->load->library('pagination');
            
				$count = $this->hospital_model->hospitalDoctorsListingCount($searchText);

				$returns = $this->paginationCompress ( "admin/hospitalDoctorsListing/", $count, 15,3 );
            
				$data['hospitalDoctorsRecords'] = $this->hospital_model->hospitalDoctorsListing($searchText, $returns["page"], $returns["segment"]);
           
				$this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
				$this->loadViews("hospitalDoctorsListing", $this->global, $data, NULL);
	
	
				}



				/**** Function For Medical Practitioner Listing ***/


				function medicalPractitioner() {
	
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				$data['searchText'] = $searchText;
            
				$this->load->library('pagination');
            
				$count = $this->hospital_model->hospitalDoctorsListingCount($searchText);

				$returns = $this->paginationCompress ( "admin/medicalPractitioner/", $count, 15 ,3);
				
				$data['medicalPractitionerRecords'] = $this->hospital_model->medicalPractitioner($searchText, $returns["page"], $returns["segment"]);
            
				$this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
				$this->loadViews("medicalPractitioner", $this->global, $data, NULL);
	
	
				}

				
				/*** Function for adding Hospital Doctor ***/
				function addNewHospitalDoctor() {
	
	
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New Hospital';

				$this->loadViews("addNewHospitalDoctors", $this->global,  NULL);
				}
	
	
				}
				
				/*** Function for add medical practitioner Form ***/

				function addNewmedicalPractitioner() {
	
	
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New Medical Practitioner';

				$this->loadViews("addNewmedicalPractitioner", $this->global,  NULL);
				}
	
	
				}




             /*** Adding Function For Doctors  ***/
            function addNewHospitalDoctors()
            {
             if($this->isAdmin() == TRUE)
            {
            $this->loadThis();
            }
            else
            {
            //$this->load->library('form_validation');
            
            // $this->form_validation->set_rules('mastercode','mastercode','trim|required|max_length[128]');
            //$this->form_validation->set_rules('doctorname','doctorname','Type|required');
            // $this->form_validation->set_rules('specialization','specialization','required');
			//$this->form_validation->set_rules('qualification','qualification','required');
			//$this->form_validation->set_rules('license','license','required');
			//$this->form_validation->set_rules('experience','experience','required');
			//$this->form_validation->set_rules('charges','charges','required');
			
			//$this->form_validation->set_rules('discountoffered','discountoffered');
			////$this->form_validation->set_rules('discountdisplayed','discountdisplayed');
			//$this->form_validation->set_rules('opinionmail','opinionmail');
			///$this->form_validation->set_rules('opinionviamailcharge','opinionviamailcharge');
			//$this->form_validation->set_rules('opinionvideo','opinionvideo');
			//$this->form_validation->set_rules('opinionviavideocharge','opinionviavideocharge');
           
           
        
            // if($this->form_validation->run() == FALSE)
            //{
            //$this->addNewHospitalDoctors();
            // }
			  //
			//else{
            $masterCode=$this->security->xss_clean($this->input->post('mastercode'));
			
            $doctorName = ucwords(strtolower($this->security->xss_clean($this->input->post('doctorname'))));
            //$email = $this->security->xss_clean($this->input->post('email'));
             $specialization = $this->input->post('specialization');
				
             $remarks = $this->security->xss_clean($this->input->post('remarks'));
			    //   $timefrom = $this->security->xss_clean($this->input->post('timefrom'));
				     
				   //   $timeto = $this->security->xss_clean($this->input->post('timeto'));
			//$timeto=$timeto[0].':00';
					
					 // echo date("g:i a", strtotime($timeto));

					  
					  //print_r($timeto);
					//  exit;
		     $qualification = $this->security->xss_clean($this->input->post('qualification'));
		     $license = $this->input->post('license');
		     $experience = $this->input->post('experience');
			 $charges = $this->input->post('charges');
			 $discountoffered = $this->input->post('discountoffered');
		     $discountdisplayed = $this->security->xss_clean($this->input->post('discountdisplayed'));
		     $opinionmail = $this->security->xss_clean($this->input->post('opinionmail'));
				
			$opinionviamailcharge = $this->security->xss_clean($this->input->post('opinionviamailcharge'));
			
            $opinionvideo = $this->security->xss_clean($this->input->post('opinionvideo'));
			$opinionviavideocharge = $this->security->xss_clean($this->input->post('opinionviavideocharge'));
				 
				 
			$target_dir = "uploads/hospitalDoctors/";
			$target_file = $target_dir . basename($_FILES["photograph"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES["photograph"]["tmp_name"]);
			if($check !== false) {
       
			$uploadOk = 1;
			} else {
       
			$uploadOk = 0;
			}
			}
			// Check if file already exists
			if (file_exists($target_file)) {
    
			$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["photograph"]["size"] > 500000) {
    
			$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
   
			$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
   
			// if everything is ok, try to upload file
			} else {
			if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
			} 
			}
				 
				 
			$photopath=base_url().$target_file;		 
				 
		 
				 
				 
				 
				 
			
			//print_r($resultid );
			$this->load->model('hospital_model');
                $docid = $this->hospital_model->lastdocid();
			$docid= $docid[0]->id;
			$doctorid=$docid+1;
			$docSubCode=$masterCode."DOC".	$doctorid;
                $doctorsInfo = array('mastercode'=>$masterCode,'docSubCode'=>$docSubCode,'doctorName'=>$doctorName, 'specialization'=>$specialization,'remarks'=>$remarks, 'qualification'=> $qualification,
                                    'license'=>$license,'experience'=>$experience,'consultationCharges'=>$charges, 'discountOffered'=>$discountoffered, 'discountDisplayed'=>$discountdisplayed,'opinionViaMail'=>$opinionmail,'opinionViaMailCharges'=>$opinionviamailcharge,'opinionViaVideoConferencing'=>$opinionvideo,'opinionViaVideoConferencingCharges'=>$opinionviavideocharge,'photograph'=>$photopath);
									
								
                $this->load->model('hospital_model');
                $result = $this->hospital_model->addNewHospitalDoctors($doctorsInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Hospital created successfully');
					//$resultschedule = $this->hospital_model->addDoctorSchedule($doctorScheduleInfo);
					
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('admin/hospitalDoctorsListing');
				//}
            
				}

				}

                /** Function for adding new medical practitioner doctor **/
				
				
				function addNewmedicalPractitioners()
				{ 
				$this->load->library('form_validation');
				
				// $this->form_validation->set_rules('organisation','organisation','trim|required|max_length[128]');
				//$this->form_validation->set_rules('type','type','Type|required');
				// $this->form_validation->set_rules('address','address','required');
				//$this->form_validation->set_rules('location','location','required');
				//$this->form_validation->set_rules('state','state','required');
				//$this->form_validation->set_rules('district','district','required');
				//$this->form_validation->set_rules('city','city','required');
			
				//$this->form_validation->set_rules('contact','contact','required|min_length[10]');
				//$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
				//$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
           
        
				// if($this->form_validation->run() == FALSE)
				//{
               // $this->addNew();
				//}
                $masterCode=$this->security->xss_clean($this->input->post('mastercode'));
			
                $doctorName = ucwords(strtolower($this->security->xss_clean($this->input->post('doctorname'))));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $specialization = $this->input->post('specialization');
				
                 $remarks = $this->security->xss_clean($this->input->post('remarks'));
				  $qualification = $this->security->xss_clean($this->input->post('qualification'));
				   $license = $this->input->post('license');
				    $experience = $this->input->post('experience');
					 $charges = $this->input->post('charges');
					  $discountoffered = $this->input->post('discountoffered');
				   $discountdisplayed = $this->security->xss_clean($this->input->post('discountdisplayed'));
			
				
				   
                $opinionmail = $this->security->xss_clean($this->input->post('opinionmail'));
				
				 $opinionviamailcharge = $this->security->xss_clean($this->input->post('opinionviamailcharge'));
			
                $opinionvideo = $this->security->xss_clean($this->input->post('opinionvideo'));
				 $opinionviavideocharge = $this->security->xss_clean($this->input->post('opinionviavideocharge'));
				 
				 
				 $target_dir = "uploads/medicalPractitioners/";
				
				$target_file = $target_dir . basename($_FILES["photograph"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES["photograph"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				} else {
       
				$uploadOk = 0;
				}
				}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["photograph"]["size"] > 500000) {
    
				$uploadOk = 0;
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
   
				$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
				} 
				}
				 
				 
				$photopath=base_url().$target_file;		 
				 
		 
				 
				 
				 
				 
			
				//print_r($resultid );
				$this->load->model('hospital_model');
                $docid = $this->hospital_model->lastmpid();
				$docid= $docid[0]->id;
	
				$doctorid=$docid+1;
		
				$docSubCode=$masterCode."DOC".	$doctorid;
                $doctorsInfo = array('mastercode'=>$masterCode,'docSubCode'=>$docSubCode,'doctorName'=>$doctorName, 'specialization'=>$specialization,'remarks'=>$remarks, 'qualification'=> $qualification,
                                    'license'=>$license,'experience'=>$experience,'consultationCharges'=>$charges, 'discountOffered'=>$discountoffered, 'discountDisplayed'=>$discountdisplayed,'opinionViaMail'=>$opinionmail,'opinionViaMailCharges'=>$opinionviamailcharge,'opinionViaVideoConferencing'=>$opinionvideo,'opinionViaVideoConferencingCharges'=>$opinionviavideocharge,'photograph'=>$photopath);
									
								
                $this->load->model('hospital_model');
                $result = $this->hospital_model->addNewHospitalDoctors($doctorsInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Hospital created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('admin/medicalPractitioner');
            
				}
		
		
		



	
				/* function for Health Checkup Package Add Form*/


				function addhealthCheckupPackage()
	
				{
			
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New healthCheckupPackage';

				$this->loadViews("addhealthCheckupPackage", $this->global,  NULL);
				}
				}
		
		
		
		       /* function for Health Checkup Test Add Form*/


				function addHealthCheckupTest()
	
				{
					
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New healthCheckupTest';

				$this->loadViews("addHealthCheckupTest", $this->global,  NULL);
				}
				}
		
		
				/*** function for adding health checkup test **/
		
				function addHealthcheckupTests()
				{
	
				if($this->isAdmin() == TRUE)
				{
			
				$this->loadThis();
				}
				else
				{
				$this->load->library('form_validation');
            
				$this->form_validation->set_rules('testname','Test name','trim|required|max_length[128]');
				//$this->form_validation->set_rules('cost','Cost','trim|required]');
				// $this->form_validation->set_rules('discount','required|max_length[20]');
           
				// if($this->form_validation->run() == FALSE)
				//{
				//echo "ll";
                //$this->addNew();
				//}
				//else
				//{
					//$packageid=$this->security->xss_clean($this->input->post('healthpackage'));
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('testname'))));
               // $cost = $this->security->xss_clean($this->input->post('cost'));
                //$discount = $this->input->post('discount');
				$username = $this->hospital_model->getUserName($this->session->userId);
			
                $testInfo = array('name'=>$name,'createdBy'=> $username[0]->name, 'createdDate'=>date('Y-m-d H:i:s'));
            
                $this->load->model('hospital_model');
                $result = $this->hospital_model->addHealthCheckUpTest($testInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Health Test created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Health Test creation failed');
                }
                
                redirect('admin/healthCheckupTest');
				//}
				}
				}
		
		
		
		
		
		
		
		        /*** function for adding health checkup package **/
		
				function addHealthcheckuppackages()
				{
	
				if($this->isAdmin() == TRUE)
				{
			
				$this->loadThis();
				}
				else
				{
				$this->load->library('form_validation');
            
				$this->form_validation->set_rules('name','Package name','trim|required|max_length[128]');
				//$this->form_validation->set_rules('cost','Cost','trim|required]');
				// $this->form_validation->set_rules('discount','required|max_length[20]');
           
				// if($this->form_validation->run() == FALSE)
				//{
				//echo "ll";
                //$this->addNew();
				//}
				//else
				//{
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('packagename'))));
                $cost = $this->security->xss_clean($this->input->post('cost'));
                $discount = $this->input->post('discount');
				    $testids = $this->input->post('test');
				
				$testids=implode(',',$testids);
		
				$description= $this->input->post('description');
				
				
				$username = $this->hospital_model->getUserName($this->session->userId);
			
                $packageInfo = array('name'=>$name,'tests'=>$testids, 'cost'=>$cost,'discount'=>$discount,'description'=>$description,'createdBy'=> $username[0]->name, 'createdDate'=>date('Y-m-d H:i:s'));
            
                $this->load->model('hospital_model');
                $result = $this->hospital_model->addHealthCheckUpPackage($packageInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Health Package created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Health Package creation failed');
                }
                
                redirect('admin/healthChekupPackageListing');
				//}
				}
				}
		
		
		
		 
				/*** Function for health checkup package lsting ***/
				function healthChekupPackageListing()
				{
			
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{   
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				$data['searchText'] = $searchText;
            
				$this->load->library('pagination');
            
				$count = $this->hospital_model->healthCheckupPackageListingCount($searchText);

				$returns = $this->paginationCompress ( "admin/healthCheckupPackageListing/", $count, 15,3 );
            
				$data['healthPackageRecords'] = $this->hospital_model->healthCheckupPackageListing($searchText, $returns["page"], $returns["segment"]);
            
				$this->global['pageTitle'] = 'Mediwheel : healthcheckup package Listing';
            
				$this->loadViews("healthCheckupPackageListing", $this->global, $data, NULL);
				}
				}
				
				
				
				
				
				
				
				 /*** Function for health checkup Tests lsting ***/
				function healthCheckupTest()
				{
			
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{   
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				$data['searchText'] = $searchText;
            
				$this->load->library('pagination');
            
				$count = $this->hospital_model->healthCheckupTetsCount($searchText);

				$returns = $this->paginationCompress ( "admin/healthChekupTest/", $count, 15,3 );
            
				$data['healthTestRecords'] = $this->hospital_model->healthChekupTest($searchText, $returns["page"], $returns["segment"]);
            
				$this->global['pageTitle'] = 'Mediwheel : healthcheckup Test Listing';
            
				$this->loadViews("healthCheckupTest", $this->global, $data, NULL);
				}
				}
				
				
				
		
		
		
				/**
				* This function is used to open edit form for  HealthCheckup Package information
				*/
				function editHealthChekupPackage($id=null)
				{
		
    
				if($id == null)
				{
                redirect('admin/healthCheckupPackageListing');
				}
				else {
				//$data['roles'] = $this->user_model->getUserRoles();
				$data['healthCheckupInfo'] = $this->hospital_model->getHealthCheckupInfo($id);
		
        
				$this->global['pageTitle'] = 'Mediwheel : Edit HealthCheckup';
            
				$this->loadViews("editHealthChekupPackage", $this->global, $data, NULL);
				}
		
		
				}
				
				
				
				/**
				* This function is used to open edit form for  HealthCheckup Package information
				*/
				function editHealthChekupTest($id=null)
				{
		
    
				if($id == null)
				{
				
                redirect('admin/healthCheckupTest');
				}
				else {
				
				//$data['roles'] = $this->user_model->getUserRoles();
				$data['healthCheckupTestInfo'] = $this->hospital_model->getHealthCheckupTestInfo($id);
           
				$this->global['pageTitle'] = 'Mediwheel : Edit HealthCheckupTest';
            
				$this->loadViews("editHealthChekupTest", $this->global, $data, NULL);
				}
		
		
				}
		
				
				
				
				
				function editHealthPackages( $id=null){
				
					
					
					
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->library('form_validation');
            
				$id = $this->input->post('id');
				$description=$this->input->post('descreption');
			
				$this->form_validation->set_rules('packagename','package name','trim|required|max_length[128]');
           
				// $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
				// $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
				// $this->form_validation->set_rules('role','Role','trim|required|numeric');
				$this->form_validation->set_rules('cost','cost','required');
				$this->form_validation->set_rules('discount','discount','required');
				if($this->form_validation->run() == FALSE)
				{
                $this->editHealthChekupPackage($id);
				}
				else
				{
                $packagename = ucwords(strtolower($this->security->xss_clean($this->input->post('packagename'))));
				
                $cost = $this->security->xss_clean($this->input->post('cost'));
				  $tests = $this->security->xss_clean($this->input->post('test'));
				$tests=implode(',',  $tests );
			
                $discount = $this->input->post('discount');
              
                $healthchekupPackageInfo = array();
              
             
               
                    $healthchekupPackageInfo = array( 
                        'name'=>ucwords($packagename), 'cost'=>$cost,'tests'=>$tests,'discount'=>$discount,'description'=>$description, 
                        'updatedDate'=>date('Y-m-d H:i:s'));
               
                
                $result = $this->hospital_model->editHealthPackage($healthchekupPackageInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('admin/healthChekupPackageListing');
				}
				}
					
					
					
					
					
				}
				
				
				
				
				function editHealthChekupTests( $id=null){
				
					
					

				$this->load->library('form_validation');
            
				$id = $this->input->post('id');
           
				//$this->form_validation->set_rules('packagename','package name','trim|required|max_length[128]');
           
				$this->form_validation->set_rules('testname','name','required');
				// $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
				// $this->form_validation->set_rules('role','Role','trim|required|numeric');
          
				if($this->form_validation->run() == FALSE)
				{
				
                $this->editHealthChekupTest($id);
				}
				else
				{
		
				
                $testname = ucwords(strtolower($this->security->xss_clean($this->input->post('testname'))));
               
              
                $healthchekupTestInfo = array();
             
             
               
                    $healthchekupTestInfo = array('name'=>ucwords($testname),'updatedDate'=>date('Y-m-d H:i:s'));
               
          
                $result = $this->hospital_model->editHealthTest($healthchekupTestInfo, $id);
             
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('admin/healthCheckupTest');
				}
        
					
					
					
					
					
				}
				
				
				
				
				
				
				
				function scheduleListing($id = NULL)
				{
					
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				//$id=$this->input->post('id');

				$data['searchText'] = $searchText;
                $data['id']=$id;
			
				$subcode=$this->hospital_model->getHospitalDoctorSubcode($id);
				if(isset($subcode[0])){
				$subcode=$subcode[0]->docSubCode;
				
				}
				
				else {
				$subcode='';
				}

				$this->load->library('pagination');
            
				$count = $this->hospital_model->hospitalScheduleListingCount($searchText,$subcode);
					$returns = $this->paginationCompress ( "admin/scheduleListing/", $count, 15 ,2);
				
				$data['doctorsschedulerecords'] = $this->hospital_model->scheduleListing($searchText,$subcode, $returns["page"], $returns["segment"]);
            
				$this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
				$this->global['pageTitle'] = 'Mediwheel : Doctors Schedule';
            
				$this->loadViews("scheduleListing", $this->global, $data, NULL);
				}
		
		
				
				
				
					function addDoctorSchedule($id=null)
	
				{
					$data['id']=$id;
			
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New Doctor Schedule';

				$this->loadViews("addDoctorSchedule",$this->global, $data,  NULL);
				}
				}
				
				
				
				
				
				
				
				
				
				
				
				function addDoctorsSchedule($id=null)
				{
	
				if($this->isAdmin() == TRUE)
				{
			
				$this->loadThis();
				}
				else
				{
				$this->load->library('form_validation');
            
				//$this->form_validation->set_rules('name','Package name','trim|required|max_length[128]');
				//$this->form_validation->set_rules('cost','Cost','trim|required]');
				// $this->form_validation->set_rules('discount','required|max_length[20]');
           
				// if($this->form_validation->run() == FALSE)
				//{
				//echo "ll";
                //$this->addNew();
				//}
				//else
				//{
               // $name = ucwords(strtolower($this->security->xss_clean($this->input->post('packagename'))));
                $days = $this->security->xss_clean($this->input->post('days'));
                $timefrom = $this->input->post('timefrom');
				    $timeto = $this->input->post('timeto');
			
				//print_r($timeto);
					 $timefroms=date('g:i A',strtotime($timefrom[0]));
					 
					$type=$this->input->post('type');
					$subcode=$this->input->post('subcode');
	
				//echo $timefrom=date('g:i A',strtotime("12:00"));

				for($i=0;$i<count($days);$i++){
					
		
				$timefromnew=date('g:i A',strtotime($timefrom[$i]));
			
			
				$timetonew=date('g:i A',strtotime($timeto[$i]));
		
			
		
                $scheduleInfo = array('type'=>$type,'subcode'=>$subcode, 'day'=>$days[$i],'timefrom'=>$timefromnew,'timeto'=>$timetonew);
				
         
               $this->load->model('hospital_model');
                $result = $this->hospital_model->addScheduledoctor($scheduleInfo);
                }
				  
			
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Health Package created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Health Package creation failed');
                }
                
                redirect('admin/scheduleListing/'.$id);
				//}
				}
				}
				
				
				
				
				function deleteDoctorSchedule()
				{
      
				$id = $this->input->post('id');
		
				$hospitalDoctorScheduleInfo = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteDoctorSchedule($id, $hospitalDoctorScheduleInfo);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
				
				
				
				function deleteHealthTestParameter()
				{
       
				$id = $this->input->post('id');
		         
				$healthCheckupTestParameterInfo = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteHealthTestParameter($id, $healthCheckupTestParameterInfo);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
				
				
				
				
				
				
				function testParameter($id=null)
				{
			
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{   
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				$data['searchText'] = $searchText;
				$data['id']=$id;
				$this->load->library('pagination');
            
				$count = $this->hospital_model->healthCheckupTestParametersCount($searchText,$id);

				$returns = $this->paginationCompress ( "admin/testParameters/", $count, 15 );
            
				$data['healthTestParameterRecords'] = $this->hospital_model->healthChekupTestParameters($searchText,$returns["page"],$returns["segment"],$id);
            
				$this->global['pageTitle'] = 'Mediwheel : healthcheckup Test Parameters';
            
				$this->loadViews("healthCheckuptestParameters", $this->global, $data, NULL);
				}
				}
				
				
				
				
				
					function addHealthCheckupTestParameter($id=null)
	
				{
					$data['id']=$id;
			
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New Test Parameter';

				$this->loadViews("addHealthCheckupTestParameter",$this->global, $data,  NULL);
				}
				}
				
				
				
				
				
				
					
				function addHealthCheckupTestParameters($id=null)
				{
					
				if($this->isAdmin() == TRUE)
				{
			
				$this->loadThis();
				}
				else
				{
					
				$this->load->library('form_validation');
            
				//$this->form_validation->set_rules('parameter','parameter name','trim|required|max_length[128]');
				//$this->form_validation->set_rules('cost','Cost','trim|required]');
				// $this->form_validation->set_rules('discount','required|max_length[20]');
           
				// if($this->form_validation->run() == FALSE)
				//{
					//echo"aaaa";
					//exit;
				//echo "ll";
                //$this->addNew();
				//}
				//else
				//{
               // $name = ucwords(strtolower($this->security->xss_clean($this->input->post('packagename'))));
                $parameter = $this->security->xss_clean($this->input->post('parameter'));
				$healthtCheckupTestid=$id;
				$user=$this->session->name;
			
	
				//echo $timefrom=date('g:i A',strtotime("12:00"));

				for($i=0;$i<sizeof($parameter);$i++){
			
			
		
		
                $parameterInfo = array('parameterName'=>$parameter[$i],'testId'=>$healthtCheckupTestid,'createdBy'=>$user,'createdDate'=>date('Y-m-d H:i:s'));
	
				$this->load->model('hospital_model');
                $result = $this->hospital_model->addHealthCheckupTestParameter($parameterInfo);
                }
				  if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Health Package created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Health Package creation failed');
                } 
			
				}
                
                redirect('admin/testParameter/'.$id);
				}
				//}
				
				
				
				
					function editHealthChekupTestParameter($id=null)
				{
		
    
				if($id == null)
				{
				
					redirect('admin/testParameter');
				}
				else {
				
				//$data['roles'] = $this->user_model->getUserRoles();
				$data['healthCheckupTestParameterInfo'] = $this->hospital_model->getHealthCheckupTestParameterInfo($id);
           
				$this->global['pageTitle'] = 'Mediwheel : Edit HealthCheckupTest';
            
				$this->loadViews("editHealthChekupTestParameter", $this->global, $data, NULL);
				}
		
		
				}
				
				
				function editHealthChekupTestParameters( $id=null){
				
					
					$testid=$this->input->post('testid');

				$this->load->library('form_validation');
            
				$id = $this->input->post('id');
           
				//$this->form_validation->set_rules('packagename','package name','trim|required|max_length[128]');
           
				$this->form_validation->set_rules('parameter','parameter','required');
				// $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
				// $this->form_validation->set_rules('role','Role','trim|required|numeric');
          
				if($this->form_validation->run() == FALSE)
				{
				
                $this->editHealthChekupTestParameter($id);
				}
				else
				{
		
				
                $parameter = ucwords(strtolower($this->security->xss_clean($this->input->post('parameter'))));
               
              
                $healthchekupTestParameterInfo = array();
             
             
               
                    $healthchekupTestParameterInfo = array('parameterName'=>ucwords($parameter),'updatedDate'=>date('Y-m-d H:i:s'));
               
          
                $result = $this->hospital_model->editHealthTestParameter($healthchekupTestParameterInfo, $id);
             
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('admin/testParameter/'.$testid);
				}
        
					
					
					
					
					
				}
				
								/**** Function for Health Checkup linking Details  ****/
				
				
				
				function healthChekupPackageLinking()
				{
					
					
				$searchText = $this->security->xss_clean($this->input->post('searchText'));
				$data['searchText'] = $searchText;
            
				$this->load->library('pagination');
            
				$count = $this->hospital_model->healthPackageLinkingCount($searchText);

				$returns = $this->paginationCompress ( "admin/healthChekupPackageLinking/", $count, 10,3 );
           
				$data['healthPackageLinking'] = $this->hospital_model->healthPackageLinking($searchText, $returns["page"], $returns["segment"]);
				
				$this->global['pageTitle'] = 'Mediwheel :Health Package Linking';
            
				$this->loadViews("healthPackageLinking", $this->global, $data, NULL);
					
					
					
					
				}
				
				
				/*** Function To Load Add Form For Health Package Linking ****/
				
				
				function addHealthPackageLinking()
	
				{

				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add New Health Package Linking';

				$this->loadViews("addHealthPackageLinking",$this->global,  NULL);
				}
				}
				
				
				function facilityname(){
		
	
				$id= '';
				$id= $this->input->post('facility');

				if($id)
				{
				$data= $this->hospital_model->fetch_facility($id);
		
				$str = "";
				
				$str = '<select id="facilityname" name="facilityname[]" multiple class="form-control" >';
				
				
				for($i=0; $i<count($data); $i++)
				{
					
					$city=$this->hospital_model->getCityName($data[$i]->city);
					
					
					$str.= '<option value='.$data[$i]->id.'>'.$data[$i]->organisation.'-'.$data[$i]->location.'-'.$city[0]->city.'</option>';
				}
				
				$str.= "</Select>";
				
				}
				echo $str;

				}
                
				
				/**** functing for addning health package linking **/
				
				
				function addHealthcheckupPackageLinking()
				{
		
				if($this->isAdmin() == TRUE)
				{
			
				$this->loadThis();
				}
				else
				{
				
				$this->load->library('form_validation');
            
				//$this->form_validation->set_rules('name','Package name','trim|required|max_length[128]');
				//$this->form_validation->set_rules('cost','Cost','trim|required]');
				// $this->form_validation->set_rules('discount','required|max_length[20]');
           
				// if($this->form_validation->run() == FALSE)
				//{
				//echo "ll";
                //$this->addNew();
				//}
				//else
				//{
					
					$facilityname=$this->input->post('facilityname');
					
					
					$facility=$this->input->post('facility');
		

                $cost = $this->security->xss_clean($this->input->post('cost'));
                $memberdiscount = $this->input->post('memberdiscount');
				$memberdiscountinpercent = $this->input->post('member_discount_inpercent');
				$healthpackage = $this->input->post('healthpackage');
			
		
				$mediwheeldiscount= $this->input->post('mediwheeldiscount');
				$mediwheeldiscountinpercent = $this->input->post('mediwheel_discount_inpercent');
				$time_slot = $this->input->post('time_slot');
				$homecollection=$this->input->post('homecollection');
				if($homecollection==''){
				$homecollection=0;
				}
				$username = $this->hospital_model->getUserName($this->session->userId);
			
				for($i=0;$i<sizeof($facilityname);$i++){
                $packageInfo = array('hospital_diagonastic_id'=>$facilityname[$i],'type'=>$facility,'packageId'=> $healthpackage, 'cost'=>$cost,'memberDiscount'=>$memberdiscount,'member_discount_inpercent'=>$memberdiscountinpercent,'mediwheelDiscount'=>$mediwheeldiscount,'mediwheel_discount_inpercent'=>$mediwheeldiscountinpercent,'time_slot'=>$time_slot,'home_collection'=>$homecollection,'createdBy'=> $username[0]->name, 'createdDate'=>date('Y-m-d H:i:s'));
   
                $this->load->model('hospital_model');
                $result = $this->hospital_model->addHealthCheckUpPackageLinking($packageInfo);
				}
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Health Package created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Health Package creation failed');
                }
                
                redirect('admin/healthChekupPackageLinking');
				//}
				}
				}
		
		
				/**
				* This function is used to delete the mastercode using Id
				* @return boolean $result : TRUE / FALSE
				*/
				function deleteHealthPackageLinking()
				{
       
				$id = $this->input->post('id');
		
				$healthPackageLinkingInfo = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deletehealthPackageLinking($id, $healthPackageLinkingInfo);
           
				if ($result > 0) { 
				echo(json_encode(array('status'=>TRUE))); 
				}
				else {
					echo(json_encode(array('status'=>FALSE))); 
				}
				}
				
				
				
				/*** Function to load edit for for healthpackage linking ***/
				
				function editHealthChekupPackageLinking($id=null)
				{
		
				if($id == null)
				{
				
                redirect('admin/healthCheckupTest');
				}
				else {
				
				//$data['roles'] = $this->user_model->getUserRoles();
				$data['healthCheckupPackageLinkingInfo'] = $this->hospital_model->getHealthCheckupPackageLinkingInfo($id);
           
				$this->global['pageTitle'] = 'Mediwheel : Edit healthCheckup Package Linking ';
            
				$this->loadViews("editHealthChekupPackageLinking", $this->global, $data, NULL);
				}
		
		
				}
				
				
				
				
				
				/*** Health Checkup Package Linking Edit ****/
				
				
				
				function editHealthPackageLinking( $id=null){
				
					
			

				$this->load->library('form_validation');
            
				
			
       
		  
				//$this->form_validation->set_rules('packagename','package name','trim|required|max_length[128]');
           
				
				 $this->form_validation->set_rules('cost','cost','required');
				// $this->form_validation->set_rules('role','Role','trim|required|numeric');
				$this->form_validation->set_rules('memberdiscount','member discount','required');
				$this->form_validation->set_rules('mediwheeldiscount','mediwheel discount','required');
				if($this->form_validation->run() == FALSE)
				{
				
                $this->editHealthChekupTest($id);
				}
				else
				{
		
				
               
               
				$facilitytype=$this->security->xss_clean($this->input->post('facilitytype'));
				$facilityname=$this->security->xss_clean($this->input->post('facilityname'));
				$healthpackage=$this->security->xss_clean($this->input->post('healthpackage'));
				$cost=$this->security->xss_clean($this->input->post('cost'));
				$member_discount_inpercent=$this->security->xss_clean($this->input->post('member_discount_inpercent'));
				$memberdiscount=$this->security->xss_clean($this->input->post('memberdiscount'));
				$mediwheel_discount_inpercent=$this->security->xss_clean($this->input->post('mediwheel_discount_inpercent'));
				$mediwheeldiscount=$this->input->post('mediwheeldiscount');
				$time_slot=$this->input->post('time_slot');
				$homecollection=$this->input->post('homecollection');
                $healthchekupPackageLinkingInfo = array();
             
             
               
                    $healthchekupPackageLinkingInfo = array('packageId'=>$healthpackage,'cost'=>$cost,'memberDiscount'=>$memberdiscount,'member_discount_inpercent'=>$member_discount_inpercent,'mediwheelDiscount'=>$mediwheeldiscount,'mediwheel_discount_inpercent'=>$mediwheel_discount_inpercent,'time_slot'=>$time_slot,'home_collection'=>$homecollection,'updatedDate'=>date('Y-m-d H:i:s'));
         
          
                $result = $this->hospital_model->editHealthPackageLinking($healthchekupPackageLinkingInfo, $id);
             
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Package Linking updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Package Linking Updation failed');
                }
                
                redirect('admin/healthChekupPackageLinking');
				}
        
					
					
					
					
					
				}
				
				
				function healthPackageSchedule($id=null)
				{
					
					
					$searchText = $this->security->xss_clean($this->input->post('searchText'));
					$data['searchText'] = $searchText;
					$data['id']=$id;
					$this->load->library('pagination');
            
					$count = $this->hospital_model->healthPackageScheduleCount($searchText,$id);
          
					$returns = $this->paginationCompress ( "admin/healthChekupPackageLinking/", $count, 15 );
           
					$data['healthPackageLinkingSchedule'] = $this->hospital_model->healthPackageSchedule($searchText, $returns["page"], $returns["segment"],$id);
          
					$this->global['pageTitle'] = 'Mediwheel :Health Package Schedule';
            
					$this->loadViews("healthPackageSchedule", $this->global, $data, NULL);
					
					
					
					
				}
				
				
				
					function addHealthPackageSchedule($id=null)
	
				{
					$data['id']=$id;
			
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				//$data['roles'] = $this->user_model->getUserRoles();
            
				$this->global['pageTitle'] = 'Mediwheel : Add Health Package Schedule';

				$this->loadViews("addHealthPackageSchedule",$this->global, $data,  NULL);
				}
				}
				
				
				
				
				
				function addHealthCheckupPackageSchedule($id=null)
				{
	          
	      
				  $result = $this->hospital_model->getHealthCheckupPackageLinkingInfo($id);
				 
				
				  
				 // $type=$result[0]->type;
				 if(isset($result[0])){
				   $hospital_diaganostic=$result[0]->hospital_diagonastic_id;
				   $packageId=$result[0]->packageId;
				 }
				if($this->isAdmin() == TRUE)
				{
			
				$this->loadThis();
				}
				else
				{
				$this->load->library('form_validation');
            
				//$this->form_validation->set_rules('name','Package name','trim|required|max_length[128]');
				//$this->form_validation->set_rules('cost','Cost','trim|required]');
				// $this->form_validation->set_rules('discount','required|max_length[20]');
           
				// if($this->form_validation->run() == FALSE)
				//{
				//echo "ll";
                //$this->addNew();
				//}
				//else
				//{
               // $name = ucwords(strtolower($this->security->xss_clean($this->input->post('packagename'))));
                $days = $this->security->xss_clean($this->input->post('days'));
                $timefrom = $this->input->post('timefrom');
				    $timeto = $this->input->post('timeto');
			
				//print_r($timeto);
					 $timefroms=date('g:i A',strtotime($timefrom[0]));
					 
					$type=$this->input->post('type');
					$subcode=$this->input->post('subcode');
	
				//echo $timefrom=date('g:i A',strtotime("12:00"));

				for($i=0;$i<count($days);$i++){
					
		
				$timefromnew=date('g:i A',strtotime($timefrom[$i]));
			
			
				$timetonew=date('g:i A',strtotime($timeto[$i]));
		
			
		
                $scheduleInfo = array('linkingId'=>$id,'hospital_diaganostic'=>$hospital_diaganostic,'package'=>$packageId, 'day'=>$days[$i],'timefrom'=>$timefrom[$i],'timeto'=>$timeto[$i]);
				
         
               $this->load->model('hospital_model');
                $result = $this->hospital_model->addSchedulepackage($scheduleInfo);
                }
				  
			
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Health Package created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Health Package creation failed');
                }
                
                redirect('admin/healthPackageSchedule/'.$id);
				//}
				}
				}
				
				
				
				
				
					
				function editHealthPackageSchedule($id=null)
				{
		        
				if($id == null)
				{
				
                redirect('admin/healthPackageSchedule');
				}
				else {
				$data['id']=$id;
				//$data['roles'] = $this->user_model->getUserRoles();
				$data['healthCheckupPackageScheduleInfo'] = $this->hospital_model->getHealthCheckupPackageScheduleInfo($id);
        
				$this->global['pageTitle'] = 'Mediwheel : Edit healthCheckup Package Schedule ';
            
				$this->loadViews("editHealthPackageSchedule", $this->global, $data, NULL);
				}
		
		
				}
				
				
				
				
				
				
				
				
				function editHealthCheckupPackageSchedule( $id=null){
				
				
		
               $result = $this->hospital_model->getHealthCheckupPackageScheduleInfo($id);
			
				  
				  //$type=$result[0]->type;
				   $hospital_diaganostic=$result[0]->hospital_diagonastic_id;
				   $packageId=$result[0]->packageId;
		           $linkingId=$result[0]->linkingId;
		
               
           
			     $days=$this->input->post('days');
			
			     $timefrom=$this->input->post('timefrom');
			
			     $timeto=$this->input->post('timeto');
                 $editscheduleInfo  = array();
             
             for($i=0;$i<sizeof($days);$i++){
               
                    $editscheduleInfo  = array('day'=>$days[$i],'timefrom'=>$timefrom[$i],'timeto'=>$timeto[$i]);
				
			
			 
               
                $result = $this->hospital_model->editHealthPackageSchedule($editscheduleInfo, $id);
             }
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Package Linking updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Package Linking Updation failed');
                }
                
                redirect('admin/healthPackageSchedule/'.$linkingId);
			
        
					
					
					
					
					
				}
				
				function deleteHealthPackageSchedule()
				{
                   
				$id = $this->input->post('id');
		        
				$healthPackageScheduleInfo = array('isDeleted'=>1);
            
				$result = $this->hospital_model->deleteHealthPackageSchedule($id, $healthPackageScheduleInfo);
            
				if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
				else { echo(json_encode(array('status'=>FALSE))); }
       
				}
				
				
				
				
				function editDoctorSchedule($id=null)
				{
		        
				if($id == null)
				{
				
                redirect('admin/scheduleListing');
				}
				else {
				$data['id']=$id;
				//$data['roles'] = $this->user_model->getUserRoles();
				$data['doctorScheduleInfo'] = $this->hospital_model->getDoctorScheduleInfo($id);
        
				$this->global['pageTitle'] = 'Mediwheel : Edit Doctor Schedule ';
                 
				$this->loadViews("editDoctorSchedule", $this->global, $data, NULL);
				}
		
		
				}
				
				
				
				
				
						
				function editDoctorscheduleInfo( $id=null){
				
				
		         
              
                 
                  $subcode=$this->input->post('subcode');
				  $docInfo=$this->hospital_model->getDoctorNameBySubcode($subcode);
				  if($docInfo[0]){
				  $docid=$docInfo[0]->id;
				  }
			     $days=$this->input->post('days');
			      
			     $timefrom=$this->input->post('timefrom');
			
			     $timeto=$this->input->post('timeto');
                 $editscheduleInfo  = array();
             
               
                    $editscheduleInfo  = array('day'=>$days,'timefrom'=>$timefrom,'timeto'=>$timeto);
				
			
			 
               
                $result = $this->hospital_model->editDoctorSchedule($editscheduleInfo, $id);
             
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Package Linking updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Package Linking Updation failed');
                }
                
                redirect('admin/scheduleListing/'.$docid);
			
        
					
					
					
					
					
				}
				
				
			
 /**
	*############################################################################
	* These  all function   created by Santosh Kumar
	*
	* ########################################################################
	*/
    
		/**
	* This function is used to delete HospitalDiaganostic
	* create by Santosh Kumar
	* @return boolean $result : TRUE / FALSE
	*/
	function deleteHospitalDiaganostic($id=null)
	{
	
				
		$this->load->model('hospital_model');
		$id = $this->input->post('id');

		$hospitalDiaganostic = array('isDeleted'=>1);

		$result = $this->hospital_model->deleteHospitalDiaganostic($id, $hospitalDiaganostic);

		if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
		else { echo(json_encode(array('status'=>FALSE))); }

	} 
	
    
	
	/**
	* This function is used to update Hospital Diaganostic
	* create by Santosh Kumar
	* @return boolean $result : TRUE / FALSE
	*/			
	 public function updateHospitalDiaganostic($id=null)
	{
		
		
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			
			
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
            
            $mastercode           			  = $this->security->xss_clean($this->input->post('mastercode'));
                $registrationNo                   = $this->input->post('registrationNo');
				$imaging                 		  = $this->input->post('imaging');
				$nabl                 			  = $this->input->post('nabl');
				$lab                 			  = $this->input->post('lab');
				$digitalXray                	  = $this->input->post('xray');
				$ultrasound               	      = $this->input->post('ultrasound');
				$ecg                       		  = $this->input->post('ecg');
				$trademill                		  = $this->input->post('trademill');
				$twoDeco                 		  = $this->input->post('eco');
				$ct                 			  = $this->input->post('ct');
				$mri                 			  = $this->input->post('mri');
				$petCt	                 		  = $this->input->post('petct');
				//$timings                 		  = $this->input->post('timings');
				$receptionArea                	  = $this->input->post('receptionarea');
				$waitingArea                 	  = $this->input->post('waitingarea');
				$parkingSpace                 	  = $this->input->post('parkingspace');
				$photograph                       = $this->input->post('photograph');
				$discountOfferedTest              = $this->input->post('discountofferedtest');
				$discountDisplayedTest            = $this->input->post('discountofferedtestdisplayed');
				$discountOfferdHealthCheckup      = $this->input->post('discountofferedhealthcheckup');
				$discountDisplayedHealthCheckupe  = $this->input->post('discountofferedhealthcheckupdisplayed');
				
			/*image  section */
				
			    $filename                  = $_FILES["photograph"]["name"];	
			    $target_dir 					= "uploads/diaganostic/";
				$target_file 					= $target_dir . basename($_FILES["photograph"]["name"]);
				$uploadOk 						= 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				
		    // Check if image file is a actual image or fake image
			if(isset($_POST["submit"]))
			{
				$check = getimagesize($_FILES["photograph"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				} else {
       
				$uploadOk = 0;
				}
			}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["photograph"]["size"] > 9000000) {
    
				$uploadOk = 0;
				  $this->session->set_flashdata('error', 'File is too large!');	
				redirect('admin/editHospitalDiaganostic/'.$id);	
				}
				// Allow certain file formats
				
				if($filename !="")
				{
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" )
				{
   
				$uploadOk = 0;
				$this->session->set_flashdata('error', 'Invalid  File Type');	
			    redirect('admin/editHospitalDiaganostic/'.$id);	
				}
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
				} 
				}
               
           		/* end image  section */
				if($imaging =='0')
				 {
				 $nabl  ='0';
                 $lab  ='0';
				 $digitalXray  ='0';
				 $ultrasound  ='0';
				 $ecg  ='0';
				 $trademill  ='0';
				 $twoDeco  ='0';
				 $ct  ='0';
				 $mri  ='0';
				 $petCt  ='0';
				 }
				
                $photopath=base_url().$target_file;	
				
				if($filename=="")
				{
				echo  $photopath = $this->input->post('hidphotograph');
				
                }
				
                $hospitalDiaganosticInfo = array(
				'mastercode'=>             $mastercode,
				//'diaganosticSubCode'=>     $diaganosticSubCode,
				'registrationNo'=>         $registrationNo,
				'imaging'=>                $imaging,
				'nabl'=>                   $nabl,
				'lab'=>                    $lab,
				'digitalXray'=>            $digitalXray,
				'ultrasound'=>             $ultrasound,
				'ecg'=>                    $ecg,
				'trademill'=>              $trademill,
				'twoDeco'=>                $twoDeco,
				'ct'=>                     $ct,
				'mri'=>                    $mri,
				'petCt'=>                  $petCt,
				//'daysOperational'=>        $daysOperational,
				//'timings'=>                $timings,
				'receptionArea'=>          $receptionArea,
				'waitingArea'=>            $waitingArea,
				'parkingSpace'=>           $parkingSpace,
				'photograph'=>             $photopath,
				'discountOfferedTest'=>    $discountOfferedTest,
				'discountDisplayedTest'=>  $discountDisplayedTest,
				'discountOfferdHealthCheckup'=>$discountOfferdHealthCheckup,
				'discountDisplayedHealthCheckupe'=>$discountDisplayedHealthCheckupe ,
				);
              
                $result = $this->hospital_model->updateHospitalDiaganostic($hospitalDiaganosticInfo, $id);
			 
                
                if($result == true)
                {
                   $this->session->set_flashdata('success', 'Hospital Diaganostic Updated successfully.');
				   
				    redirect('admin/editHospitalDiaganostic/'.$id);	
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital Diaganostic  updated failed');
                }
                
               //  redirect('admin/hospitalDiaganosticListing');
			  
			   redirect('admin/editHospitalDiaganostic/'.$id);		
			  
			 
        }
        
				
	}
	
	/**
	* This function is used for adding new hospital diaganostic center
	*created by Santosh 25 Aug 2018
	*/

		function addNewHospitalDiaganostics()
			{
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				
                $masterCode 					= $this->security->xss_clean($this->input->post('mastercode'));
			
                $registrationNo				    = $this->security->xss_clean($this->input->post('registrationNo'));
                $imaging 							= $this->security->xss_clean($this->input->post('imaging'));
                $nabl 							= $this->input->post('nabl');
				$lab 							= $this->security->xss_clean($this->input->post('lab'));
				$xray 							= $this->security->xss_clean($this->input->post('xray'));
				$ultrasound 					= $this->input->post('ultrasound');
				$ecg 							= $this->input->post('ecg');
				$trademill 						= $this->input->post('trademill');
				$eco 							= $this->input->post('eco');
				$ct 							= $this->security->xss_clean($this->input->post('ct'));
			    $mri 							= $this->security->xss_clean($this->input->post('mri'));
				$petct 							= $this->security->xss_clean($this->input->post('petct'));
				$receptionarea 					= $this->security->xss_clean($this->input->post('receptionarea'));
				$waitingarea 					= $this->security->xss_clean($this->input->post('waitingarea'));
				$homecollection 				= $this->security->xss_clean($this->input->post('homecollection'));
				$parkingspace 					= $this->security->xss_clean($this->input->post('parkingspace'));
				$discountofferedtest 			= $this->security->xss_clean($this->input->post('discountofferedtest'));
				  $discountofferedtestdisplayed = $this->security->xss_clean($this->input->post('discountofferedtestdisplayed'));
				 $discountofferedhealthcheckup   = $this->security->xss_clean($this->input->post('discountofferedhealthcheckup'));
			   $discountofferedhealthcheckupdisplayed = $this->security->xss_clean($this->input->post('discountofferedhealthcheckupdisplayed'));
			   
				$target_dir 					= "uploads/diaganostic/";
				$target_file 					= $target_dir . basename($_FILES["photograph"]["name"]);
				$uploadOk 						= 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				// Check if image file is a actual image or fake image
			if(isset($_POST["submit"]))
			{
				$check = getimagesize($_FILES["photograph"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				} else {
       
				$uploadOk = 0;
				}
			}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["photograph"]["size"] > 9000000) {
    
			    $this->session->set_flashdata('error', 'File is too large!');
				$uploadOk = 0;	
				redirect('admin/addNewHospitalDiaganosticform');	
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
   
				$this->session->set_flashdata('error', 'Invalid  file type!');
				$uploadOk = 0;	
				redirect('admin/addNewHospitalDiaganosticform');
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
				} 
				}
				 
				 if($imaging =='0')
				 {
				 $nabl  ='0';
                 $lab  ='0';
				 $xray  ='0';
				 $ultrasound  ='0';
				 $ecg  ='0';
				 $trademill  ='0';
				 $eco  ='0';
				 $ct  ='0';
				 $mri  ='0';
				 $petCt  ='0';
				 }
				 
				 
				$photopath=base_url().$target_file;		 
				$this->load->model('hospital_model');
                $lastdiaid = $this->hospital_model->lastdiaid();
				$lastdiaid= $lastdiaid[0]->id;
				$lastdiaid=$lastdiaid+1;
				
				$diaSubCode=$masterCode."DIA".	$lastdiaid;
				$diaganosticsInfo = array(
				'mastercode'=>$masterCode,
				'diaganosticSubCode'=>$diaSubCode,
				'registrationNo'=>$registrationNo,
				'imaging'=>$imaging,
				'nabl'=>$nabl,
				'lab'=>$lab,
				'digitalXray'=> $xray,
				'ultrasound'=>$ultrasound,
                 'ecg'=>$ecg,
				 'trademill'=>$trademill,
				 'twoDeco'=>$eco,
				 'ct'=>$ct,
				 'mri'=>$mri,
				 'petct'=>$petct,
				 'receptionArea'=>$receptionarea,
				 'waitingArea'=>$waitingarea,
				 'homecollection'=>$homecollection,
				 'parkingSpace'=>$parkingspace,
				 'discountofferedtest'=>$discountofferedtest,
				 'discountDisplayedTest'=>$discountofferedtestdisplayed,
				 'discountOfferdHealthCheckup'=>$discountofferedhealthcheckup,
				 'discountDisplayedHealthCheckupe'=>$discountofferedhealthcheckupdisplayed,
				  'photograph'=>$photopath
				 );
				$this->load->model('hospital_model');
                $result = $this->hospital_model->addNewHospitalDiaganostics($diaganosticsInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Hospital Diaganostics created successfully');
					redirect('admin/addNewHospitalDiaganosticform');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital Diaganostics creation failed');
                }
                
               // redirect('admin/hospitalDiaganosticListing');
				//}
            
				}

		    }
		
    /**
	* This function is used for edit Hospital Diaganostic
	*created by Santosh 25 Aug 2018
	*/
	
	function editHospitalDiaganostic($id=null)
	{
	
	  $this->load->library('form_validation');
	

		if($id == null)
		{
			redirect('admin/healthCheckupPackageListing');
		}
		else {
		$data['HospitalDiaganosticInfo'] = $this->hospital_model->getHospitalDiaganosticInfo($id);
		
		$this->global['pageTitle'] = 'Mediwheel : Edit Hospital Diaganostic';
		
		$this->loadViews("editHospitalDiaganostic", $this->global, $data, NULL);
		}
	
	
	}
		
		
	
	/**
	* This function is used for adding new diaganostic center form
	*created by Santosh 18 Aug 2018
	*/

	function addNewHospitalDiaganosticform() 
	{


		if($this->isAdmin() == TRUE)
		{
		$this->loadThis();
		}
		else
		{
		$this->load->model('hospital_model');
		//$data['roles'] = $this->user_model->getUserRoles();
	
		$this->global['pageTitle'] = 'Mediwheel : Add New Hospital Diaganostic';

		$this->loadViews("addNewHospitalDiaganostic", $this->global,  NULL);
		}


	}
	
	
	
	/**
	* This function is used for Hospital Diaganostic Listing 
	*created by Santosh 18 Aug 2018
	*/
			
	function hospitalDiaganosticListing()
	{
  
		$searchText = $this->security->xss_clean($this->input->post('searchText'));
		$data['searchText'] = $searchText;

		$this->load->library('pagination');

		$count = $this->hospital_model->hospitaldiagnosticListingCount($searchText);

		$returns = $this->paginationCompress ( "admin/hospitalDiaganosticListing/", $count, 15,3 );

		$data['hospitalDiaganosticRecords'] = $this->hospital_model->hospitalDiaganosticListing($searchText, $returns["page"], $returns["segment"]);

		$this->global['pageTitle'] = 'Mediwheel : Hospital Listing';

		$this->loadViews("hospitalDiaganosticListing", $this->global, $data, NULL);
    }
	
	

    
	/**
	* This function is used to hospital Chemist ShopsListing
	*created by santosh 18 Aug 2018
	*/
					
	public function hospitalChemistShopsListing()
	{
			
		$searchText = $this->security->xss_clean($this->input->post('searchText'));
		$data['searchText'] = $searchText;

		$this->load->library('pagination');

		$count = $this->hospital_model->hospitalChemistShopCount($searchText);

		$returns = $this->paginationCompress ("hospitalChemistShopsListing/", $count, 15 ,3);

		$data['hospitalDoctorsRecords'] = $this->hospital_model->hospitalChemistShopsListing($searchText, $returns["page"], $returns["segment"]);

		$this->global['pageTitle'] = 'Mediwheel : Hospital Chemist Listing';

		$this->loadViews("hospitalChemistShopListing", $this->global, $data, NULL);
	}
		
		
	/**
	* This function is used to open form for add a shop to the system
	*created by santosh 28 Aug 2018
	*/
		
		function addNewChemistShop()
		{
	
	
				if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				else
				{
				$this->load->model('hospital_model');
				
				$this->global['pageTitle'] = 'Mediwheel : Add New Hospital';

				$this->loadViews("addNewChemistShop", $this->global,  NULL);
				}
	
	
		}


    	/**
		* This function is used to open form for add a shop to the system
		*created by santosh 28 Aug 2018
		*/
        function addHospitalChemistShop()
        {
				 if($this->isAdmin() == TRUE)
				{
				$this->loadThis();
				}
				 else
				{

					$masterCode					   = $this->security->xss_clean($this->input->post('mastercode'));
					
					$registrationn_no			   = $this->security->xss_clean($this->input->post('registrationn_no'));
				   	 $days 						   = $this->security->xss_clean($this->input->post('days'));
					 $offeron_ayurvedic_medicines  = $this->security->xss_clean($this->input->post('offeron_ayurvedic_medicines'));
					 $displayon_ayurvedic_medicines = $this->input->post('displayon_ayurvedic_medicines');
					 $home_delivery 					= $this->input->post('home_delivery');
					 $offeredon_homeopathy_medicines					    = $this->input->post('offeredon_homeopathy_medicines');
					 $displayon_homeopathy_medicines 				= $this->input->post('displayon_homeopathy_medicines');
					 $offeredon_allopathy_medicines 			= $this->security->xss_clean($this->input->post('offeredon_allopathy_medicines'));
					 $displayedon_allopathy_medicines 					= $this->security->xss_clean($this->input->post('displayedon_allopathy_medicines'));
						
					$timings		    = $this->security->xss_clean($this->input->post('timings'));
					
					$offeredon_unani_medicines 					= $this->security->xss_clean($this->input->post('offeredon_unani_medicines'));
					$displayon_unani_medicines 			= $this->security->xss_clean($this->input->post('displayon_unani_medicines'));
					
					$dis_off_onconsumables 			= $this->security->xss_clean($this->input->post('dis_off_onconsumables'));
					$dis_off_onfmcg 			= $this->security->xss_clean($this->input->post('dis_off_onfmcg'));
					$dis_off_onspecific_brand 			= $this->security->xss_clean($this->input->post('dis_off_onspecific_brand'));
					$dis_tobe_diplay_onconsumables 			= $this->security->xss_clean($this->input->post('dis_tobe_diplay_onconsumables'));
					$dis_tobe_display_onemcg 			= $this->security->xss_clean($this->input->post('dis_tobe_display_onemcg'));
					$dis_tobe_display_onspecific_brand 			= $this->security->xss_clean($this->input->post('dis_tobe_display_onspecific_brand'));
						 
					$target_dir 					= "uploads/medicalPractitioners/";
					$target_file				    = $target_dir . basename($_FILES["photograph"]["name"]);
					$uploadOk = 1;
					$imageFileType    				= strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
					// Check if image file is a actual image or fake image
					if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["photograph"]["tmp_name"]);
					if($check !== false) {
			   
					$uploadOk = 1;
					} else {
			   
					$uploadOk = 0;
					}
					}
					// Check if file already exists
					if (file_exists($target_file)) {
			
					$uploadOk = 0;
					}
					// Check file size
					if ($_FILES["photograph"]["size"] > 9000000) {
			
					$this->session->set_flashdata('error', 'File is too large!');
				    $uploadOk = 0;	
				    redirect('admin/addNewChemistShop');
					}
					// Allow certain file formats
					if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
					&& $imageFileType != "gif" ) {
		   
				    $this->session->set_flashdata('error', 'Invalid  file type!');
				    $uploadOk = 0;	
				    redirect('admin/addNewChemistShop');	
					}
					// Check if $uploadOk is set to 0 by an error
					if ($uploadOk == 0) {
		   
					// if everything is ok, try to upload file
					} else {
					if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
				
					} 
					}
						 
						 
					 $photopath=base_url().$target_file;		 
					
					   //print_r($resultid );
					
					    $this->load->model('hospital_model');
						
						$chid = $this->hospital_model->lastchid();
						$chid= $chid[0]->id;
						$chemshopid=$chid+1;
						$shopsubcode=$masterCode."CH".$chemshopid;
						$chemistshopInfo = array(
						'mastercode'=>$masterCode,
						'registrationn_no'=>$registrationn_no,
						'shopsubcode'=>$shopsubcode,
						'days'=>$days, 
						'offeron_ayurvedic_medicines'=>$offeron_ayurvedic_medicines,
						'displayon_ayurvedic_medicines'=>$displayon_ayurvedic_medicines, 
						'home_delivery'=> $home_delivery,
						'offeredon_homeopathy_medicines'=>$offeredon_homeopathy_medicines,
						'displayon_homeopathy_medicines'=>$displayon_homeopathy_medicines,
						'offeredon_allopathy_medicines'=>$offeredon_allopathy_medicines, 
						'displayedon_allopathy_medicines'=>$displayedon_allopathy_medicines,
						'timings'=>$timings,
						'offeredon_unani_medicines'=>$offeredon_unani_medicines,
						'displayon_unani_medicines'=>$displayon_unani_medicines,
						'dis_off_onconsumables'=>$dis_off_onconsumables,
						
						'dis_off_onfmcg'=>$dis_off_onfmcg,
						'dis_off_onspecific_brand'=>$dis_off_onspecific_brand,
						'dis_tobe_diplay_onconsumables'=>$dis_tobe_diplay_onconsumables,
						'dis_tobe_display_onemcg'=>$dis_tobe_display_onemcg,
						'dis_tobe_display_onspecific_brand'=>$dis_tobe_display_onspecific_brand,
						
						'photograph'=>$photopath);
											
						$this->load->model('hospital_model');
						$result = $this->hospital_model->addNewHospitalChemistShops($chemistshopInfo);
						
						if($result > 0)
						{
							$this->session->set_flashdata('success', 'New Chemist Shop Created Successfully');
						    redirect('admin/addNewChemistShop');
						}
						else
						{
							$this->session->set_flashdata('error', 'Chemist Shop creation failed');
						}
						
						redirect('admin/hospitalChemistShopsListing');
						//}
					
				}

		}
		
		
		
		public function deletehospitalChemistShop($id=null)
		{
		$this->load->model('hospital_model');
	
         $id = $this->input->post('id');
		$hospitalChemistShopinfo = array('isDeleted'=>1);

		$result = $this->hospital_model->deletehospitalChemistShop($id ,$hospitalChemistShopinfo);

		if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
		else { echo(json_encode(array('status'=>FALSE))); }
			
		}
		
		
		
	/**
	* This function is used to edit Hospital Chemist Shop
	*created by santosh 18 Aug 2018
	*/

	function edithospitalChemistShop($id=null)
	{
	
	  $this->load->library('form_validation');
	

		if($id == null)
		{
			redirect('admin/hospitalChemistShopsListing');
		}
		else {
		$data['HospitalChemistShopInfo'] = $this->hospital_model->getHospitalChemistShopInfo($id);
		
		$this->global['pageTitle'] = 'Mediwheel : Edit Hospital Diaganostic';
		
		$this->loadViews("editHospitalChemistShop", $this->global, $data, NULL);
		}
	
	
	}



	/**
	* This function is used to update Hospital ChemistShop
	* create by Santosh Kumar
	* @return boolean $result : TRUE / FALSE
	*/			
	 public function updateHospitalChemistShop($id=null)
	{
		
		
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			
			
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
            
                $mastercode           			    = $this->security->xss_clean($this->input->post('mastercode'));
                $shopsubcode                         = $this->input->post('shopsubcode');
				$registrationn_no                 	 = $this->input->post('registrationn_no');
				$days                 			     = $this->input->post('days');
				$ayurvedic_medicines                 = $this->input->post('ayurvedic_medicines');
				$offeron_ayurvedic_medicines         = $this->input->post('offeron_ayurvedic_medicines');
				$displayon_ayurvedic_medicines       = $this->input->post('displayon_ayurvedic_medicines');
				$home_delivery                       = $this->input->post('home_delivery');
				$homeopathy_medicines                = $this->input->post('homeopathy_medicines');
				
				$offeredon_homeopathy_medicines      = $this->input->post('offeredon_homeopathy_medicines');
				$displayon_homeopathy_medicines      = $this->input->post('displayon_homeopathy_medicines');
				$allopathy_medicines                 = $this->input->post('allopathy_medicines');
				$offeredon_allopathy_medicines	     = $this->input->post('offeredon_allopathy_medicines');
				$timings                 		     = $this->input->post('timings');
				$displayedon_allopathy_medicines     = $this->input->post('displayedon_allopathy_medicines');
				$unani_medicines                 	 = $this->input->post('unani_medicines');
				$offeredon_unani_medicines           = $this->input->post('offeredon_unani_medicines');
				$displayon_unani_medicines           = $this->input->post('displayon_unani_medicines');
				$dis_off_onconsumables               = $this->input->post('dis_off_onconsumables');
				$dis_off_onfmcg                      = $this->input->post('dis_off_onfmcg');
				$dis_tobe_diplay_onconsumables       = $this->input->post('dis_tobe_diplay_onconsumables');
				$dis_off_onspecific_brand            = $this->input->post('dis_off_onspecific_brand');
				$dis_tobe_display_onemcg             = $this->input->post('dis_tobe_display_onemcg');
				$dis_tobe_display_onspecific_brand   = $this->input->post('dis_tobe_display_onspecific_brand');
				$photograph                         = $this->input->post('photograph');
				
			/*image  section */
				
			    $filename                       = $_FILES["photograph"]["name"];	
			    $target_dir 					= "uploads/chemistshop/";
				$target_file 					= $target_dir . basename($_FILES["photograph"]["name"]);
				$uploadOk 						= 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				
		    // Check if image file is a actual image or fake image
			if(isset($_POST["submit"]))
			{
				$check = getimagesize($_FILES["photograph"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				} else {
       
				$uploadOk = 0;
				}
			}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["photograph"]["size"] > 9000000) {
    
				$uploadOk = 0;
				  $this->session->set_flashdata('error', 'File is too large!');	
				redirect('admin/edithospitalChemistShop/'.$id);	
				}
				// Allow certain file formats
				
				if($filename !="")
				{
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" )
				{
   
				$uploadOk = 0;
				$this->session->set_flashdata('error', 'Invalid  File Type');	
			    redirect('admin/edithospitalChemistShop/'.$id);	
				}
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
				} 
				}
               
           		/* end image  section */
				if($ayurvedic_medicines =='0')
				 {
				 $offeron_ayurvedic_medicines  ='0';
                 $displayon_ayurvedic_medicines  ='0';
				}
				
				 if($homeopathy_medicines =='0')
				 {
				 $offeredon_homeopathy_medicines  ='0';
                 $displayon_homeopathy_medicines  ='0';
				 }
				 
				 if($allopathy_medicines =='0')
				 {
				 $offeredon_allopathy_medicines  ='0';
                 $displayedon_allopathy_medicines  ='0';
				 }
				 
				 if($unani_medicines =='0')
				 {
				 $offeredon_unani_medicines  ='0';
                 $displayon_unani_medicines  ='0';
				 }
				
                $photopath=base_url().$target_file;	
				
				if($filename=="")
				{
				 $photopath = $this->input->post('hidphotograph');
				
                }
				
                $hospitalDiaganosticInfo = array(
				'mastercode'=>                    $mastercode,
				'shopsubcode'=>                   $shopsubcode,
				'registrationn_no'=>              $registrationn_no,
				'days'=>                          $days,
				'ayurvedic_medicines'=>           $ayurvedic_medicines,
				'offeron_ayurvedic_medicines'=>   $offeron_ayurvedic_medicines,
				'displayon_ayurvedic_medicines'=> $displayon_ayurvedic_medicines,
				'home_delivery'=>                 $home_delivery,
				'homeopathy_medicines'=>          $homeopathy_medicines,
				'offeredon_homeopathy_medicines'=>$offeredon_homeopathy_medicines,
				'displayon_homeopathy_medicines'=>$displayon_homeopathy_medicines,
				'allopathy_medicines'=>           $allopathy_medicines,
				'offeredon_allopathy_medicines'=> $offeredon_allopathy_medicines,
				'displayedon_allopathy_medicines'=>$displayedon_allopathy_medicines,
				'timings'=>                       $timings,
				'unani_medicines'=>               $unani_medicines,
				'offeredon_unani_medicines'=>     $offeredon_unani_medicines,
				'displayon_unani_medicines'=>     $displayon_unani_medicines,
				'dis_off_onconsumables'=>         $dis_off_onconsumables,
				'dis_off_onfmcg'=>                $dis_off_onfmcg,
				'dis_tobe_diplay_onconsumables'=> $dis_tobe_diplay_onconsumables,
				'dis_off_onspecific_brand'=>      $dis_off_onspecific_brand,
				'dis_tobe_display_onemcg'=>       $dis_tobe_display_onemcg,
				'dis_tobe_display_onspecific_brand'=>$dis_tobe_display_onspecific_brand,
				'photograph'=>                    $photopath
				);
              
                $result = $this->hospital_model->updateHospitalChemistShop($hospitalDiaganosticInfo, $id);
			 
                
                if($result == true)
                {
                   $this->session->set_flashdata('success', 'Hospital Chemist Shop Updated successfully.');
				   
				    redirect('admin/edithospitalChemistShop/'.$id);	
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital Chemist Shop updated failed');
                }
                
               //  redirect('admin/hospitalDiaganosticListing');
			  
			   redirect('admin/edithospitalChemistShop/'.$id);		
			  
			 
        }
        
				
	}



			

	
	
}
		





