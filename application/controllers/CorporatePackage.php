<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

/**
 * Class : CorporatePackage (CorporatePackageController)
 * customer Class to control all operations.
 *created by Deepak 27 September 2018
 */
class CorporatePackage extends BaseController
{
    /*** This is default constructor of the class*/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Corporatepackage_model');
        $this->load->helper('url');
        $this->load->library('encryption');
        $this->load->library('session');
        $this->isLoggedIn();
    }

    /*** This function used to load the first*/
    public function index()
    {
        $this->global['pageTitle'] = 'MediWheel : Corporate Packages';
    }

    /*** This function is used to load the corporate client form*/
    function addCorporateClient($clientId = '')
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if (!empty($clientId) && $clientId != '' && $clientId > 0) {
                $this->global['pageTitle'] = 'Mediwheel : Edit Corporate Client User';
                $data['clientDetails'] = $this->Corporatepackage_model->getBuyer('id', $clientId);
            } else {
                $this->global['pageTitle'] = 'Mediwheel : Add Corporate Client User';
                $data['clientDetails'] = array('id' => '', 'name' => '', 'email' => '', 'mobile' => '');
            }
            $this->loadViews("admin/addCorporateClient", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to add new client & update client to the database
     */
    function addCorporateClientPostData()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Full Name', 'trim|required|max_length[128]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|min_length[10]');
            if ($this->form_validation->run() == 1) {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('name'))));
                $email = $this->security->xss_clean($this->input->post('email'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $clientEmailCount = $this->Corporatepackage_model->checkUserEmailExist($this->input->post('email'));
                if (count($clientEmailCount) == 0 && empty($this->input->post('id'))) {
                    $result = $this->db->insert('tbl_buyers', array('mobile' => $this->input->post('mobile'), 'email' => $this->input->post('email'),
                        'name' => $this->input->post('name'), 'password' => '', 'corporate_code' => '', 'corporate_link' => md5($this->input->post('email')),
                        'client_id' => '', 'is_corporate_user' => 1, 'added_date' => date("Y-m-d H:i:s")));
                    $this->session->set_flashdata('success', 'Corporate Client Created Successfully');
                    redirect('admin/addCorporateClient');
                } else {
                    $this->db->where('id', $this->input->post('id'));
                    $query = $this->db->update('tbl_buyers', array('name' => $this->input->post('name'), 'mobile' => $this->input->post('mobile'),
                        'corporate_code' => '', 'client_id' => '', 'last_modified_date' => date("Y-m-d H:i:s")));
                    $this->session->set_flashdata('success', 'Corporate Client Updated Successfully');
                    redirect('admin/editCorporateClient/' . $this->input->post('id'));
                }
                $this->session->set_flashdata('error', 'Client Already Exist');
                redirect('admin/addCorporateClient');
            }
            $this->session->set_flashdata('error', 'Please Check For Validation');
            redirect('admin/addCorporateClient');
        }
    }

    /*** This function is used to display corporate client list*/
    function corporateClientListing($clientId = '', $page = '')
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Corporatepackage_model->corporateClientListingCount($clientId, $searchText);
            $returns = $this->paginationCompress("admin/corporateClientListing/$clientId/", $count, 5, 4);
            $data['clientRecords'] = $this->Corporatepackage_model->corporateClientListing($clientId, $searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'Mediwheel : Corporate Client Listing';
            $this->global['clientId'] = $clientId;
            $this->loadViews("admin/corporateClientList", $this->global, $data, NULL);
        }
    }

    /*** Function for assigning packages to corporate ***/
    function assignPackageToCorporate()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->global['pageTitle'] = 'Mediwheel : Assign Health Package To Corporate';
            $this->loadViews("admin/assignPackageToCorporate", $this->global, NULL);
        }
    }

    /*** Function for posting data of health packages to database  ***/
    function assignHealthPackageToCorporatePostData()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('healthpackage', 'healthpackage', 'trim|required|max_length[128]');
            $this->form_validation->set_rules('corporateclient', 'corporateclient', 'Type|required');
            $this->form_validation->set_rules('startdate', 'startdate', 'required');
            $this->form_validation->set_rules('enddate', 'enddate', 'required');
            if (is_array($this->input->post('healthpackage'))) {
                $healthPackage = $this->input->post('healthpackage');
                $packages = implode(',', $healthPackage);
            } else {
                $this->session->set_flashdata('error', 'Please Select Health Check Package');
                redirect('admin/assignHealthPackageToCorporate');
            }
            if ($this->input->post('corporateclient') == 0) {
                $this->session->set_flashdata('error', 'Please Select Corporate Client');
                redirect('admin/assignHealthPackageToCorporate');
            }

            /*echo count($healthPackage);
            echo "<pre>";
            print_r($healthPackage);
            echo "<pre>";
            print_r($this->input->post());
            die;*/


            $tmpName = explode('.', $_FILES["dataImport"]["name"]);
            $ext = end($tmpName);
            if ($_FILES['dataImport']['size'] > 0 && $ext == 'csv') {
                $filename = date('dmY') . '_' . uniqid() . '.' . $ext;
                $uploadFile = move_uploaded_file($_FILES["dataImport"]["tmp_name"], "uploads/corporate_clients/" . $filename);
                if ($uploadFile == 1) {
                    $corporateClientDataArray = $this->csvToArray(('uploads/corporate_clients/' . $filename));
                    $counter = 0;
                    foreach ($corporateClientDataArray as $corporateClientData) {
                        $buyerEmailMobileCount = $this->Corporatepackage_model->checkUserEmailExist($corporateClientData['emp_email'], $corporateClientData['emp_mobile']);
                        if (count($buyerEmailMobileCount) == 0) {
                            $this->db->insert('tbl_buyers', array('mobile' => $corporateClientData['emp_mobile'], 'email' => $corporateClientData['emp_email'],
                                'name' => $corporateClientData['emp_name'], 'password' => '', 'corporate_code' => $corporateClientData['emp_code'], 'corporate_link' => md5($corporateClientData['emp_email']),
                                'client_id' => $this->input->post('corporateclient'), 'is_corporate_user' => 1, 'added_date' => date("Y-m-d H:i:s")));
                            $last_insert_id = $this->db->insert_id();
                            foreach ($healthPackage as $package) {
                                $this->db->insert('tbl_buyer_assigned_packages', array('packageid' => $package, 'buyer_id' => $last_insert_id, 'client_id' => $this->input->post('corporateclient')));
                            }
                            $counter++;
                        }
                    }
                    if ($counter > 0) {
                        foreach ($healthPackage as $package) {
                            $this->db->insert('tbl_client_assigned_package', array('package_id' => $package, 'client_id' => $this->input->post('corporateclient'),
                                'service_start_date' => $this->input->post('startdate'), 'service_end_date' => $this->input->post('enddate'), 'assigned_by' => $this->session->userdata['userId'],
                                'status' => 1, 'assigned_date' => date("Y-m-d H:i:s")));
                            $insert_id = $this->db->insert_id();
                            $this->db->where('id', $insert_id);
                            $query = $this->db->update('tbl_client_assigned_package', array('policy_id' => 'MED0000000' . $insert_id));
                        }
                        $this->session->set_flashdata('success', 'Corporate List Assigned Successfully');
                        redirect('admin/assignHealthPackageToCorporate');
                    } else {
                        $this->session->set_flashdata('error', 'No Unique Record Found In Client List');
                        redirect('admin/assignHealthPackageToCorporate');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please Check File Size File Uploading Error');
                    redirect('admin/assignHealthPackageToCorporate');
                }
                redirect('admin/assignHealthPackageToCorporate');
            }
            $this->session->set_flashdata('error', 'Please Upload CSV File Format');
            redirect('admin/assignHealthPackageToCorporate');
        }
    }

    /*** Function for csv data converting to array  ***/
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /*** This function is used to display corporate client list*/
    function packageCorporateClientListing()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Corporatepackage_model->packageCorporateClientListingCount($searchText);
            $returns = $this->paginationCompress("admin/packageCorporateClientListing/", $count, 10, 3);
            $data['clientRecords'] = $this->Corporatepackage_model->packageCorporateClientListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'Mediwheel : Package Corporate Client Listing';
            $this->loadViews("admin/packageCorporateClientList", $this->global, $data, NULL);
        }
    }

}