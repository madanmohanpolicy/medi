<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller
{

    /*
    *  Putting all default things which are required under the constructor
    */
    function __construct()
    {

        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('Cart_model');
        $this->load->library('session');

    }


    public function index()
    {
        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {

            $this->load->model('cart_model');

            //echo "<pre>";
            //PRINT_R($_SESSION);

            //echo $qty = $this->cart_model->getQuantity(27014,6);
            //die;

            $uid = $this->session->userdata('front_id');
            $cart = '';
            //$cart = $this->cart_model->getCart($this->session->userdata('front_id'));

            $url = base_url() . "webcart/getCart/?uid=$uid";
            $url = preg_replace('/\s+/', '%20', $url);
            $response = $this->execute_webservices($url);
            $response = json_decode($response);

            //print_r($response);
            //echo "</pre>";
            $total = "0";
            if ($response->error == 1) {
                $message = "No Product Available";
                $total = "0";
            } else {
                $message = $response->message;

            }

            $data = array(
                'page_title' => 'Mediwheel- My Cart',
                'cart_data' => $message
            );
            //echo "<pre>";
            //print_r($data);
            //echo "</pre>";
            //if(isset($this->session->userdata['cart_data']))
            //{
            $data_cart_val = array(
                'cart_data' => $response->message->total_row
            );
            $this->session->set_userdata($data_cart_val);
            //}

            //echo "<pre>";
            //print_r($this->session);
            //echo "</pre>";

            $this->load->view('front_pages/includes/header', $data);
            $this->load->view('front_pages/cart', $data);
            $this->load->view('front_pages/includes/footer', $data);
        }
    }


    /**
     * This function used to add products in cart
     * It funtion calls once user click on add to cart
     * Created by Madan on 1st Sept, 2018
     * @param comments goes here
     */
    public function add_cart()
    {
        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {


            $uid = $this->session->userdata['front_id'];
            $pid = $this->input->get('pid');
            $su = $this->input->get('su');
            $city = $this->input->get('city');
            $mrp = $this->input->get('mrp');
            $pname = $this->input->get('pname');
            $packSizeLabel = $this->input->get('packSizeLabel');
            $oPrice = $this->input->get('oPrice');
            $discounted_price = $this->input->get('discounted_price');

            // Check if all entered values are correct
            if ($city != '' && $su > 0 && $oPrice > 0 && $pid > 0) {
                $message = '';

                $url = base_url() . "webcart/?pid=$pid&su=$su&uid=$uid&city=$city&mrp=$mrp&pname=$pname&packSizeLabel=$packSizeLabel&oPrice=$oPrice&discounted_price=$discounted_price";
                $url = preg_replace('/\s+/', '%20', $url);
                $response = $this->execute_webservices($url);
                $response = json_decode($response);

            }


            redirect('cart');
        }
    }


    /**
     * This function used to delete products in cart
     * this funtion gets called once user click on "remove" on cart page.
     * Created by Madan on 1st Sept, 2018
     * @param comments goes here
     */
    public function cart_rem()
    {
        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {

            $this->load->model('cart_model');
            $uid = $this->session->userdata['front_id'];
            $id = $this->input->get('id');

            // Check if all entered values are correct
            if ($uid != '' && $id > 0) {
                $message = '';
                $cartInfo = array('isDeleted' => '1');
                $this->cart_model->remCart($cartInfo, $id);
                $this->session->userdata['cart_data'] = $this->session->userdata['cart_data'] - 1;
            }
            redirect('cart');
        }
    }


    /**
     * This function used to increase products in cart
     * this funtion gets called once user click on "+" on cart page against any product.
     * Created by Madan on 1st Sept, 2018
     * @param comments goes here
     */
    public function cart_plus()
    {
        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {

            $this->load->model('cart_model');
            $uid = $this->session->userdata['front_id'];
            $id = $this->input->get('id');
            $qty = $this->input->get('qty');

            // Check if all entered values are correct
            if ($uid != '' && $id > 0) {
                $message = '';
                $qty = $qty + 1;
                $cartInfo = array('quantity' => $qty);
                $this->cart_model->changeCart($id, $cartInfo);
            }
            redirect('cart');
        }
    }


    /**
     * This function used to decrease products in cart
     * this funtion gets called once user click on "-" on cart page against any product.
     * Created by Madan on 5th Sept, 2018
     * @param comments goes here
     */
    public function cart_minus()
    {
        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {

            $this->load->model('cart_model');
            $uid = $this->session->userdata['front_id'];
            $id = $this->input->get('id');
            $qty = $this->input->get('qty');
            $qty = $this->input->get('qty');

            // Check if all entered values are correct
            if ($uid != '' && $id > 0) {
                if ($qty > 1) {
                    $message = '';
                    $qty = $qty - 1;
                    $cartInfo = array('quantity' => $qty);
                    $this->cart_model->changeCart($id, $cartInfo);
                } else {
                    $cartInfo = array('isDeleted' => '1');
                    $this->cart_model->remCart($cartInfo, $id);
                    $this->session->userdata['cart_data'] = $this->session->userdata['cart_data'] - 1;
                }
            }
            redirect('cart');
        }
    }


    /**
     * This function used to open the form to take user's address.
     * This funtion calls once user click on checkout page
     * Created by Madan on 6th Sept, 2018
     * @param comments goes here
     */
    public function checkout()
    {

        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {
            $this->load->model('cart_model');
            if ($this->input->get('new') == 'address' && !empty($this->input->get('id')) && $this->input->get('id') > 0) {
                $uid = $this->input->get('id');
                $field = 'id';
            } else {
                $uid = $this->session->userdata['front_id'];
                $field = 'buyer_id';
            }
            $user_address = $this->cart_model->getAddress($uid, $field);
            if ($this->input->get('new') == 'address' && !empty($this->input->get('id')) && $this->input->get('id') > 0) {
                $addressInfo = array();
                $addressInfo = array('id' => $user_address['message'][0]->id, 'address' => $user_address['message'][0]->address, 'landmark' => $user_address['message'][0]->landmark, 'city' => addslashes($user_address['message'][0]->city), 'pincode' => $user_address['message'][0]->pincode, 'locality' => $user_address['message'][0]->locality, 'state' => $user_address['message'][0]->state, 'customer_name' => $user_address['message'][0]->customer_name, 'customer_mobile' => $user_address['message'][0]->customer_mobile, 'address_type' => $user_address['message'][0]->address_type);
                $data = array('address' => $addressInfo, 'page_title' => 'Mediwheel - Add New Address');
            } else {
                $addressInfo = array();
                $addressInfo = array('id' => '', 'address' => '', 'landmark' => '', 'city' => '', 'pincode' => '', 'locality' => '', 'state' => '', 'customer_name' => '', 'customer_mobile' => '', 'address_type' => '');
                $data = array('address' => $addressInfo, 'page_title' => 'Mediwheel - Add New Address');
            }
            $this->load->view('front_pages/includes/header', $data);

            if ($this->input->get('rem') == 'address' && !empty($this->input->get('id')) && $this->input->get('id') > 0) {
                $this->db->delete('tbl_buyer_address', array('id' => $this->input->get('id')));
                redirect('cart/checkout');
            }

            if ($user_address['total_row'] > 0 && $this->input->get('new') != 'address') {
                $uid = $this->session->userdata('front_id');
                $cart = '';
                $url = base_url() . "webcart/getCart/?uid=$uid";
                $url = preg_replace('/\s+/', '%20', $url);
                $response = $this->execute_webservices($url);
                $response = json_decode($response);
                $total = "0";
                if ($response->error == 1) {
                    $message = "No Product Available";
                    $total = "0";
                } else {
                    $message = $response->message;
                }
                $data = array('address' => $user_address,
                    'page_title' => 'Mediwheel- Confirm Order',
                    'cart_data' => $message
                );
                $this->load->view('front_pages/checkout', $data);
            } else {
                $this->load->view('front_pages/address', $data);
            }
            $this->load->view('front_pages/includes/footer', $data);
        }
    }

    /**
     * This function used to take address input and save that as user's address.
     * This funtion calls once user click on Submit button on add new address form
     * Created by Madan on 8th Sept, 2018
     * @param comments goes here
     */
    public function saveCartAddress()
    {
        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {
            $address = $this->input->post('address');
            $landmark = $this->input->post('landmark');
            $pincode = $this->input->post('pincode');
            $locality = $this->input->post('locality');
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $customer_name = $this->input->post('customer_name');
            $customer_mobile = $this->input->post('customer_mobile');
            $address_type = $this->input->post('address_type');
            $buyer_id = $this->session->userdata['front_id'];

            if (!empty($this->input->post('id')) && $this->input->post('id') > 0) {
                $updated_date = date("Y-m-d H:i:s");
                $addressInfo = array();
                $addressInfo = array('address' => $address, 'landmark' => $landmark, 'city' => addslashes($city), 'pincode' => $pincode, 'locality' => $locality, 'state' => $state, 'customer_name' => $customer_name, 'customer_mobile' => $customer_mobile, 'address_type' => $address_type, 'last_modified_date' => $updated_date);
                $this->db->where('id', $this->input->post('id'));
                $query = $this->db->update('tbl_buyer_address', $addressInfo);
            }

            if (empty($this->input->post('id')) && $address != '' && $pincode > 0 && $state != '' && $customer_mobile != '' && $buyer_id != '') {
                $this->load->model('cart_model');
                $added_date = date("Y-m-d H:i:s");
                $addressInfo = array();
                $addressInfo = array('address' => $address, 'landmark' => $landmark, 'city' => addslashes($city), 'pincode' => $pincode, 'locality' => $locality, 'state' => $state, 'customer_name' => $customer_name, 'customer_mobile' => $customer_mobile, 'address_type' => $address_type, 'added_date' => $added_date, 'status' => '1', 'buyer_id' => $buyer_id);
                $this->cart_model->addAddress($addressInfo);
            }
            redirect('cart/checkout');
        }
    }

    /**
     * This function used to take otp input and pass to api data.
     * This funtion calls once user click on Submit button on otp form
     * Created by Deepak on 26 Sept, 2018
     * @param comments goes here
     */
    public function thanks()
    {
        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {
            $data = array('page_title' => 'Mediwheel - Thanks For Order');
            $uid = $this->session->userdata('front_id');
            $url = base_url() . "webcart/getCart/?uid=$uid";
            $url = preg_replace('/\s+/', '%20', $url);
            $response = $this->execute_webservices($url);
            $response = json_decode($response);
            $total = "0";
            if ($response->error == 1) {
                $message = "No Product Available";
                $total = "0";
            } else {
                $message = $response->message;
            }

            $data = array(
                'page_title' => 'Mediwheel- Thanks For Order',
                'cart_data' => $message
            );

            if(!empty($this->input->post('otp')) && strlen($this->input->post('otp')) == 6){
                $verifyOTPDetails = $this->verifyOTP($this->session->userdata['front_mobile'], $this->input->post('otp'));

               /*echo "<pre>";
               print_r($verifyOTPDetails);
               die;*/

                $this->db->insert('tbl_orders', array('user_id' => $this->session->userdata['front_id'], 'address_id' => 2, 'order_date' => date("Y-m-d H:i:s")));
                $this->load->view('front_pages/includes/header', $data);
                $this->load->view('front_pages/order_thanks', $data);
                $this->load->view('front_pages/includes/footer', $data);
            }

        }
    }

    /**
     * This function used to show users final page to confirm order.
     * This funtion calls once user enter the final payment page.
     * Created by Madan on 8th Sept, 2018
     * @param comments goes here
     */
    public function confirmOrder()
    {

        if (!isset($this->session->userdata['is_logged_in'])) {
            redirect("buyer/login_view");
        } else {

            $data = array('page_title' => 'Mediwheel - Confirm Order');
            $this->load->model('cart_model');

            $uid = $this->session->userdata['front_id'];

            $user_address = $this->cart_model->getAddress($uid, 'buyer_id');


            //echo "<pre>";
            //print_r($user_address);

            $uid = $this->session->userdata('front_id');

            $cart = '';
            //$cart = $this->cart_model->getCart($this->session->userdata('front_id'));

            $url = base_url() . "webcart/getCart/?uid=$uid";
            $url = preg_replace('/\s+/', '%20', $url);
            $response = $this->execute_webservices($url);
            $response = json_decode($response);

            $total = "0";
            if ($response->error == 1) {
                $message = "No Product Available";
                $total = "0";
            } else {
                $message = $response->message;

            }

            $data = array(
                'page_title' => 'Mediwheel- Confirm Order',
                'address' => $user_address,
                'cart_data' => $message
            );

            //if(isset($this->session->userdata['cart_data']))
            //{
            $data_cart_val = array(
                'cart_data' => $response->message->total_row
            );
            $this->session->set_userdata($data_cart_val);
            //}

            /*echo "<pre>";
            print_r($this->session->userdata);
            echo "</pre>";*/

            $sigUpResponse = $this->signUp1mgAfterCheckout($this->session->userdata['front_name'], $this->session->userdata['front_email'], $this->session->userdata['front_mobile'], '123456');
            /*echo "<pre>";
            print_r($sigUpResponse);
            die;*/

            if ($sigUpResponse->status == 'success') {
                $otpAfterSignUp = $this->getOTP_AfterSignUp($this->session->userdata['front_mobile']);
                if ($otpAfterSignUp->status == 'success') {
                    /*echo "<pre>";
                    print_r($otpAfterSignUp);
                    die;*/

                    $this->load->view('front_pages/includes/header', $data);
                    $this->load->view('front_pages/confirm_otp_page', $otpAfterSignUp);
                    $this->load->view('front_pages/includes/footer', $data);
                }
            }
        }
    }

    function signUp1mgAfterCheckout($name, $email, $phone, $password)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://stagapi.1mg.com/webservices/users/signup",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"email\"\r\n\r\n$email\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"name\"\r\n\r\n$name \r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"password\"\r\n\r\n$password\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"phone_number\"\r\n\r\n$phone\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"cart_oos\"\r\n\r\ntrue\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "accept: application/vnd.healthkartplus.v7+json",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "hkp-platform: HealthKartPlus-9.0.0-Android",
                "postman-token: 847fbb91-7792-da6f-df7e-e44532926a68"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response);
        }
    }

    function getOTP_AfterSignUp($phone)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://stagapi.1mg.com/webservices/users/token/create",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data;  name=\"username\"\r\n\r\n$phone\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"isRetry\"\r\n\r\ntrue\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "accept: application/vnd.healthkartplus.v7+json",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "hkp-platform: HealthKartPlus-9.0.0-Android",
                "postman-token: 847fbb91-7792-da6f-df7e-e44532926a68"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response);
        }
    }

    function verifyOTP($phone, $token)
    {
        /*$token = "581099";
        $phone = "8800695965";*/

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://stagapi.1mg.com/webservices/users/token/verify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"verification_token\"\r\n\r\n$token\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"username\"\r\n\r\n$phone\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: 
            form-data; name=\"return_token\"\r\n\r\ntrue\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "accept: application/vnd.healthkartplus.v7+json",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "hkp-platform: HealthKartPlus-9.0.0-Android",
                "postman-token: 3b99726d-3039-399b-7d64-8a33b7ee01cb",
                "x-api-key: 1mg_client_access_key"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response);
        }
    }

    /**
     * This function used to run the Curl
     * @param comments goes here
     */
    public function execute_webservices($url)
    {
        $response = '';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/vnd.healthkartplus.v7+json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        return $response;
    }


}
