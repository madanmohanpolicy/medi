<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdetails extends CI_Controller {

	/*
	*  Putting all default things which are required under the constructor
	*/
	function __construct()
	{

		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		//$this->load->model('User_model');
    

    }
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
		
		$data = array(
          'page_title' => 'Mediwheel- Your Welness Partner',
          
          );
		  
		  $otc = '';
		  $city = '';
			if($this->input->get('otc'))
			{
				$otc = $this->input->get('otc');
			}
			if($this->input->get('city'))
			{
				$city = $this->input->get('city');
			}else
		{
			$city = "Noida";
		}
		
		  $url = "https://stagapi.1mg.com/webservices/otc-details/$otc?&city=$city&locale=en";

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",  
			CURLOPT_HTTPHEADER => array(
			"accept: application/vnd.healthkartplus.v7+json",
			"cache-control: no-cache",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			"hkp-platform: HealthKartPlus-9.0.0-Android",
			"postman-token: 7b1124de-c376-60e5-8d90-80daaafd346a",
			"x-api-key: 1mg_client_access_key"
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			//echo "cURL Error #:" . $err;
			} else {
		//	echo "<pre>";
		//	print_r(json_decode($response));
			}
			
			
		$result = json_decode($response);
		/*echo "<pre>";
		
		print_r($result->otc_icons[0]->icon_url);
		print_r($result->consult_doctor->slug);
		
		
		for($i=0; $i<count($result->ad_tags); $i++)
		{
			echo $result->ad_tags[$i]->value;
			echo $result->ad_tags[$i]->key;
		}
		
		*/
		
		
		$data = array(
          'page_title' => 'Mediwheel - Product Details',
		  'final_result' => $result,
          
          );
			
		
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view('front_pages/pdetails',$data);
		$this->load->view('front_pages/includes/footer',$data);
	}
}
