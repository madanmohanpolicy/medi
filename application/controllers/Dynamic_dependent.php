<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dynamic_dependent extends CI_Controller {
 
 public function __construct()
 {
  parent::__construct();
  $this->load->model('dynamic_dependent_model');
 }

 function index()
 {
  $state = $this->db->get("state")->result();
      $this->load->view('addNewHospital', array('countries' => $countries )); 
 }

 function fetch_district()
 {
  if($this->input->post('state_id'))
  {
   echo $this->dynamic_dependent_model->fetch_state($this->input->post('state_id'));
  }
 }

 function fetch_city()
 {
  if($this->input->post('district_id'))
  {
   echo $this->dynamic_dependent_model->fetch_city($this->input->post('district_id'));
  }
 }
  
}
?>