<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Psearch extends CI_Controller {

	
	/*
	*  Putting all default things which are required under the constructor
	*/
	function __construct()
	{

		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('Home_model');
    

    }
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
		$this->load->helper('url'); 
		$this->load->model('home_model');
		//echo "<pre>";
		//print_r($_POST);
				
		
		
		$city_id = '';
		$pname = '';
		 
		if($this->input->post('city'))
		{
			$city_id = $this->input->post('city');
		}
		else
		{
			$city_id = "6260";
		}
		
		if($this->input->post('pname'))
		{
			$pname = $this->input->post('pname');
		}
		
		if($this->input->post('module'))
		{
			$module = $this->input->post('module');
		}
		
		
		$data = array(
		'search_city' => $city_id,
		'search_keyword' => $pname					
		);
		$this->session->set_userdata($data);
		
		if($module == "icon2")
		{
			redirect("health/");
		}
		else if($module == "icon3")
		{
			redirect("docbooking/");
		}
		
		
		$city_detail=$this->home_model->getCityName($city_id);
		//print_r();
		//die;
		//echo $city = $city[0]->city;
		//die;	
		//echo "<pre>";
		//echo "madan";
		//print_r($this->input->post('pname'));
		//echo "</pre>";
		
		$city = $city_detail[0]->city;
		if($pname != '')
		{
			$url = "https://stagapi.1mg.com/webservices/search-beta?name=$pname&pageSize=15&city=$city&pageNumber=0&type=product";
			
			

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",  
			CURLOPT_HTTPHEADER => array(
			"accept: application/vnd.healthkartplus.v7+json",
			"cache-control: no-cache",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			"hkp-platform: HealthKartPlus-9.0.0-Android",
			"postman-token: 7b1124de-c376-60e5-8d90-80daaafd346a",
			"x-api-key: 1mg_client_access_key"
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			//echo "cURL Error #:" . $err;
			} else {
			//echo "<pre>";
			//print_r(json_decode($response));
			}

		}
		$result = json_decode($response);
		if($result->totalRecordCount >0 )
		{
			$final_result = $result->result;
		}
		
	
		  
		// for category
		
		if($pname != '')
		{
			
			
			//for category 
			
			$url = "https://stagapi.1mg.com/webservices/catalog/category/38/widgets/paginated?city=$city&pageNumber=0&pageSiz=10&brand=crocin";
			
			//end

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",  
			CURLOPT_HTTPHEADER => array(
			"accept: application/vnd.healthkartplus.v7+json",
			"cache-control: no-cache",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			"hkp-platform: HealthKartPlus-9.0.0-Android",
			"postman-token: 7b1124de-c376-60e5-8d90-80daaafd346a",
			"x-api-key: 1mg_client_access_key"
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			//echo "cURL Error #:" . $err;
			} else {
			//echo "<pre>";
			//print_r(json_decode($response));
			}

		}
		$result = json_decode($response);
		if($result->breadcrumbs >0 )
		{
			$category_result = $result;
		}
		//echo "<pre>";
		//print_r($category_result);
		
			$data = array(
          'page_title' => 'Mediwheel - Search Your Medicine',
		  'final_result' => $final_result,
		   'category_result' => $category_result,
          
          );
		
		
		
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view('front_pages/psearch',$data);
		$this->load->view('front_pages/includes/footer',$data);
	}
}
