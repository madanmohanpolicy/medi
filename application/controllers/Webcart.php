<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Webcart extends CI_Controller
 {

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');
				$this->load->model('cart_model');

	}
	
	/**
	* This function used to create the cart
	* It funtion calls once user click on add to cart and creates cart and return true or false.
	* Created by Madan on 1st Sept, 2018
	*/	
	public function index()
	{			
		$pid= "";
		$uid= "";
		$su= "";
		$city= "";
		$mrp = "";
		$pname = "";
		$packSizeLabel = "";
		$oPrice = "";
		$discounted_price = "";
		
		//initializing the message variable
		$error = 0;
		$message = "";		
		
		// Getting the user's information 
		$uid= $this->input->get('uid');
		$pid= $this->input->get('pid');
		$su= $this->input->get('su');
		$city= $this->input->get('city');
		$mrp= $this->input->get('mrp');
		$pname= $this->input->get('pname');
		$packSizeLabel= $this->input->get('packSizeLabel');
		$oPrice= $this->input->get('oPrice');
		$discounted_price= $this->input->get('discounted_price');
		
		// Check if all entered values are correct
		if($this->check_name($city) && $su>0 && $oPrice>0 && $pid>0)
		{
			$added_date = date("Y-m-d H:i:s");			
			$cartInfo = array('	product_id'=>$pid, 'added_date'=>$added_date,'city'=>addslashes($city),'su'=>$su,'mrp'=>$mrp ,'oprice'=>$oPrice,'product_name'=>$pname,'discounted_price'=>$discounted_price,'mrp'=>$mrp,'mrp'=>$mrp,'status'=>'1','packSizeLabel'=>$packSizeLabel,'user_id'=>$uid,'quantity'=>'1');
			
			
			$getCartProduct = $this->cart_model->getQuantity($pid,$uid);
			if($getCartProduct)
			{
				$id = $getCartProduct[0]->id;
				$qty = $getCartProduct[0]->quantity;
				$qty = $qty+1;
				$cartInfo = array('quantity'=>$qty);
				$this->cart_model->changeCart($id,$cartInfo);
				$message = "Cart Added";
			}	
			else if($this->cart_model->addCart($cartInfo))
			{					
				$message = "Cart Added";
			}
			else
			{
				$message = "Error : product could not be inserted. Please try again";
				$error = 1;
			}
		}
		else
		{
			$message = "Invalid Request";
			$error = 1;
		}			
		

		/* output in necessary format */	
		header('Content-type: application/json');
		echo json_encode(array('message'=>$message, 'error'=>$error));
		/* disconnect from the db */
		
    }
	
	
	
	
	
	/**
	* This function used to get the cart details
	* this funtion calls once user click on cart.
	* Created by Madan on 4th Sept, 2018
	*/	
	public function getCart()
	{			
		// get the user id to fetch cart details
		$uid = "";
		$uid = $this->input->get('uid');
		
		$error = 0;
		$message = "";
		
		if($uid != '')
		{					
			$message = $this->cart_model->getCart($uid);
			
		}
		else
		{
			$error = 1;
			$message = "Invalid User Session";
		}	

		/* output in necessary format */	
		header('Content-type: application/json');
		echo json_encode(array('message'=>$message, 'error'=>$error));
		/* disconnect from the db */
		
    }
	
	function check_name($name)
	{		
		if(strlen($name) >= 2)
		{
			
		return true;	
		}else{
			return false;
			 }
	}
	
	
	

 }

?>
