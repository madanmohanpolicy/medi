    <?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Health extends CI_Controller
	{

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');
	}
	
	/**
	* This function used to show the health package details
	*/
	public function index()
	{
		//if($this->session->is_logged_in)
		//{
			$search_city='';
			if($this->session->search_city){
			$search_city=$this->session->search_city;
			}
			if($this->session->corporate_city){
			$search_city=$this->session->corporate_city;
			}
		
		
			//$search_city='4540';
			$this->load->model('healthpackage_model');
			$data['healthPackageRecords'] = $this->healthpackage_model->healthPackageListing($search_city);
			$data['search_city']=$search_city;
			//$this->session->userdata['is_logged_in'])
			$data['page_title'] = 'Mediwheel : Health Packages';
			$this->load->view('front_pages/includes/header',$data);		
			$this->load->view('front_pages/health',$data,NULL);	
			$this->load->view('front_pages/includes/footer',$data);
		//}
		//else
		//{
			/*$data = array(
			'front_goto_page' => "health"
			);
			$this->session->set_userdata($data);
			redirect(base_url().'buyer/login_view/');*/
		//}
	}
	  /**** Function for heathpackage booking ***/
	function booking(){
	if($this->session->is_logged_in)
		{
			$is_corporate_user=0;
			$corporatename='';
	$packagename=$this->input->get('package');
	$packageid=$this->input->get('packageId');
	$organisation=$this->input->get('organisation');
	$organisationid=$this->input->get('organisationid');
	$cost=$this->input->get('cost');
	$discount=$this->input->get('discount');
	$costafterdiscount=$this->input->get('costafterdiscount');
	$appointmentdate=$this->input->get('appointmentdate');
	$this->load->model('healthpackage_model');
	$email=$this->session->front_email;
    $resultCorpUser = $this->healthpackage_model->getCorporateUserInfoByEmail($email);
	
	if($resultCorpUser[0]->corporate_name){
		$corporatename=	$resultCorpUser[0]->corporate_name;
			
		}
		
		
		if($resultCorpUser[0]->is_corporate_user){
			$is_corporate_user=1;
			
		}
		
	$appointmenttime=$this->input->get('appointmenttime');
	$userId=$this->session->front_id;
	$userName=$this->session->front_name;
	$userEmail=$this->session->front_email;
	$userMobile=$this->session->front_mobile;
	$to=$userEmail;
	$sub="Mediwheel.in : Health Package Booking";
	
	$msg ="
Dear $userName,

Thanks for Health package booking with us. 
We will intimate you after booking confirmation

Thanks,
Mediwheel Team";
	
	$this->load->model('healthpackage_model');
	 $rec= $this->healthpackage_model->ifBooked($packageid,$organisationid,$appointmentdate,$appointmenttime);
		
	if($rec>0){
	echo "This Time Slot is Already Booked ";
	}
	else{
	$bookingid = $this->healthpackage_model->lastbookid();
	
	$bookid=$bookingid[0]->id;

	$bookingId='MWH'.(1000000+$bookid+1);

	$bookinginfo=array('booking_id'=>$bookingId,'user_id'=>$userId,'user_name'=>$userName,'user_email'=>$userEmail,'user_mobile'=>$userMobile,'hospital_id'=>$organisationid,'hospital_name'=>$organisation,'cost'=>$cost,'discount'=>$discount,'cost_after_discount'=>$costafterdiscount,'package_id'=>$packageid,'package_name'=>$packagename,'appointment_date'=>$appointmentdate,'appointment_time'=>$appointmenttime,'is_corporate_user'=>$is_corporate_user,'corporate_name'=>$corporatename);

	$this->load->model('healthpackage_model');
    $result = $this->healthpackage_model->addNewBooking($bookinginfo);
           
    if($result > 0)
                {
                    echo " Booking Successfully Completed";
					$this->sendMailAFterHealthPackageBooking($to,$sub,$msg);
                }
                else
                {
                    echo "There is some Problem in Booking";
                }
			 
			 }
		}
		
		else
		{
			$data = array(
			'front_goto_page' => "health"
			);
			$this->session->set_userdata($data);
			redirect(base_url().'buyer/login_view/');
		}
		//redirect("health/");
		
	}
	
	
	
	
	 function sendMailAFterHealthPackageBooking($to,$sub,$msg)
	{
		$this->load->library('email');
		$this->email->from('noreply@mediwheel.in', 'Mediwheel : Health Checkup Booking');		
		$this->email->to($to);
		$this->email->bcc('madan.me@gmail.com');
		$this->email->subject($sub);
		$this->email->message($msg);
		$this->email->send();
	}
	
	
	
	 /*** Function For Report Upload Form Load**/
	 function reportUpload(){
		if($this->session->is_logged_in){
		 $data['page_title'] = 'Mediwheel : Health Packages Reports';
		$this->load->view('front_pages/includes/header',$data);		
		 $this->load->view('front_pages/reportupload',$data,NULL);
		 $this->load->view('front_pages/includes/footer',$data);
		}
		else{
			 redirect(base_url().'buyer/login_view/');
		}
	 }
	 /*** Function For Report Upload Form Load**/
	/*** Function For Report Upload **/
	public function reportUploads() { 
	
	if($this->session->is_logged_in){
		
		$target_dir = "uploads/reports/";
				$today = date("F j, Y, g:i a");
				$reportname=basename($_FILES["report"]["name"]);
				
				$reportname1=explode('.',$reportname);
			$rprtname=$reportname1[0].'_'.time().'.'.$reportname1[1];
				$target_file = $target_dir .$rprtname;
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES["report"]["tmp_name"]);
				if($check !== false) {
       
				$uploadOk = 1;
				}
				else {
       
				$uploadOk = 0;
				}
				}
				// Check if file already exists
				if (file_exists($target_file)) {
    
				$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["report"]["size"] > 5000000) {
    
				$uploadOk = 0;
				}
				// Allow certain file formats
				if($imageFileType != "pdf"&& $imageFileType!="jpg" && $imageFileType!="jpeg") {
   
				$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
   
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($_FILES["report"]["tmp_name"], $target_file)) {
        
				} 
				}
				
				$reportpath=base_url().$target_file;
				
				
				$comment= $this->input->post('comment');
				$userId=$this->session->front_id;
				$userName=$this->session->front_name;
				$userEmail=$this->session->front_email;
				
				$reportInfo=array('user_id'=>$userId,'user_name'=>$userName,'user_email'=>$userEmail,'report_path'=>$reportpath,'comment'=>$comment,'report_upload_date'=>$today);
				$this->load->model('healthpackage_model');
				$result = $this->healthpackage_model->addNewReport($reportInfo);
                
				if($result > 0)
                {
              $msg=" Report Successfully Uploaded";
				 
                }
                else
                {
               $msg="There is some Problem in Report Uploading";
                }
				$this->session->set_flashdata('report_upload_success_msg', $msg);
	           redirect('health/reportUpload');
			   
	}
	
	else {
		 redirect(base_url().'buyer/login_view/');
	}
      }

		function reportView(){
			
			if($this->session->is_logged_in){
				
		$user_id=$this->session->front_id;
		$this->load->model('healthpackage_model');
	    $data['reportViewInfo'] = $this->healthpackage_model->reportInfo($user_id);
		 $data['page_title'] = 'Mediwheel : Health Packages Reports View';
		$this->load->view('front_pages/includes/header',$data);		
		$this->load->view('front_pages/reportview',$data,NULL);
		$this->load->view('front_pages/includes/footer');
	 }
		
		else{
			 redirect(base_url().'buyer/login_view/');
		}

    }
	
	

    }

?>
