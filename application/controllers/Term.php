<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Term extends CI_Controller {

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				//$this->load->model('terms');
				//$this->load->library('session');

	}
	
	/**
	* This function used to open the page for "terms and conditions".
	* Its default funtion of this controller.
	* Created By Madan on 23rd Aug
	*/
	public function index()
	{
		$data = array(
			  'page_title' => 'Mediwheel - Terms & Conditions',
			  
			  );
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view("front_pages/terms.php");
		$this->load->view('front_pages/includes/footer',$data);
	}
	
	/**
	* This function used to open the page for "Legal".	
	* Created By Madan on 23rd Aug
	*/
	public function legal()
	{
		$data = array(
			  'page_title' => 'Mediwheel - Legal',
			  
			  );
		$this->load->view('front_pages/includes/header',$data);
		$this->load->view("front_pages/legal.php");
		$this->load->view('front_pages/includes/footer',$data);
	}


}

?>
