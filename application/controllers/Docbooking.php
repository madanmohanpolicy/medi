    <?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Docbooking extends CI_Controller
	{

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');
				//$this->load->library('mail');


	}
	
	/**
	* This function used to open the foem for sigining up user.
	* Its default funtion of this controller.
	* @param comments goes here
	*/
	public function index()
	{
		$specializationid='';
		$search_city='';
		$search_city=$this->session->search_city;
		$specialisation=$this->session->search_keyword;
	  
		$specialization=trim($specialisation);
		 $this->load->model('doctor_consultation_model');
		 $this->doctor_consultation_model->doctorsListing($search_city,$specialization);
       $spcl= $this->doctor_consultation_model->getDoctorsSpecializationId($specialization);
	if($spcl){
		$specializationid= $spcl[0]->id;
	}
	
		$data['doctorsRecords'] = $this->doctor_consultation_model->doctorsListing($search_city,$specializationid);
		
		
		$data['search_city']=$search_city;
		$data['specialisationid']=$specializationid;
	
		//$this->session->userdata['is_logged_in'])
		
		$data['page_title'] = 'Mediwheel : Doctor Appointment';
		$this->load->view('front_pages/includes/header',$data);		
		$this->load->view('front_pages/doctor_booking',$data,NULL);	
		$this->load->view('front_pages/includes/footer',$data);
		}
		
		
		
		function booking(){
	if($this->session->is_logged_in)
		{
		
	$docname=$this->input->get('docname');
	$docid=$this->input->get('docid');
	$mastercode=$this->input->get('mastercode');
	$consultationCharges=$this->input->get('consultationCharges');
	$discountDisplayed=$this->input->get('discountDisplayed');
	$actualcharge=$this->input->get('actualcharge');
	$appointmentdate=$this->input->get('appointmentdate');
	$subcode=$this->input->get('subcode');
	
	$appointmenttime=$this->input->get('appointmenttime');
	$userId=$this->session->front_id;
	$userName=$this->session->front_name;
	$userEmail=$this->session->front_email;
	$userMobile=$this->session->front_mobile;
	$to=$userEmail;
	$sub="Mediwheel.in : Doctor Appointment Booking";
	
	$msg ="
Dear $userName,

Thanks for doctor appointment booking with us. 
We will intimate you after booking confirmation

Thanks,
Mediwheel Team";
	
	$this->load->model('doctor_consultation_model');
	 $rec= $this->doctor_consultation_model->ifBooked($docid,$mastercode,$appointmentdate,$appointmenttime);
		$hosname=$this->doctor_consultation_model->getHospitalName($mastercode);
		if($hosname){$hospitlName=$hosname[0]->organisation;}
	if($rec>0){
	echo "This Time Slot is Already Booked ";
	}
	else{
	$bookingid = $this->doctor_consultation_model->lastbookid();
	
	$bookid=$bookingid[0]->id;

	$bookingId='MWD'.(1000000+$bookid+1);

	$bookinginfo=array('booking_id'=>$bookingId,'user_id'=>$userId,'user_name'=>$userName,'user_email'=>$userEmail,'user_mobile'=>$userMobile,'hospital_mastercode'=>$mastercode,'hospital_name'=>$hospitlName,'cost'=>$consultationCharges,'discount'=>$discountDisplayed,'cost_after_discount'=>$actualcharge,'doctor_id'=>$docid,'doctor_name'=>$docname,'docSubCode'=>$subcode,'appointment_date'=>$appointmentdate,'appointment_time'=>$appointmenttime);

	$this->load->model('doctor_consultation_model');
    $result = $this->doctor_consultation_model->addNewBooking($bookinginfo);
           
    if($result > 0)
                {
                    echo " Booking Successfully Completed";
					$this->sendMailAFterHealthPackageBooking($to,$sub,$msg);
                }
                else
                {
                    echo "There is some Problem in Booking";
                }
			 
			 }
		}
		
		else
		{
			$data = array(
			'front_goto_page' => "Docbooking"
			);
			$this->session->set_userdata($data);
			redirect(base_url().'buyer/login_view/');
		}
		//redirect("health/");
		
	}
	
	
	
	
	 function sendMailAFterHealthPackageBooking($to,$sub,$msg)
	{
		$this->load->library('email');
		$this->email->from('noreply@mediwheel.in', 'Mediwheel : Health Checkup Booking');		
		$this->email->to($to);
		$this->email->bcc('madan.me@gmail.com');
		$this->email->subject($sub);
		$this->email->message($msg);
		$this->email->send();
	}
	
	

		
		function paginationCompress($link, $count, $perPage = 10, $segment = '3') {
		
		$this->load->library ( 'pagination' );

		$config ['base_url'] = base_url () . $link;
		$config ['total_rows'] = $count;
		$config ['uri_segment'] = $segment;
		$config ['per_page'] = $perPage;
		$config ['num_links'] = 5;
		$config ['full_tag_open'] = '<nav><ul class="pagination">';
		$config ['full_tag_close'] = '</ul></nav>';
		$config ['first_tag_open'] = '<li class="arrow">';
		$config ['first_link'] = 'First';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="arrow">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li class="arrow">';
		$config ['next_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li class="arrow">';
		$config ['last_link'] = 'Last';
		$config ['last_tag_close'] = '</li>';
	
		$this->pagination->initialize ( $config );
		$page = $config ['per_page'];
	
		$segment = $this->uri->segment ( $segment );

		return array (
				"page" => $page,
				"segment" => $segment
		);
	}
		
		
	  
	

    }

?>
