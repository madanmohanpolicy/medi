<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Weblogin extends CI_Controller
 {

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');


	}
	
	/**
	* This function used to check the user's emailid and password
	* It funtion calls once user click on login
	* Created by Madan on 31st Aug, 2018
	* @param comments goes here
	*/	
	public function index()
	{			
		$email= "";		
		$password = "";
		
		//initializing the message variable
		$error = 0;
		$message = "";
		$this->load->model('buyer_model');
		
		// Getting the user's information 
		$email = $this->security->xss_clean($this->input->get('email'));
		$password = $this->security->xss_clean($this->input->get('password'));
		
		// Check if all entered values are correct
		if($this->check_email($email))
		{
			
			// Check if user's email and password ar ecorrect and user is active
			$data = $this->buyer_model->buyer_login($email,$password);
			if($data)
			{	
				
				$message = "Login Successfull";				
			}
			else
			{
				$message = "Login Failed";
				$error = 1;
			}			
		}
		else
		{
			$message = "Invalid Data";
			$error = 1;
		}

		/* output in necessary format */	
		header('Content-type: application/json');
		echo json_encode(array('message'=>$message, 'error'=>$error, 'data'=>$data ));
		/* disconnect from the db */
		
    }
	
	
	// check if the entered email is valid.
	function check_email($email)
	{ 	
	    if(filter_var($email, FILTER_VALIDATE_EMAIL))
			{
						   return true;
			 }else{
					return false;
					}		
		
	}

	

 }

?>
