<?php

 if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Hospital (HospitalController)
 * Hospital Class to control all mastercode related operations.
**/
class Hospital extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('hospital_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
		
        $this->global['pageTitle'] = 'Mediwheel : Dashboard';
        
        $this->loadViews("hospitalListing", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the mastercode list
     */
    function hospitalListing()
    {
       
               
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->hospital_model->hospitalListingCount($searchText);

			$returns = $this->paginationCompress ( "hospitalListing/", $count, 10 );
            
            $data['hospitalRecords'] = $this->hospital_model->hospitalListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
            $this->loadViews("hospitalListing", $this->global, $data, NULL);
        }


    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('hospital_model');
            //$data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Mediwheel : Add New Hospital';

            $this->loadViews("addNewHospital", $this->global,  NULL);
        }
    }

    
   
    
    /**
     * This function is used to add new mastercode to the system
     */
    function addNewHospital()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
           $this->form_validation->set_rules('organisation','organisation','trim|required|max_length[128]');
           // $this->form_validation->set_rules('type','type','Type|required');
          ///  $this->form_validation->set_rules('address','address','required');
			//$this->form_validation->set_rules('location','location','required');
			///$this->form_validation->set_rules('state','state','required');
			///$this->form_validation->set_rules('district','district','required');
			///$this->form_validation->set_rules('city','city','required');
			
			//$this->form_validation->set_rules('contact','contact','required|min_length[10]');
			//$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
			//$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
           
        
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $organisation = ucwords(strtolower($this->security->xss_clean($this->input->post('organisation'))));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $type = $this->input->post('type');
				
                 $location = $this->security->xss_clean($this->input->post('location'));
				  $address = $this->security->xss_clean($this->input->post('address'));
				   $district = $this->input->post('district');
				    $city = $this->input->post('city');
					 $state = $this->input->post('state');
				
					  $pincode = $this->input->post('pincode');
				   $contact = $this->security->xss_clean($this->input->post('contact'));
			
				   $contact=implode(',',$contact);
				   
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
				$mobile=implode(',',$mobile);
				 $landline = $this->security->xss_clean($this->input->post('landline'));
				$landline=implode(',',$landline);
                $email = $this->security->xss_clean($this->input->post('email'));
				$email=implode(',',$email);
				 $this->load->model('hospital_model');
                $resultid = $this->hospital_model->lastid();
				//print_r($resultid );
				
if($type=='1'){
	$type='CH';
	 $docid = $this->hospital_model->lastmastercodeid($type);
	
	$id=$docid[0]->id;

				$mastercode='CH'.(1000000+$id+1);
			
}

if($type=='2'){
	
	$type='DIA';
	 $docid = $this->hospital_model->lastmastercodeid($type);
	
	$id=$docid[0]->id;
				$mastercode='DIA'.(1000000+$id+1);
}
if($type=='3'){
	$type='HA';
	 $docid = $this->hospital_model->lastmastercodeid($type);
	
	$id=$docid[0]->id;
				$mastercode='HA'.(1000000+$id+1);
}
		if($type=='4'){
		$type='CP';
	 $docid = $this->hospital_model->lastmastercodeid($type);
	
	$id=$docid[0]->id;
	
				$mastercode='CP'.(1000000+$id+1);
		}
		if($type=='5'){
		$type='MP';
	 $docid = $this->hospital_model->lastmastercodeid($type);
	
	$id=$docid[0]->id;
				$mastercode='MP'.(1000000+$id+1);
		}
		if($type=='6'){
			
		$type='OT';
	 $docid = $this->hospital_model->lastmastercodeid($type);
	
	$id=$docid[0]->id;
				$mastercode='OT'.(1000000+$id+1);
		}
		
			
                $hospitalInfo = array('mastercode'=>$mastercode,'organisation'=>$organisation, 'type'=>$type,'location'=>$location, 'address'=> $address,
                                    'district'=>$district,'city'=>$city,'state'=>$state, 'pincode'=>$pincode, 'contactname'=>$contact,'landline'=>$landline,'mobile'=>$mobile,'email'=>$email);
									 

                $this->load->model('hospital_model');
                $result = $this->hospital_model->addNewHospital($hospitalInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Hospital created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('admin/hospitalListing');
            }
        }
    }

    
    /**
     * This function is used load mastercode edit information
     * @param number $userId : Optional : This is user id
     */
    function editOldHospital($id = NULL)
    {

        
            if($id == NULL)
            {
			
                redirect('admin/hospitalListing');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['hospitalInfo'] = $this->hospital_model->getHospitalInfo($id);
            
            $this->global['pageTitle'] = 'Mediwheel : Edit Hospital';
            
            $this->loadViews("editOldHospital", $this->global, $data, NULL);
       
    }
	
	
	 function editHospitalDoctor($id = NULL)
    {

        
            if($id == NULL)
            {
			
                redirect('admin/hospitalDoctorsListing');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['hospitalDoctorInfo'] = $this->hospital_model->getHospitalDoctorInfo($id);
            
            $this->global['pageTitle'] = 'Mediwheel : Edit Hospital';
            
            $this->loadViews("editHospitalDoctor", $this->global, $data, NULL);
       
    }
	
	
	 function editMedicalPractitionerDoctor($id = NULL)
    {

        
            if($id == NULL)
            {
			
                redirect('admin/medicalPractitioner');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['medicalPractitionerInfo'] = $this->hospital_model->getHospitalDoctorInfo($id);
            
            $this->global['pageTitle'] = 'Mediwheel : Edit Hospital';
            
            $this->loadViews("editMedicalPractitionerDoctor", $this->global, $data, NULL);
       
    }
	
	
    
    
    /**
     * This function is used to edit the mastercode information
     */
    function editHospital($id = null)
    { 
 
        
            $this->load->library('form_validation');
             
             

            $this->form_validation->set_rules('organisation','organisation','trim|required|max_length[128]');
            //$this->form_validation->set_rules('type','type','Type|required');
            //$this->form_validation->set_rules('address','address','required');
			//$this->form_validation->set_rules('location','location','required');
			//$this->form_validation->set_rules('state','state','required');
			//$this->form_validation->set_rules('district','district','required');
			//$this->form_validation->set_rules('city','city','required');
			
			//$this->form_validation->set_rules('contact','contact','required|min_length[10]');
			//$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
			//$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
                   if($this->form_validation->run() == FALSE)
            { 
               $this->editOldHospital($id);
           }
           else
            {
                 $organisation = ucwords(strtolower($this->security->xss_clean($this->input->post('organisation'))));
				
				
                //$email = $this->security->xss_clean($this->input->post('email'));
                $type = $this->input->post('type');
				
                 $location = $this->security->xss_clean($this->input->post('location'));
				  $address = $this->security->xss_clean($this->input->post('address'));
				   $district = $this->input->post('district');
				    $city = $this->input->post('city');
					 $state = $this->input->post('state');
					  $pincode = $this->input->post('pincode');
					
				   $contact= $this->security->xss_clean($this->input->post('contact'));
				 
				   $contact=implode(',',$contact);
				 
				 
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
				$mobile=implode(',',$mobile);
				$landline = $this->security->xss_clean($this->input->post('landline'));
				$landline=implode(',',$landline);
                $email = $this->security->xss_clean($this->input->post('email'));
				$email=implode(',',$email);
				 $this->load->model('hospital_model');
                $resultid = $this->hospital_model->lastid();
                $hospitalInfo = array();
              
                $hospitalInfo = array('organisation'=>$organisation, 'type'=>$type,'location'=>$location, 'address'=> $address,
                                    'district'=>$district,'city'=>$city,'state'=>$state, 'pincode'=>$pincode, 'contactname'=>$contact,'landline'=>$landline,'mobile'=>$mobile,'email'=>$email);

                
                $result = $this->hospital_model->editHospital($hospitalInfo, $id);
              
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Hospital updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital updation failed');
                }
                
                redirect('admin/hospitalListing');
            }
        }
		
		
		
		
		  function editHospitalDoctors($id = null)
    { 
 
        
            $this->load->library('form_validation');
             
             
//$this->load->library('form_validation');
            
          // $this->form_validation->set_rules('mastercode','mastercode','trim|required|max_length[128]');
            //$this->form_validation->set_rules('doctorname','doctorname','Type|required');
           // $this->form_validation->set_rules('specialization','specialization','required');
			//$this->form_validation->set_rules('qualification','qualification','required');
			//$this->form_validation->set_rules('license','license','required');
			//$this->form_validation->set_rules('experience','experience','required');
			//$this->form_validation->set_rules('charges','charges','required');
			
			//$this->form_validation->set_rules('discountoffered','discountoffered');
			////$this->form_validation->set_rules('discountdisplayed','discountdisplayed');
			//$this->form_validation->set_rules('opinionmail','opinionmail');
			///$this->form_validation->set_rules('opinionviamailcharge','opinionviamailcharge');
			//$this->form_validation->set_rules('opinionvideo','opinionvideo');
			//$this->form_validation->set_rules('opinionviavideocharge','opinionviavideocharge');
           
           
        
          // if($this->form_validation->run() == FALSE)
            //{
               //$this->addNewHospitalDoctors();
           // }
			//
			//else{
                $masterCode=$this->security->xss_clean($this->input->post('mastercode'));
		$doctortype=$this->input->post('doctortype');
		
                $doctorName = ucwords(strtolower($this->security->xss_clean($this->input->post('doctorname'))));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $specialization = $this->input->post('specialization');
				
                 $remarks = $this->security->xss_clean($this->input->post('remarks'));
				  $qualification = $this->security->xss_clean($this->input->post('qualification'));
				   $license = $this->input->post('license');
				    $experience = $this->input->post('experience');
					 $charges = $this->input->post('charges');
					  $discountoffered = $this->input->post('discountoffered');
				   $discountdisplayed = $this->security->xss_clean($this->input->post('discountdisplayed'));
			
				
				   
                $opinionmail = $this->security->xss_clean($this->input->post('opinionmail'));
				
				 $opinionviamailcharge = $this->security->xss_clean($this->input->post('opinionviamailcharge'));
			
                $opinionvideo = $this->security->xss_clean($this->input->post('opinionvideo'));
				 $opinionviavideocharge = $this->security->xss_clean($this->input->post('opinionviavideocharge'));
				 
				 
				 $target_dir = "uploads/medicalPractitioners/";
$target_file = $target_dir . basename($_FILES["photograph"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["photograph"]["tmp_name"]);
    if($check !== false) {
       
        $uploadOk = 1;
    } else {
       
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    
    $uploadOk = 0;
}
// Check file size
if ($_FILES["photograph"]["size"] > 500000) {
    
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
   
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
   
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
    } 
}
				 
				 
		$photopath=base_url().$target_file;		 
				 
		 
				 
				 
				 
				 

                $doctorsInfo = array('doctorName'=>$doctorName, 'specialization'=>$specialization,'remarks'=>$remarks, 'qualification'=> $qualification,
                                    'license'=>$license,'experience'=>$experience,'consultationCharges'=>$charges, 'discountOffered'=>$discountoffered, 'discountDisplayed'=>$discountdisplayed,'opinionViaMail'=>$opinionmail,'opinionViaMailCharges'=>$opinionviamailcharge,'opinionViaVideoConferencing'=>$opinionvideo,'opinionViaVideoConferencingCharges'=>$opinionviavideocharge,'photograph'=>$photopath);
                $result = $this->hospital_model->editHospitalDoctors($doctorsInfo, $id);
              
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Hospital updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hospital updation failed');
                }
                if($doctortype=='hospital'){
                redirect('admin/hospitalDoctorsListing');
				
				}
			if($doctortype=='medicalpractitioner'){
                redirect('admin/medicalPractitioner');
            }
        
		
			}
		
		
		
		
		
		
		
  

    /**
     * This function is used to delete the mastercode using Id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteHospital()
    {
       
            $id = $this->input->post('id');
		
            $hospitalInfo = array('isDeleted'=>1);
            
            $result = $this->hospital_model->deleteHospital($id, $hospitalInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
       
    }
	
	 function deleteHospitalDoctor()
    {
       
            $id = $this->input->post('id');
		
            $hospitalDoctorInfo = array('isDeleted'=>1);
            
            $result = $this->hospital_model->deleteHospitalDoctor($id, $hospitalDoctorInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
       
    }
	
	 function deleteMedicalPractitioner()
    {
       
            $id = $this->input->post('id');
		
            $medicalPractitionerInfo = array('isDeleted'=>1);
            
            $result = $this->hospital_model->deleteMedicalPractitionerDoctor($id, $medicalPractitionerInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
       
    }
	
    
   
   

    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
	
	
		/*function for District dropdown */
	
	function district(){
	
		$id= '';
		$id= $this->input->post('state_id');

			if($id)
			{
				$data= $this->hospital_model->fetch_district($id);
				
				$str = "";
				$str = '<select class="form-control required" id="district" name="district">';
				$str.= '<option value="0">Select District</option>';
				//print_r( $data);
				for($i=0; $i<count($data); $i++)
				{
					
					$str.= '<option value='.$data[$i]->id.'>'.$data[$i]->district.'</option>';
				}
				
				$str.= "</Select>";
				
			}
			echo $str;

		}
		/*function for City dropdown */
		function city(){
	
		$id= '';
		$id= $this->input->post('district_id');

			if($id)
			{
				$data= $this->hospital_model->fetch_city($id);
				
				$str = "";
				
				$str = '<select class="form-control required" id="city" name="city">';
				$str.= '<option value="0">Select City</option>';
				//print_r( $data);
				for($i=0; $i<count($data); $i++)
				{
					$str.= '<option value='.$data[$i]->id.'>'.$data[$i]->city.'</option>';
				}
				
				$str.= "</Select>";
				
			}
			echo $str;

		}

			/*function for State dropdown */
		
		function pincode(){
	
		$id= '';
		$id= $this->input->post('city_id');

			if($id)
			{
				$data= $this->hospital_model->fetch_pincode($id);
				
				$str = "";
				
				$str = '<select class="form-control required" id="pincode" name="pincode">';
				$str.= '<option value="0">Select Pincode</option>';
				//print_r( $data);
				for($i=0; $i<count($data); $i++)
				{
					$str.= '<option value='.$data[$i]->id.'>'.$data[$i]->pincode.'</option>';
				}
				
				$str.= "</Select>";
				
			}
			echo $str;

		}
		
		
		
		


function hospitalDoctorsListing() {
	
	    $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->hospital_model->hospitalDoctorsListingCount($searchText);

			$returns = $this->paginationCompress ( "hospitalDoctorsListing/", $count, 10 );
            
            $data['hospitalDoctorsRecords'] = $this->hospital_model->hospitalDoctorsListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
            $this->loadViews("hospitalDoctorsListing", $this->global, $data, NULL);
	
	
}






function medicalPractitioner() {
	
	    $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->hospital_model->hospitalDoctorsListingCount($searchText);

			$returns = $this->paginationCompress ( "medicalPractitioner/", $count, 10 );
            
            $data['medicalPractitionerRecords'] = $this->hospital_model->medicalPractitioner($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Mediwheel : Hospital Listing';
            
            $this->loadViews("medicalPractitioner", $this->global, $data, NULL);
	
	
}

function addNewHospitalDoctor() {
	
	
	if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('hospital_model');
            //$data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Mediwheel : Add New Hospital';

            $this->loadViews("addNewHospitalDoctors", $this->global,  NULL);
        }
	
	
}

function addNewmedicalPractitioner() {
	
	
	if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('hospital_model');
            //$data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Mediwheel : Add New Medical Practitioner';

            $this->loadViews("addNewmedicalPractitioner", $this->global,  NULL);
        }
	
	
}





 function addNewHospitalDoctors()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            //$this->load->library('form_validation');
            
          // $this->form_validation->set_rules('mastercode','mastercode','trim|required|max_length[128]');
            //$this->form_validation->set_rules('doctorname','doctorname','Type|required');
           // $this->form_validation->set_rules('specialization','specialization','required');
			//$this->form_validation->set_rules('qualification','qualification','required');
			//$this->form_validation->set_rules('license','license','required');
			//$this->form_validation->set_rules('experience','experience','required');
			//$this->form_validation->set_rules('charges','charges','required');
			
			//$this->form_validation->set_rules('discountoffered','discountoffered');
			////$this->form_validation->set_rules('discountdisplayed','discountdisplayed');
			//$this->form_validation->set_rules('opinionmail','opinionmail');
			///$this->form_validation->set_rules('opinionviamailcharge','opinionviamailcharge');
			//$this->form_validation->set_rules('opinionvideo','opinionvideo');
			//$this->form_validation->set_rules('opinionviavideocharge','opinionviavideocharge');
           
           
        
          // if($this->form_validation->run() == FALSE)
            //{
               //$this->addNewHospitalDoctors();
           // }
			//
			//else{
                $masterCode=$this->security->xss_clean($this->input->post('mastercode'));
			
                $doctorName = ucwords(strtolower($this->security->xss_clean($this->input->post('doctorname'))));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $specialization = $this->input->post('specialization');
				
                 $remarks = $this->security->xss_clean($this->input->post('remarks'));
				  $qualification = $this->security->xss_clean($this->input->post('qualification'));
				   $license = $this->input->post('license');
				    $experience = $this->input->post('experience');
					 $charges = $this->input->post('charges');
					  $discountoffered = $this->input->post('discountoffered');
				   $discountdisplayed = $this->security->xss_clean($this->input->post('discountdisplayed'));
			
				
				   
                $opinionmail = $this->security->xss_clean($this->input->post('opinionmail'));
				
				 $opinionviamailcharge = $this->security->xss_clean($this->input->post('opinionviamailcharge'));
			
                $opinionvideo = $this->security->xss_clean($this->input->post('opinionvideo'));
				 $opinionviavideocharge = $this->security->xss_clean($this->input->post('opinionviavideocharge'));
				 
				 
				 $target_dir = "uploads/medicalPractitioners/";
$target_file = $target_dir . basename($_FILES["photograph"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["photograph"]["tmp_name"]);
    if($check !== false) {
       
        $uploadOk = 1;
    } else {
       
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    
    $uploadOk = 0;
}
// Check file size
if ($_FILES["photograph"]["size"] > 500000) {
    
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
   
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
   
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
    } 
}
				 
				 
		$photopath=base_url().$target_file;		 
				 
		 
				 
				 
				 
				 
			
				//print_r($resultid );
 $this->load->model('hospital_model');
                $docid = $this->hospital_model->lastdocid();
	$docid= $docid[0]->id;
		$doctorid=$docid+1;
			$docSubCode=$masterCode."DOC".	$doctorid;
                $doctorsInfo = array('mastercode'=>$masterCode,'docSubCode'=>$docSubCode,'doctorName'=>$doctorName, 'specialization'=>$specialization,'remarks'=>$remarks, 'qualification'=> $qualification,
                                    'license'=>$license,'experience'=>$experience,'consultationCharges'=>$charges, 'discountOffered'=>$discountoffered, 'discountDisplayed'=>$discountdisplayed,'opinionViaMail'=>$opinionmail,'opinionViaMailCharges'=>$opinionviamailcharge,'opinionViaVideoConferencing'=>$opinionvideo,'opinionViaVideoConferencingCharges'=>$opinionviavideocharge,'photograph'=>$photopath);
									
								
                $this->load->model('hospital_model');
                $result = $this->hospital_model->addNewHospitalDoctors($doctorsInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Hospital created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('admin/hospitalDoctorsListing');
			//}
            
        }

	}


function addNewmedicalPractitioners()
    { 
          $this->load->library('form_validation');
            
           // $this->form_validation->set_rules('organisation','organisation','trim|required|max_length[128]');
            //$this->form_validation->set_rules('type','type','Type|required');
           // $this->form_validation->set_rules('address','address','required');
			//$this->form_validation->set_rules('location','location','required');
			//$this->form_validation->set_rules('state','state','required');
			//$this->form_validation->set_rules('district','district','required');
			//$this->form_validation->set_rules('city','city','required');
			
			//$this->form_validation->set_rules('contact','contact','required|min_length[10]');
			//$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
			//$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
           
        
           // if($this->form_validation->run() == FALSE)
            //{
               // $this->addNew();
            //}
                $masterCode=$this->security->xss_clean($this->input->post('mastercode'));
			
                $doctorName = ucwords(strtolower($this->security->xss_clean($this->input->post('doctorname'))));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $specialization = $this->input->post('specialization');
				
                 $remarks = $this->security->xss_clean($this->input->post('remarks'));
				  $qualification = $this->security->xss_clean($this->input->post('qualification'));
				   $license = $this->input->post('license');
				    $experience = $this->input->post('experience');
					 $charges = $this->input->post('charges');
					  $discountoffered = $this->input->post('discountoffered');
				   $discountdisplayed = $this->security->xss_clean($this->input->post('discountdisplayed'));
			
				
				   
                $opinionmail = $this->security->xss_clean($this->input->post('opinionmail'));
				
				 $opinionviamailcharge = $this->security->xss_clean($this->input->post('opinionviamailcharge'));
			
                $opinionvideo = $this->security->xss_clean($this->input->post('opinionvideo'));
				 $opinionviavideocharge = $this->security->xss_clean($this->input->post('opinionviavideocharge'));
				 
				 
				 $target_dir = "uploads/hospitaloctors/";
				
$target_file = $target_dir . basename($_FILES["photograph"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["photograph"]["tmp_name"]);
    if($check !== false) {
       
        $uploadOk = 1;
    } else {
       
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    
    $uploadOk = 0;
}
// Check file size
if ($_FILES["photograph"]["size"] > 500000) {
    
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
   
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
   
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["photograph"]["tmp_name"], $target_file)) {
        
    } 
}
				 
				 
		$photopath=base_url().$target_file;		 
				 
		 
				 
				 
				 
				 
			
				//print_r($resultid );
 $this->load->model('hospital_model');
                $docid = $this->hospital_model->lastmpid();
	$docid= $docid[0]->id;
	
		$doctorid=$docid+1;
		
			$docSubCode=$masterCode."DOC".	$doctorid;
                $doctorsInfo = array('mastercode'=>$masterCode,'docSubCode'=>$docSubCode,'doctorName'=>$doctorName, 'specialization'=>$specialization,'remarks'=>$remarks, 'qualification'=> $qualification,
                                    'license'=>$license,'experience'=>$experience,'consultationCharges'=>$charges, 'discountOffered'=>$discountoffered, 'discountDisplayed'=>$discountdisplayed,'opinionViaMail'=>$opinionmail,'opinionViaMailCharges'=>$opinionviamailcharge,'opinionViaVideoConferencing'=>$opinionvideo,'opinionViaVideoConferencingCharges'=>$opinionviavideocharge,'photograph'=>$photopath);
									
								
                $this->load->model('hospital_model');
                $result = $this->hospital_model->addNewHospitalDoctors($doctorsInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Hospital created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('admin/medicalPractitioner');
            
        }
    

	
	}
		





