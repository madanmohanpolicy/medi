<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Websignup extends CI_Controller
 {

	public function __construct(){

			parent::__construct();
				$this->load->helper('url');
				$this->load->library('encryption');
				$this->load->library('session');


	}
	
	/**
	* This function used to open the form for signup of user.
	* It funtion calls once user click on signup
	* Created by Madan on 31st Aug, 2018
	* @param comments goes here
	*/	
	public function index()
	{			
		$name= "";
		$email= "";
		$mobile= "";
		$password = "";
		
		//initializing the message variable
		$error = 0;
		$message = "";
		$this->load->model('buyer_model');
		
		// Getting the user's information 
		$name= $this->input->get('name');
		$email= $this->input->get('email');
		$mobile= $this->input->get('mobile');
		$password_encrypt= $this->input->get('password');
		
		// Check if all entered values are correct
		if($this->check_name($name) && $this->check_email($email) && $this->check_mobile($mobile))
		{
			// Check if user's email is already existing or not. if not exist then it adds.
			if($this->buyer_model->is_email_exist($email))
			{
				$name = preg_replace('/%20/', ' ', $name);
				$added_date = date("Y-m-d H:i:s");
				//$password = rand(10000,99999);
				//$password_encrypt = md5($password);
				$buyerInfo = array('password'=>$password_encrypt, 'added_date'=>$added_date,'name'=>addslashes($name),'mobile'=>$mobile,'email'=>$email ,'status'=>1);
				
				if($this->buyer_model->insertBuyer($buyerInfo))
				{					
					$message = "User Registered Successfully";
				}
				else
				{
					$message = "Error : Data could not be inserted. Please try again";
				}
			}
			else
			{
				$message = "User Already Exist";
				$error = 1;
			}
			
		}
		else
		{
			$message = "Invalid Data";
			$error = 1;
		}

		/* output in necessary format */	
		header('Content-type: application/json');
		echo json_encode(array('message'=>$message, 'error'=>$error , 'password'=>$password));
		/* disconnect from the db */
		
    }
	
	
	
	
	
	
	function check_name($name)
	{		
		if(strlen($name) >= 2)
		{
			
		return true;	
		}else{
			return false;
			 }
	}

	function check_email($email)
	{ 	
	   
	   if(filter_var($email, FILTER_VALIDATE_EMAIL))
			{
						   return true;
			 }else{
					return false;
					}
		
		
	}

	function check_mobile($mobile)
	{   
		 if(preg_match('/^\d{10}$/',$mobile)){
		return true;
		 }else{
			 return false;
		 }
	}	

 }

?>
