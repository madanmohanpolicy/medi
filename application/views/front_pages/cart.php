<section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="cart_wrapper">
            <div class="container">
                <div class="title">My Cart</div>

                <div class="cart_block">
                    <div class="left">

                       

				<?php //echo "aaaaa = ";
					//print_r($cart_data);
					//die;
					//echo "aaaaa = ".$cart_data->error;
					if($cart_data->error == 0 && $cart_data->total_row > 0)
					{
						$total_mrp = 0;
						$total_offered_price = 0;
						$total_discount = 0;
						
						for($i=0; $i<count($cart_data->message); $i++)
						{
				?>

				   <div class="cartBox">
					<div class="pro_name_price">
						<div class="pro_name">
								<?php echo $cart_data->message[$i]->product_name; ?>
								<span><?php echo $cart_data->message[$i]->packSizeLabel; ?></span>
						</div>
						<div class="pro_price">
								₹<?php echo $cart_data->message[$i]->oprice; ?>
								<span class="pro_real_price">MRP₹<?php echo $cart_data->message[$i]->mrp; ?></span>
						</div>
					</div>

					<div class="pro_remove_add">
						<div class="remove"><i class="fa fa-trash-o" aria-hidden="true"></i><a href="<?php echo base_url(); ?>cart/cart_rem/?id=<?php echo $cart_data->message[$i]->id; ?>"> Remove</a></div>
						<div class="add_pro">
							<a href="<?php echo base_url(); ?>cart/cart_minus/?qty=<?php echo $cart_data->message[$i]->quantity;?>&id=<?php echo $cart_data->message[$i]->id; ?>"><button class="dec_number">-</button></a>    
							<input type="text" value="<?php echo number_format($cart_data->message[$i]->quantity,0); ?>" class="cart_number" />
							
							<a href="<?php echo base_url(); ?>cart/cart_plus/?qty=<?php echo $cart_data->message[$i]->quantity;?>&id=<?php echo $cart_data->message[$i]->id; ?>"><button class="add_number" onclick="">+</button></a>
						</div>
					</div>

				</div>
				
				<?php
						$total_mrp = $total_mrp + ($cart_data->message[$i]->mrp*$cart_data->message[$i]->quantity);
						$total_offered_price = $total_offered_price + ($cart_data->message[$i]->oprice*$cart_data->message[$i]->quantity);							
						}
				
					
						$total_discount = $total_mrp - $total_offered_price;
					}
					else
					{
						echo "No Product is available in your cart. Continue Shopping";
					}
					
				
			?>	
			
                    </div>
					<?php 
					if($cart_data->total_row > 0)
					{
					?>
                    <div class="right">
                     <!--  <div class="login_toapplycup right_box"><a href="login.html">Login</a> to Apply Coupon</div> -->
                        <div class="pros_price_details right_box">

                            <div class="mrp_box"><span>MRP Total</span> <span>₹<?php echo $total_mrp; ?></span></div>
                            <div class="mrp_box"><span>Price Discount</span> <span>- ₹<?php echo $total_discount; ?></span></div>
                            <div class="mrp_box"><span>Shipping Charges</span> <span>As per delivery address</span></div>
                            <div class="mrp_box tobepaid"><span>To be paid</span> <span>₹<?php echo $total_offered_price; ?></span></div>
                            
							<?php 
							if($total_discount > 0)
							{
							?>
							<div class="totalsaving">Total Savings <span>₹<?php echo $total_discount; ?></span></div>
							<?php
							}
							?>

                        </div>

                        <div class="pros_checkout right_box">
                         <!--   <div class="mrp_box"><span>Your delivery location</span> <a href="#" class="checkoutlocation"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $cart_data->message[0]->city; ?></a></div>
                            <div class="mrp_box">-->
							
							
													   
								   <a href="<?php echo base_url(); ?>cart/checkout" class="btn_red">CHECKOUT <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								
							
							
							
							
							</div>
                        </div>

                    </div>
					<?php
					}
					?>
                </div>

            </div>
        </div>
        

    </section>