<?php
$user_id= $this->session->userdata('id');
 
if(!$user_id){
 
 redirect('buyer/login_view');
}
 
 ?>
<section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="myaccount_wrapper">
            <div class="container">
                <h1>Your Profile Details</h1>
                               <?php
							/*	if($this->session->flashdata('success_msg')!="")
								{
								echo $this->session->flashdata('success_msg'); 
								}*/	

       							
								?> 
                    <div class="accountwrap">
                        <div class="left_wrap">
                            <div class="title">Basic information</div>
                            <div class="name_wrapper">

                                <ul class="name_details">
                                    <li><span>Full name</span> <?php echo ucfirst($this->session->userdata('name')); ?></li>
                                    
                                    <li><span>Mobile no.</span> <?php echo $this->session->userdata('mobile'); ?></li>
                                    <li><span>Primary Email address</span> <?php echo $this->session->userdata('email'); ?></li>
									
                                    <li><a href="<?php echo base_url().'buyer/editprofile?id='.$this->session->userdata('id').'&email='.$this->session->userdata('email')?>" class="btn_red">EDIT Details</a></li>
                                </ul>

                                <div class="img_block">
                                    <img src="img/profile_pic.png" alt="">
                                </div>

                            </div>
                        </div>

                        <div class="right_wrap">
                            <div class="title">Change password</div>
							<?php if(isset($message)) { echo $message; } ?>
                            <form action="<?php echo base_url(); ?>buyer/changePassword" id="form_main"  name="form_main"   onSubmit="return validatePassword();"  method="post">
							<DIV  class="tblSaveForm">
                                <div class="form-group">
                                    <label for="currentPassword">Old password</label>
                                    <input type="password"  id="currentPassword"  name="currentPassword"  placeholder="Enter old password">
								
                                </div>
                                <div class="form-group">
                                    <label for="newpass">New password</label>
                                    <input type="password" name="newPassword" id="newPassword" placeholder="Enter new password">
									<span id="newPassword"
                        class="required"></span>
                                </div>
                                <div class="form-group">
                                    <label for="cnfpass">Confirm password</label>
                                    <input type="password" name="confirmPassword" id="confirmPassword" placeholder="Re-type new password"><span id="confirmPassword"
                    class="required"></span>
                                </div>
                                <div class="form-group">
								<input  class="btn_red" type="submit" name="submit" style="width: 50%;"
                        value="Submit" class="btnSubmit">
                                    
                                    <button class="btn_green" type="reset">Cancel</button>
                                </div>
                            </form>
                        </div>
						</DIV>

                    </div>
            </div>
        </div>

        

    </section>