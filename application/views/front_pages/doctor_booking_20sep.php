<div class="wrapper">        
            
        <div class="container">
                <div class="abvl_doc_heading">
					<h1>Available Dooctors</h1>
					<div class="search_block_head">
						<form>
							<div class="form-group location">
								<select>
									<option>Select Location</option>
									<option>Delhi</option>
									<option>Mumbai</option>
									<option>Pune</option>
									<option>Nagpur</option>
									
								</select>
							</div>							
							<div class="form-group type">
								<select>
									<option>Select Doctor Type</option>
									<option>Dentist</option>
									<option>Cosmetic/aesthetic Dentist</option>
									<option>Pediatric Dentist</option>
									<option>Restorative Dentist</option>
									
								</select>
							</div>
							<div class="form-group btn">
								<input type="submit" vlaue="Search">
							</div>
							
						</form>
					</div>
				</div>

                

            <div class="pack_wrapper">
                    
					
					
					
					<div class="packbox doct_list">
                        <div class="doc_img_block">
							<img src="<?php echo base_url();?>assets/img/doc_img.jpg" alt="">
						</div>
						
						<div class="doc_details_name">
							<div class="doc_title">Dr. M. R. Pujari</div>
							<div class="doc_specifications">
								<p>BDS, MDS - Oral Medicine and Radiology</p>
								<p>13 years experience</p>
								<p>Dentist</p>
							</div>						
						
							
							<div class="doc_clients_cmnts">
								<p>"Root canal was painless and doctor is very good in handling patients and results are up to the mark."  <span> Parvati, visited for rct - root canal treatment</span></p>
							</div>							
						</div>
						
						<div class="doc_speciafications">
							<ul>								
								<li class="doc_location"><a href="#">Dwarka , Delhi</span></a></li>
								<li class="doc_fee"><i class="fa fa-inr" aria-hidden="true"></i> 200</li>
								<li class="doc_available"><span class="greenColorlink">Available Today</span></li>
							</ul>
								
								<div class="doc_cotact_blk">
								<a href="javascript:void(0)" class="BookAppointmentBTN">Book Appointment</a>
							</div>
							
						</div>	

							<div class="bkap_wrap">
                                            <div class="close">X</div>
                                            <strong>Select your preferred appointment  date and time</strong>
    
                                            <div class="date-list owl-carousel">
                                                <div class="item">
                                                    <div class="dateBox">
                                                        <span class="day">Sun</span>
                                                        <span class="date">02 Sep</span>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                        <div class="dateBox">
                                                            <span class="day">Sun</span>
                                                            <span class="date">02 Sep</span>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                            <div class="dateBox">
                                                                <span class="day">Sun</span>
                                                                <span class="date">02 Sep</span>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                                <div class="dateBox">
                                                                    <span class="day">Sun</span>
                                                                    <span class="date">02 Sep</span>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                    <div class="dateBox">
                                                                        <span class="day">Sun</span>
                                                                        <span class="date">02 Sep</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>
    
                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
    
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
    
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
    
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
    
                                            </div>
    
                                            <ul class="time_ul">
                                                    <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                    <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                    <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                    <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                    <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                    <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                    <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                    <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                    <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                    <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                    <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                    <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                    <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
    
                                            </ul>
                                    </div>


									


						
                        
                    </div><!--End Doc Info BOX-->
					
					
					
            </div>

            
        </div>
    </div>