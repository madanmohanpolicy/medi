<script>

function checkforgetPass(frm)
{
	// Check name field			
	var email = $('#email').val().trim();
	
	
	// Check email field
	if(email == "")
	{
		$('#email_error').html("Please Enter Your Email Id");
		$('#email').focus();
		
		return false;
	}
	else if(!(validateEmail(email)))
	{	
		$('#email_error').html("Please Enter Your Correct Email Id");
		$('#email').focus();
		
		return false;
		
	}
	else
	{
		$('#email_error').html("");
		return true;
	}
	
	
}
</script>
<section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="myaccount_wrapper">
            <div class="container">
			
                <div class="loginForm">
				
                     <div class="title">Forgot Password</div>
						   
								<?php
								if($this->session->flashdata('success_msg')!="")
								{
								echo $this->session->flashdata('success_msg'); 
								}			
								?> 
						
						
						
					
                    <form action="password_reset" id="form_main" method="post" onsubmit="return checkforgetPass();" >
                        <div class="form-group">
                            <label for="username">Email ID</label><div class="error" id="email_error"></div>
                            <input type="text" id="email" name="email"  placeholder="Please Enter Your Registered Email " />
							 
									
                        </div>
                     
								
                        <div class="form-group" id="contact_submit">
						
                            
							<input class="btn_red" type="submit" value="Continue" />
                        </div>

                        

                        

                    </form>
                </div>
            </div>
        </div>

	</section>
