<script>

function verifyotp(frm)
{
	// Check name field		
	var mobile = $('#mobile').val().trim();	
		
	// Check mobile field
	if(mobile == "")
	{
		$('#mobile_error').html("Please Enter Your OTP");
		$('#mobile').focus();
		
		return false;
	}
	else if(!($.isNumeric(mobile)))
	{	
		$('#mobile_error').html("Please Enter Your 5 digit OTP");
		$('#mobile').focus();
		
		return false;
		
	}
	else if(mobile.length <5)
	{	
		$('#mobile_error').html("Please Enter Your 5 digit OTP");
		$('#mobile').focus();		
		return false;
		
	}
	else
	{
		$('#mobile_error').html("");
	}
	
	
	$('#form_main').submit();
	
}
</script>   
    <section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="myaccount_wrapper">
            <div class="container">
                <div class="loginForm">
                       <!-- <div class="title">Verify Your Mobile</div>-->
						<?php  
						if( $this->session->flashdata('success_msg') == "Mobile Verified")
						{
							echo "Thanks! Your mobile has been verified. Your login id is your email and use the one time pin as password. Requesting you to please change the password after login.";
						
						}
						else
						{
							if($this->session->flashdata('success_msg') != "User Registered Successfully")
							{
								echo $this->session->flashdata('success_msg');
							}
						?>
						<br>
                    <form action="<?php echo base_url(); ?>buyer/verifymobile" id="form_main" name="form_main" method="post" onsubmit="return verifyotp(this);">
                        
                        <div class="form-group">
                            <label for="number">Enter OTP :</label><div class="error" id="mobile_error"></div>
                            <input type="tel" name="mobile" id="mobile" placeholder="Enter the OTP sent to your registered mobile "  maxlength="5">
                        </div>
                        

                        <div class="form-group">
                            <button class="btn_red"  type="submit" value="verify_mobile" name="register" >Verify Mobile</button>
                        </div>

                        <div class="form-group dontHave">
                                Already have an account? <a href="<?php echo base_url(); ?>buyer/login_view">Login</a>
                        </div>

                        <div class="form-group loginProblem">
                           By signing up, you agree to our <a href="<?php echo base_url(); ?>term">Terms and Conditions</a>
                        </div>

                    </form>
					<?php
						}
					?>
                </div>
            </div>
        </div>

        

    </section>