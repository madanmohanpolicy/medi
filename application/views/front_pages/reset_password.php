<script>

function checkResetPass(frm)
{
	// Check name field			
	var npassword = $('#npassword').val().trim();
	var cpassword = $('#cpassword').val().trim();
	
	
	// Check email field
	if(npassword == "")
	{
		$('#email_error').html("Please Enter Your New Password");
		$('#npassword').focus();
		
		return false;
	}
	else if(cpassword == "")
	{
		$('#email_error2').html("Please Enter Your Confirm Password");
		$('#cpassword').focus();
		
		return false;
	}
	else if(cpassword != npassword)
	{
		$('#email_error').html("New Password and Confirm Password should be same");
		$('#cpassword').focus();
		
		return false;
	}
	else
	{
		$('#email_error').html("");
		return true;
	}
	
	
}
</script>
<section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="myaccount_wrapper">
            <div class="container">
			
                <div class="loginForm">
				
                     <div class="title">Reset Password</div>
						   
								<?php
								if($this->session->flashdata('success_msg')!="")
								{
								echo $this->session->flashdata('success_msg'); 
								}			
								?> 
						
						
						
					
                    <form action="reset_password" id="form_main" method="post" onsubmit="return checkResetPass();">
						<input type = "hidden" name="url_val" value="<?php echo $url_val; ?>" >
                        <div class="form-group">
                            <label for="username">New Password</label><div class="error" id="email_error" ></div>
                            <input type="password" name="npassword"   id="npassword" maxlength="20">				 
						</div>
						
						<div class="form-group">
                            <label for="username">Confirm password</label><div class="error" id="email_error2"></div>
                            <input type="password" name="cpassword"  id="cpassword" maxlength="20">			 
						</div>
                     
								
                        <div class="form-group" id="contact_submit">
						
                            
							<input class="btn_red" type="submit" name="reset_form" id="reset_form" value="Reset Password" />
                        </div>

                        

                        

                    </form>
                </div>
            </div>
        </div>

	</section>
