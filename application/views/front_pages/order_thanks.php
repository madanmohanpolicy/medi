<section class="inner_body_wrapper" style="min-height: 400px;">
    <div class="cart_wrapper">
        <div class="container">
            <div class="thankyou_block">
                <h1><img src="<?php echo base_url(); ?>assets/img/verified.svg" alt="">Order Placed.				   <span>Thank you!</span></h1>
                <p class="nxt_update">Next Update in 30 minutes</p>
                <p class="orderId">Order ID: P026718199573088</p>
                <p class="genuine_medicines_text">
                    <img src="<?php echo base_url(); ?>assets/img/thankscart.svg" alt="">
                    <span>100% Genuine medicines <br> Doorstep Delivery <br> Great Savings</span>
                </p>
                <p class="btn_block">
                    <a href="<?php echo base_url(); ?>">Track Your Order</a>
                    <a href="<?php echo base_url(); ?>">Home</a>
                </p>
            </div>
        </div>
    </div>
</section>


