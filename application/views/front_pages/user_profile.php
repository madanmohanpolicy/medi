<section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="myaccount_wrapper">
            <div class="container">
                <h1>Your Profile Details</h1>

                    <div class="accountwrap">
                        <div class="left_wrap">
                            <div class="title">Basic information</div>
                            <div class="name_wrapper">

                                <ul class="name_details">
                                    <li><span>Full name</span> <?php echo $this->session->userdata('front_name'); ?></li>
                                    <li><span>Mobile no.</span> <?php echo $this->session->userdata('front_mobile');?></li>
                                    <li><span>Primary Email address</span> <?php echo $this->session->userdata('front_email');?></li>
                                    <li><a href="#" class="btn_red">EDIT Details</a></li>
                                </ul>

                                <div class="img_block">
                                    <img src="img/profile_pic.png" alt="">
                                </div>

                            </div>
                        </div>

                        <div class="right_wrap">
                            <div class="title">Change password</div>
							<?php  
						
							echo $this->session->flashdata('success_msg');
						?>
                            <form action="changepwd" id="form_main" method="post" onsubmit="return checkpass();">
                                <div class="form-group">
									<div id="error11">&nbsp;</div>
                                    <label for="oldpass">Old password</label>
                                    <input type="password" name="opassword" id="opassword" placeholder="Enter old password">
                                </div>
                                <div class="form-group">
								<div id="error12">&nbsp;</div>
                                    <label for="newpass">New password</label>
                                    <input type="password" name="npassword" id="npassword" placeholder="Enter new password">
                                </div>
                                <div class="form-group">
								<div id="error13">&nbsp;</div>
                                    <label for="cnfpass">Confirm password</label>
                                    <input type="password" name="cpassword" id="cpassword" placeholder="Re-type new password">
                                </div>
                                <div class="form-group">
                                    <button class="btn_red" type="submit">Update Password</button>
                                    <button class="btn_green" type="reset">Cancel</button>
                                </div>
                            </form>
                        </div>

                    </div>
            </div>
        </div>

        

    </section>
<script>
function checkpass()
{
var opassword = document.getElementById("opassword").value.trim();
	var npassword = document.getElementById("npassword").value.trim();
	var cpassword = document.getElementById("cpassword").value.trim();
	if(opassword == "")
	{		
		$("#error11").text("Please enter your existing password").show().fadeOut(50000);
		document.getElementById("opassword").focus();	
		return false;
	}
	if(npassword == "")
	{		
		$("#error12").text("Please enter your new password").show().fadeOut(50000);
		document.getElementById("npassword").focus();	
		return false;
	}
	if(npassword.length < 6)
	{
		$("#error12").text("Password must be 6 characters long.").show().fadeOut(50000);
		document.getElementById("npassword").focus();	
		return false;
	}
	if(opassword == npassword)
	{		
		$("#error12").text("Entered new password is same as existing password").show().fadeOut(50000);
		document.getElementById("npassword").focus();	
		return false;
	}
	
	if(cpassword == "")
	{		
		$("#error13").text("Please enter your confirm password").show().fadeOut(50000);
		document.getElementById("cpassword").focus();	
		return false;
	}
	if(cpassword != npassword)
	{		
		$("#error13").text("Your confirm password is not matching with new password").show().fadeOut(50000);
		document.getElementById("cpassword").focus();	
		return false;
	}
	return true;
}
</script>