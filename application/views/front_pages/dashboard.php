<!DOCTYPE html>
<html lang="en">  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mediwheel Test</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet"> 
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
</head>
<body>
    <div id="dashboard_wrapper">
            <div class="left-block">
                <a href="#" class="logo"><img src="<?php echo base_url(); ?>assets/img/logo.svg" alt=""></a>
                <ul class="navigation">
                    <li class="active"><a href="dashboard.html"><span class="lnr lnr-screen"></span> Dashboard</a></li>
                    <li><a href="personal-profile.html"><span class="lnr lnr-user"></span> Personal Profile</a>
                        <span class="lnr lnr-chevron-right sebmunu-click"></span>
                        <ul class="submenu">
                            <li><a href="personal-profile.html"><span class="lnr lnr-chevron-right"></span> Family Members</a></li>
                            <li><a href="personal-profile.html"><span class="lnr lnr-chevron-right"></span> Family Doctors</a></li>
                        </ul>
                    </li>
                    <li><a href="health-profile.html"><span class="lnr lnr-heart-pulse"></span> Health Profile</a>
                        <span class="lnr lnr-chevron-right sebmunu-click"></span>
                        <ul class="submenu">
                                <li><a href="biometrics.html"><span class="lnr lnr-chevron-right"></span> Biometrics</a></li>
                                <li><a href="lab-parameters.html"><span class="lnr lnr-chevron-right"></span> Lab Parameters</a></li>
                                <li><a href="medication.html"><span class="lnr lnr-chevron-right"></span> Medication</a></li>
                                <li><a href="surgery.html"><span class="lnr lnr-chevron-right"></span> Surgery</a></li>
                                <li><a href="physical-activity.html"><span class="lnr lnr-chevron-right"></span> Physical Activity</a></li>
                                <li><a href="hra-summary.html"><span class="lnr lnr-chevron-right"></span> HRA Summary</a></li>
                        </ul>
                    </li>
                    <li><a href="health-records.html"><span class="lnr lnr-dice"></span> Health Records</a>
                        <span class="lnr lnr-chevron-right sebmunu-click"></span>
                        <ul class="submenu">
                                <li><a href="manage-records.html"><span class="lnr lnr-chevron-right"></span> Manage Records</a></li>
                                <li><a href="view-hra-report.html"><span class="lnr lnr-chevron-right"></span> View HRA Report</a></li>
                                <li><a href="download-hra-report.html"><span class="lnr lnr-chevron-right"></span> Download HRA Report</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><span class="lnr lnr-layers"></span> Assessments</a>
                        <span class="lnr lnr-chevron-right sebmunu-click"></span>
                        <ul class="submenu">
                                <li><a href="assessmentsHealthrisk.html"><span class="lnr lnr-chevron-right"></span> Health Risk (HRA)</a></li>
                                <li><a href="assessmentsSummary.html"><span class="lnr lnr-chevron-right"></span> Summary</a></li>
                        </ul>
                    </li>
                    <li><a href="consult-specialist.html"><span class="lnr lnr-users"></span> Consult Specialist</a></li>
                    <li><a href="wellness-centre.html"><span class="lnr lnr-apartment"></span> Wellness Centre</a>
                        <span class="lnr lnr-chevron-right sebmunu-click"></span>
                        <ul class="submenu">
                                <li><a href="quiz-tools.html"><span class="lnr lnr-chevron-right"></span> Quiz & Tools</a></li>
                                <li><a href="#"><span class="lnr lnr-chevron-right"></span> Care@Mediwheel</a></li>
                        </ul>
                    </li>
                    <li><a href="rewards.html"><span class="lnr lnr-diamond"></span> Rewards</a>
                        <span class="lnr lnr-chevron-right sebmunu-click"></span>
                        <ul class="submenu">
                                <li><a href="how-it-works.html"><span class="lnr lnr-chevron-right"></span> How it Works</a></li>
                                <li><a href="earn.html"><span class="lnr lnr-chevron-right"></span> Earn</a></li>
                                <li><a href="redeem.html"><span class="lnr lnr-chevron-right"></span> Redeem</a></li>
                                <li><a href="redemption-history.html"><span class="lnr lnr-chevron-right"></span> Redemption History</a></li>
                        </ul>
                    </li>
                    <li><a href="refer-your-friends.html"><span class="lnr lnr-users"> </span>Refer your Friends</a>
                        <span class="lnr lnr-chevron-right sebmunu-click"></span>
                        <ul class="submenu">
                                <li><a href="refer-your-friends.html"><span class="lnr lnr-chevron-right"></span> Refer your Friends</a></li>
                                <li><a href="invite-history.html"><span class="lnr lnr-chevron-right"></span> Invite History</a></li>
                        </ul>
                    </li>
                    <li><a href="market-place.html"><span class="lnr lnr-store"></span> Market Place</a></li>
                </ul>
            </div><!--end Left Section-->

            <div class="right-block">
                <div class="header">
                    <div class="menubars"><span class="lnr lnr-menu"></span></div>
                    <div class="top-right">

                       <ul>
                            <li>
                                <span class="my_rewards top_clock"><span class="lnr lnr-diamond"></span> 3700  <i class="lnr lnr-chevron-down"></i></span>
                                <ul class="rewards">
                                    <li><a href="#">My Reward </a></li>
                                    <li><a href="#">Reward History</a></li>
                                </ul>
                            </li>
                            <li>
                                    <span class="lnr lnr-magnifier top_clock"></span>
                                    <div class="search_block rewards">
                                        <form action="">
                                            <input type="search" name="" id="" placeholder="Search here...">
                                            <button type="submit"><span class="lnr lnr-magnifier search"></span></button>
                                        </form>
                                    </div>
                            </li>

                            <li>
                                <span class="notificationAlrt top_clock"><span class="lnr lnr-alarm"></span> <span class="count">5</span></span>
                                <ul class="rewards">
                                    <li><a href="#">Lorem ipsum dolor sit amet </a></li>
                                    <li><a href="#">lorem ipsum</a></li>
                                </ul>
                            </li>

                            <li>
                                    <span class="helloUser top_clock "><span class="lnr lnr-user"></span> Naushad Ali <i class="lnr lnr-chevron-down"></i></span>
                                    <ul class="rewards profileSlide">
                                        <li><a href="#"><span class="lnr lnr-user"></span> Self </a></li>
                                        <li><a href="#"><span class="lnr lnr-user"></span> Madan</a></li>
                                        <li><a href="#"><span class="lnr lnr-user"></span> Pradeep</a></li>
                                        <li><a href="#"><span class="lnr lnr-users"></span>  Add new family member</a></li>
                                        <li><a href="#"> <span class="lnr lnr-exit"></span> Log out</a></li>
                                        
                                    </ul>
                            </li>
                       </ul>

                    </div>
                </div>

                <div class="dashbord_container">
                        <div class="white_box borderTop">
                            <div class="wellnessScore">
                                <div class="wellnesCount">55</div>
                                <div class="wellNessText">
                                    <div class="title">Wellness score 55 (out of 100)</div>
                                    <p>Cool!!! Seems you are doing quite good on your health aspect. Do keep using the portal to 
                                            keep track and further improve on your health.</p>
                                </div>
                            </div>

                        <ul class="scoreList">
                            <li class="gender"><span>Male</span> Gender</li>
                            <li class="age"><span>19</span> Age</li>
                            <li class="bloodGroup"><span>B+</span>Blood Group</li>
                            <li class="bmi"><span>25.33</span>BMI</li>
                            <li class="weight"><span>65 KG</span>Weight</li>
                            <li class="Waistsize"><span>29.03 inch</span>Waist Size</li>
                            <li class="whr"><span>0.88</span>WHR</li>
                        </ul>

                        </div>

                        <div class="starHealthBox">
                            <a href="#" class="items Box1">
                                <span class="lnr lnr-heart"></span>
                                <strong>Start Health Risk <br> Assessment</strong>
                            </a>
                            <a href="#" class="items Box2">
                                    <span class="lnr lnr-pie-chart"></span>
                                    <strong>Track your Health  <br> Parameters</strong>
                            </a>
                            <a href="#" class="items Box3">
                                <span class="lnr lnr-briefcase"></span>
                                <strong>Store your Health <br> Reports</strong>
                            </a>
                            <a href="#" class="items Box4">
                                <span class="lnr lnr-store"></span>
                                <strong>Market <br> Place</strong>
                            </a>
                        </div>

                        <div class="dashBorad_row">
                                <div class="healthSummary">
                                    <div>

                                        <div class="title">
                                            <h2>Health Summary </h2>
                                            <a href="#" class="downloadBtn"><span class="lnr lnr-cloud-download"></span> Download Report</a>
                                        </div>

                                        <div class="tablinks">
                                            <ul>
                                                <li><a href="#WellnessSummary" class="active">Wellness Summary</a></li>
                                                <li><a href="#RiskSummary">Risk Summary</a></li>
                                                <li><a href="#SuggestedEvaluations">Suggested Evaluations</a></li>
                                                <li><a href="#HealthIssues">Health Issues</a></li>
                                                <li><a href="#HealthParameters">Health Parameters</a></li>

                                            </ul>
                                        </div>

                                        <div class="tabContainer">
                                            <div class="tabcon show-tab-container" id="WellnessSummary">
                                                    The following chart depicts your wellness score percentage in each of the following category. If you find that the wellness score percentage is 0% in any category it could be because either you have not answered or you have scored 0 marks in that category.
                                                    <div class="graph_block">
                                                        <img src="../img/graphImg.jpg" alt="">
                                                    </div>
                                            </div>
                                            <div class="tabcon" id="RiskSummary">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laborum, distinctio! Adipisci quis architecto fugit eligendi reiciendis, eveniet libero ea quisquam doloribus possimus ducimus porro aspernatur labore magnam aut obcaecati molestias.</div>
                                            <div class="tabcon" id="SuggestedEvaluations">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates labore delectus fuga, veniam, esse laudantium quos voluptatibus at exercitationem quasi omnis nobis non nostrum. Totam molestiae esse assumenda dolore animi.</div>
                                            <div class="tabcon" id="HealthIssues">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquid laboriosam cumque, aliquam neque repellendus atque harum! Maiores, incidunt. Illum incidunt nobis explicabo, nostrum nesciunt eius maiores consequuntur cum exercitationem magnam?</div>
                                            <div class="tabcon" id="HealthParameters">Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate, atque quod repellendus asperiores non hic vitae in temporibus doloribus unde voluptas doloremque distinctio nulla dolores soluta ipsa aspernatur ipsum velit.</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="suggested_box">
                                        <div>
                                            
                                           <ul class="sugg_icon_div">
                                               <li><a href="#">
                                                    <span class="lnr lnr-magic-wand"></span>
                                                   <span class="link">Suggested (4)</span>
                                               </a></li>

                                               <li>
                                                   <a href="#">
                                                        <span class="lnr lnr-thumbs-up"></span>
                                                        <span class="link">Completed (0)</span>
                                                   </a>
                                               </li>

                                               <li>
                                                   <a href="#">
                                                        <span class="lnr lnr-list"></span>  
                                                       <span class="link">In Progress (0)</span>
                                                   </a>
                                               </li>

                                           </ul> 


                                           <ul id="rightSugTabsLink">
                                               <li>
                                                   <a href="#Suggested" class="active">Suggested (4)</a>
                                               </li>
                                               <li>
                                                   <a href="#Others">Others</a>
                                                </li>
                                           </ul>

                                           <div id="rightSugTabsLinkContainer">
                                               <div class="tabcon-right show-tab-container" id="Suggested">
                                                    <table width="100%">
                                                        <tr>
                                                            <th width="80%" class="text-left">Assessment</th>
                                                            <th width="20%" class="text-left">Risk</th>
                                                        </tr>
                                                        <tr>
                                                            <td>Diabetic</td>
                                                            <td>Low</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Diabetic</td>
                                                            <td>Low</td>
                                                        </tr>

                                                            <tr>
                                                                <td>Diabetic</td>
                                                                <td>Low</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Diabetic</td>
                                                                <td>Low</td>
                                                            </tr>

                                                    </table>
                                               </div>
                                               <div class="tabcon-right" id="Others">
                                                    Others
                                               </div>
                                           </div>


                                        </div>
                                </div>

                        </div>

                    </div>

                    <div id="dashBoradFooter">
                            © 2018 Mediwheel
                    </div>

                </div><!--end Right Section-->

            </div>

    </div>

    
    



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    <script>
        $(document).ready(function(){
            $("ul.navigation").mCustomScrollbar({
                scrollButtons:{enable:true},
                theme:"light-thick",
                scrollbarPosition:"outside"
            });
        })
    </script>
</body>
</html>