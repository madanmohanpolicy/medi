
<?php 
 $url=base_url();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Health checkup reports </title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/custom.css" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
        <header class="header">
                <a href="http://www.mediwheel.in/"> <img src="img/logo.png" alt="" class="logo"> </a>
                <div class="container">Health checkup reports <a href="http://www.mediwheel.in/" class="homebtn"><i class="fa fa-home" aria-hidden="true"></i> Home</a></div>
        </header>
        
        

        <div class="container" class="form_block">
                <h1 style="text-align:left">Reports</h1>
                <div class="tabs">
                        <a href="index">Health checkup packages</a>
                        <a href="<?php echo $url;?>health/reportUpload">Upload Reports</a>
                        <a href="#" class="active">View Reports</a>
                    </div>

                    <div class="report_list">
						
					 <?php if($reportViewInfo){
						 foreach($reportViewInfo as $value){ ?>
							 <div class="report">
							<a href="<?php echo $value->report_path;?>" target="_blank" class="blueColor"><?php echo basename($value->comment);?></a></br>  <?php echo $value->report_upload_date;?> </div>  
						<?php  }
						 
						 
					 }?>
                        
                   
					</div>
        </div>

    </div>


    <div id="popUpModal">
        <div class="modal-body">
                <button class="close">X</button>
                Thanks for selecting the packages with us. Our reperesentative will contact you soon.
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>