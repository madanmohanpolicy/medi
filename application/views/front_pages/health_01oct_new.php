
			<?php
			
			if(!$this->session->is_logged_in)
			{ 
				$data = array(
				'front_goto_page' => "health"
				);
				$this->session->set_userdata($data);
				?>
				<input type="hidden" name="isloggedin" id="isloggedin" value="0">
			<?php	
			}
			else
			{?>
				<input type="hidden" name="isloggedin" id="isloggedin" value="1">
			<?php	
				
			}

	//print_r($this->session);
	//exit;

             $userId=$this->session->front_id;
			 $userName=$this->session->front_name;
             $url=base_url();

           // echo $url;
			//print_r($healthPackageRecords);
			
			//print_r($healthPackageschedule);

			?>
<input type="hidden" name="bookingdate" id="bookingdate" value="">
<input type="hidden" name="id_p" id="id_p" value="">
<input type="hidden" name="id_d" id="id_d" value="">
<input type="hidden" name="main_div_id" id="main_div_id" value="">
			<div class="wrapper">        
            
			<div class="container">
                <h1>Available Health checkup packages for members</h1>

                <div class="tabs">
                    <a href="" class="active">Health checkup packages</a>
					<?php if($this->session->is_logged_in){ ?>
                    <a href="<?php echo $url;?>health/reportUpload">Upload Reports</a>
                    <a href="<?php echo $url;?>health/reportView">View Reports</a>
					<?php } ?>
                </div>
				<?php 
				if(!$healthPackageRecords){
				 echo "No Records Found";
			 }
             

			 ?>
               
            <div class="pack_wrapper">
			
			        <?php 
					
					$main_div = 1;
					$main_id = 0;
					if($healthPackageRecords){
					foreach($healthPackageRecords as $value )
					{ 
						
						if($this->session->mdi == $main_div)
						{
							$style = 'style="display:block;"';
						}
						else
						{
							$style = 'style="display:none;"';
						}
					
					$testids=$value->tests;
					$testsaidarray= explode(',',$testids);
					$testcount=sizeof($testsaidarray);
					
					$slot=$value->time_slot;
					$pkgId=$value->PackageId;
					//$hosid=$value->hospitalId;
					$hosid=$value->id;
					$time24=$value->time_24_hours;
					$hospitaltimefrom=$value->timefrom;
					$hospitaltimeto=$value->timeto;
					$r=0;
					 $testparameterCounttotal=0;
					for($w=0;$w<$testcount;$w++){
						$testscnt = $this->healthpackage_model->getHealthCheckupTestInfo($testsaidarray[$w]); 
						if($testscnt){
							
							 $testparameterCounttotal+=$this->healthpackage_model->getHealthCheckupTestParameterCount($testscnt[0]->id);
							$r++;
							
						}
					}
					//echo $pkgId.'<br>';
					$healthPackageschedule = $this->healthpackage_model->getHealthPackageSchedule($pkgId,$hosid);
				   
					
			     /* if(sizeof($healthPackageschedule)){
				for($m=0;$m<sizeof($healthPackageschedule);$m++){
					$starttime=strtotime($healthPackageschedule[$m]->timefrom);
					
					$endtime=strtotime($healthPackageschedule[$m]->timeto);
			    
					$slot=30;
				
                  $timediff=round(abs($starttime - $endtime) / 60,2);
				 
				  
				  
				  $total_slot=floor($timediff/$slot);
				 
				  }}*/
					//$timediff=strtotime(date_diff($endtime-$starttime))/60;
					//echo $timediff;
					//print_r($healthPackageschedule);
					?>
					
                    <div class="packbox">
                        <div class="titleblock">
                                <div class="title greenColor"><?php echo $value->name;?>
                                    <span>by : <?php echo $value->organisation;?></span> 
                                    <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> <?php echo $value->location .', '.$value->city;?> </span> 
                                    <!--<span><i class="fa fa-phone magentaColor" aria-hidden="true"></i>  <?php //echo $value->landline.', '. $value->mobile;?></span>-->
                                    <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> <?php if($time24){echo "24*7";} else { echo $hospitaltimefrom." - ".$hospitaltimeto;}?></span>
                                </div>
								
								<div class="test_count_block">
									<a href="javascript:void(0)"><?php if($r)echo $r.' Test';?></a>
								</div>
								
                                <div class="price_block">
                                        <div class="price">
										   <?php  if($value->member_discount_inpercent&&$value->memberDiscount>0){ ?>
					<div class="offertag"><?php   echo $value->memberDiscount.'% off'; ?></div> <?php } ?>
					<?php if($value->memberDiscount>0){?>
					<span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  <?php  echo $value->cost;?></span><?php } ?>
											<?php  if($value->member_discount_inpercent){$discount=($value->cost*$value->memberDiscount)/100; 
											$actualCost=($value->cost-$discount);}
											if(!$value->member_discount_inpercent){
												$actualCost=$value->cost-$value->memberDiscount;
											}
											 ?>
                                            <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i> <?php  if($actualCost>0) echo $actualCost;?></span>                                   </span>
                                        </div>
                                        <a href="javascript:void(0)" class="bookapoinment" onclick="keep_id(<?php echo $main_div;?>)">Book your appointment</a>
                                </div>
                               
                                <div id="bkap_wrap_$main_div" class="bkap_wrap" <?php echo $style; ?>>
											<div class="bkp_body">
												<div class="close">X</div>
                                        <strong>
										<?php if($healthPackageschedule){
											
										echo "Select your preferred appointment  date and time";
										}
										else
										{
											echo "No Appointment is available for this date.";
										}
										?>
										</strong>

                                        <div class="date-list owl-carousel">
										
										<?php 
								$main_div++;
										$style = "";
										for($i=0;$i<=9;$i++) {
											
										if($i!=2)
										{
											$style = "style='display:none';";
										}
										else
										{
											$style = "";
										}
                                         $NewDate=Date('d M', strtotime("+$i days"));
										 if($healthPackageschedule){
											 
											 $countschedule=0;
												$days='';
												$dayname=Date('D', strtotime("+$i days"));
												
												
													$avldays=explode(',',substr($days,0,-1));
											 
											 foreach($healthPackageschedule as $rec){
													$days.=substr($rec->day,0,3).',';
													
												if(substr($rec->day,0,3)==$dayname){
													
													$countschedule++;										
													}}
										
											
											if($i<2)
											{
												$my_style = 'class="item booking_closed"'; 											
											}
											else if($countschedule==0&& $i>1)
											{											 
												 $my_style = 'class="item app_not_available"'; 
											}
											else
											{
												$my_style = 'class="item nothing"'; 													
											}
										 	
											?>
                                            
											<div  <?php echo $my_style; ?>  onclick="show_slot('<?php echo $i;?>','<?php echo $value->id;?>','<?php echo $NewDate;?>');">
                                                <div class="dateBox">
												<?php 
												
												if($i<2){
													
													echo "<font color='black'> Booking Closed</font>";
													
												}
											      else if($countschedule==0&& $i>1){
													  $myclass = "app_not_available";
													  echo "<font color='red'>Appointment Not Available</font>";
												  }
												 

												  ?>
                                                    <span class="day"><?php echo $dayname; ?></span>
                                                    <span class="date"><?php echo $NewDate;?></span>
                                                </div>
												
                                            </div>
											
										
													   <?php }
													 ?>
														   
													  <?php  
													   
													   }   ?>
                                        </div>
										
										
										<?php
										
					
				//print_r($rec);*/
			
                   					
											//echo $dayname;
											

                                             
											  
												//$new_time = date("H:i:s", strtotime('+30 minutes', strtotime($stime))); 
												//echo 'time-'.$new_time;
										//	
										$style2 = "";
	                                    for($i=0;$i<=9;$i++) {
											
											
										if($i!=2)
										{
											$style2 = "style='display:none';";
										}
										else
										{
											$style2 = "";
										}										//if($countschedule>0){?>			
										 
											<ul class="time_ul" id="<?php echo $value->id?>_slot_<?php echo $i;?>" <?php echo $style2;?> style='display:none'; >
											<?php 
											 
											     for($k=0;$k<sizeof($healthPackageschedule);$k++){
													
													 
													 $starttime=strtotime($healthPackageschedule[$k]->timefrom);
					
					$endtime=strtotime($healthPackageschedule[$k]->timeto);
			    if(!$value->time_slot){
					$slot=30;
				}
                  $timediff=round(abs($starttime - $endtime) / 60,2);
				 
				
				  if($slot){
					 
				  $total_slot=floor($timediff/$slot);
				  }                                     $dayname=Date('D', strtotime("+$i days"));
														if($dayname==substr($healthPackageschedule[$k]->day,0,3)){ 
														$dnew=date('d M', strtotime("+$i days"));
										  $timealreadyset=array();
										$this->load->model('healthpackage_model');
	                $rec= $this->healthpackage_model->ifBookedTime($pkgId,$hosid,$dnew);
					$t=0;
					foreach($rec as $rec1){
						$timealreadyset[$t]=$rec1->appointment_time;
						$t++;
					}
											 $stime=$healthPackageschedule[$k]->timefrom;
											 $stimenew='';
											
											
											
											for($j=0;$j<$total_slot;$j++)
											{ 
										if( $stimenew==''){
											
											$stimenew=$stime;
										}
										else {
											$stimenew= date("H:i:s", strtotime('+'.$slot.'  minutes', strtotime($stimenew)));
										}
										   
											
											?>
                                             
												
												<li><label for="1"><input type="radio" name="title_"<?php echo $j;?> id="title_"<?php echo $j;?> onclick="show_confirmation_div('<?php echo $value->name;?>','<?php echo $pkgId;?>','<?php echo $value->organisation;?>','<?php echo $value->cost;?>','<?php echo $value->memberDiscount;?>','<?php echo $actualCost;?>','<?php echo $NewDate;?>','<?php echo date("h:i:A", strtotime($stimenew));?>','<?php echo $userId;?>','<?php echo $userName;?>','  <?php echo $url;?>','<?php echo $hosid;?>' ,'<?php echo $main_id;?>');"    <?php if(in_array(date("h:i:A", strtotime($stimenew)),$timealreadyset)) echo "disabled";?>><?php echo date("h:i:A", strtotime($stimenew));?></label></li>
                                                
														<?php }}?>
											
												
												<?php  } ?>
											</ul>
										<?php } ?>
										
										<div class="doyouwant" id="confirmation_div_id_<?php echo $main_id;?>" style="display:none;">
											<div id="confirmation_div_id_val_<?php echo $main_id;?>" style="display:inline-block; font-weight:600; color:#0c75c0;"></div>
											<a href="javascript:void(0)" class="bookNowBTN" onclick="booking()"; style="display:inline-block">Book Now</a>
											
										</div>
											</div>
                                        
                                </div>
								
								
								
                             <div class="testdetails_wrap">
								<div class="inside_body">
									<div class="close">X</div>
									<div class="popupDis"></div>
									<ul class="discription_ul">
										
										<li class="bigfont">Tests <span><?php echo $testparameterCounttotal;?></span></li>
										<?php 
									
                                          for($u=0;$u<sizeof($testsaidarray);$u++){
                                      
										$testinfofront = $this->healthpackage_model->getHealthCheckupTestInfo($testsaidarray[$u]); 
										  if($testinfofront){
											  
											   $testparametername=$this->healthpackage_model->getHealthCheckupTestParameterInfo( $testinfofront[0]->id);
								               $testparameterCount=$this->healthpackage_model->getHealthCheckupTestParameterCount($testinfofront[0]->id);
									
										//print_r($testparametername);
									
										?>
										
										<li><?php echo  $testinfofront[0]->name;?><span><?php if($testparameterCount){ echo $testparameterCount;}?></span>
										    <?php if($testparametername) {?>
											<ul class="test_detailsul">
											
												<?php foreach($testparametername as $parametervalue){
													
													?>
											<li><?php echo $parametervalue->parameterName;  } ?>
											</li>
												
											</ul><?php }?>
										</li>
										  <?php } } ?>
									</ul>
								</div>
							 </div>
                        </div>
                        
                    </div><!--End Pack One-->

                    <?php 
					$main_id++;
					} }
					$this->session->mdi = "";
					unset($this->session->mdi);
					?>
											<input type="hidden" id="packagename" value="">
											<input type="hidden" id="packageId" value="">
											<input type="hidden" id="organisation" value="">
											<input type="hidden" id="cost" value="">
											<input type="hidden" id="discount" value="">
											<input type="hidden" id="costafterdiscount" value="">
											<input type="hidden" id="appointmentdate" value="">
											<input type="hidden" id="appointmenttime" value="">
											<input type="hidden" id="userId" value="">
											<input type="hidden" id="userName" value="">
											<input type="hidden" id="url1" value="">
											<input type="hidden" id="organisationid" value="">       
                           
                        
            
            </div>

            
        </div>
    </div>
	
	<div id="msgAfterhealthcheckup">
		<div class="modal-body">
			<div class="close">X</div>
			<div id="showresmsg"></div>
			
		</div>
	</div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<?php

$pradeep = 0;
if($pradeep == 1)
{
?>	
<script>
		$(document).ready(function(){
		   setTimeout(function(){
			 $('#msgAfterhealthcheckup').addClass('open');
				$('#msgAfterhealthcheckup .modal-body').addClass('slideInDown');
				$('.bkap_wrap').hide(); 
		   }, 1500);
		});
	</script>
<?php
}
?>
<script>

function show_slot(id,uid,ndate)
{
	document.getElementById('bookingdate').value=ndate;
	document.getElementById('id_p').value=id;
	document.getElementById('id_d').value=uid;
	var sid = uid+"_slot_"+id;
	//alert(sid);
	//alert(document.getElementById(sid).style.display);
	
	if(document.getElementById(sid).style.display == "none")
	{
		//alert(sid);
		if(document.getElementById(sid))
		{
			//alert("ssss");
			document.getElementById(sid).style.display="block";
		}
		else
		{
			//alert("div is not there");
		}
	
	
		for(var i=2; i<=9; i++)
		{
			
			if(i != id)
			{
				
				var sid = uid+"_slot_"+i;
				//alert(sid);
				if(document.getElementById(sid))
				{
				document.getElementById(sid).style.display = "none";
				}
			}
		}
	}
	
	
	
}

//show_slot('6','6','04 Oct');

/*
function booking(packagename,organisation,cost,discount,costafterdiscount,appointmentdate,appointmenttime,userId,userName){ 


alert(packagename+','+organisation+','+cost+','+discount+','+','+costafterdiscount+','+appointmentdate+','+appointmenttime+','+userId+','+userName);


	$.ajax({
		
			type : "POST",
			dataType : "json",
			url : 'http://localhost/mediwheel/health/booking',
			data : { userId : 1 }
			
			}).done(function(data){
				alert(data);
				console.log(data);
				
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});

};*/


function show_confirmation_div(packagename,packageId,organisation,cost,discount,costafterdiscount,appointmentdate,appointmenttime,userId,userName,url1,organisationid,confirmation_div_id)
{
	var appointmentdate=document.getElementById('bookingdate').value;
	
	document.getElementById('packagename').value =packagename;
	document.getElementById('packageId').value =packageId;
	document.getElementById('organisation').value =organisation;
	document.getElementById('cost').value =cost;
	document.getElementById('discount').value =discount;
	document.getElementById('costafterdiscount').value =costafterdiscount;
	document.getElementById('appointmentdate').value =appointmentdate;
	document.getElementById('appointmenttime').value =appointmenttime;
	document.getElementById('userId').value =userId;
	document.getElementById('userName').value =userName;
	document.getElementById('url1').value =url1;
	document.getElementById('organisationid').value =organisationid;
	
	//alert(appointmentdate);
	var mainid = "confirmation_div_id_"+confirmation_div_id;
	var mainidval = "confirmation_div_id_val_"+confirmation_div_id;
	
	document.getElementById(mainidval).innerHTML= "Are you sure to book this Health Checkup Package on "+appointmentdate+" at "+ appointmenttime; 
	if(document.getElementById(mainid).style.display=="none")
	{
		document.getElementById(mainid).style.display="block";
	}
	else{
		document.getElementById(mainid).style.display=="none";
	}
}



function booking()
{	
	var packagename = document.getElementById('packagename').value;
	var packageId = document.getElementById('packageId').value;
	var organisation = document.getElementById('organisation').value;
	var cost = document.getElementById('cost').value;
	var discount = document.getElementById('discount').value;
	var costafterdiscount = document.getElementById('costafterdiscount').value;
	var appointmentdate = document.getElementById('appointmentdate').value;
	var appointmenttime = document.getElementById('appointmenttime').value;
	var userId = document.getElementById('userId').value;
	var userName = document.getElementById('userName').value;
	var url1 = document.getElementById('url1').value;
	var organisationid = document.getElementById('organisationid').value;
	var main_div_id = document.getElementById('main_div_id').value;
	
	var id_p = document.getElementById('id_p').value;
	
	var id_d = document.getElementById('id_d').value;
	
	var isloggedin	= document.getElementById("isloggedin").value;
	if(isloggedin == 0)
	{
		document.location.href = url1+"buyer/login_view/?packageId="+packageId+"&appointmentdate="+appointmentdate+"&appointmenttime="+appointmenttime+"&mdi="+main_div_id+"&id_p="+id_p+"&id_d="+id_d;
		return true;
	}
	else
	{
//var confirmation=confirm("Are you sure to book this appointment on "+appointmentdate+" at "+ appointmenttime );	
//alert(packagename+','+organisation+','+cost+','+discount+','+costafterdiscount+','+appointmentdate+','+appointmenttime+','+userId+','+userName+','+url1);	

	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null) {
		alert ("Browser does not support HTTP Request")
		return;
	}
	var Url=url1;

	var url=Url+"health/booking?package="+packagename+"&packageId="+packageId+"&organisation="+organisation+"&cost="+cost+"&discount="+discount+"&costafterdiscount="+costafterdiscount+"&appointmentdate="+appointmentdate+"&appointmenttime="+appointmenttime+"&userId="+userId+"&userName="+userName+"&organisationid="+organisationid;	

	xmlHttp.onreadystatechange=stateChangedPradeep;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);	

	}
}

function stateChangedPradeep()
{	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var response=xmlHttp.responseText;	
		//document.getElementById("showLike").innerHTML = response;	
		//alert(response);	
		show_book_confirmation(response);
	}
	else
	{
		show_book_waiting();
	}
}

function show_book_confirmation(response){
		document.getElementById("showresmsg").innerHTML = '';
		document.getElementById("showresmsg").innerHTML = response;	
		$('#msgAfterhealthcheckup').addClass('open');
		$('#msgAfterhealthcheckup .modal-body').addClass('slideInDown');
		$('.bkap_wrap').hide();
		
	}
	
function show_book_waiting(){
		document.getElementById("showresmsg").innerHTML = '';
		document.getElementById("showresmsg").innerHTML = '<img src="<?php echo base_url();?>assets/img/load.gif">';	
		$('#msgAfterhealthcheckup').addClass('open');
		$('#msgAfterhealthcheckup .modal-body').addClass('slideInDown');
		$('.bkap_wrap').hide();
		
	}

//creating xml object for ajax
function GetXmlHttpObject() {
	var xmlHttp=null;
        try {
		xmlHttp=new XMLHttpRequest();
	}
        catch (e) {
		try {
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
        return xmlHttp;
}
		
		

function keep_id(id)
{
	document.getElementById('main_div_id').value =id;
}


</script>