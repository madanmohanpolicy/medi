 <footer class="footer">
        <div class="container">
            <div class="ftr_left">
                <div class="frt_item">
                    <strong><a href="<?php echo base_url(); ?>health"> Health</a></strong>
                  <!--  <ul>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                    </ul>-->
                </div>

                <div class="frt_item">
                    <strong>HOSPITALIZATION</strong>
                   <!-- <ul>
                        <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                        <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                        <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                        <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                        <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                        <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                    </ul>-->
                </div>

                <div class="frt_item">
                    <strong><a href="<?php echo base_url(); ?>buyer/login_view/"> Sign IN</a></strong>
                   <!-- <ul>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                            <li><a href="javascript:void(0)">Lorem ipsum dolor</a></li>
                    </ul>-->
                </div>
                
                <div class="frt_item">
                    <strong><a href="<?php echo base_url(); ?>home/about"> About Us</a></strong>
                 <p class="ftraboutdata">
					The goal of our organization is to have continuum of care for the individuals, one which is connected and integrated on all levels. We are imparting personalized care to the individual by covering all the aspects of healthcare management.
						<a href="<?php echo base_url(); ?>home/about" style="color:yellow"> read more... </a>

				 </p>
                </div>

            </div>

            <div class="ftr_right">
                <div class="title">
                        Download Mediwheel App Now!
                </div>

                <div class="appstoreimg">
                    <a href="http://tiny.cc/nv1hvy"  target="_blank"><img src="<?php echo base_url(); ?>assets/img/google_store.png" alt=""></a>
                    <a href="http://tiny.cc/9v1hvy"  target="_blank"><img src="<?php echo base_url(); ?>assets/img/itune.png" alt=""></a>
                </div>

                <ul class="socilftr">
                        <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <!--   <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>-->
                </ul>

            </div>

        </div>


        <div class="copyright">
            <div class="container">
                <div class="copy-left">© 2018-2019, Mediwheel Healthcare Services Ltd.</div>
                <div class="copy-right"><a href="<?php echo base_url(); ?>term">Terms of use</a>    |    <a href="<?php echo base_url(); ?>term/legal">Legal</a></div>
            </div>
            <img src="<?php echo base_url(); ?>assets/img/gotop.png" alt="" id="gotop">
        </div>

    </footer>

    
    
    












    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?php echo base_url(); ?>assets/fancybox/jquery.fancybox.js"></script>
<script>
AOS.init();
</script>
	
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>