<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $page_title; ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet"> 
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
	
</head>
<body>
    
    <header class="header">
        <div class="top_header">
            <div class="container">
                <div class="left_top">
                    <ul>
                        <li>
                            <a href="tel:+911139001234"><i class="fa fa-phone" aria-hidden="true"></i> +91 11 3900 1234</a>
                        </li>
                        <li>
                            <a href="mailto:info@mediwheel.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@mediwheel.com</a>
                        </li>
                    </ul>
                </div>
                <div class="right_top">
                    <ul>
                        <li>
                            <a href="#"><i class="fa fa-info" aria-hidden="true"></i> Get a Quote</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Need A Agent</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!--end Top Links-->

        <div class="logo_block">
            <div class="container">
                <a href="" class="logo">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="">
                </a>
                <ul class="top_nav">
                  <!--  <li><a href="#"><i class="fa fa-id-card-o" aria-hidden="true"></i> E-card</a></li>         
                    <li><a href="#"><i class="fa fa-file-text" aria-hidden="true"></i> Claims</a></li>         
                    <li><a href="#"><i class="fa fa-file-o" aria-hidden="true"></i> Plan hospitalization</a></li>         
                    <li><a href="#"><i class="fa fa-sitemap" aria-hidden="true"></i> Network hospitals</a></li>-->
					
					<?php if($this->session->userdata('is_logged_in')){
                           ?>
						<li><a href="<?php echo base_url(); ?>buyer/user_logout"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Log Out</a></li>
                   
					<?php }else{  ?>
					 <li><a href="<?php echo base_url(); ?>buyer/login_view/"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Login</a></li>
					<li><a href="<?php echo base_url(); ?>buyer/"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Sign Up</a></li>
					<?php }?>
					<li class="cartDetails_li">
                        <a href="javascript:void(0)" class="cartDetails"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="count">20</span></a>
                        
                        <div class="itemsummyBox">
                            <div class="itemSummry">
                                    Order Summary <span>20 Item(s)</span>
                            </div>

                            <ul class="itemsList">
                                <li><a href="">Shelcal -500 Tablet <span>₹ 82.00</span></a></li>
                                <li><a href="">Shelcal -500 Tablet <span>₹ 82.00</span></a></li>                          
                               
                            </ul>

                            <div class="proccedtoCheck">
                                <a href="javascript:void(0)">PROCEED TO CART <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>

                        </div>

                    </li>
                </ul>
            </div>
        </div><!--end Logo and Links-->
    </header><!--End Header-->


  