   
    <section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="myaccount_wrapper">
            <div class="container">
                <div class="loginForm">
                        <div class="title">Create Your Account</div>
						<?php  
						if( $this->session->flashdata('success_msg') == "User Registered Successfully")
						{
							//echo "Thanks for doing registration with us. Your login id is your email and the password has been sent to your email. Requesting you to please login your account to avail the services.";
						
						}
						else
						{
							echo $this->session->flashdata('success_msg');
						?>
						<br>
                    <form action="<?php echo base_url(); ?>buyer/register_buyer" id="form_main" name="form_main" method="get" onsubmit="return validateForm(this);">
                        <div class="form-group">
                            <label for="name">Name</label> <div class="error" id="name_error"></div>
                            <input type="text" name="name" id="name" placeholder="Enter Your Name Here"  maxlength="70"  >
                        </div>
                        <div class="form-group">
                            <label for="number">Mobile Number</label><div class="error" id="mobile_error"></div>
                            <input type="tel" name="mobile" id="mobile" placeholder="Enter Your Mobile Number"  maxlength="10">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label><div class="error" id="email_error"></div>
                            <input type="email" name="email" id="email" placeholder="Enter Your Email" maxlength="50">
                        </div>                        

                       <!-- <div class="form-group">
                            <label for="checkbox" class="checkbox"><input type="checkbox" id="checkbox"> Yes, I am a doctor</label>
                        </div> -->

                        <div class="form-group">
                            <button class="btn_red"  type="submit" value="Register" name="register" >Sign Up</button>
                        </div>

                        <div class="form-group dontHave">
                                Already have an account? <a href="<?php echo base_url(); ?>buyer/login_view">Login</a>
                        </div>

                        <div class="form-group loginProblem">
                           By signing up, you agree to our <a href="<?php echo base_url(); ?>term">Terms and Conditions</a>
                        </div>

                    </form>
					<?php
						}
					?>
                </div>
            </div>
        </div>

        

    </section>