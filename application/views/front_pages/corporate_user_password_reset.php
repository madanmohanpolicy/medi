<?php 

if($front_corporate_user_otp){
	
$otp=$front_corporate_user_otp;
$skey=$front_corporate_user_secretkey;



?>
<script>

function checkResetPass(otpnew)
{
	var otpnew;
	
	// Check name field			
	var npassword = $('#npassword').val().trim();
	var cpassword = $('#cpassword').val().trim();
	var otp=$('#otp').val().trim();
	
	// Check email field
	if(npassword == "")
	{
		$('#email_error').html("Please Enter Your New Password");
		$('#npassword').focus();
		
		return false;
	}
	else if(otp == "")
	{
		$('#email_error3').html("Please Enter Your OTP");
		$('#otp').focus();
		
		return false;
	}
	else if(cpassword == "")
	{
		$('#email_error2').html("Please Enter Your Confirm Password");
		$('#cpassword').focus();
		
		return false;
	}
	else if(cpassword != npassword)
	{
		$('#email_error').html("New Password and Confirm Password should be same");
		$('#cpassword').focus();
		
		return false;
	}
	
	else if(otp != otpnew)
	{
		$('#email_error3').html("Wrong OTP");
		$('#otp').focus();
		
		return false;
	}
	else
	{
		$('#email_error').html("");
		return true;
	}
	
	
}
</script>
<section class="inner_body_wrapper" style="min-height: 400px;">
        
        <div class="myaccount_wrapper">
            <div class="container">
			
                <div class="loginForm">
				
                     <div class="title">Create Password</div>
						   
								<?php
								if($this->session->flashdata('success_msg')!="")
								{
								echo $this->session->flashdata('success_msg'); 
								}			
								?> 
						
						
						
					
                    <form action="corporate/createPassword" id="form_main" method="post" onsubmit="return checkResetPass('<?php if(isset($otp)) echo $otp;?>');">
						<input type = "hidden" name="skey" value="<?php  if(isset($skey)) echo $skey; ?>" >
                        <div class="form-group">
                            <label for="username">New Password</label><div class="error" id="email_error" ></div>
                            <input type="password" name="npassword"   id="npassword" maxlength="20" >				 
						</div>
						
						<div class="form-group">
                            <label for="username">Confirm password</label><div class="error" id="email_error2"></div>
                            <input type="password" name="cpassword"  id="cpassword" maxlength="20">			 
						</div>
                     
									<div class="form-group">
                            <label for="otp">OTP</label><div class="error" id="email_error3"></div>
                            <input type="password" name="otp"  id="otp" maxlength="20">			 
						</div>
                        <div class="form-group" id="contact_submit">
						
                            
							<input class="btn_red" type="submit" name="create_form" id="create_form" value="Create Password" />
                        </div>

                        

                        

                    </form>
                </div>
            </div>
        </div>

	</section>
<?php } 
else {
	echo "You have already created your password. Please"."<a href='buyer/login_view/'> Login</a>";
	
} 
?>
