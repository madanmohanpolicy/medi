<?php 
 $url=base_url();
if($this->session->flashdata('report_upload_success_msg')){
	
	echo $this->session->flashdata('report_upload_success_msg');
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Health checkup reports </title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?php echo $url;?>assets/css/custom.css" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
        <header class="header">
                <a href="http://www.mediwheel.in/"> <img src="img/logo.png" alt="" class="logo"> </a>
                <div class="container">Health checkup reports <a href="http://www.mediwheel.in/" class="homebtn"><i class="fa fa-home" aria-hidden="true"></i> Home</a></div>
        </header>
        
        

        <div class="container" class="form_block">
                <h1 style="text-align:left">Upload your report</h1>
				
                <div class="tabs">
                        <a href="index">Health checkup packages</a>
                        <a href="#" class="active">Upload Reports</a>
                        <a href="<?php echo $url;?>health/reportView">View Reports</a>
                    </div>
					

					
					
            <form action="reportUploads" method="post" name="reportform"  class="report_upload" enctype="multipart/form-data">
                <div class="form-group" >
                    <label for="" style="display:inline-block; min-width:100px;">Select File</label>
                    <input type="file" name="report" id="report" required>
                </div>
                <div class="form-group" class="form_block">
                    <label for="">Report Name</label>
                    <input name="comment" id="comment" type="text" class="form-group" required ></textarea>
                </div>

                <button class="bookapoinment" style="margin:0" onclick="uploadreport( <?php echo $url;?>);">Submit</button>
				 
            </form>
            
        </div>

    </div>
     

    <div id="popUpModal">
        <div class="modal-body">
                <button class="close">X</button>
                Thanks for selecting the packages with us. Our reperesentative will contact you soon.
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	
	$('INPUT[type="file"]').change(function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
       
        case 'pdf':
        case 'jpg':
		case 'jpeg':
		 
            $('#report').attr('disabled', false);
            break;
        default:
            alert('Please upload pdf file');
            this.value = '';
    }
	
	
	var file_size = $('#report')[0].files[0].size;
	if(file_size>2000000) {
		alert('Please Choose Small Size Pdf');
		return false;
	} 
});




function uploadreport(url1)
{
	
//var confirmation=confirm("Are you sure to upload this report);	
//alert(packagename+','+organisation+','+cost+','+discount+','+costafterdiscount+','+appointmentdate+','+appointmenttime+','+userId+','+userName+','+url1);

alert(url1);

var fullPath = document.getElementById('report').value;
if (fullPath) {
    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
    var filename = fullPath.substring(startIndex);
    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
    }
   
}


	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null) {
		alert ("Browser does not support HTTP Request")
		return;
	}
	var Url=url1;
    var comment= document.getElementById('comment');
	var url=Url+"health/uploads?filename="+ filename+"&comment="+comment;	
alert(url);
	xmlHttp.onreadystatechange=stateChangedPradeep;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);	


function stateChangedPradeep()
{	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var response=xmlHttp.responseText;	
		//document.getElementById("showLike").innerHTML = response;	
	alert(response);		
	}
}


//creating xml object for ajax
function GetXmlHttpObject() {
	var xmlHttp=null;
        try {
		xmlHttp=new XMLHttpRequest();
	}
        catch (e) {
		try {
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
        return xmlHttp;
}
}

</script>
    <script src="js/custom.js"></script>
</body>
</html>