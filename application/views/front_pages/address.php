<section class="inner_body_wrapper" style="min-height: 400px;">

    <div class="cart_wrapper">
        <div class="container">
            <div class="title">Add New Address</div>

            <div class="cart_block">
                <div class="left">
                    <form action="saveCartAddress" id="form_main" name="form_main" method="post"
                          onsubmit="return validateAddressForm(this);" class="cart-address">

                        <input type="hidden" value="<?php echo $address['id']; ?>" name="id" id="id" />

                        <div class="form-group">
                            <label for="address">Address</label>
                            <div class="error" id="address_error"></div>
                            <textarea name="address" id="address" cols="30" rows="4"
                                      placeholder="Flat Number, Building Name, Street/Locality"><?php echo $address['address']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <input type="text" name="landmark" value="<?php echo $address['landmark']; ?>" id="landmark" placeholder="Enter Your Landmark"
                                   maxlength="70">
                        </div>
                        <div class="form-group">
                            <label for="pincode">Pincode</label>
                            <div class="error" id="pincode_error"></div>
                            <input type="text" name="pincode" value="<?php echo $address['pincode']; ?>" id="pincode" maxlength="6" placeholder="Enter Your Pincode"
                                   maxlength="70">
                        </div>
                        <div class="form-group">
                            <label for="locality">Locality</label>
                            <input type="text" name="locality" value="<?php echo $address['locality']; ?>" id="locality" placeholder="Enter Your locality Here"
                                   maxlength="70">
                        </div>
                        <div class="form-group">
                            <label for="city">City </label>
                            <div class="error" id="city_error"></div>
                            <input type="text" name="city" value="<?php echo $address['city']; ?>" id="city" placeholder="Enter Your City Here" maxlength="70">
                        </div>
                        <div class="form-group">
                            <label for="state">State</label>
                            <div class="error" id="state_error"></div>
                            <input type="text" name="state" value="<?php echo $address['state']; ?>" id="state" placeholder="State" maxlength="70">
                        </div>
                        <div class="form-group">
                            <label for="customer_name">Customer Name</label>
                            <div class="error" id="customer_name_error"></div>
                            <input type="text" name="customer_name" value="<?php echo $address['customer_name']; ?>" id="customer_name"
                                   placeholder="Enter Customer Name Here" maxlength="70">
                        </div>
                        <div class="form-group">
                            <label for="customer_mobile">Customer Mobile Number</label>
                            <div class="error" id="customer_mobile_error"></div>
                            <input type="tel" name="customer_mobile" value="<?php echo $address['customer_mobile']; ?>" id="customer_mobile"
                                   placeholder="Enter Customer Mobile Number" maxlength="10">
                        </div>
                        <div class="form-group radio_input">
                            <div class="error" id="address_type_error"></div>
                            <label for="home_ra"><input type="radio" name="address_type" <?php if($address['address_type']=='Home' ) echo "checked"  ;?> id="address_type" value="Home">
                                Home</label>
                            <label for="office_ra"><input type="radio" name="address_type" <?php if($address['address_type']=='Office' ) echo "checked"  ;?> id="address_type"
                                                          value="Office"> Office</label>
                            <label for="other_ra"><input type="radio" name="address_type" <?php if($address['address_type']=='Other' ) echo "checked"  ;?> id="address_type"
                                                         value="Other"> Other</label>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn_green">Save</button>
                            <button type="reset" class="btn_red">Reset</button>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div>

</section>

<script type="text/javascript">
    function validateAddressForm(frm) {

        var address = $('#address').val().trim();
        var pincode = $('#pincode').val().trim();
        var city = $('#city').val().trim();
        var state = $('#state').val().trim();
        var customer_name = $('#customer_name').val().trim();
        var customer_mobile = $('#customer_mobile').val().trim();
        var address_type = $('#address_type').val().trim();

        if (address == "") {
            $('#address_error').html("Please Enter Address");
            $('#address').focus();
            return false;
        } else {
            $('#address_error').html("");
        }

        if (pincode == "") {
            $('#pincode_error').html("Please Enter Pincode");
            $('#pincode').focus();
            return false;
        } else if (!($.isNumeric(pincode))) {
            $('#pincode_error').html("Please Enter Correct Pincode");
            $('#pincode').focus();
            return false;
        } else if (pincode.length < 6) {
            $('#pincode_error').html("Pincode Must be in 6 Digit");
            $('#pincode').focus();
            return false;
        } else {
            $('#pincode_error').html("");
        }

        if (city == "") {
            $('#city_error').html("Please Enter City");
            $('#city').focus();
            return false;
        } else {
            $('#city_error').html("");
        }

        if (state == "") {
            $('#state_error').html("Please Enter State");
            $('#state').focus();
            return false;
        } else {
            $('#state_error').html("");
        }

        if (customer_name == "") {
            $('#customer_name_error').html("Please Enter Customer Name");
            $('#customer_name').focus();
            return false;
        } else {
            $('#customer_name_error').html("");
        }

        if (customer_mobile == "") {
            $('#customer_mobile_error').html("Please Enter Customer Mobile");
            $('#customer_mobile').focus();
            return false;
        } else {
            $('#customer_mobile_error').html("");
        }
        if (address_type == "") {
            $('#address_type_error').html("Please Select Address");
            $('#address_type').focus();
            return false;
        } else {
            $('#address_type_error').html("");
        }

        $('#form_main').submit();

    }
</script>