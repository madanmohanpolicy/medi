<section class="inner_body_wrapper" style="min-height: 400px;">
    <div class="myaccount_wrapper">
        <div class="container">
            <div class="loginForm">
                <div class="title">Enter OTP Details</div>
                <form action="thanks" id="form_main" method="post" onsubmit="return checkOTPDetails(this);">
                    <div class="form-group">
                        <label for="otp">Email OTP</label>
                        <div class="error" id="otp_error"></div>
                        <input type="text" id="otp" name="otp" minlength="6" max="6" placeholder="Please Enter OTP"/>
                    </div>
                    <div class="form-group" id="contact_submit">
                        <input class="btn_red" type="submit" value="Continue"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function checkOTPDetails(frm) {
        var otp = $('#otp').val().trim();
        if (otp == "") {
            $('#otp_error').html("Please Enter OTP Details");
            $('#otp').focus();
            return false;
        } else if (!($.isNumeric(otp))) {
            $('#otp_error').html("Please Enter Correct OTP Details");
            $('#otp').focus();
            return false;
        } else if (otp.length < 6) {
            $('#otp_error').html("OTP Must be in 6 Digit");
            $('#otp').focus();
            return false;
        } else {
            $('#otp_error').html("");
        }
        $('#form_main').submit();
    }
</script>