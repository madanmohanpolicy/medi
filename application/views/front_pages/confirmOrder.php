<section class="inner_body_wrapper" style="min-height: 400px;">

    <div class="cart_wrapper">
        <div class="container">
            <div class="title">Confirm Order</div>

            <div class="cart_block">
                <div class="left">

                    Select Address :

                    <?php //echo "aaaaa = ";
                    //echo "<pre>";
                    //print_r($address);
                    //print_r($cart_data);
                    //die;

                    for ($i = 0; $i < count($address['message']); $i++) {
                        if ($i == 0) {
                            $selected_address = "checked";
                        } else {
                            $selected_address = "";
                        }
                        ?>
                        <div class="selectConAdd">
                            <div class="check">
                                <input type="radio" name="address_type"
                                       value="<?php echo $address['message'][$i]->address_type; ?>" <?php echo $selected_address; ?>>
                            </div>
                            <ul>
                                <li><strong><?php echo $address['message'][$i]->address_type; ?></strong></li>
                                <li><?php echo $address['message'][$i]->customer_name; ?></li>
                                <li><?php echo $address['message'][$i]->customer_mobile; ?></li>
                                <li><?php echo $address['message'][$i]->address; ?> <?php echo $address['message'][$i]->locality; ?>
                                    , <br><?php echo $address['message'][$i]->landmark; ?>
                                    , <?php echo $address['message'][$i]->city; ?>
                                    - <?php echo $address['message'][$i]->state; ?>
                                    - <?php echo $address['message'][$i]->pincode; ?></li>
                            </ul>
                        </div>
                        <?php
                    }


                    $total_mrp = 0;
                    $total_offered_price = 0;
                    $total_discount = 0;

                    for ($i = 0; $i < count($cart_data->message); $i++) {
                        ?>
                        <?php
                        $total_mrp = $total_mrp + ($cart_data->message[$i]->mrp * $cart_data->message[$i]->quantity);
                        $total_offered_price = $total_offered_price + ($cart_data->message[$i]->oprice * $cart_data->message[$i]->quantity);
                    }

                    $total_discount = $total_mrp - $total_offered_price;

                    ?>


                </div>
                <?php
                if ($cart_data->total_row > 0) {
                    ?>
                    <div class="right">
                        <!--  <div class="login_toapplycup right_box"><a href="login.html">Login</a> to Apply Coupon</div> -->
                        <div class="pros_price_details right_box">

                            <div class="mrp_box"><span>MRP Total</span> <span>₹<?php echo $total_mrp; ?></span></div>
                            <div class="mrp_box"><span>Price Discount</span>
                                <span>- ₹<?php echo $total_discount; ?></span></div>
                            <div class="mrp_box"><span>Shipping Charges</span> <span>As per delivery address</span>
                            </div>
                            <div class="mrp_box tobepaid"><span>To be paid</span>
                                <span>₹<?php echo $total_offered_price; ?></span></div>

                            <?php
                            if ($total_discount > 0) {
                                ?>
                                <div class="totalsaving">Total Savings <span>₹<?php echo $total_discount; ?></span>
                                </div>
                                <?php
                            }
                            ?>

                        </div>

                        <div class="pros_checkout right_box">
                            <div class="mrp_box"><span>Your delivery location</span> <a href="#"
                                                                                        class="checkoutlocation"><i
                                            class="fa fa-map-marker"
                                            aria-hidden="true"></i> <?php echo $cart_data->message[0]->city; ?></a>
                            </div>
                            <div class="mrp_box">
                                <a href="<?php echo base_url(); ?>cart/placeOrder" class="btn_red">Place Order <i
                                            class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>

                    </div>
                    <?php
                }
                ?>
            </div>

        </div>
    </div>


</section>