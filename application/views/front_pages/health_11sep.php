<div class="wrapper">        
            
        <div class="container">
                <h1>Available Health checkup packages for members</h1>

                <div class="tabs">
                    <a href="index.html" class="active">Health checkup packages</a>
                    <a href="submit-reports.html">Upload Reports</a>
                    <a href="view-reports.html">View Reports</a>
                </div>

            <div class="pack_wrapper">
                    <div class="packbox">
                        <div class="titleblock">
                                <div class="title greenColor">Save & Smile Package 
                                    <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                    <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                    <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                    <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                </div>
                                <div class="price_block">
                                        <div class="price">
                                            <div class="offertag">50% off</div>
                                            <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                            <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                        </div>
                                        <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                </div>

                                <div class="bkap_wrap">
                                        <div class="close">X</div>
                                        <strong>Select your preferred appointment  date and time</strong>

                                        <div class="date-list owl-carousel">
                                            <div class="item">
                                                <div class="dateBox">
                                                    <span class="day">Sun</span>
                                                    <span class="date">02 Sep</span>
                                                </div>
                                            </div>
                                            <div class="item">
                                                    <div class="dateBox">
                                                        <span class="day">Sun</span>
                                                        <span class="date">02 Sep</span>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                        <div class="dateBox">
                                                            <span class="day">Sun</span>
                                                            <span class="date">02 Sep</span>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                            <div class="dateBox">
                                                                <span class="day">Sun</span>
                                                                <span class="date">02 Sep</span>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                                <div class="dateBox">
                                                                    <span class="day">Sun</span>
                                                                    <span class="date">02 Sep</span>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                    <div class="dateBox">
                                                                        <span class="day">Sun</span>
                                                                        <span class="date">02 Sep</span>
                                                                    </div>
                                                                </div>

                                                                <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>

                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>

                                        </div>

                                        <ul class="time_ul">
                                                <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>

                                        </ul>
                                </div>

                        </div>
                        
                    </div><!--End Pack One-->

                    <div class="packbox">
                            <div class="titleblock">
                                    <div class="title greenColor">Save & Smile Package 
                                        <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                        <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                        <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                        <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                    </div>
                                    <div class="price_block">
                                            <div class="price">
                                                <div class="offertag">50% off</div>
                                                <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                            </div>
                                            <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                    </div>
    
                                    <div class="bkap_wrap">
                                            <div class="close">X</div>
                                            <strong>Select your preferred appointment  date and time</strong>
    
                                            <div class="date-list owl-carousel">
                                                <div class="item">
                                                    <div class="dateBox">
                                                        <span class="day">Sun</span>
                                                        <span class="date">02 Sep</span>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                        <div class="dateBox">
                                                            <span class="day">Sun</span>
                                                            <span class="date">02 Sep</span>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                            <div class="dateBox">
                                                                <span class="day">Sun</span>
                                                                <span class="date">02 Sep</span>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                                <div class="dateBox">
                                                                    <span class="day">Sun</span>
                                                                    <span class="date">02 Sep</span>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                    <div class="dateBox">
                                                                        <span class="day">Sun</span>
                                                                        <span class="date">02 Sep</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>
    
                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
    
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
    
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
    
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
    
                                            </div>
    
                                            <ul class="time_ul">
                                                    <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                    <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                    <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                    <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                    <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                    <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                    <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                    <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                    <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                    <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                    <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                    <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                    <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
    
                                            </ul>
                                    </div>
    
                            </div>
                            
                        </div><!--End Pack One-->


                        <div class="packbox">
                                <div class="titleblock">
                                        <div class="title greenColor">Save & Smile Package 
                                            <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                            <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                            <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                            <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                        </div>
                                        <div class="price_block">
                                                <div class="price">
                                                    <div class="offertag">50% off</div>
                                                    <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                    <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                                </div>
                                                <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                        </div>
        
                                        <div class="bkap_wrap">
                                                <div class="close">X</div>
                                                <strong>Select your preferred appointment  date and time</strong>
        
                                                <div class="date-list owl-carousel">
                                                    <div class="item">
                                                        <div class="dateBox">
                                                            <span class="day">Sun</span>
                                                            <span class="date">02 Sep</span>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                            <div class="dateBox">
                                                                <span class="day">Sun</span>
                                                                <span class="date">02 Sep</span>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                                <div class="dateBox">
                                                                    <span class="day">Sun</span>
                                                                    <span class="date">02 Sep</span>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                    <div class="dateBox">
                                                                        <span class="day">Sun</span>
                                                                        <span class="date">02 Sep</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
        
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
        
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
        
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
        
                                                                                    <div class="item">
                                                                                            <div class="dateBox">
                                                                                                <span class="day">Sun</span>
                                                                                                <span class="date">02 Sep</span>
                                                                                            </div>
                                                                                        </div>
        
                                                </div>
        
                                                <ul class="time_ul">
                                                        <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                        <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                        <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                        <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                        <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                        <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                        <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                        <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                        <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                        <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                        <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                        <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                        <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
        
                                                </ul>
                                        </div>
        
                                </div>
                                
                            </div><!--End Pack One-->

                            <div class="packbox">
                                    <div class="titleblock">
                                            <div class="title greenColor">Save & Smile Package 
                                                <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                                <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                                <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                                <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                            </div>
                                            <div class="price_block">
                                                    <div class="price">
                                                        <div class="offertag">50% off</div>
                                                        <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                        <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                            </div>
            
                                            <div class="bkap_wrap">
                                                    <div class="close">X</div>
                                                    <strong>Select your preferred appointment  date and time</strong>
            
                                                    <div class="date-list owl-carousel">
                                                        <div class="item">
                                                            <div class="dateBox">
                                                                <span class="day">Sun</span>
                                                                <span class="date">02 Sep</span>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                                <div class="dateBox">
                                                                    <span class="day">Sun</span>
                                                                    <span class="date">02 Sep</span>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                    <div class="dateBox">
                                                                        <span class="day">Sun</span>
                                                                        <span class="date">02 Sep</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
            
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
            
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="item">
                                                                                            <div class="dateBox">
                                                                                                <span class="day">Sun</span>
                                                                                                <span class="date">02 Sep</span>
                                                                                            </div>
                                                                                        </div>
            
                                                                                        <div class="item">
                                                                                                <div class="dateBox">
                                                                                                    <span class="day">Sun</span>
                                                                                                    <span class="date">02 Sep</span>
                                                                                                </div>
                                                                                            </div>
            
                                                    </div>
            
                                                    <ul class="time_ul">
                                                            <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                            <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                            <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                            <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                            <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                            <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                            <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                            <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                            <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                            <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                            <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                            <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                            <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
            
                                                    </ul>
                                            </div>
            
                                    </div>
                                    
                                </div><!--End Pack One-->


                                <div class="packbox">
                                        <div class="titleblock">
                                                <div class="title greenColor">Save & Smile Package 
                                                    <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                                    <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                                    <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                                    <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                                </div>
                                                <div class="price_block">
                                                        <div class="price">
                                                            <div class="offertag">50% off</div>
                                                            <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                            <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                                        </div>
                                                        <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                                </div>
                
                                                <div class="bkap_wrap">
                                                        <div class="close">X</div>
                                                        <strong>Select your preferred appointment  date and time</strong>
                
                                                        <div class="date-list owl-carousel">
                                                            <div class="item">
                                                                <div class="dateBox">
                                                                    <span class="day">Sun</span>
                                                                    <span class="date">02 Sep</span>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                    <div class="dateBox">
                                                                        <span class="day">Sun</span>
                                                                        <span class="date">02 Sep</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
                
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
                
                                                                                    <div class="item">
                                                                                            <div class="dateBox">
                                                                                                <span class="day">Sun</span>
                                                                                                <span class="date">02 Sep</span>
                                                                                            </div>
                                                                                        </div>
                
                                                                                        <div class="item">
                                                                                                <div class="dateBox">
                                                                                                    <span class="day">Sun</span>
                                                                                                    <span class="date">02 Sep</span>
                                                                                                </div>
                                                                                            </div>
                
                                                                                            <div class="item">
                                                                                                    <div class="dateBox">
                                                                                                        <span class="day">Sun</span>
                                                                                                        <span class="date">02 Sep</span>
                                                                                                    </div>
                                                                                                </div>
                
                                                        </div>
                
                                                        <ul class="time_ul">
                                                                <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                                <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                                <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                                <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                                <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                                <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                                <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                                <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                                <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                                <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                                <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                                <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                                <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
                
                                                        </ul>
                                                </div>
                
                                        </div>
                                        
                                    </div><!--End Pack One-->


                                    <div class="packbox">
                                            <div class="titleblock">
                                                    <div class="title greenColor">Save & Smile Package 
                                                        <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                                        <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                                        <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                                        <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                                    </div>
                                                    <div class="price_block">
                                                            <div class="price">
                                                                <div class="offertag">50% off</div>
                                                                <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                                <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                                            </div>
                                                            <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                                    </div>
                    
                                                    <div class="bkap_wrap">
                                                            <div class="close">X</div>
                                                            <strong>Select your preferred appointment  date and time</strong>
                    
                                                            <div class="date-list owl-carousel">
                                                                <div class="item">
                                                                    <div class="dateBox">
                                                                        <span class="day">Sun</span>
                                                                        <span class="date">02 Sep</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
                    
                                                                                    <div class="item">
                                                                                            <div class="dateBox">
                                                                                                <span class="day">Sun</span>
                                                                                                <span class="date">02 Sep</span>
                                                                                            </div>
                                                                                        </div>
                    
                                                                                        <div class="item">
                                                                                                <div class="dateBox">
                                                                                                    <span class="day">Sun</span>
                                                                                                    <span class="date">02 Sep</span>
                                                                                                </div>
                                                                                            </div>
                    
                                                                                            <div class="item">
                                                                                                    <div class="dateBox">
                                                                                                        <span class="day">Sun</span>
                                                                                                        <span class="date">02 Sep</span>
                                                                                                    </div>
                                                                                                </div>
                    
                                                                                                <div class="item">
                                                                                                        <div class="dateBox">
                                                                                                            <span class="day">Sun</span>
                                                                                                            <span class="date">02 Sep</span>
                                                                                                        </div>
                                                                                                    </div>
                    
                                                            </div>
                    
                                                            <ul class="time_ul">
                                                                    <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                                    <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                                    <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                                    <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                                    <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                                    <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                                    <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                                    <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                                    <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                                    <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                                    <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                                    <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                                    <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
                    
                                                            </ul>
                                                    </div>
                    
                                            </div>
                                            
                                        </div><!--End Pack One-->


                                        <div class="packbox">
                                                <div class="titleblock">
                                                        <div class="title greenColor">Save & Smile Package 
                                                            <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                                            <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                                            <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                                            <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                                        </div>
                                                        <div class="price_block">
                                                                <div class="price">
                                                                    <div class="offertag">50% off</div>
                                                                    <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                                    <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                                                </div>
                                                                <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                                        </div>
                        
                                                        <div class="bkap_wrap">
                                                                <div class="close">X</div>
                                                                <strong>Select your preferred appointment  date and time</strong>
                        
                                                                <div class="date-list owl-carousel">
                                                                    <div class="item">
                                                                        <div class="dateBox">
                                                                            <span class="day">Sun</span>
                                                                            <span class="date">02 Sep</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="item">
                                                                                            <div class="dateBox">
                                                                                                <span class="day">Sun</span>
                                                                                                <span class="date">02 Sep</span>
                                                                                            </div>
                                                                                        </div>
                        
                                                                                        <div class="item">
                                                                                                <div class="dateBox">
                                                                                                    <span class="day">Sun</span>
                                                                                                    <span class="date">02 Sep</span>
                                                                                                </div>
                                                                                            </div>
                        
                                                                                            <div class="item">
                                                                                                    <div class="dateBox">
                                                                                                        <span class="day">Sun</span>
                                                                                                        <span class="date">02 Sep</span>
                                                                                                    </div>
                                                                                                </div>
                        
                                                                                                <div class="item">
                                                                                                        <div class="dateBox">
                                                                                                            <span class="day">Sun</span>
                                                                                                            <span class="date">02 Sep</span>
                                                                                                        </div>
                                                                                                    </div>
                        
                                                                                                    <div class="item">
                                                                                                            <div class="dateBox">
                                                                                                                <span class="day">Sun</span>
                                                                                                                <span class="date">02 Sep</span>
                                                                                                            </div>
                                                                                                        </div>
                        
                                                                </div>
                        
                                                                <ul class="time_ul">
                                                                        <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                                        <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                                        <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                                        <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                                        <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                                        <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                                        <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                                        <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                                        <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                                        <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                                        <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                                        <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                                        <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
                        
                                                                </ul>
                                                        </div>
                        
                                                </div>
                                                
                                            </div><!--End Pack One-->


                                            <div class="packbox">
                                                    <div class="titleblock">
                                                            <div class="title greenColor">Save & Smile Package 
                                                                <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                                                <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                                                <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                                                <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                                            </div>
                                                            <div class="price_block">
                                                                    <div class="price">
                                                                        <div class="offertag">50% off</div>
                                                                        <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                                        <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                                                    </div>
                                                                    <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                                            </div>
                            
                                                            <div class="bkap_wrap">
                                                                    <div class="close">X</div>
                                                                    <strong>Select your preferred appointment  date and time</strong>
                            
                                                                    <div class="date-list owl-carousel">
                                                                        <div class="item">
                                                                            <div class="dateBox">
                                                                                <span class="day">Sun</span>
                                                                                <span class="date">02 Sep</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="item">
                                                                                            <div class="dateBox">
                                                                                                <span class="day">Sun</span>
                                                                                                <span class="date">02 Sep</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="item">
                                                                                                <div class="dateBox">
                                                                                                    <span class="day">Sun</span>
                                                                                                    <span class="date">02 Sep</span>
                                                                                                </div>
                                                                                            </div>
                            
                                                                                            <div class="item">
                                                                                                    <div class="dateBox">
                                                                                                        <span class="day">Sun</span>
                                                                                                        <span class="date">02 Sep</span>
                                                                                                    </div>
                                                                                                </div>
                            
                                                                                                <div class="item">
                                                                                                        <div class="dateBox">
                                                                                                            <span class="day">Sun</span>
                                                                                                            <span class="date">02 Sep</span>
                                                                                                        </div>
                                                                                                    </div>
                            
                                                                                                    <div class="item">
                                                                                                            <div class="dateBox">
                                                                                                                <span class="day">Sun</span>
                                                                                                                <span class="date">02 Sep</span>
                                                                                                            </div>
                                                                                                        </div>
                            
                                                                                                        <div class="item">
                                                                                                                <div class="dateBox">
                                                                                                                    <span class="day">Sun</span>
                                                                                                                    <span class="date">02 Sep</span>
                                                                                                                </div>
                                                                                                            </div>
                            
                                                                    </div>
                            
                                                                    <ul class="time_ul">
                                                                            <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                                            <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                                            <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                                            <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                                            <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                                            <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                                            <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                                            <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                                            <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                                            <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                                            <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                                            <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                                            <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
                            
                                                                    </ul>
                                                            </div>
                            
                                                    </div>
                                                    
                                                </div><!--End Pack One-->

                                                <div class="packbox">
                                                        <div class="titleblock">
                                                                <div class="title greenColor">Save & Smile Package 
                                                                    <span>by : P.H.DIAGNOSTIC CENTRE</span> 
                                                                    <span><i class="fa fa-map-marker blueColor" aria-hidden="true"></i> BHAVANI PETH </span> 
                                                                    <span><i class="fa fa-phone magentaColor" aria-hidden="true"></i> 096571 12233</span>
                                                                    <span><i class="fa fa-clock-o orangeColor" aria-hidden="true"></i> 09:00 AM - 06:00 PM</span>
                                                                </div>
                                                                <div class="price_block">
                                                                        <div class="price">
                                                                            <div class="offertag">50% off</div>
                                                                            <span class="realPrice"><i class="fa fa-inr" aria-hidden="true"></i>  2000</span>
                                                                            <span class="offerPrice"><i class="fa fa-inr" aria-hidden="true"></i>  999</span>                                   </span>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="bookapoinment">Book your appointment</a>
                                                                </div>
                                
                                                                <div class="bkap_wrap">
                                                                        <div class="close">X</div>
                                                                        <strong>Select your preferred appointment  date and time</strong>
                                
                                                                        <div class="date-list owl-carousel">
                                                                            <div class="item">
                                                                                <div class="dateBox">
                                                                                    <span class="day">Sun</span>
                                                                                    <span class="date">02 Sep</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item">
                                                                                    <div class="dateBox">
                                                                                        <span class="day">Sun</span>
                                                                                        <span class="date">02 Sep</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="dateBox">
                                                                                            <span class="day">Sun</span>
                                                                                            <span class="date">02 Sep</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="item">
                                                                                            <div class="dateBox">
                                                                                                <span class="day">Sun</span>
                                                                                                <span class="date">02 Sep</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="item">
                                                                                                <div class="dateBox">
                                                                                                    <span class="day">Sun</span>
                                                                                                    <span class="date">02 Sep</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="item">
                                                                                                    <div class="dateBox">
                                                                                                        <span class="day">Sun</span>
                                                                                                        <span class="date">02 Sep</span>
                                                                                                    </div>
                                                                                                </div>
                                
                                                                                                <div class="item">
                                                                                                        <div class="dateBox">
                                                                                                            <span class="day">Sun</span>
                                                                                                            <span class="date">02 Sep</span>
                                                                                                        </div>
                                                                                                    </div>
                                
                                                                                                    <div class="item">
                                                                                                            <div class="dateBox">
                                                                                                                <span class="day">Sun</span>
                                                                                                                <span class="date">02 Sep</span>
                                                                                                            </div>
                                                                                                        </div>
                                
                                                                                                        <div class="item">
                                                                                                                <div class="dateBox">
                                                                                                                    <span class="day">Sun</span>
                                                                                                                    <span class="date">02 Sep</span>
                                                                                                                </div>
                                                                                                            </div>
                                
                                                                                                            <div class="item">
                                                                                                                    <div class="dateBox">
                                                                                                                        <span class="day">Sun</span>
                                                                                                                        <span class="date">02 Sep</span>
                                                                                                                    </div>
                                                                                                                </div>
                                
                                                                        </div>
                                
                                                                        <ul class="time_ul">
                                                                                <li><label for="1"><input type="radio" name="title" id="1"> 09:00 AM</label></li>
                                                                                <li><label for="2"><input type="radio" name="title" id="2"> 09:30 AM</label></li>
                                                                                <li><label for="3"><input type="radio" name="title" id="3"> 10:00 AM</label></li>
                                                                                <li><label for="4"><input type="radio" name="title" id="4"> 10:00 AM</label></li>
                                                                                <li><label for="5"><input type="radio" name="title" id="5"> 11:00 AM</label></li>
                                                                                <li><label for="6"><input type="radio" name="title" id="6"> 11:00 AM</label></li>
                                                                                <li><label for="7"><input type="radio" name="title" id="7"> 12:00 AM</label></li>
                                                                                <li><label for="8"><input type="radio" name="title" id="8"> 12:00 AM</label></li>
                                                                                <li><label for="9"><input type="radio" name="title" id="9"> 01:00 PM</label></li>
                                                                                <li><label for="10"><input type="radio" name="title" id="10"> 01:30 PM</label></li>
                                                                                <li><label for="11"><input type="radio" name="title" id="11"> 02:00 PM</label></li>
                                                                                <li><label for="12"><input type="radio" name="title" id="12"> 02:30 PM</label></li>
                                                                                <li><label for="13"><input type="radio" name="title" id="13"> 03:00 PM</label></li>
                                
                                                                        </ul>
                                                                </div>
                                
                                                        </div>
                                                        
                                                    </div><!--End Pack One-->
                        
                            
                           
                        
            
            </div>

            
        </div>
    </div>