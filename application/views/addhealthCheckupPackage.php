<style>
ul.multiselect-container.dropdown-menu {
    max-height: 240px;
    overflow: auto;
}
</style>
<?php 



$healthTests=$this->hospital_model->getHealthTestsName();

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Health Checkup Package  Management
        <small>Add </small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Health Checkup Package Details </h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="healthcheckuppackage" action="<?php echo base_url().'admin/';?>addHealthcheckuppackages" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename">Package Name</label>
                                        <input type="text" class="form-control " value=""  id="packagename" name="packagename" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cost">Cost</label>
                                        <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control required " id="cost" value="" name="cost" maxlength="128">
                                    </div>
                                </div>
                            </div>
							
							
							   <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename">Tests</label>
										
										
										 <select id="test" name="test[]" multiple class="form-control"  >
										 
										 <?php 
										
									
										foreach($healthTests as $value){
											
										$parametercount=$this->hospital_model->gethealthCheckupTestParametersCount($value->id);
										
											?>
											<option value="<?php echo $value->id;?>"><?php echo $value->name.'&nbsp;('.$parametercount.' parameter)';
									  
										}
									   ?></option>
                                     
										 
										 
   
  
                                    </select>
										
									   
                                    </div>
                                    
                               
                            </div> 
							
							     <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount">Discount</label>
                                        <input type="text"  oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control required" id="discount" name="discount" maxlength="20">
                                    </div>
                                </div>
							
							</div>
							
							
								
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename">  Referred By</label>
                                        <input type="text" class="form-control " value=""  id="referred_by" name="referred_by" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
									<br/>
                                        <label for="cost">Need To Visit Hospital</label>
                                          <input type="radio" value="1" id="needvisithospitalYes" checked="checked"  name="needto_visit_hospital">Yes
										 <input type="radio" id="needvisithospitalNo" value="0" name="needto_visit_hospital">No
                                    </div>
                                </div>
                            </div>
							
								<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename">  Home Sample Collection</label>
                                        <input type="radio" value="1" id="homecollectionYes" checked="checked"  name="home_samplecollection">Yes
										 <input type="radio" id="homecollectionNo" value="0" name="home_samplecollection">No
                                    </div>
                                    
                                </div>
								
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cost">Fasting</label>
                                          <input type="radio" value="1" id="fastingYes" checked="checked"  name="fasting">Yes
										 <input type="radio" id="fastingNo" value="0" name="fasting">No
                                    </div>
                                </div>
                            </div>
								
								
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename"> Center Visit</label>
                                        <input type="radio" value="1" id="centervisitYes" checked="checked"  name="center_visit">Yes
										 <input type="radio" id="centervisitNo" value="0" name="center_visit">No
                                    </div>
                                    
                                </div>
                          
                            </div>	
								
								
								
									  <label for="discount displayed">24X7(Availability)</label>  
										Yes <input type="radio" id="chktimeYes"  value="1" name="chk_time" />
										 No<input type="radio" id="chktimeNo" value="0" checked="checked" name="chk_time" />
										 
							
                    
                                  							
							  <div class="row"  id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom" id="timefrom" class="form-control required">
									
								</div>
									
								
							
									<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto"  class="form-control required">
								
								</div>
							 
							 
                            </div>
							<div class="row"  id="divcharge">
							<div class="col-md-6">
                             <div class="form-group">
							<label for="timeto">Charge</label>
							 <input type="text" class="form-control " value=""  id="pkg_charge" name="pkg_charge" maxlength="128">
							</div></div>
							
							</div>
							<br/>
									<div class="row">
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea  class="form-control required" id="description" name="description" ></textarea>
                                    </div>
                                </div>
                               </div>
                                </div>
								
								
							
							<div class="box-footer">

                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
		
							
                            </div>
                          </div><!-- /.box-body -->
    

                    </form>
					
					
					    <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
					
                </div>
            </div>
        
        </div>    
    </section>
    
</div>
<script>
$(document).ready(function() {

	
	  /**
     * this script use for  from_time  and to_time
     * Create by Santosh Kumar Verma
	 * Date:13 September 2018
     * start
     */
    var tcounter = 2;	
    var counter2 = 1;
	var timefromname = "timefrom"+counter2;
	$('#timefrom').timepicker();
	$('#timeto').timepicker();	
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + tcounter);
                
	newTextBoxDiv.after().html('<div class="col-md-4"><label for="contact-person">Sample Collection Days</label><select id="days" name="days[]" class="form-control"><option id="1">Monday</option><option id="2">Tuesday</option><option id="3">Wednesday</option><option id="4">Thursday</option><option id="5">Friday</option><option id="6">Saturday</option><option id="7">Sunday</option></select></div><div class="col-md-4"><label for="timefrom">Time From</label> <input type="text" name="timefrom[]" id="timefrom'+counter2+'" class="form-control"></div><div class="col-md-4"><label for="timeto">Time To</label><input type="text" name="timeto[]" id="timeto'+counter2+'" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");	
    $('#timefrom'+counter2).timepicker();
    $('#timeto'+counter2).timepicker();		
	tcounter++;
	counter2++;
    });	
	
    /**
    * 
    * end
    */	
	
	
});

</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>