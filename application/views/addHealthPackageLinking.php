<?php 
$healthpackage=$this->hospital_model->getHealthCheckupPackage();
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Health Checkup Package  Linking  
        <small>Add </small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
		
		<div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Health Checkup Package Linking Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="healthcheckuptestlinking" action="<?php echo base_url().'admin/';?>addHealthcheckupPackageLinking" method="post" role="form">
                        <div class="box-body">
						
						<div id="divpackge">
						 
						 
						  <div class="row">
								
								  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="healthe">Health Package</label>
                                        <select class="form-control required"  id="healthpackage" name="healthpackage" >
										<option value="0">Select HealthPackage</option>
										<?php foreach($healthpackage as $value){
											?>
										<option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
										<?php } ?>
											<select>
											
											<span class="hcodeSpan"></span>
                                </div>
                                    
                               
                                
                                </div>
								
								
								
								<div class="col-md-6">
                                    <div class="form-group">
                                         <label for="doctor">Enter Vendor Decided Name*</label>
                                        <input type="text" class="form-control" value="" id="vendor_decided_name" name="vendor_decided_name" maxlength="128" required>
                                       
                                    </div>
                                </div>
							
								
								
								
							</div>
						
                            <div class="row">
							
							  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="facilitytype">Facility Type</label>
                                        <select class="form-control"  id="facility2" name="facility2"  required>
										<option value="0">Select Facility Type</option>
										<option value="1">Hospital</option>
											<option value="2">Diaganostic Center</option>
											<select>
											
                                    </div>
                                    
                            </div>
						
						
							
							<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Facility Name</label>
                                        <select class="livesearch form-control required" id="mastercode" name="mastercode" >
                                            <option value=''>Select Facility</option>
                                        </select>
                                    </div>
                                </div>
								
								
							
									
						
							
							
							
                                    
                    </div>	


					<div  id="TextBoxesGroup"  class='textBoxesGroup1'>	
								<div class="textBoxDiv1 row">							
											
											<div class="col-md-6">
											 <label for="subcode">Subcode </label>
													<select class="livesearch form-control" id="subcode" name="subcode[]" required >
														<option value=''>Select Subcode</option>
													</select>
											</div>
											<div class="col-md-6">
												<label for="landline">Cost</label> 
												<input type="text" id="cost" name="cost[]" class="form-control required">
											</div>
											<?php 
                                              //$counter_radio = 0;	 

											 ?>
											
											<div class="col-md-6">
												<label for="memberdiscount">Member Discount</label>
												<input type="radio"  name="member_discount_inpercent0[]" value="0" checked>In Rs.
										       <input type="radio"  name="member_discount_inpercent0[]" value="1">In %
												
												<input type="text" name="member_discount[]" id="member_discount" class="form-control">
											</div>
											
											<div class="col-md-6">
												<label for="mediwheeldiscount">Mediwheel Discount</label>
												<input type="radio"  name="mediwheel_discount_inpercent0[]" value="0" checked>In Rs.
										        <input type="radio"    name="mediwheel_discount_inpercent0[]" value="1">In %
												<input type="text" name="mediwheel_discount[]" id="mediwheeldiscount" class="form-control">
											</div>
											
								</div>
					</div>	
					
					

							

					
                  </div>	              
                        
						<!--div class="row">
						 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <div class="set_div">
											<label for="setforall">Set this for All </label>
											<input type="radio" value="1" id="setforallYes" checked="checked"  name="setforall">Yes
											<input type="radio" id="setforallNo" value="0" name="setforall">No
										</div>
                                    </div>
                                    
                                </div>
                       </div-->
					   
					   
					   
						
						
						
						
						
							


			
                        <div class="addremoveBtnsBlock">
						<input type='hidden' value='2' id='cntid'>
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
                        </div>
						
						
				
				
				
				
				
				
				
				
				
				
					<div class="row" >

					 
						
						 <div class="col-md-6"> 
						
						
						<input type="submit" class="btn btn-primary" id="btnSubmit" value="Submit" />
						<input type="reset" class="btn btn-default" value="Reset" />
						
						
						</div>
						
					  </div>
					
					</div>
						<!-- /.box-body -->
					</form>
                </div>
            </div>
            
        </div>    
    </section>
    
</div>

<style>
	.textBoxDiv1 {padding:15px 0 0;}
	.textBoxDiv1 .col-md-6 {margin-bottom:15px;}
	.textBoxesGroup1 {background: #fff;padding: 5px 15px; margin-bottom: 15px; border: 1px solid #ccc;}
	.addremoveBtnsBlock {padding:15px 0;}
</style>

<script>

$(document).ready(function(){
	
//start
	
	//$('#mastercode').change(function() { 
	$('#addButton').click(function() { 
	var mid = $("#mastercode option:selected").val();
	var cntid = $("#cntid").val();
	var cntid2 = $("#cntid").val();
	document.getElementById("cntid").value = parseInt(cntid2)+1;
	//alert(cntid);
	//alert(cntid2);
	

  if(mid != '')
  { 

   $.ajax({
    url: baseURL+"admin/addmoreDiasubcode/"+mid+"/"+cntid,
    method:"POST",
    data:{id:mid,
	cnt:cntid,
	},
	
    success:function(data)
    {
		
		//alert(data);
		var final_data = data.split("@@@@");
		//alert(final_data);
		
		var dynamic_id = "subcode"+final_data[1];
		
	 //$("#subcode1"+final_data[1]).chosen('destroy');
	// $("#subcode1"+final_data[1]).html(final_data[0]);
	 
	 document.getElementById(dynamic_id).innerHTML = "";
	 document.getElementById(dynamic_id).innerHTML = final_data[0];
     //$(".livesearch").chosen();	
	
	//$("#cntid").val() = final_data[1] + 1;
	
    }
   });
  }
 else{
	 
	
  $('#subcode1').html('<span id="subcode1" name="subcode1"> </span>');
  
  
  //$('#subcode1').html('<input type="text" name="subcode1[]" id="subcode1" class="form-control">');
 
 }

	
	
	

	
 });
//end	
    var counter = 2;		
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	.attr("id", 'TextBoxDiv' + counter);
                
	var counter_radio = 1;	
	//alert();
	
	newTextBoxDiv.after().html('<div class="textBoxesGroup1"><div class="textBoxDiv1 row"><div class="col-md-6"><label for="subcode">Subcode </label><select class="livesearch form-control" id="subcode'+counter+'" name="subcode[]" required><option value="">Select Subcode</option></select></div><div class="col-md-6"><label for="Cost">Cost</label><input type="text" id="cost" name="cost[]" class="form-control"></div><div class="col-md-6"><label for="memberdiscount">Member Discount</label><input  type="radio" name="member_discount_inpercent'+counter_radio+'[]" value="0" checked>In Rs.<input type="radio"  name="member_discount_inpercent'+counter_radio+'[]" value="1">In %<input type="text" name="member_discount[]" id="memberdiscount" class="form-control"></div><div class="col-md-6"><label for="mobile">Mediwheel Discount</label> <input type="radio"  name="mediwheel_discount_inpercent'+counter_radio+'[]" value="0" checked>In Rs.<input type="radio"  name="mediwheel_discount_inpercent'+counter_radio+'[]" value="1">In %<input type="text" name="mediwheel_discount[]" id="mediwheeldiscount" class="form-control"></div></div></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");				
	counter++;
	counter_radio++;
    });
	
   
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
	
		 

	
	
});

</script>

<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript">
</script>
<script src="<?php echo base_url(); ?>assets/js/common.js" type="text/javascript">
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
 <script > </script>			
					
				
			
			
			
                                
                            
					
					
				
					
				
			
			
            
	

     
