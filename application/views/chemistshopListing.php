<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Doctors
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
				<?php 
			$module=$this->session->module;
			
				$permission= explode(',',$this->session->permission);
				if(in_array('1',$permission)&&$module=='1'|| $role == ROLE_ADMIN){
				?>
                    <a class="btn btn-primary" href="<?php echo base_url().'admin/'; ?>addNewHospitalDoctor"><i class="fa fa-plus"></i> Add New</a>
					<?php 
			 }
				 ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Doctors List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url().'admin/' ?>hospitalDoctorsListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
					<th>Sr. No.</th>
					<th>Master Code</th>
					<th>Doctor Sub Code</th>
                        <th>Doctor Name</th>
                        <th>Specialization</th>
                        <th>Remarks</th>
                        <th>Qualification</th>
                        <th>License/Registration No.</th>
						<th>Years of Experience </th>
						<th>Consultation Charges</th>
						<th>Action</th>
						
                    </tr>
                    <?php
					$sr=0;
				
                    if(!empty($hospitalDoctorsRecords))
                    {
						
                        foreach($hospitalDoctorsRecords as $value)
                        {
							
							$sr++;
                    ?>
					
				
                    <tr>
					<td><?php echo $sr;?></td>
					<td><?php echo $value->masterCode;?></td>
					<td><?php echo $value->docSubCode;?></td>
					<td><?php echo $value->doctorName;?></td>
					<td><?php echo $value->specialization;?></td>
					
					
					
					
					
					</td>
					<td><?php echo $value->remarks;?></td>
					<td><?php echo $value->qualification;?></td>
					<td><?php echo $value->license;?></td>
					<td><?php echo $value->experience;?></td>
					<td><?php echo $value->consultationCharges;?></td>
					  <td class="text-center">
                        
								<?php if(in_array('2',$permission)&&$module=='1'|| $role == ROLE_ADMIN){ ?>
								<a class="btn btn-sm btn-info" href="<?php echo base_url().'admin/editHospitalDoctor/'.$value->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a></td><?php } 
									if(in_array('3',$permission)&&$module=='1'|| $role == ROLE_ADMIN){
								?>
									<td><a class="btn btn-sm btn-danger deleteHospitalDoctor" href="#" data-id="<?php echo $value->id; ?>" title="Delete"><i class="fa fa-trash"></i></a><?php } ?>
                        </td> 
                     
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "hospitalDoctorsListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
