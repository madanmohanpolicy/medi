<?php 
$state = $this->hospital_model->getState();
$organisationType=$this->hospital_model->getType();
?>
<?php 
 $mastercode=$this->hospital_model->getHospitalMasterCode();

 ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>  Diaganostic Centre 
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
		
		 <div class="col-md-12">
                 <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
		
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Diagnostic Centre Detail  </h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
					
					
                    <?php $this->load->helper("form");
					 ?>
				
                    <form role="form" id="addHospitalDiaganosticForm" action="<?php echo base_url().'admin/' ?>addNewHospitalDiaganostics" method="post" enctype="multipart/form-data" role="form">
                       
					  <div class="box-body">
                            <div class="row">
							
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                      
										   <select class="livesearch" id="mastercode" name="mastercode" style="width:300px;" required >
                                            <option value="0">Select Mastercode*</option>
                                           
                                               <?php foreach($mastercode   as $value){?>
                                                  <option value="<?php echo $value->mastercode;?>"><?php echo $value->organisation.'-'.$value->mastercode;?></option>  
											   <?php } ?>
                                        </select>
										
										
										<span class="mcodeSpan"></span>
                                    </div>
                                </div>
                                <div class="col-md-8">                                
                                    <div class="form-group">
                                        <label for="registrationNo">Registration No.*</label>
                                        <input type="text" class="form-control" value="" id="registrationNo" name="registrationNo" maxlength="128"  >
										<span id="elmNameError" class="errorMsg"> <span>
                                    </div>
                                    
                                </div>
								
                            </div>
							
							
							<div class="row">
							
							
							 
							 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="registrationNo">ISO </label>
											<input type="radio" value="1" id="isoYes" checked="checked"  name="iso">Yes
											<input type="radio" id="isoNo" value="0" name="iso">No
										</div>
                                    </div>
                                    
                                </div>
								
								   <div class="col-md-6">
                                    <div class="form-group">
                                       
                                    </div>
                                </div>
                             </div>
							 <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="lab">Lab*</label>
											<input type="radio" value="1" id="lab" checked="checked"  name="lab">Yes
											<input type="radio" id="lab" value="0" name="lab">No
										</div>
                                    </div>
                                </div>
								
								   <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="nabl">NABL *</label>
											<input type="radio" value="1" id="nabl" checked="checked"  name="nabl">Yes
											<input type="radio" id="nabl" value="0" name="nabl">No
										</div>
                                    </div>
                                </div>
							
							</div>
							 
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="imaging">Imaging  *</label>
											<input type="radio" value="1" id="imgchkYes" checked="checked"  name="imaging">Yes
											<input type="radio" id="imgchkNo" value="0" name="imaging">No
										</div>
                                    </div>
                                </div>
                               
                            </div>
		                 
						 <div  id="divimaging">					
                         
							
							
		<div class="row" style="margin-top:15px; ">
             <div class="col-md-6">
			<div class="form-group">
				<div class="check_div">
					<label for="xray">Digital X-ray*</label>
					 <input type="radio" value="1" id="xrayYes" checked="checked"  name="xray">Yes
					 <input type="radio" id="xrayNo" value="0" name="xray">No
				</div>
			</div>
									
			<div id="divoxrayffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
						<label>Offered Cost</label>  
						<input type="text" class="form-control required" value="" id="digitalxray_offer" name="digitalxray_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					<label>Displayed Cost</label>
					<input type="text" class="form-control required" value="" id="digitalxray_display" name="digitalxray_display" maxlength="128">
					</div>
				</div>
			</div>
		    </div>							
									
                               
		   <div class="col-md-6">
				<div class="form-group">
					<div class="check_div">
						<label for="ecg">Ecg*</label>
						<input type="radio" value="1" id="ecgYes" checked="checked"  name="ecg">Yes
						<input type="radio" id="ecgNo" value="0" name="ecg">No
					</div>
				</div>
				
				<div id="divecgoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
                    <label>Offered Cost</label>  
					<input type="text" class="form-control required" value="" id="" name="ecg_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					<label>Displayed Cost</label>
					<input type="text" class="form-control required" value="" id="" name="ecg_display" maxlength="128">
					</div>
				</div>
			</div>
				
			</div>
		</div>
                           
						   
	   <div class="row">
	   
		 <div class="col-md-6">
				<div class="form-group">
					<div class="check_div">
						<label for="ultrasound">Ultrasound*</label>
						<input type="radio" value="1" id="ultrasoundYes" checked="checked"  name="ultrasound">Yes
						<input type="radio" id="ultrasoundNo" value="0" name="ultrasound">No
					</div>
				</div>
				
				<div id="divultrasoundoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
						<label>Offered Cost</label>
						<input type="text" class="form-control required" value="" id="" name="ultrasound_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
						<label>Displayed Cost</label>
						<input type="text" class="form-control required" value="" id="" name="ultrasound_display" maxlength="128">
					</div>
				</div>
			</div>
			</div>
			
			 <div class="col-md-6">
				<div class="form-group">
					<div class="check_div">
						<label for="ct">CT*</label>
						<input type="radio" value="1" id="ctYes" checked="checked"  name="ct">Yes
						<input type="radio" id="ctNo" value="0" name="ct">No
					</div>
				</div>
				
				
				<div id="divctoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
						<label>Offered Cost</label>
						<input type="text" class="form-control required" value="" id="" name="ct_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
						<label>Displayed Cost</label>
						<input type="text" class="form-control required" value="" id="" name="ct_display" maxlength="128">
					</div>
				</div>
			</div>
			</div>
		   
		  
		</div>
							
								<div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="opinionviamail">MRI*</label>										 
											<input type="radio" value="1" id="mriYes" checked="checked"  name="mri">Yes
											<input type="radio" id="mriNo" value="0" name="mri">No
										</div>
                                    </div>
									
									<div id="divmrioffer" class="margin0">
										<div class="col-md-4">
											<div class="form-group">
												<label>Offered Cost</label>  
												<input type="text" class="form-control required" value="" id="" name="mri_offer" maxlength="128">
											</div>
										</div>
										 <div class="col-md-4">
											<div class="form-group">	
												<label>Displayed Cost </label>
												<input type="text" class="form-control required" value="" id="" name="mri_display" maxlength="128">
											</div>
										</div>
									</div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="opinion via mail charges">PET CT*</label>
											 <input type="radio" value="1" id="petctYes" checked="checked"  name="petct">Yes
											 <input type="radio" id="petctNo" value="0" name="petct">No
										</div>
                                    </div>
									
									<div id="divpetctoffer" class="margin0">
										<div class="col-md-4">
											<div class="form-group">
												<label>Offered Cost</label>
												<input type="text" class="form-control required" value="" id="" name="petct_offer" maxlength="128">
											</div>
										</div>
										 <div class="col-md-4">
											<div class="form-group">	
												<label>Displayed Cost </label>
												<input type="text" class="form-control required" value="" id="" name="petct_display" maxlength="128">
											</div>
										</div>
									</div>
									
                                </div>
				</div>
							
							

 </div>
 
							<div class="row offer-onn">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="eco">2D Eco*</label>
											<input type="radio" value="1" id="ecoYes" checked="checked"  name="eco">Yes
											<input type="radio" id="ecoNo" value="0" name="eco">No
										</div>
                                    </div>
									
									<div id="divecooffer" class="margin0">
										<div class="col-md-4">
											<div class="form-group">
												<label>Offered Cost</label>  
												<input type="text" class="form-control required" value="" id="" name="eco_offer" maxlength="128">
											</div>
										</div>
										 <div class="col-md-4">
											<div class="form-group">	
												<label>Displayed Cost</label>
												<input type="text" class="form-control required" value="" id="" name="eco_display" maxlength="128">
											</div>
										</div>
									</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="trademill">Trademill*</label>
											<input type="radio" value="1" id="trademillYes" checked="checked"  name="trademill">Yes
											<input type="radio" id="trademillNo" value="0" name="trademill">No
										</div>
                                    </div>
									
									<div id="divtrademilloffer">
										<div class="col-md-4">
											<div class="form-group">
												<label>Offered Cost</label>
												<input type="text" class="form-control required" value="" id="" name="trademill_offer" maxlength="128">
											</div>
										</div>
										 <div class="col-md-4">
											<div class="form-group">	
												<label>Displayed Cost</label>
												<input type="text" class="form-control required" value="" id="" name="trademill_display" maxlength="128">
											</div>
										</div>
									</div>
                                </div>
                            </div>
                             <div class="row offer-onn">
							 
							  <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="waitingarea">Waiting Area*</label>
											<input type="radio" value="1" id="waitingareaYes" checked="checked"  name="waitingarea">Yes
											<input type="radio" id="waitingareaNo" value="0" name="waitingarea">No
										</div>
                                    </div>
									
									<!--<div id="divwaitingareaoffer" class="margin0">
										<div class="col-md-4">
											<div class="form-group">
												<label>Offered on</label>
												<input type="text" class="form-control required" value="" id="" name="waitingarea_offer" maxlength="128">
											</div>
										</div>
										 <div class="col-md-4">
											<div class="form-group">	
												<label>Dispalay on</label>
												<input type="text" class="form-control required" value="" id="" name="waitingarea_display" maxlength="128">
											</div>
										</div>
									</div>-->
                                </div>
								 <div class="col-md-6">
										<div class="form-group">
											<div class="check_div">
												<label for="receptionarea">Reception Area *</label>										 
												<input type="radio" value="1" id="receptionareaYes" checked="checked"  name="receptionarea">Yes
												<input type="radio" id="receptionareaNo" value="0" name="receptionarea">No
											</div>
										</div>
										<!--<div id="divreceptionareaoffer" class="margin0">
											<div class="col-md-4">
												<div class="form-group">
													<label>Offered on</label>
													<input type="text" class="form-control required" value="" id="" name="receptionarea_offer" maxlength="128">
												</div>
											</div>
											 <div class="col-md-4">
												<div class="form-group">	
													<label>Dispalay on </label>
													<input type="text" class="form-control required" value="" id="" name="receptionarea_display" maxlength="128">
												</div>
											</div>
										</div>-->
									</div>
								
								
								
				              </div>			


								<div class="row offer-onn">
								
									<div class="col-md-6">
										<div class="form-group">
											<div class="check_div">
												<label for="parkingspace">Parking Space*</label>
												<input type="radio" value="1" id="parkingspaceYes" checked="checked"  name="parkingspace">Yes
												<input type="radio" id="parkingspaceNo" value="0" name="parkingspace">No
											</div>
										</div>
										
										<!--<div id="divparkingspaceoffer" class="margin0">
											<div class="col-md-4">
												<div class="form-group">
													<label>Offered on</label>  
													<input type="text" class="form-control required" value="" id="" name="parkingspace_offer" maxlength="128">
												</div>
											</div>
											 <div class="col-md-4">
												<div class="form-group">	
													<label>Dispalay on </label>
													<input type="text" class="form-control required" value="" id="" name="parkingspace_display" maxlength="128">
												</div>
											</div>
										</div>-->
										
										
									</div>
								
								
								
									<div class="col-md-6">
										<div class="form-group">
											<div class="check_div">
												<label for="receptionarea">Hard Copy *</label>										 
												<input type="radio" value="1" id="hard_copy" checked="checked"  name="hard_copy">Yes
												<input type="radio" id="hard_copy" value="0" name="hard_copy">No
											</div>
										</div>
									</div>
									
								</div>
							
							
							 <div class="row offer-onn">
							  <div class="col-md-6">
                                    <div class="form-group">                                     
										<div class="check_div">
											<label for="homecollection">Home Collection*</label>										
											<input type="radio" value="1" id="hcollectYes"   name="homecollection">Yes
											<input type="radio" id="hcollectNo" value="1"  checked="checked" name="homecollection">No
										</div>                                        
                                    </div>
							 </div>
							 
							 
							 <div class="col-md-6">
								<div class="form-group">
									<div class="check_div">
										<label for="softcopy_onmail">Soft Copy on Mail *</label>										 
										<input type="radio" value="1" id="softcopy_onmail" checked="checked"  name="softcopy_onmail">Yes
										<input type="radio" id="softcopy_onmail" value="0" name="softcopy_onmail">No
									</div>
								</div>
							</div>
							
							<div id="divhomecollectioncharge">
							  <div class="col-md-6">
                                    <div class="form-group">
								
									<label >Charge  (In Rs)</label> 
									<input type="text" name="hcollect_charge" id="hcollect_charge" class="form-control required">
									
								</div></div>
							  </div>
							  
							  <div id="divhomecollectiontime">
							   
							   <div class="col-md-6">
                                    <div class="form-group">
								
									<label >Time (In Hours)</label> 
									<input type="text" name="hcollect_time" id="hcollect_time"  class="form-control">
									
								</div></div>
							  </div>
							 
							 </div>
							 
							  
							   
							 <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        
										 <label for="discountofferdtest">Discount Offered On Test *</label>
                                      <input type="text" class="form-control " id="discountofferedtest" name="discountofferedtest" >
                                        <span id="disError" class="errorMsg"> <span>
                                    </div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discountofferdtest">Discount Offered On Test Displayed*</label>
                                      <input type="text" class="form-control" id="discountofferedtestdisplayed" name="discountofferedtestdisplayed" >
									    <span id="disoffError" class="errorMsg"> <span>    
                                    </div>
								
								</div>
								
							</div>
							
							 <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discountofferedhealthcheckup">Discount Offered  On Health Checkup* </label>
										
										  <input type="text" class="form-control " id="discountofferedhealthcheckup" name="discountofferedhealthcheckup" >
										
                                         <span id="disoffhchkError" class="errorMsg"> <span>
                                    </div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discountofferedhealthcheckupdisplayed">Discount Offered  On Health Checkup Displayed*</label>
                                      <input type="text" class="form-control " id="discountofferedhealthcheckupdisplayed" name="discountofferedhealthcheckupdisplayed" >
									  
									  <span id="disoffhdisplaychkError" class="errorMsg"> <span>
                                    </div>
								
								</div>
								
							</div>
							  
										
							<div class="row offer-onn">
								<div class="col-sm-12">
									<div class="check_div">
										<label for="discount displayed">24X7(Availability)</label>  
										<input type="radio" id="chktimeYes"  value="1" name="chk_time" /> Yes	
										<input type="radio" id="chktimeNo" value="0" checked="checked" name="chk_time" /> No
									</div>
								</div>
								
								<div class="" id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom" >Time From</label> 
									<input type="text" name="timefrom" id="timefrom" class="form-control required">
									
								</div>
									
								
							
									<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto"  class="form-control required">
								
								</div>
							 
							 
                            </div>
								
							</div> 
							
							<div class="row offer-onn">
								<div class="col-sm-12">
									<div class="check_div">
										<label for="discount displayed">Sunday Open</label>  
										<input type="radio" id="sundayopenYes"  value="1" name="sundayopen" /> Yes
										<input type="radio" id="sundayopenNo" value="0" checked="checked" name="sundayopen" /> No
									</div>  
								</div>							
								 <div id="divsundayopen" >								 
									<div class="col-md-4">									
											<label for="timefrom">Time From</label> 
											<input type="text" name="sun_timefrom" id="suntimefrom" class="form-control required">
											
									</div>
									
								
							
									<div class="col-md-4">
										<label for="timeto">Time To</label>
										<input type="text" name="suntimeto" id="suntimeto"  class="form-control required">
									
									</div>							 
								 
								</div>
							</div>
							
                    
                                  							
							  
							
							
							
										
							
							<div class="row offer-onn">							
                             
								<div class="col-md-6">
									<div class="form-group">
										<div class="check_div">
											<label for="discount displayed">X-Ray Film </label>  
											<input type="radio" id="xray_filmYes"  value="1" name="xray_film" /> Yes
											<input type="radio" id="xray_filmNo" value="0" checked="checked" name="xray_film" /> No
										</div>
									</div>
								</div>							
							
							</div>
							
							<div class="row offer-onn">
							
                             
							 		<div class="col-md-6">
										<div class="form-group">
											<div class="check_div">
												<label for="discount displayed">Discount Available On Package</label>  
											 <input type="radio" id="discount_availableon_packageYes"  value="1" name="discount_availableon_package" /> Yes
											<input type="radio" id="discount_availableon_packageNo" value="0" checked="checked" name="discount_availableon_package" /> No
											</div>
										</div>
									</div>
							
							
                             
									<div class="col-md-6">
										 <div class="form-group">
											<div class="check_div">
												<label for="discount displayed">Discount Available On Individual Test</label>  
												<input type="radio" id="centretime"  value="1" name="discountavailableonindividualtests" /> Yes
												<input type="radio" id="centretime" value="0" checked="checked" name="discountavailableonindividualtests" /> No
											</div>
										 </div>
									</div>
							
							
							</div>
                    
                                  							
							 	<div class="row offer-onn">
							
                             
							 		<div class="col-md-6">
										<div class="form-group">
											<div class="check_div">
												<label for="discount displayed">Report upload </label>  
												<input type="radio" id="reportsuploadYes"  value="1" name="reportsupload" /> Yes
												<input type="radio" id="reportsuploadNo" value="0" checked="checked" name="reportsupload" /> No
											</div>
										</div>
									</div>
							
							
							</div>
							 
							
							
							   <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph* </label>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                        <span id="fileError" class="errorMsg"> <span>
                                    </div>
                                </div>
								
								</div>
								
								
								
							
								  <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea class="form-control" id="address" name="address" ></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" class="form-control  equalTo" id="text" id="location" name="location" maxlength="50">
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <select class="form-control " id="state" name="state">
                                           
                                           
										   
										    <option value="0">Select State</option>
                                            <?php
                                        foreach($state as $row)
                                          {
                                       echo '<option value="'.$row->id.'">'.$row->state.'</option>';
                                          }
                                         ?>
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
								
                                        <label for="District">District</label>
                                       <select class="form-control " id="district" name="district">
									   
									   <option value="0"/>Select State First</option>
									   
									   </Select>
                                    </div>
                                </div>
                            </div>
                           
						   
						   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <select class="form-control " id="city" name="city">
                                           
											
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pincode">pincode</label>
                                       <select class="form-control " id="pincode" name="pincode">
                                            <option >Select Pincode</option>
											
											</select>OR  <input type="text" class="form-control " id="pincode1" name="pincode1" maxlength="50">
                                    </div>
                                </div>
                            </div>
								
								
						
								
								
								
								
				<div id='TextBoxesGroup'>	
					<div class="TextBoxDiv1">
								
								<div class="col-md-3">
								<label for="contact-person">Contact Person</label>
								<input type="text" id="contact-person" name="contact[]" class="form-control"> 
								</div>
								<div class="col-md-3">
									<label for="landline">Land Line</label> 
									<input type="text" id="landline" name="landline[]" class="form-control">
								</div>
								<div class="col-md-3">
									<label for="email">Email</label>
									<input type="email" name="email[]" id="email" class="form-control">
								</div>
								<div class="col-md-3">
									<label for="mobile">Mobile</label>
									<input type="text" name="mobile[]" id="mobile" class="form-control">
								</div>
					</div>
				</div>	



			
                        <div>
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
							<div>
							 
							  <br/>
							</div>
                        </div>
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
							
								
			</div>
				
				
				
		</div>		
								
								
	</div>			

				
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit"  class="btn btn-primary"  id="btnSubmit" value="Submit" />
                            <input type="reset" class="btn btn-default"   value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
			

 
      

	  </div>    
    </section>
    
</div>
<script>
$(document).ready(function(){

    var counter = 2;		
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<div class="col-md-3"><label for="contact-person">Contact Persion</label><input type="text" id="contact-person" name="contact[]" class="form-control"> </div><div class="col-md-3"><label for="landline">Land Line</label> <input type="text" id="landline" name="landline[]" class="form-control"></div><div class="col-md-3"><label for="email">Email</label><input type="email" name="email[]" id="email" class="form-control"></div><div class="col-md-3"><label for="mobile">Mobile</label><input type="text" name="mobile[]" id="mobile" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");				
	counter++;
    });
	
	
	
	

     
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
	
		 
	$('#timefrom').timepicker();
	$('#timeto').timepicker();
	
	$('#suntimefrom').timepicker();
	$('#suntimeto').timepicker();
	
	
	
});

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
 <script > $(".livesearch").chosen();</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>