
<?php 



if(!empty($medicalPractitionerInfo))
{
	
    foreach ($medicalPractitionerInfo as $uf)
    {
        $id = $uf->id;
        $mcode = $uf->masterCode;
	
        $docSubCode = $uf->docSubCode;
        $doctorName = $uf->doctorName;
        $specialization =$uf->specialization;
		$remarks=$uf->remarks;
		$qualification=$uf->qualification;
		$license=$uf->license;
		$experience=$uf->experience;
		$consultationCharges=$uf->consultationCharges;
		$discountOffered=$uf->discountOffered;
		$discountDisplayed=$uf->discountDisplayed;
		$opinionViaMail=$uf->opinionViaMail;
		
		$opinionViaMailCharges=$uf->opinionViaMailCharges;
	
		$opinionViaVideoConferencing=$uf->opinionViaVideoConferencing;
		
		$opinionViaVideoConferencingCharges=$uf->opinionViaVideoConferencingCharges;
		
		
		
		
		$opinionmail=$uf->opinionmail;
		$opinionvideo=$uf->opinionvideo;
		$time_24_hours=$uf->time_24_hours;
		$timefrom=$uf->timefrom;
		$timeto=$uf->timeto;
		$addressof_visitclinic=$uf->addressof_visitclinic;
		$availableonphone=$uf->availableonphone;
		
		
		
		$contactname=$uf->contactname;
	    $contactname=explode(',',$contactname);

		$email=$uf->email;
		$email=explode(',',$email);
		
		$landline=$uf->landline;
		$landline=explode(',',$landline);
		
		$mobile=$uf->mobile;
		$time_24_hours=$uf->time_24_hours;
		$mobile=explode(',',$mobile);
		
	    $photograph=$uf->photograph;
		
		
    }
}
 ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>Hospital Referred Doctors 
        <small> Edit </small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Doctor Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form");
					 ?>
				
                    <form role="form" id="addHospitalReferredDoctors" action="<?php echo base_url().'admin/' ?>editHospitalDoctors/<?php echo $id;?>" method="post" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
							
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                        <input type="text" class="form-control required" value="<?php echo $mcode;?>" id="mastercode" name="mastercode" maxlength="128" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="doctor">Name Of Doctor*</label>
                                        <input type="text" class="form-control required" value="<?php echo $doctorName;?>" id="doctorname" name="doctorname" maxlength="128">
                                    </div>
                                    
                                </div>
                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Specialization*</label>
                                        <input type="text" class="form-control required" id="specialization" value="<?php echo $specialization;?>"  name="specialization" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea   id="remarks"  name="remarks" class="form-control " ><?php echo $remarks;?></textarea>
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" class="form-control " value="<?php echo $qualification;?>" id="qualification" name="qualification" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="license">License/Registration No.</label>
                                        <input type="text" value="<?php echo $license;?>" class="form-control " id="license" name="license" >
                                    </div>
                                </div>
                            </div>
                           
						   
						   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="experience">Years of Experience</label>
                                        <input type="text" class="form-control  " value=" <?php echo $experience;?>" id="experience" name="experience" >
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="charges">Consultation Charges*</label>
                                        <input type="text" class="form-control required " value=" <?php echo $consultationCharges;?>" id="charges" name="charges" >
                                    </div>
                                </div>
                            </div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount Offered*</label>
                                        <input type="text" class="form-control required " id="discountoffered" value=" <?php echo $discountOffered;?>" name="discountoffered" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount displayed">Discount Diplayed*</label>
                                        <input type="text" class="form-control required " id="discountdisplayed" value=" <?php echo $discountDisplayed;?>" name="discountdisplayed" >
                                    </div>
                                </div>
                            </div>
							
							
							
							<div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Mail*</label>
										 
                                        <input type="radio" value="1" id="opinionmailYes" <?php if($opinionmail=='1' ) echo "checked"  ;?>   name="opinionmail">Yes
										 <input type="radio" id="opinionmailNo" value="0"  <?php if($opinionmail=='0' ) echo "checked"  ;?>name="opinionmail">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6" id="optionviamaildiv">
                                    <div class="form-group">
                                        <label for="opinion via mail charges">opinion via mail charges*</label>
                                        <input type="text" class="form-control required " id="opinionviamailcharge" name="opinionviamailcharge" value="<?php echo $opinionViaMailCharges;  ?>"  >
                                    </div>
                                </div>
				         </div>	


                             <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Video Conferencing *</label>
										 
                                        <input type="radio" value="1" id="opinionvideoYes" <?php if($opinionvideo=='1' ) echo "checked"  ;?>  name="opinionvideo">Yes
										 <input type="radio" id="opinionvideoNo" value="0"  <?php if($opinionvideo=='0' ) echo "checked"; ?>  name="opinionvideo">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6" id="videoConferdiv">
                                    <div class="form-group">
                                        <label for="opinion via video charges">Opinion Via Video charges*</label>
                                        <input type="text" class="form-control required " id="opinionviavideocharge" name="opinionviavideocharge" value="<?php echo $opinionViaVideoConferencingCharges;  ?>" >
                                    </div>
                                </div>
				      </div>
						

					 
					   <label for="discount displayed">24X7(Availability)</label>  
										Yes <input type="radio" id="chktimeYes"  value="1" <?php if($time_24_hours=='1' ) echo "checked"  ;?> name="chk_time" />
										 No<input type="radio" id="chktimeNo" value="0"  <?php if($time_24_hours=='0' ) echo "checked"  ;?>  name="chk_time" />
										 
							
                    
                                  							
							  <div class="row"  id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom"  value="<?php echo $timefrom;?>" id="timefrom" class="form-control required">
									
								</div>
									
								
							
									<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto"  value="<?php echo $timeto;?>" class="form-control required">
								
								</div>
							 
							 
                            </div><br/>
							
							   <div class="row">
							
									
							  <div class="col-md-6">
                              <div class="form-group">
								 <label for="license">Address of visiting Clinic</label>
                                        <input type="text" class="form-control required" id="license" name="addressof_visitclinic" value="<?php echo $addressof_visitclinic;  ?>" >	
									
							</div>
							</div>
							<br/>
							 <div class="col-md-6">
                              <div class="form-group">
									
								   <label for="discount displayed">Available on Phone</label>  
										Yes <input type="radio" id="chktimeYes"  value="1" name="availableonphone" />
										 No<input type="radio" id="chktimeNo" value="0" checked="checked" name="availableonphone" />	
							</div>
							</div>
									
						</div>

						   
						   
						   	    <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph *</label><img src="<?php echo $photograph;?>" height="50" width="50"></img>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                        
                                    </div>
                                </div>
								
								
								
				</div>
						   
						   
						   
						   
						   <?php for($i=0;$i<sizeof($contactname);$i++){ ?>
							 </div>
							<div id='TextBoxesGroup'>	
							<div class="TextBoxDiv1">

								<div class="col-md-3">
								<label for="contact-person">Contact Persion</label>
								
								
								<input type="text" id="contact-person" name="contact[]" value="<?php echo $contactname[$i]; ?>" class="form-control"> 
								
								</div>
								<div class="col-md-3">
									<label for="landline">Land Line</label> 
									
									<input type="text" id="landline" value="<?php echo $landline[$i]; ?>" name="landline[]" class="form-control">
									
								</div>
								<div class="col-md-3">
									<label for="email">Email</label>
									
									<input type="text" name="email[]" value="<?php echo $email[$i]; ?>" id="email" class="form-control">
									
								</div>
								<div class="col-md-3">
									<label for="mobile">Mobile</label>
									
									<input type="text" name="mobile[]" value="<?php if(isset($mobile[$i]))  echo $mobile[$i]; ?>" id="mobile" class="form-control">
									
								</div>
							</div>
			
						<?php } ?>						
                           		
  							<div>
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
							</div>
						   
						   
						   
								

				
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="hidden" name="doctortype" value="medicalpractitioner">
						<input type="hidden" name="photograph1" value="<?php echo $photograph;?>">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
   
</div>
<script>
$(document).ready(function(){

    var counter = 2;		
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<div class="col-md-3"><label for="contact-person">Contact Persion</label><input type="text" id="contact-person" name="contact[]" class="form-control"> </div><div class="col-md-3"><label for="landline">Land Line</label> <input type="text" id="landline" name="landline[]" class="form-control"></div><div class="col-md-3"><label for="email">Email</label><input type="email" name="email[]" id="email" class="form-control"></div><div class="col-md-3"><label for="mobile">Mobile</label><input type="text" name="mobile[]" id="mobile" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");				
	counter++;
    });
	
	
    
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
	 
	
  $('#timefrom').timepicker();
	$('#timeto').timepicker();		
			
  });
  

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>