<?php 


$doctorInfo=$this->hospital_model->getHospitalDoctorInfo($id);

 ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Add Doctor Schedule 
        <small>Add </small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo 'Enter Doctor -'.$doctorInfo[0]->doctorName .' Schedule Details';?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="healthcheckuptest" action="<?php echo base_url().'admin/';?>addDoctorsSchedule/<?php echo $id;?>" method="post" role="form">
                        <div class="box-body">
                 	<div id='TextBoxesGroup'>		
							<div id="TextBoxDiv1">
								<div class="col-md-4">
								<label for="days">OPD Consultation Days</label>
									<select name="days[]" class="form-control required">
									 <option id='1'>Monday</option>
									 <option id='2'>Tuesday</option>
									 <option id='3'>Wednesday</option>
									 <option id='4'>Thursday</option>
									 <option id='5'>Friday</option>
									 <option id='6'>Saturday</option>
									 <option id='7'>Sunday</option>
									</select>
								</div>
								<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom[]" id="timefrom" class="form-control required">
									
								</div>
									
								
							
									<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto[]" id="timeto"  class="form-control required">
								
								</div>
							</div>
						</div>
						
						<div>
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
						<div>
						
						
						
								
						</div>
				</div>		
								
                            </div>
                           
							
							
							
		
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						
						<input type="hidden" name="type" value="doctor">
						<input type="hidden" name="subcode" value="<?php echo $doctorInfo[0]->docSubCode;?>">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
				 <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
				
            </div>
           
        </div>    
    </section>
    
</div>

<script>
// Code to add more fields for doctor
$(document).ready(function(){

    var counter = 2;	

var counter2 = 1;
	var timefromname = "timefrom"+counter2;
	$('#timefrom').timepicker();
	$('#timeto').timepicker();	
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<div class="col-md-4"><label for="contact-person">Sample Collection Days</label><select id="days" name="days[]" class="form-control"><option id="1">Monday</option><option id="2">Tuesday</option><option id="3">Wednesday</option><option id="4">Thursday</option><option id="5">Friday</option><option id="6">Saturday</option><option id="7">Sunday</option></select></div><div class="col-md-4"><label for="timefrom">Time From</label> <input type="text" name="timefrom[]" id="timefrom'+counter2+'" class="form-control"></div><div class="col-md-4"><label for="timeto">Time To</label><input type="text" name="timeto[]" id="timeto'+counter2+'" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");	
$('#timefrom'+counter2).timepicker();
$('#timeto'+counter2).timepicker();		
	counter++;
	counter2++;
    });

     
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
		
    
  });

</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>