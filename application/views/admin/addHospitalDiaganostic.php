
<?php 


$mastercode=$this->hospital_model->getHospitalMasterCode();

 ?>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Hospital Doctorsn <small>Add Doctor</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Doctor Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form");
					 ?>
				
                    <form role="form" id="addHospitalDiaganosticForm" action="<?php echo base_url().'admin/' ?>addNewHospitalDoctors" method="post" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
							
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                        <select class="livesearch" id="mastercode" name="mastercode">
                                            <option value="0">Select Mastercode*</option>
                                           
                                               <?php foreach($mastercode   as $value){?>
                                                  <option value="<?php echo $value->mastercode;?>"><?php echo $value->mastercode;?></option>  
											   <?php } ?>
                                        </select>
										
										<span class="mcodeSpan"></span>

										
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="doctor">Name Of Doctor*</label>
                                        <input type="text" class="form-control required" value="" id="doctorname" name="doctorname" maxlength="128">
                                    </div>
                                    
                                </div>
                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Specialization*</label>
                                        <input type="text" class="form-control required" id="specialization" name="specialization" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea   id="remarks"  name="remarks" class="form-control " ></textarea>
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" class="form-control " id="qualification" name="qualification" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="license">License/Registration No.</label>
                                        <input type="text" class="form-control " id="license" name="license" >
                                    </div>
                                </div>
                            </div>
                           
						   
						   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="experience">Years of Experience</label>
                                        <input type="text" class="form-control  " id="experience" name="experience" >
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="charges">Consultation Charges*</label>
                                        <input type="text" class="form-control required " id="charges" name="charges" >
                                    </div>
                                </div>
                            </div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount Offered*</label>
                                        <input type="text" class="form-control required " id="discountoffered" name="discountoffered" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount displayed">Discount Diplayed*</label>
                                        <input type="text" class="form-control required " id="discountdisplayed" name="discountdisplayed" >
                                    </div>
                                </div>
                            </div>
							<div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Mail*</label>
										 
                                        <input type="radio" value="1" id="opinionmail" checked="checked"  name="opinionmail">Yes
										 <input type="radio" id="opinionmail" value="0" name="opinionmail1">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinion via mail charges">opinion via mail charges*</label>
                                        <input type="text" class="form-control required " id="opinionviamailcharge" name="opinionviamailcharge" >
                                    </div>
                                </div>
				</div>	


                             <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Video Conferencing *</label>
										 
                                        <input type="radio" value="yes" id="opinionvideo" checked="checked"  name="opinionvideo">Yes
										 <input type="radio" id="opinionvideo" value="no" name="opinionvideo">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinion via video charges">Opinion Via Video charges*</label>
                                        <input type="text" class="form-control required " id="opinionviavideocharge" name="opinionviavideocharge" >
                                    </div>
                                </div>
				</div>			


                                <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph </label>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                        
                                    </div>
                                </div>
								
								</div>
								
							<div class="input_fields_wrap">
								</a></div>
								<div class="col-md-4">
								<label for="contact-person">OPD Consultation Days</label>
								<select id="days" name="days[]" class="form-control">
                                 <option id='1'>Monday</option>
								 <option id='2'>Tuesday</option>
								 <option id='3'>Wednesday</option>
								 <option id='4'>Thursday</option>
								 <option id='5'>Friday</option>
								 <option id='6'>Saturday</option>
								 <option id='7'>Sunday</option>

								</select>
								</div>
								<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<select  id="timefrom" name="timefrom" class="form-control">
<?php for($i = 0; $i < 24; $i++): ?>
  <option value="<?= $i; ?>"><?= $i % 12 ? $i % 12 : 12 ?>:00 <?= $i >= 12 ? 'pm' : 'am' ?></option>
<?php endfor ?>
</select>
									
									</div>
									
								
								<div class="col-md-4">
									<label for="timeto">Time To</label>
									<select  id="timefrom" name="timefrom" class="form-control">
<?php for($i = 0; $i < 24; $i++): ?>
  <option value="<?= $i; ?>"><?= $i % 12 ? $i % 12 : 12 ?>:00 <?= $i >= 12 ? 'pm' : 'am' ?></option>
<?php endfor ?>
</select>
								</div>
								
							</div>
				</div>		
								
								
				</div>			

				
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" id="btnSubmit" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
 <script > $(".livesearch").chosen();</script>
<script>
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><div class="col-md-4" style="margin-top:15px"><input type="text" name="contact[]" class="form-control"/></div><div class="col-md-4" style="margin-top:15px"><input type="text" name="landline[]" class="form-control"/></div><div class="col-md-4" style="margin-top:15px"><input type="text" name="email[]" class="form-control"/></div></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

</script>
