
<?php 



if(!empty($hospitalDoctorInfo))
{
	
    foreach ($hospitalDoctorInfo as $uf)
    {
        $id = $uf->id;
        $mcode = $uf->masterCode;
	
        $docSubCode = $uf->docSubCode;
        $doctorName = $uf->doctorName;
        $specialization =$uf->specialization;
		$remarks=$uf->remarks;
		$qualification=$uf->qualification;
		$license=$uf->license;
		$experience=$uf->experience;
		$consultationCharges=$uf->consultationCharges;
		$discountOffered=$uf->discountOffered;
		$discountDisplayed=$uf->discountDisplayed;
		$opinionViaMail=$uf->opinionViaMail;
		
		$opinionViaMailCharges=$uf->opinionViaMailCharges;
	
		$opinionViaVideoConferencing=$uf->opinionViaVideoConferencing;
		
		$opinionViaVideoConferencingCharges=$uf->opinionViaVideoConferencingCharges;
		
		
		$photograph=$uf->photograph;
		
		
    }
}
 ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Hospital Doctors
        <small> Edit </small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Doctor Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form");
					 ?>
				
                    <form role="form" id="editHospitalDoctors" action="<?php echo base_url().'admin/' ?>editHospitalDoctors/<?php echo $id;?>" method="post" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
							
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                        <input type="text" class="form-control required" value="<?php echo $mcode;?>" id="matercode" name="mastercode" maxlength="128" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="doctor">Name Of Doctor*</label>
                                        <input type="text" class="form-control required" value="<?php echo $doctorName;?>" id="doctorname" name="doctorname" maxlength="128">
                                    </div>
                                    
                                </div>
                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Specialization*</label>
                                        <input type="text" class="form-control required" id="specialization" value="<?php echo $specialization;?>"  name="specialization" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea   id="remarks"  name="remarks" class="form-control " ><?php echo $remarks;?></textarea>
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" class="form-control " value="<?php echo $qualification;?>" id="qualification" name="qualification" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="license">License/Registration No.</label>
                                        <input type="text" value="<?php echo $license;?>" class="form-control " id="license" name="license" >
                                    </div>
                                </div>
                            </div>
                           
						   
						   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="experience">Years of Experience</label>
                                        <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control  " value=" <?php echo $experience;?>" id="experience" name="experience" >
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="charges">Consultation Charges*</label>
                                        <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control required " value=" <?php echo $consultationCharges;?>" id="charges" name="charges" >
                                    </div>
                                </div>
                            </div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount Offered*</label>
                                        <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control required " id="discountoffered" value=" <?php echo $discountOffered;?>" name="discountoffered" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount displayed">Discount Diplayed*</label>
                                        <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control required " id="discountdisplayed" value=" <?php echo $discountDisplayed;?>" name="discountdisplayed" >
                                    </div>
                                </div>
                            </div>
							<div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Mail*</label>
										 
                                        <input type="radio" value="1" id="opinionmailYes" <?php if($opinionViaMail=='1') echo"checked" ;?> name="opinionmail">Yes
										 <input type="radio" id="opinionmailNo" value="0" <?php if($opinionViaMail=='0') echo"checked" ;?>name="opinionmail">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6" id="optionviamaildiv">
                                    <div class="form-group">
                                        <label for="opinion via mail charges">opinion via mail charges*</label>
                                        <input type="text" class="form-control required " id="opinionviamailcharge" value=" <?php echo $opinionViaMailCharges;?>" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="opinionviamailcharge" >
                                    </div>
                                </div>
				</div>	


                             <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Video Conferencing *</label>
										 
                                        <input type="radio" value="1" <?php if($opinionViaVideoConferencing=='1') echo"checked" ;?> id="opinionvideoYes" checked="checked"  name="opinionvideo">Yes
										 <input type="radio" id="opinionvideoNo" <?php if($opinionViaVideoConferencing=='0') echo"checked" ;?> value="0" name="opinionvideo">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6" id="videoConferdiv">
                                    <div class="form-group">
                                        <label for="opinion via video charges">Opinion Via Video charges*</label>
                                        <input type="text" class="form-control required " id="opinionviavideocharge" value=" <?php echo $opinionViaVideoConferencingCharges;?>"  oninput="this.value=this.value.replace(/[^0-9]/g,'');"   name="opinionviavideocharge" >
                                    </div>
                                </div>
				</div>			


                                <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph *</label><img src="<?php echo $photograph;?>" height="50" width="50"></img>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                        
                                    </div>
                                </div>
								
								
								
				</div>			

				
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="hidden" name="doctortype" value="hospital">
						<input type ="hidden" name="photograph1" id="photograph1" value="<?php echo $photograph;?>">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>