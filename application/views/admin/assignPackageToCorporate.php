<?php
$healthPackages = $this->Corporatepackage_model->getHealthPackageList();
$clientDetails = $this->Corporatepackage_model->getCorporateClientList();
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fa fa-users"></i> Assign Health Checkup Package To Corporate Client</h1>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addCorporatePackages"
                          action="<?php echo base_url() . 'admin/' ?>assignHealthPackageToCorporatePostData"
                          method="post"
                          enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Health Checkup Package</label>
                                        <select class="form-control required" id="module" name="healthpackage[]"
                                                multiple>
                                            <?php foreach ($healthPackages as $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="corporateclient">Corporate Client</label>
                                        <select class="livesearch" required id="corporateclient" name="corporateclient"
                                                style="width:300px;">
                                            <option value="0">Select Corporate Client*</option>
                                            <?php foreach ($clientDetails as $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->client_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="product_start_date">
                                        <label for="startdate">Service Start Date</label>
                                        <input type="date" name="startdate" class="form-control required">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="product_end_date">
                                        <label for="enddate">Service End Date</label>
                                        <input type="date" name="enddate" class="form-control required">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Upload CSV </label>
                                        <input type="file" class="form-control required" id="dataImport"
                                               name="dataImport">
                                    </div>
                                    <strong><a href="<?php echo base_url() . 'uploads/corporate_clients/Client_File_Format.csv'; ?>">Click
                                            To Download CSV File Format For Data Import</a></strong>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="box-footer">
                    <input type="submit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
                    <input type="reset" class="btn btn-default" value="Reset"/>
                </div>
                </form>
            </div>

            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if ($error) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if ($success) {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<script> $(".livesearch").chosen();</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
