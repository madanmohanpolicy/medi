<?php 
$mastercode=$this->hospital_model->getChemistShopMasterCode();
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Chemist Shop 
        <small>Add Chemist Shop</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Chemist Shop Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form");
					 ?>
				
                    <form role="form" id="addHospitalDoctors" action="<?php echo base_url().'admin/' ?>addHospitalChemistShop" method="post" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
							
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                        <select class="livesearch" id="mastercode" name="mastercode" style="width:300px;">
                                            <option value="0">Select Mastercode*</option>
                                           
                                               <?php foreach($mastercode   as $value){?>
                                                  <option value="<?php echo $value->mastercode;?>"><?php echo $value->organisation.'-'.$value->mastercode;?></option>  
											   <?php } ?>
                                        </select>	
										<span class="mcodeSpan"></span>
                                    </div>
                                </div>
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="doctor">Registration No*</label>
                                        <input type="text" class="form-control required" value="" id="registrationn_no" name="registrationn_no" maxlength="128">
                                    </div>
                                    
                                </div>
                             
                            </div>
							
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label for="doctor">Owner Name </label>
                                        <input type="text" class="form-control required" value="" id="owner_name" name="owner_name" maxlength="128">
                                    </div>
                                </div>
                                
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Days</label>
                                        <input type="text" class="form-control required " id="days" name="days" >
                                    </div>
                                </div>
							</div>
							
							
							 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label for="doctor">Shop Address </label>
                                        <input type="text" class="form-control required" value="" id="shop_address" name="shop_address" maxlength="128">
                                    </div>
                                </div>
                                
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount</label>
                                        <input type="text" class="form-control required " id="discount" name="discount" >
                                    </div>
                                </div>
							</div>
							
							
							<div class="row offer-onn">
							<div class="col-md-6">
							<div class="form-group">
								<div class="check_div">
									<label for="discount displayed">Ayurvedic Medicines*</label>  
									<input type="radio" id="chk4Yes" value="1" name="ayurvedic_medicines" /> Yes
									<input type="radio" id="chkNo" value="0" name="ayurvedic_medicines" checked="checked" /> No
								</div>
								
							</div>
							</div>
								
							<div id="dvayurvedic" class="col-sm-12 margin0" style="display: none">
							  <div class="col-md-6">
                                   <div class="form-group">
                                     
									
										   
										
										Offered on Medicines<input type="text" class="form-control required" value="" id="" name="offeron_ayurvedic_medicines" maxlength="128">
										
                                    </div> </div>
										
										  <div class="col-md-6">
                                   <div class="form-group">
										
										Displayed on Medicines<input type="text" class="form-control required" value="" id="" name="displayon_ayurvedic_medicines" maxlength="128">
											
										</div>
									   
                                    </div>
									
                               
                              
								
								
							</div>
							
							</div>
							
							
							
							
							
							<div class="row offer-onn">
							<div class="col-md-6">
							<div class="form-group">
								<div class="check_div">
									<label for="discount displayed">Homeopathy   Medicines*</label>  
									<input type="radio" id="chkYes"  value="1" name="homeopathy_medicines" /> Yes
									<input type="radio" id="chkNo" value="0" checked="checked" name="homeopathy_medicines" /> No
								</div>
							</div>
							</div>
							
							<div class="col-sm-12 margin0" id="dvhomeopathy" style="display: none">
                                <div class="col-md-6">
                                    <div class="form-group">
                                     
									
										   
										
											Offered on Medicines<input type="text" class="form-control required" value="" id="offeredon_homeopathy_medicines" name="offeron_homeopathy_medicines" maxlength="128">  
											</div>
                                </div>
									<div class="col-md-6">
                                    <div class="form-group">
											Displayed on Medicines<input type="text" class="form-control required" value="" id="homeopathy_medicines" name="displayon_homeopathy_medicines" maxlength="128">
										
									   
                                    </div>
                                </div>
                           </div>
							
							</div>
							
							
						   
						   
						<div class="row offer-onn">
						<div class="col-md-6">
							<div class="form-group">
								<div class="check_div">
									<label for="discount displayed">Allopathy Medicines*</label>  
									<input type="radio" id="chk2Yes" value="1" name="allopathy_medicines" /> Yes
									<input type="radio" id="chkNo" value="0"  checked="checked" name="allopathy_medicines" /> No
								</div>
							</div>
						</div>
						
						<div class="col-sm-12 margin0" id="dvallopathy" style="display: none">
						   
						      <div class="col-md-6">
							  
							   <div class="form-group">
                                     
									
										   
										
										Offered on Medicines	<input type="text" class="form-control required" value="" id="" name="offeredon_allopathy_medicines" maxlength="128">
								</div>
                                    
                                </div>
								 <div class="col-md-6">
							     <div class="form-group">
										Displayed on Medicines		<input type="text" class="form-control required" value="" id="" name="displayedon_allopathy_medicines" maxlength="128">
										
									   
                                    </div>
                                    
                                </div>
                               
								
								
							 </div>
						
						</div>
						   
                           
						
							
							
								<div class="row offer-onn">
								<div class="col-md-6">
									<div class="form-group">
										<div class="check_div">
											<label for="discount displayed">Unani Medicines*</label>  
											<input type="radio" id="chk3Yes"  value="1"name="unani_medicines" /> Yes
											<input type="radio" id="chkNo" value="0" checked="checked" name="unani_medicines" /> No
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 margin0" id="dvunani" style="display: none">
								  <div class="col-md-6">
									  
										
										   <div class="form-group">
										 
											  
											
												Offered on Medicines <input type="text" class="form-control required" value="" id="" name="offeredon_unani_medicines" maxlength="128">
									</div>
									</div>
											 <div class="col-md-6">
									  
										
										   <div class="form-group">	
												Dispalay on Medicines <input type="text" class="form-control required" value="" id="" name="displayon_unani_medicines" maxlength="128">
												
											
										   
										</div>
										
									</div>
									
								</div>
								
								</div>
							
							   
							
							
								 <div class="row offer-onn">
							 
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount displayed">Timings </label>
                                        <input type="text" class="form-control required " id="timings" name="timings" >
                                    </div>
                                </div>
								<div class="col-md-6">
								   
									 <div class="form-group">
										<div class="check_div">
											<label for="discount displayed">Home Delivery*</label>  
											<input type="radio" id="chk5Yes" value="1" name="home_delivery" /> Yes
											<input type="radio" id="chkNo" value="0" checked="checked" name="home_delivery" /> No
										</div>
									</div>
                                </div>
							 
							 </div>
							
							   <div class="row offer-onn">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount offered on Consumables*</label>
                                        <input type="text" class="form-control required" id="dis_off_onconsumables" name="dis_off_onconsumables" >
                                    </div>
                                </div>
                                
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount to be displayed on Consumables*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_diplay_onconsumables" 
										name="dis_tobe_diplay_onconsumables" >
                                    </div>
                                </div>
							
							</div>
							
							   <div class="row offer-onn">
							   
							    <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount offered on FMCG*</label>
                                        <input type="text" class="form-control required " id="dis_off_onfmcg" name="dis_off_onfmcg" >
                                    </div>
                                </div>
								
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount to be displayed on FMCG*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_display_onemcg" 
										name="dis_tobe_display_onemcg" >
                                    </div>
                                </div>
							   
                               
                                
							
							</div>
							
							   <div class="row offer-onn">
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount offered on Specific Brand*</label>
                                        <input type="text" class="form-control required" id="dis_off_onspecific_brand" name="dis_off_onspecific_brand" >
                                    </div>
                                </div>
                                
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount to be displayed on Specific Brand*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_display_onspecific_brand" 
										name="dis_tobe_display_onspecific_brand"  >
                                    </div>
                                </div>
							</div>
							
							
							<div class="row offer-onn">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="check_div">
											<label for="discount displayed">24X7(Availability)</label>  
											<input type="radio" id="chktimeYes"  value="1" name="chk_time" /> Yes
											<input type="radio" id="chktimeNo" value="0" checked="checked" name="chk_time" /> No
										</div>
									</div>
								</div>
								
								 <div class="col-sm-12"  id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom" id="timefrom" class="form-control">
									
								</div>
									
								
							
									<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto"  class="form-control">
								
								</div>
							 
							 
                            </div>
								
							</div>
							
							
										 
							
                    
                                  							
							 
							
							
							
							
							  <div class="row">
							  
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph </label>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                        
                                    </div>
                                </div>
                               
                            </div>
							
							
							
							<div class="row">
							    <fieldset class="col-sm-12">
							     <legend>Contact Detail:</legend>
										<div class="col-md-6">
											<div class="form-group">
												 <label for="doctor">Name</label>
												<input type="text" class="form-control required" value="" id="name" name="name" maxlength="128">
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label for="discount offered">Email</label>
												<input type="text" class="form-control required " id="email" name="email" >
											</div>
								
								        </div>
										<div class="col-md-6">
											<div class="form-group">
												 <label for="doctor">Mobile No.</label>
												<input type="text" class="form-control required" value="" id="mobile" name="mobile" maxlength="128">
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label for="discount offered">Address:</label>
												<input type="text" class="form-control required " id="address" name="address" >
											</div>
								
								        </div>
								</fieldset>
							</div>
							
						
							
								 
				      </div>	
                      </div>
			  
					  </div>

            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>					  
                     </div>
				 
					 
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary"  id="btnSubmit" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>

        </div>    
    </section>
    
</div>
<script>
$(document).ready(function() {
	
	
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><div class="col-md-4" style="margin-top:15px"><input type="text" name="contact[]" class="form-control"/></div><div class="col-md-4" style="margin-top:15px"><input type="text" name="landline[]" class="form-control"/></div><div class="col-md-4" style="margin-top:15px"><input type="text" name="email[]" class="form-control"/></div></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
	
	
	
	/**
     * this script use for  from_time  and to_time
     * Create by Santosh Kumar Verma
	 * Date:13 September 2018
     * start
     */
    var tcounter = 2;	
    var counter2 = 1;
	var timefromname = "timefrom"+counter2;
	$('#timefrom').timepicker();
	$('#timeto').timepicker();	
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + tcounter);
                
	newTextBoxDiv.after().html('<div class="col-md-4"><label for="contact-person">Sample Collection Days</label><select id="days" name="days[]" class="form-control"><option id="1">Monday</option><option id="2">Tuesday</option><option id="3">Wednesday</option><option id="4">Thursday</option><option id="5">Friday</option><option id="6">Saturday</option><option id="7">Sunday</option></select></div><div class="col-md-4"><label for="timefrom">Time From</label> <input type="text" name="timefrom[]" id="timefrom'+counter2+'" class="form-control"></div><div class="col-md-4"><label for="timeto">Time To</label><input type="text" name="timeto[]" id="timeto'+counter2+'" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");	
    $('#timefrom'+counter2).timepicker();
    $('#timeto'+counter2).timepicker();		
	tcounter++;
	counter2++;
    });	
	
    /**
    * 
    * end
    */	
	
	
});


 
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
 <script > $(".livesearch").chosen();</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>