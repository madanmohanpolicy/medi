<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Master Code
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
				<?php 
			$module=$this->session->module;
	$modulenew=explode(',',$module);
				$permission= explode(',',$this->session->permission);
				if(in_array('1',$permission)&&in_array('4',$modulenew)|| $role == ROLE_ADMIN){
					
				
				?>
                    <a class="btn btn-primary" href="<?php echo base_url().'admin/'; ?>addNewHospital"><i class="fa fa-plus"></i> Add New</a>
					<?php 
			 }
				 ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Mastercode List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url().'admin/' ?>hospitalListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
					<th>Sr. No.</th>
					<th>Master Code</th>
                        <th>Organisation</th>
                        <th>Facility name</th>
                        <th>Address</th>
                        <th>Location</th>
                        <th>State Name</th>
						<th>District Name</th>
						<th>City Name</th>
						<th>Status</th>
						<th>Action</th>
						
                    </tr>
                    <?php
					$sr=0;
					
					
                    if(!empty($hospitalRecords))
                    {
						
                        foreach($hospitalRecords as $key=>$value)
                        {
							
						$stateName=$this->hospital_model->getStateName($value->state)	;
					$DistrictName=$this->hospital_model->getDistrictName($value->district)	;
					$cityName=$this->hospital_model->getCityName($value->city)	;
					
					$pincode=$this->hospital_model->getPincode($value->pincode)	;
				
							$sr++;
                    ?>
					

                    <tr>
					<td><?php echo $sr;?></td>
					<td><?php echo $value->mastercode;?></td>
					<td><?php echo $value->organisation;?></td>
					<td><?php if($value->type=='1') {echo "Chemist Shop" ;}?>
					<?php if($value->type=='2') {echo "Diaganostic Center " ;}?>
					<?php if($value->type=='3') {echo "Health Aggregator " ;}?>
					<?php if($value->type=='4') {echo "Hospital" ;}?>
					<?php if($value->type=='5') {echo "Medical Practitinor " ;}?>
					<?php if($value->type=='6') {echo "Other" ;}?>
					
					
					
					
					
					</td>
					
					
					
					<td><?php echo $value->address;?></td>
					<td><?php  echo $value->location;?></td>
					<td><?php if($stateName) { echo $stateName[0]->state; }?></td>
					<td><?php if($DistrictName) { echo $DistrictName[0]->district;}?></td>
					
					<td><?php if($cityName) { echo $cityName[0]->city; } ?></td>
					
					
			    <td> 
				<?php 
			    $module=$this->session->module;
			
				$permission= explode(',',$this->session->permission);
				if(in_array('2',$permission)&&$module=='2'|| $role == ROLE_ADMIN){
				?>
			   
				  <?php if($value->status==1){ ?>
				<a href="<?php echo base_url().'admin/chnageMasterCodeStatus/'.$value->id.'/'.$value->status; ?>"  class="btn btn-sm btn-info"  >InActive</a> 
				 <?php } else{ ?>
				  <a href="<?php echo base_url().'admin/chnageMasterCodeStatus/'.$value->id.'/'.$value->status; ?>" class="btn btn-sm  btn-danger" data-id="<?php echo $value->id; ?>" >Active</a>
				 <?php }
				}
				 ?></td>
					
					
                      <td class="text-center">
                          
								<?php if(in_array('2',$permission)&&in_array('4',$modulenew)|| $role == ROLE_ADMIN){ ?>
								<a class="btn btn-sm btn-info" href="<?php echo base_url().'admin/editOldHospital/'.$value->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a><?php } 
									if(in_array('3',$permission)&&in_array('4',$modulenew)|| $role == ROLE_ADMIN){
								?>
									<a class="btn btn-sm btn-danger deleteHospital" href="#" data-id="<?php echo $value->id; ?>" title="Delete"><i class="fa fa-trash"></i></a><?php } ?>
                        </td> 
                    </tr>
                    <?php
					
					
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "admin/hospitalListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
