<?php




 $organisationType=$this->hospital_model->getType();



if(!empty($hospitalInfo))
{
	
    foreach ($hospitalInfo as $uf)
    {
		
        $id = $uf->id;
        $mastercode = $uf->mastercode;
        $organisation = $uf->organisation;
        $type = $uf->type;
		
        $address =$uf->address;
		$location=$uf->location;
		$stateid=$uf->state;
	
		$districtid=$uf->district;
		
		
		$cityid=$uf->city;
		$pincodeid=$uf->pincode;
		$pincode1=$uf->pincodeother;
		$nabh_accredited=$uf->nabh_accredited;
		

		$nabl_accredited=$uf->nabl_accredited;
		
		
		$description=$uf->description;
		$contactname=$uf->contactname;
	$contactname=explode(',',$contactname);

		$email=$uf->email;
		$email=explode(',',$email);
		
		$landline=$uf->landline;
		$landline=explode(',',$landline);
		
		$mobile=$uf->mobile;
		$time_24_hours=$uf->time_24_hours;
		$mobile=explode(',',$mobile);
		
    }
	
	$timefrom=$hospitalInfo[0]->timefrom;
	$timeto=$hospitalInfo[0]->timeto;
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Mastercode  
        <small>Edit </small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
		
			<div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
		
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Mastercode Details </h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addHospital" action="<?php echo base_url().'admin/' ?>editHospital/<?php echo $id;?>" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="organisation">Name Of Organisation</label>
                                        <input type="text" class="form-control required" value="<?php echo $organisation; ?>" id="organisation" name="organisation" maxlength="128" readonly>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type">Type</label>
                                         <select class="form-control required" id="type" name="type">
                                            <option value="0">Select Type</option>
                                           <?php foreach($organisationType as $value){?>
                                                    <option value="<?php echo $value->id;?>"  <?php if( $value->id==$type)  echo "selected"; ?>><?php echo $value->organisation;?>fds</option>
													
										   <?php } ?>
                                           
                                        </select>
										
									 <input type="text" class="form-control required equalTo" id="text" id="type" name="type" value="<?php echo $value->organisation;?>" maxlength="50" readonly>	
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea class="form-control" id="address" name="address" ><?php echo $address; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" class="form-control required equalTo" id="text" id="location" name="location" value="<?php echo $organisation; ?>" maxlength="50">
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <select class="form-control required" id="state" name="state">
                                            <option value="0">Select State</option>
											 <?php
										$state = $this->hospital_model->getState();
										$district = $this->hospital_model->fetch_district($stateid);
									
										$city = $this->hospital_model->fetch_city($districtid);
										$pincode = $this->hospital_model->fetch_pincode($cityid);
									
							
                                        foreach($state as $row)
                                          {
											  ?>
											
                                       <option value="<?php echo $row->id; ?>" <?php  if($row->id==$stateid) echo "selected";?> ><?php echo $row->state;?></option>
                                          <?php }
                                         ?>
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="District">District</label>
                                       <select class="form-control required" id="district" name="district">
                                            <option value="0">Select District</option>
											<?php 
											  foreach($district as $row)
                                          { ?>
											  <option value="<?php echo $row->id; ?>" <?php  if ($row->id==$districtid) echo "selected"; ?>><?php echo $row->district;?></option>
										  <?php } ?>
											</select>
                                    </div>
                                </div>
                            </div>
                           
						   
						   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <select class="form-control required" id="city" name="city">
                                            <option value="0">Select City</option>
											
											<?php foreach($city as $row){
												?>
											 <option value="<?php echo $row->id; ?>" <?php  if ($row->id==$cityid) echo "selected"; ?>><?php echo $row->city;?></option>
										  <?php } ?>
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pincode">pincode</label>
                                       <select class="form-control required" id="pincode" name="pincode">
									   
                                            <option value="0">Select Pincode</option> 
												<?php foreach($pincode as $row){
												
												?>
												 <option value="<?phpecho $row->id; ?>" <?php  if ($row->pincode==$pincodeid) echo "selected"; ?>><?php echo $row->pincode;?></option>
										  <?php } ?>
											
											</select>OR  <input type="text" class="form-control " id="pincodeother" value= "<?php if($pincodeid=='') echo $pincode1;?>" name="pincode1" maxlength="50">
                                    </div>
                                </div>
                            </div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">NABH Accredited</label>
										 
                                        <input type="radio" value="1" id="nabh1" <?php if($nabh_accredited=='1' ) echo "checked"  ;?> name="nabh">Yes
										 <input type="radio" id="nabh2" value="0" <?php if($nabh_accredited=='0' ) echo "checked"  ;?> name="nabh">No
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pincode">NABL Accredited</label>
                                        <input type="radio" id="nabl1" value="1" name="nabl"   <?php if($nabl_accredited=='1' ) echo "checked"  ;?> >Yes
										 <input type="radio"  id="nabl2" value=0  <?php if($nabl_accredited=='0' ) echo "checked"  ;?> name="nabl">No
                                    </div>
                                </div>
                            </div>
							
							<label for="discount displayed">24X7(Availability) </label>  
										Yes <input type="radio" id="chktimeYes"  value="1" name="chk_time"  <?php if($time_24_hours=='1' ) echo "checked"  ;?> />
										 No<input type="radio" id="chktimeNo" value="0"  name="chk_time"  <?php if($time_24_hours=='0' ) echo "checked"  ;?>/>
							
							<div class="row" id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom[]" id="timefrom" value="<?php echo $timefrom;?>" class="form-control required">
									
									
								</div>
									
								
								<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto[]" id="timeto" value="<?php echo $timeto;?>" class="form-control required">
								</div>
							 
							 
                            </div><br/>
							
							
							
							
							<!--<div class="col-md-12 text-right"><a href="#" class="add_field_button">Add More</a></div>-->
						<?php for($i=0;$i<sizeof($contactname);$i++){ ?>
							 </div>
							<div id='TextBoxesGroup'>	
							<div class="TextBoxDiv1">

								<div class="col-md-3">
								<label for="contact-person">Contact Persion</label>
								
								
								<input type="text" id="contact-person" name="contact[]" value="<?php echo $contactname[$i]; ?>" class="form-control"> 
								
								</div>
								<div class="col-md-3">
									<label for="landline">Land Line</label> 
									
									<input type="text" id="landline" value="<?php echo $landline[$i]; ?>" name="landline[]" class="form-control">
									
								</div>
								<div class="col-md-3">
									<label for="email">Email</label>
									
									<input type="text" name="email[]" value="<?php echo $email[$i]; ?>" id="email" class="form-control">
									
								</div>
								<div class="col-md-3">
									<label for="mobile">Mobile</label>
									
									<input type="text" name="mobile[]" value="<?php if(isset($mobile[$i]))  echo $mobile[$i]; ?>" id="mobile" class="form-control">
									
								</div>
							</div>
			
						<?php } ?>						
                           		
  							<div>
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
							</div>
						<div>
						
						
						
								
						</div>
							
				</div>			
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
						</div>	
                    </form>
					
		
					
                </div>
            </div>
           
        </div>    
    </section>
    
</div>
<script>
$(document).ready(function(){
	

	

    var counter = 2;		
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<div class="col-md-3"><label for="contact-person">Contact Persion</label><input type="text" id="contact-person" name="contact[]" class="form-control"> </div><div class="col-md-3"><label for="landline">Land Line</label> <input type="text" id="landline" name="landline[]" class="form-control"></div><div class="col-md-3"><label for="email">Email</label><input type="email" name="email[]" id="email" class="form-control"></div><div class="col-md-3"><label for="mobile">Mobile</label><input type="text" name="mobile[]" id="mobile" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");				
	counter++;
    });

     
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
	 
	 
	 
	 // add by santosh 
	 
	$('#timefrom').timepicker();
	$('#timeto').timepicker();
		
    
  });



</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>

