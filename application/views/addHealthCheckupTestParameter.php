<?php 


$HealthCheckupTest=$this->hospital_model->getHealthCheckupTestInfo($id);

 ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>  Add  HealthCheckup Test Parameter 
       
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo 'Enter Test -<font color="blue">'.$HealthCheckupTest[0]->name .'</font> Parameter Details';?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="healthcheckuptestparameter" action="<?php echo base_url().'admin/';?>addHealthCheckupTestParameters/<?php echo $id;?>" method="post" role="form">
                        <div class="box-body">
                 	<div id='TextBoxesGroup'>		
							<div id="TextBoxDiv1">
								<div class="col-md-12">
								
									<input type="text" name="parameter[]" placeholder="Parameter" class="form-control">
								</div>
								
								
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top:20px;">
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
						<div>
						
						
						
								
						</div>
				</div>		
								
                            </div>
                           
							
							
							
		
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						
						<input type="hidden" name="type" value="doctor">
						<input type="hidden" name="subcode" value="<?php //echo $doctorInfo[0]->docSubCode;?>">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>

<script>
// Code to add more fields for doctor
$(document).ready(function(){

    var counter = 2;		
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<div class="col-md-12"><input type="text" name="parameter[]" id="parameter" placeholder="Parameter" class="form-control" style="margin-top:15px;"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");				
	counter++;
    });

     
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
		
    
  });

</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>