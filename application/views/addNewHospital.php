
<?php 
$state = $this->hospital_model->getState();
$organisationType=$this->hospital_model->getType();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Mastercode  
        <small>Mastercode Generate</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
		
		 <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
		
		
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Details for Mastercode </h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addHospital" action="<?php echo base_url().'admin/' ?>addNewHospital" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="organisation">Name Of Organisation</label>
                                        <input type="text" class="form-control required" value="" id="organisation" name="organisation" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type">Type</label>
                                        <select class="form-control required" id="type" name="type">
                                            <option value="0">Select Type</option>
                                           <?php foreach($organisationType as $value){ ?>
                                                    <option value="<?php echo $value->id;?>"><?php echo $value->organisation;?></option>
													
										   <?php } ?>
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea class="form-control" id="address" name="address" ></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" class="form-control required equalTo" id="text" id="location" name="location" maxlength="50">
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <select class="form-control required" id="state" name="state">
                                           
                                           
										   
										    <option value="0">Select State</option>
                                            <?php
                                        foreach($state as $row)
                                          {
                                       echo '<option value="'.$row->id.'">'.$row->state.'</option>';
                                          }
                                         ?>
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
								
                                        <label for="District">District</label>
                                       <select class="form-control required" id="district" name="district">
									   
									   <option value="0"/>Select State First</option>
									   
									   </Select>
                                    </div>
                                </div>
                            </div>
                           
						   
						   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <select class="form-control required" id="city" name="city">
                                           
											
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pincode">pincode</label>
                                       <select class="form-control required" id="pincode" name="pincode">
                                            <option >Select Pincode</option>
											
											</select>OR  <input type="text" class="form-control " id="pincode1" name="pincode1" maxlength="50">
                                    </div>
                                </div>
                            </div>
							
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">NABH Accredited</label>
										 
                                        <input type="radio" value="yes" id="nabh1" checked="checked"  name="nabh">Yes
										 <input type="radio" id="nabh2" value="no" name="nabh">No
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pincode">NABL Accredited</label>
                                        <input type="radio" id="nabl1"c name="nabl" checked="checked" >Yes
										 <input type="radio"  id="nabl2" name="nabl">No
                                    </div>
                                </div>
                            </div>
							
							
								<label for="discount displayed">24X7(Availability)</label>  
										Yes <input type="radio" id="chktimeYes"  value="1" name="chk_time" />
										 No<input type="radio" id="chktimeNo" value="0" checked="checked" name="chk_time" />
										 
							

									<div class="row"  id="divtime">
								
								
                                   
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom" id="timefrom" class="form-control required">
									
								</div>
									
								
							
									<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto"  class="form-control required">
								
								</div>
							 
							 
                            </div><br/>
							
							
							<div id='TextBoxesGroup'>	
							<div class="TextBoxDiv1">
								
								<div class="col-md-3">
								<label for="contact-person">Contact Person</label>
								<input type="text" id="contact-person" name="contact[]" class="form-control"> 
								</div>
								<div class="col-md-3">
									<label for="landline">Land Line</label> 
									<input type="text" id="landline" name="landline[]" class="form-control">
								</div>
								<div class="col-md-3">
									<label for="email">Email</label>
									<input type="email" name="email[]" id="email" class="form-control">
								</div>
								<div class="col-md-3">
									<label for="mobile">Mobile</label>
									<input type="text" name="mobile[]" id="mobile" class="form-control">
								</div>
							</div>
				</div>	



			
                        <div>
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
						<div>
						 
						  <br/>
		
						 
                        </div><!-- /.box-body -->
                      </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
           
			
        </div> 


					
    </section>
    
</div>
<script>




$(document).ready(function(){

    var counter = 2;		
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<div class="col-md-3"><label for="contact-person">Contact Persion</label><input type="text" id="contact-person" name="contact[]" class="form-control"> </div><div class="col-md-3"><label for="landline">Land Line</label> <input type="text" id="landline" name="landline[]" class="form-control"></div><div class="col-md-3"><label for="email">Email</label><input type="email" name="email[]" id="email" class="form-control"></div><div class="col-md-3"><label for="mobile">Mobile</label><input type="text" name="mobile[]" id="mobile" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");				
	counter++;
    });
	
	
	
	

     
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
		

//add by santosh
	$('#timefrom').timepicker();
	$('#timeto').timepicker();





  
  });




  
  
  
  




</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js">

</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>