<?php



//$packageid = '';
//$cost = '';
//$discount = '';


if(!empty($healthCheckupTestParameterInfo))
{
    foreach ($healthCheckupTestParameterInfo as $uf)
    {
	
        $id = $uf->id;
	
        $parameter = $uf->parameterName;
	$testId=$uf->testId;
        //$cost = $uf->cost;
       // $discount = $uf->discount;
    
		
    }
}



?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Health Test Parameter
        <small> Edit Health Test Parameter</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Health Test Parameter Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url().'admin/';?>editHealthChekupTestParameters/<?php echo $id;?>" method="post"  id="healthcheckuptest" role="form">
                        <div class="box-body">
                            <div class="row">
							
						
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="testname">Parameter Name</label>
                                        <input type="text" class="form-control " value="<?php echo $parameter;?>"  id="parameter" name="parameter" maxlength="128">
                                    </div>
                                    
                                </div>
                                
                            </div>
                           
                            </div>
                           
							
							
							
		
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="hidden" name="testid" value="<?php echo $testId;?>">
						 <input type="hidden" class="form-control " value="<?php echo $id;?>"  id="id" name="id" >
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>