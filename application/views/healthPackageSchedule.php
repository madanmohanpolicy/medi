<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Health Package Schedule
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
			
			<?php 
			$module=$this->session->module;
	$modulenew=explode(',',$module);
				$permission= explode(',',$this->session->permission);
				if(in_array('1',$permission)&&in_array('7',$modulenew)|| $role == ROLE_ADMIN){ ?>
               
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url().'admin/'; ?>addHealthPackageSchedule/<?php echo $id;?>"><i class="fa fa-plus"></i> Add New</a>
                </div><?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Doctors Schedule</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url().'admin/' ?>healthPackageSchedule/<?php echo $id;?>" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
					    <th>Sr. No.</th>
                        <th>Package Name</th>
						
						<th>Facility Name</th>
						<!-- <th>Package Name</th>-->
                        <th>Day</th>
                        <th>Time From</th>
                       <th> Time To</th>
					   
					   
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
					
					     $i=0;
					foreach($healthPackageLinkingSchedule as $value){
					
					$packageName=$this->hospital_model->getHealthCheckupPackageName($value->package);
					$organisation=$this->hospital_model->getHospitalInfo($value->hospital_diaganostic);
						//$doctorName=$this->hospital_model->getDoctorNameBySubcode($value->subcode);
					
					
			     $i++;
                    ?>
                    <tr>
					    <td><?php echo $i;?></td>
						
                       
                        <td><?php   if($packageName[0])echo $packageName[0]->name;?></td>
						  <td><?php  if($organisation[0]) echo $organisation[0]->organisation;?></td>
                        <td><?php echo $value->day;?></td>
						    <td><?php echo $value->timefrom;?></td>
							    <td><?php echo $value->timeto;?></td>
                       
                        <td >
						
						
                       
                            <?php if(in_array('2',$permission)&&in_array('7',$modulenew)|| $role == ROLE_ADMIN){ ?><a class="btn btn-sm btn-info" href="<?php echo base_url().'admin/editHealthPackageSchedule/'.$value->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a><?php } ?>
                            <?php if(in_array('3',$permission)&&in_array('7',$modulenew)|| $role == ROLE_ADMIN){ ?><a class="btn btn-sm btn-danger deletepackageschedule" href="#" data-id="<?php echo $value->id; ?>" title="Delete"><i class="fa fa-trash"></i></a><?php } ?>
                        </td>
                    </tr>
                    
					<?php } ?>
       
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "scheduleListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
