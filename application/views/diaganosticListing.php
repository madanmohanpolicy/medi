<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>Diaganostic Centre
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
				<?php 
			      $module=$this->session->module;
			      $permission= explode(',',$this->session->permission);
				  if(in_array('1',$permission)&&$module=='1'|| $role == ROLE_ADMIN){
				  ?>
                    <a class="btn btn-primary" href="<?php echo base_url().'admin/'; ?>addNewDiaganosticform"><i class="fa fa-plus"></i> Add New</a>
				<?php 
			      }
				 ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Diaganostic Centres List</h3>
                    <div class="box-tools">
					<p style=" margin:0 0 0 350px; color:green;"> <?php echo $this->session->flashdata('success'); ?></p>  <p style=" margin:0 0 0 350px; color:red;"> <?php echo $this->session->flashdata('error'); ?></p>
                        <form action="<?php echo base_url().'admin/' ?>hospitalDiaganosticListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
					<th>Sr. No.</th>
					<th>Master Code</th>
					<th>Diaganostic Centre Sub Code</th>
                        <th>Registration No.</th>
                        <th>NABL</th>
                        <th>Lab</th>
                        <th>Home Collection</th>
						
						<th>Status</th>
						<th>Action</th>
						
                    </tr>
                    <?php
					$sr=0;
				
                    if(!empty($diaganosticRecords))
                    {
						
                        foreach($diaganosticRecords as $value)
                        {
							
							$sr++;
                    ?>
					<tr>
					<td><?php echo $sr;?></td>
					<td><?php echo $value->organisation.'-'.$value->masterCode;?></td>
					<td><?php echo $value->diaganosticSubCode;?></td>
					<td><?php echo $value->registrationNo;?></td>
				    <td><?php if($value->nabl==1){echo "Yes";}else{echo "No";}?></td>
					<td><?php if($value->lab==1){echo "Yes";}else{echo "No";}?></td>
					<td><?php if($value->homeCollection==1){echo "Yes";}else{echo "No";}?></td>
					
					<td> 
				<?php 
			    $module=$this->session->module;
			
				$permission= explode(',',$this->session->permission);
				if(in_array('2',$permission)&&$module=='2'|| $role == ROLE_ADMIN){
				?>
			   
				  <?php if($value->status==1){ ?>
				<a href="<?php echo base_url().'admin/chnageDiaganosticCenterSubcodeStatus/'.$value->id.'/'.$value->status; ?>"  class="btn btn-sm btn-info"  >InActive</a> 
				 <?php } else{ ?>
				  <a href="<?php echo base_url().'admin/chnageDiaganosticCenterSubcodeStatus/'.$value->id.'/'.$value->status; ?>" class="btn btn-sm  btn-danger" data-id="<?php echo $value->id; ?>" >Active</a>
				 <?php }
				}
				 ?></td>
					
					<td class="text-center">
					
					   <!--<a class="btn btn-sm btn-primary" href="<? //= base_url().'login-history/'.$record->userId; ?>" title="Login history"><i class="fa fa-history"></i></a> | -->
					   
					   	<?php 
			$module=$this->session->module;
			
				$permission= explode(',',$this->session->permission);
				if(in_array('2',$permission)&&$module=='2'|| $role == ROLE_ADMIN){
				?>
                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'admin/editDiaganostic/'.$value->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
				<?php }?>
				
					<?php 
			$module=$this->session->module;
			
				$permission= explode(',',$this->session->permission);
				if(in_array('3',$permission)&&$module=='3'|| $role == ROLE_ADMIN){
				?>
							
                            <a class="btn btn-sm btn-danger deleteDiaganostic" href="#" data-id="<?php echo $value->id; ?>" title="Delete"><i class="fa fa-trash"></i></a>
							
				<?php } ?>
					</td> 
                     
                    </tr>
                    <?php
                     }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
               jQuery("#searchList").attr("action", baseURL + "admin/diaganosticListing/" + value);
			
            jQuery("#searchList").submit();
        });
    });
</script>
