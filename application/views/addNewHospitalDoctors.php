
<?php 


$mastercode=$this->hospital_model->getHospitalMasterCode();

 ?>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Hospital Doctors
        <small>Add Doctor</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Doctor Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form");
					 ?>
				
                    <form role="form" id="addHospitalDoctors" action="<?php echo base_url().'admin/' ?>addNewHospitalDoctors" method="post" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
							
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                        <select class="livesearch" id="mastercode" name="mastercode" style="width:300px;" required >
                                            <option value="0">Select Mastercode*</option>
                                           
                                               <?php foreach($mastercode   as $value){?>
                                                  <option value="<?php echo $value->mastercode;?>"><?php echo $value->organisation.'-'.$value->mastercode;?></option>  
											   <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="doctor">Name Of Doctor*</label>
                                        <input type="text" class="form-control required" value="" id="doctorname" name="doctorname" maxlength="128">
                                    </div>
                                    
                                </div>
                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Specialization*</label>
                                        <input type="text" class="form-control required" id="specialization" name="specialization" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea   id="remarks"  name="remarks" class="form-control " ></textarea>
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" class="form-control " id="qualification" name="qualification" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="license">License/Registration No.</label>
                                        <input type="text" class="form-control " id="license" name="license" >
                                    </div>
                                </div>
                            </div>
                           
						   
						   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="experience">Years of Experience</label>
                                        <input type="text" class="form-control  " id="experience" name="experience" >
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="charges">Consultation Charges*</label>
                                        <input type="text" class="form-control required " id="charges" name="charges" >
                                    </div>
                                </div>
                            </div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount Offered*</label>
                                        <input type="text" class="form-control required " id="discountoffered" name="discountoffered" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount displayed">Discount Diplayed*</label>
                                        <input type="text" class="form-control required " id="discountdisplayed" name="discountdisplayed" >
                                    </div>
                                </div>
                            </div>
							<div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Mail*</label>
										 
                                        <input type="radio" value="1" id="opinionmailYes" checked="checked"  name="opinionmail">Yes
										 <input type="radio" id="opinionmailNo" value="0" name="opinionmail">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6" id="optionviamaildiv">
                                    <div class="form-group">
                                        <label for="opinion via mail charges">opinion via mail charges*</label>
                                        <input type="text" class="form-control required " id="opinionviamailcharge" name="opinionviamailcharge" >
                                    </div>
                                </div>
				</div>	


                             <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Opinion Via Video Conferencing *</label>
										 
                                        <input type="radio" value="yes" id="opinionvideoYes" checked="checked"  name="opinionvideo">Yes
										 <input type="radio" id="opinionvideoNo" value="no" name="opinionvideo">No
                                    </div>
                                </div>
								
								
								 <div class="col-md-6" id="videoConferdiv">
                                    <div class="form-group">
                                        <label for="opinion via video charges">Opinion Via Video charges*</label>
                                        <input type="text" class="form-control required " id="opinionviavideocharge" name="opinionviavideocharge" >
                                    </div>
                                </div>
				</div>			

                                   <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opinionviamail">Relationship Manager  *</label>
										 
                                        <input type="text" class="form-control required " id="relship_manager" name="relship_manager" >
                                    </div>
                                </div>
								
								
								
				              </div>	
				
				
				
                                <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph </label>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                        
                                    </div>
                                </div>
								
								</div>
					
					
								
			</div>			

				
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
				
				  <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
				
				
            </div>
          
        </div>    
    </section>
    
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
 <script > $(".livesearch").chosen();</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8">
 
</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>