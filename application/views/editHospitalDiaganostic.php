<?php
$organisationType=$this->hospital_model->getType();
if(!empty($HospitalDiaganosticInfo))
{
    foreach ($HospitalDiaganosticInfo as $uf)
    {
        $id 					= $uf->id;
        $mastercode 			= $uf->mastercode;
        $diaganosticSubCode 	= $uf->diaganosticSubCode;
        $registrationNo			= $uf->registrationNo;
		$imaging				= $uf->imaging;
		$nabl					= $uf->nabl;
		$lab					= $uf->lab;
		
		$digitalXray			= $uf->digitalXray;
		$digitalxray_offer		= $uf->digitalxray_offer;
		$digitalxray_display	= $uf->digitalxray_display;
		
		$ultrasound				= $uf->ultrasound;
		$ultrasound_offer		= $uf->ultrasound_offer;
		$ultrasound_display		= $uf->ultrasound_display;
		
		$ecg					= $uf->ecg;
		$ecg_offer				= $uf->ecg_offer;
		$ecg_display			= $uf->ecg_display;
		
		$trademill				= $uf->trademill;
		$trademill_offer		= $uf->trademill_offer;
		$trademill_display		= $uf->trademill_display;
		
		$twoDeco				= $uf->twoDeco;
		$eco_offer				= $uf->eco_offer;
		$eco_display			= $uf->eco_display;
		
		$ct						= $uf->ct;
		$ct_offer				= $uf->ct_offer;
		$ct_display				= $uf->ct_display;
		
		$mri					= $uf->mri;
		$mri_offer				= $uf->mri_offer;
		$mri_display			= $uf->mri_display;
		
		$petCt					= $uf->petCt;
		$petct_offer			= $uf->petct_offer;
		$petct_display			= $uf->petct_display;
		
		$daysOperational		= $uf->daysOperational;
		$timings				= $uf->timings;
		
		$receptionArea			= $uf->receptionArea;
		$receptionarea_offer	= $uf->receptionarea_offer;
		$receptionarea_display	= $uf->receptionarea_display;
		
		$waitingArea			= $uf->waitingArea;
		$waitingarea_offer		= $uf->waitingarea_offer;
		$waitingarea_display	= $uf->waitingarea_display;
		
		$parkingSpace			= $uf->parkingSpace;
		$parkingspace_offer		= $uf->parkingspace_offer;
		$parkingspace_display	= $uf->parkingspace_display;
		
		$photograph			    = $uf->photograph;
	    $hcollect_time		    = $uf->hcollect_time;
		$hcollect_charge        =$uf->hcollect_charge;
		$hard_copy              = $uf->hard_copy;
		$softcopy_onmail        = $uf->softcopy_onmail;
		
		$discountOfferedTest	= $uf->discountOfferedTest;
		$discountDisplayedTest	= $uf->discountDisplayedTest;
		$discountOfferdHealthCheckup = $uf->discountOfferdHealthCheckup;
		$discountDisplayedHealthCheckupe = $uf->discountDisplayedHealthCheckupe;
		
		$timefrom					= $uf->timefrom;
		$timeto					    = $uf->timeto;
		$time_24_hours				= $uf->time_24_hours;
		$createdBy			    = $uf->createdBy;
		
		$sundayopen = $uf->sundayopen;
		$sun_timefrom = $uf->sun_timefrom;
		$suntimeto = $uf->suntimeto;
		$reportsupload = $uf->reportsupload;
		$discount_availableon_package = $uf->discount_availableon_package;
		$discountavailableonindividualtests = $uf->discountavailableonindividualtests;
		$xray_film = $uf->xray_film;
		
		$address =$uf->address;
		$location=$uf->location;
		$stateid=$uf->state;
	    $districtid=$uf->district;
		$cityid=$uf->city;
		$pincodeid=$uf->pincode;
		$pincode1=$uf->pincodeother;
    	
    }
}



if(!empty($HospitalDiaganosticcontactInfo))
{
    foreach ($HospitalDiaganosticcontactInfo as $contactinfo)
    {
        
        $contactname 			= $contactinfo->contactname;
        $email			        = $contactinfo->email;
		$landline				= $contactinfo->landline;
		$mobile					= $contactinfo->	mobile;
		$cid					= $contactinfo->	id;
		$mastercode				= $contactinfo->	mastercode;
    	$diaganosticSubCode	    = $contactinfo->	diaganosticSubCode;
    }
}
//echo "<pre>";
//print_r($HospitalDiaganosticcontactInfo);
//echo "</pre>";
//die;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Hospital  Diaganostic   
        <small> Edit Hospital Diaganostic</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
		
		
				<div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php  } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				
				
						<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>

						<div class="row">


						<?php echo validation_errors('<div  class="alert alert-danger alert-dismissable">', ' <button  type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
						</div> 
				
				
                
             </div>	
			 
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Hospital Diaganostic Details <?php echo $id; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                   <form role="form" action="<?php echo base_url().'admin/';?>updateHospitalDiaganostic/<?php echo $id;?>" method="post"  name="editHospitalDiaganosticForm"  id="editHospitalDiaganosticForm" role="form"  enctype="multipart/form-data" >
                        
					<div class="box-body">
						
						       <div class="row">
							 <input type="hidden" value="<?php echo $id; ?>" name="id" id="id" />  
							 
							 <input type="hidden" value="<?php echo $mastercode; ?>" name="mastercode" id="mastercode" /> 
							 
							 <input type="hidden" value="<?php echo $diaganosticSubCode; ?>" name="diaganosticSubCode" id="diaganosticSubCode" /> 
							 
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                         <input type="text" class="form-control" value="<?php echo $mastercode;?>" id="mastercode" name="mastercode" maxlength="128" readonly>
										 	
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="registrationNo">Registration No.*</label>
                                        <input type="text" class="form-control" value="<?php echo $registrationNo;?>" id="registrationNo" name="registrationNo" maxlength="128" >
									 <span id="mcodeError" class="errorMsg"> <span>
										
                                    </div>
                                    
                                </div>
                             
                            </div>
							
							
							
							
							
							<div class="row">
							
							
							 
							 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="registrationNo">ISO </label>
											<input type="radio" value="1" id="isoYes" checked="checked"  name="iso">Yes
											<input type="radio" id="isoNo" value="0" name="iso">No
										</div>
                                    </div>
                                    
                                </div>
								
								   <div class="col-md-6">
                                    <div class="form-group">
                                       
                                    </div>
                                </div>
                             </div>
							 <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
										<div class="check_div">
											<label for="lab">Lab*</label>
											<input type="radio" value="1" id="lab" checked="checked"  name="lab">Yes
											<input type="radio" id="lab" value="0" name="lab">No
										</div>
                                    </div>
                                </div>
								
								   <div class="col-md-6">
                                    <div class="form-group">
										<div class="check_div">
											<label for="nabl">NABL *</label>
											<input type="radio" value="1" id="nabl" checked="checked"  name="nabl">Yes
											<input type="radio" id="nabl" value="0" name="nabl">No
										</div>
                                    </div>
                                </div>
							
							</div>
							 
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="imaging">Imaging  *</label>
											<input type="radio" value="1" id="imgchkYes" <?php if($imaging=='1' ) echo "checked"  ;?>   name="imaging">Yes
											<input type="radio" id="imgchkNo" value="0" <?php if($imaging=='0' ) echo "checked"  ;?> name="imaging">No
										</div>
                                    </div>
                                </div>
                               
                            </div>
		                 
						 <div  id="divimaging">					
                         
							
							
		<div class="row" style="margin-top:15px;">
             <div class="col-md-6">
			<div class="form-group">
				<div class="check_div">
					<label for="xray">Digital X-ray*</label>
				 <input type="radio" value="1" id="xrayYes" <?php if($digitalXray=='1' ) echo "checked"  ;?>  name="xray">Yes
				 <input type="radio" id="xrayNo" value="0" <?php if($digitalXray=='0' ) echo "checked"  ;?>  name="xray">No
				</div>
			</div>
									
			<div id="divoxrayffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
					<label>Offered on  </label>
					<input type="text" class="form-control required" value="<?php echo $digitalxray_offer;?>" id="digitalxray_offer" name="digitalxray_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					<label>Dispalay on</label>
					<input type="text" class="form-control required" value="<?php echo $digitalxray_display;?>" id="digitalxray_display" name="digitalxray_display" maxlength="128">
					</div>
				</div>
			</div>
		    </div>							
									
                               
		   <div class="col-md-6">
				<div class="form-group">
					<div class="check_div">
						<label for="ecg">Ecg*</label>
						<input type="radio" value="1" id="ecgYes" <?php if($ecg=='1' ) echo "checked"  ;?>   name="ecg">Yes
						 <input type="radio" id="ecgNo" value="0" <?php if($ecg=='0' ) echo "checked"  ;?>  name="ecg">No
					</div>
				</div>
				
				<div id="divecgoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
                    <label>Offered on</label>  <input type="text" class="form-control required" value="<?php echo $ecg_offer;?>" id="" name="ecg_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
						<label>Dispalay on</label>
					<input type="text" class="form-control required" value="<?php echo $ecg_display;?>" id="" name="ecg_display" maxlength="128">
					</div>
				</div>
			</div>
				
			</div>
		</div>
                           
						   
	   <div class="row">
	   
		 <div class="col-md-6">
				<div class="form-group">
					<div class="check_div">
						<label for="ultrasound">Ultrasound*</label>
						<input type="radio" value="1" id="ultrasoundYes"  <?php if($ultrasound=='1' ) echo "checked"  ;?>  name="ultrasound">Yes
						<input type="radio" id="ultrasoundNo" value="0" <?php if($ultrasound=='0' ) echo "checked"  ;?> name="ultrasound">No
					</div>
				</div>
				
				<div id="divultrasoundoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
                    Offered on  <input type="text" class="form-control required" value="<?php echo $ultrasound_offer;?>" id="" name="ultrasound_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					Dispalay on <input type="text" class="form-control required" value="<?php echo $ultrasound_display;?>" id="" name="ultrasound_display" maxlength="128">
					</div>
				</div>
			</div>
			</div>
			
			 <div class="col-md-6">
				<div class="form-group">
					<div class="check_div">
						<label for="ct">CT*</label>
						<input type="radio" value="1" id="ctYes" <?php if($ct=='1' ) echo "checked"  ;?>  name="ct">Yes
						<input type="radio" id="ctNo" value="0"  <?php if($ct=='0' ) echo "checked"  ;?> name="ct">No

					</div>
				</div>
				
				
				<div id="divctoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
					<label>Offered on</label>
					<input type="text" class="form-control required" value="<?php echo $ct_offer;?>" id="" name="ct_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					<label>Dispalay on</label>
					<input type="text" class="form-control required" value="<?php echo $ct_display;?>" id="" name="ct_display" maxlength="128">
					</div>
				</div>
			</div>
			</div>
		   
		  
		</div>
							
								<div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
										<div class="check_div">
											<label for="opinionviamail">MRI*</label>
											<input type="radio" value="1" id="mriYes" <?php if($ct=='1' ) echo "checked"  ;?>  name="mri">Yes
											<input type="radio" id="mriNo" value="0" <?php if($ct=='0' ) echo "checked"  ;?> name="mri">No

										</div>
                                    </div>
									
				<div id="divmrioffer" class="margin0">
					<div class="col-md-4">
						<div class="form-group">
						Offered on  <input type="text" class="form-control required" value="<?php echo $mri_offer;?>" id="" name="mri_offer" maxlength="128">
						</div>
					</div>
					 <div class="col-md-4">
						<div class="form-group">	
						Dispalay on <input type="text" class="form-control required" value="<?php echo $mri_display;?>" id="" name="mri_display" maxlength="128">
						</div>
					</div>
				</div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="opinion via mail charges">PET CT*</label>
                                         <input type="radio" value="1" id="petctYes" 
										 <?php if($petCt=='1' ) echo "checked"  ;?>  name="petct">Yes
										 <input type="radio" id="petctNo" value="0"  <?php if($petCt=='0' ) echo "checked"  ;?> name="petct">No
										</div>
                                    </div>
								<div id="divpetctoffer" class="margin0">
								<div class="col-md-4">
									<div class="form-group">
									<label>Offered on</label>  
									<input type="text" class="form-control required" value="<?php echo $petct_offer;?>" id="" name="petct_offer" maxlength="128">
									</div>
								</div>
								 <div class="col-md-4">
									<div class="form-group">	
									<label>Dispalay on</label>
									<input type="text" class="form-control required" value="<?php echo $petct_display;?>" id="" name="petct_display" maxlength="128">
									</div>
								</div>
							</div>
									
                                </div>
				</div>
							
							

 </div>
 
							<div class="row offer-onn">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="eco">2D Eco*</label>
											<input type="radio" value="1" id="ecoYes" <?php if($twoDeco=='1' ) echo "checked"  ;?>  name="eco">Yes
											<input type="radio" id="ecoNo" value="0" <?php if($twoDeco=='0' ) echo "checked"  ;?> name="eco">No
										</div>
                                    </div>
									
								<div id="divecooffer" class="margin0">
								<div class="col-md-4">
									<div class="form-group">
									<label>Offered on</label><input type="text" class="form-control required" value="<?php echo $eco_offer;?>" id="" name="eco_offer" maxlength="128">
									</div>
								</div>
								 <div class="col-md-4">
									<div class="form-group">	
									<label>Dispalay on</label>
									<input type="text" class="form-control required" value="<?php echo $eco_display;?>" id="" name="eco_display" maxlength="128">
									</div>
								</div>
							</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<div class="check_div">
												<label for="trademill">Trademill*</label>
												 <input type="radio" value="1" id="trademillYes"  <?php if($trademill=='1' ) echo "checked"  ;?>  name="trademill">Yes
												 <input type="radio" id="trademillNo" value="0" <?php if($trademill=='0' ) echo "checked"  ;?>  name="trademill">No
											</div>
										</div>
                                    </div>
									
			<div id="divtrademilloffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
                    <label>Offered on</label>  <input type="text" class="form-control required" value="<?php echo $trademill_offer;?>" id="" name="trademill_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					<label>Dispalay on</label> <input type="text" class="form-control required" value="<?php echo $trademill_display;?>" id="" name="trademill_display" maxlength="128">
					</div>
				</div>
			</div>
                                </div>
                            </div>
                             <div class="row offer-onn">
							 
							  <div class="col-md-6">
                                    <div class="form-group">
										<div class="check_div">
											<label for="waitingarea">Waiting Area*</label>
											<input type="radio" value="1" id="waitingareaYes" <?php if($waitingArea=='1' ) echo "checked"  ;?>  name="waitingarea">Yes
											<input type="radio" id="waitingareaNo" value="0" <?php if($waitingArea=='0' ) echo "checked"  ;?> name="waitingarea" >No
										</div>
                                    </div>
									
									<!--<div id="divwaitingareaoffer" class="margin0">
									<div class="col-md-4">
										<div class="form-group">
										<label>Offered on</label>  <input type="text" class="form-control required" value="<?php echo $waitingarea_offer;?>" id="waitingarea_offer"          name="waitingarea_offer" maxlength="128">
										</div>
									</div>
									 <div class="col-md-4">
										<div class="form-group">	
										<label>Dispalay on</label> <input type="text" class="form-control required" value="<?php echo $waitingarea_display;?>" id="waitingarea_display"          name="waitingarea_display" maxlength="128">
										</div>
									</div>
								</div>-->
                                </div>
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="receptionarea">Reception Area *</label>
										 
                                        <input type="radio" value="1" id="receptionareaYes" <?php if($receptionArea=='1' ) echo "checked" ;?>   name="receptionarea">Yes
										 <input type="radio" id="receptionareaNo" value="0"  <?php if($receptionArea=='0' ) echo "checked" ;?> name="receptionarea">No
										</div>
                                    </div>
									
									
				<!--<div id="divreceptionareaoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
                    <label>Offered on </label> <input type="text" class="form-control required" value="<?php echo $receptionarea_offer;?>" id="" name="receptionarea_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					<label>Dispalay on</label> <input type="text" class="form-control required" value="<?php echo $receptionarea_display;?>" id="" name="receptionarea_display" maxlength="128">
					</div>
				</div>
			</div>-->
                                </div>
								
								
								
				              </div>			


								 <div class="row offer-onn">
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="parkingspace">Parking Space*</label>
                                      <input type="radio" value="1" id="parkingspaceYes" <?php if($parkingSpace=='1' ) echo "checked"  ;?>    name="parkingspace">Yes
										 <input type="radio" id="parkingspaceNo" value="0" <?php if($parkingSpace=='0' ) echo "checked"  ;?>  name="parkingspace">No
										</div>
                                    </div> 
			<!--<div id="divparkingspaceoffer" class="margin0">
                <div class="col-md-4">
                    <div class="form-group">
                    <label>Offered on</label>  <input type="text" class="form-control required" value="<?php echo $parkingspace_offer;?>" id="" name="parkingspace_offer" maxlength="128">
					</div>
				</div>
				 <div class="col-md-4">
                    <div class="form-group">	
					<label>Dispalay on</label> <input type="text" class="form-control required" value="<?php echo $parkingspace_display;?>" id="" name="parkingspace_display" maxlength="128">
					</div>
				</div>
			</div>-->
								</div>
								
								
								
								<div class="col-md-6">
                                    <div class="form-group">
										<div class="check_div">
											<label for="receptionarea">Hard Copy *</label>
											<input type="radio" value="1" id="hard_copy" checked="checked"  name="hard_copy">Yes
											<input type="radio" id="hard_copy" value="0" name="hard_copy">No
										</div>
									</div>
								</div>
								</div>
							
							
							 <div class="row offer-onn">
							  <div class="col-md-6">
                                    <div class="form-group">
										 <div class="check_div">
												<label for="homecollection">Home Collection*</label>										
												<input type="radio" value="1" id="hcollectYes"   name="homecollection">Yes
												<input type="radio" id="hcollectNo" value="1"  checked="checked" name="homecollection">No
										 </div>
                                        
                                    </div>
							 </div>
							 
							 
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="check_div">
											<label for="softcopy_onmail">Soft Copy on Mail *</label>
											<input type="radio" value="1" id="softcopy_onmail" checked="checked"  name="softcopy_onmail">Yes
											<input type="radio" id="softcopy_onmail" value="0" name="softcopy_onmail">No
										</div>
                                    </div></div>
									
									
									 <div id="divhomecollectioncharge">
							  <div class="col-md-6">
                                    <div class="form-group">
								
									<label >Charge  (In Rs)</label> 
									<input type="text" name="hcollect_charge"  value="<?php echo $hcollect_charge;?>" id="hcollect_charge" class="form-control required">
									
								</div></div>
							  </div>
							   <div id="divhomecollectiontime">
							   
							   <div class="col-md-6">
                                    <div class="form-group">
								
									<label >Time (In Hours)</label> 
									<input type="text" name="hcollect_time" value="<?php echo $hcollect_time;?>" id="hcollect_time"  class="form-control">
									
								</div></div>
							  </div>
							 
							 </div>
							 
							 
							 
							
							
							
							
							
							
							
				 <div class="row">
						<div class="col-md-6">
                                    <div class="form-group">
                                        
										 <label for="discountofferdtest">Discount Offered On Test *</label>
                                      <input type="text" class="form-control " id="discountofferedtest" name="discountofferedtest"  value="<?php echo $discountOfferedTest; ?>" >
                                       <span id="disError" class="errorMsg"> <span> 
                                    </div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discountofferdtest">Discount Offered On Test Displayed*</label>
                                      <input type="text" class="form-control " id="discountofferedtestdisplayed" name="discountofferedtestdisplayed" value="<?php echo $discountDisplayedTest; ?>" >
									  
									   <span id="disoffError" class="errorMsg"> <span> 
                                    </div>
								
								</div>
								
							</div>
							
							 <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discountofferedhealthcheckup">Discount Offered  On Health Checkup* </label>
										
										  <input type="text" class="form-control " id="discountofferedhealthcheckup" name="discountofferedhealthcheckup"  value="<?php echo $discountOfferdHealthCheckup; ?>" >
										
                                        <span id="disoffhchkError" class="errorMsg"> <span>
                                    </div>
                                </div>
								
								
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discountofferedhealthcheckupdisplayed">Discount Offered  On Health Checkup Displayed*</label>
                                      <input type="text" class="form-control " id="discountofferedhealthcheckupdisplayed" name="discountofferedhealthcheckupdisplayed"  value="<?php echo $discountDisplayedHealthCheckupe; ?>" >
									  
									   <span id="disoffhdisplaychkError" class="errorMsg"> <span>
                                    </div>
								
								</div>
								
							</div>
							
							<div class="row offer-onn">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="check_div">
											<label for="discount displayed">24X7(Availability)</label>  
										<input type="radio" id="chktimeYes"  value="1" name="chk_time"  <?php if($time_24_hours=='1' ) echo "checked"  ;?> /> Yes
										
										 <input type="radio" id="chktimeNo" value="0"  name="chk_time"  <?php if($time_24_hours=='0' ) echo "checked"  ;?>/>No
										</div>
									</div>
								</div>
								
								 <div  id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom" id="timefrom" value="<?php echo $timefrom;?>" class="form-control">
									
								</div>
								
										<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto" value="<?php echo $timeto;?>" class="form-control">
								</div>
							 
							</div>		
								
							</div>

							<div class="row offer-onn">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="check_div">
											<label for="discount displayed">Sunday Open</label>  
										<input type="radio" id="sundayopenYes"  value="1"  <?php if($sundayopen=='1' ) echo "checked"  ;?>  name="sundayopen" /> Yes 
										 <input type="radio" id="sundayopenNo" value="0" <?php if($sundayopen=='0' ) echo "checked"  ;?>  name="sundayopen" /> No
										</div> 
									</div>
								</div>
								
									<div id="divsundayopen">
                             
											<div class="col-md-4">
										
											<label for="timefrom">Time From</label> 
											<input type="text" name="sun_timefrom" id="suntimefrom" value="<?php echo $sun_timefrom; ?>" class="form-control required">
											
										</div>
											
										
									
											<div class="col-md-4">
											<label for="timeto">Time To</label>
											<input type="text" name="suntimeto" id="suntimeto"  value="<?php echo $suntimeto; ?>" class="form-control required">
										
										</div>
									 
									 
									</div>
								
							</div>
							
                    
                                  							
							  
							
							<div class="row">
							
                             
							 		<div class="col-md-6">
							       <div class="form-group">
										<div class="check_div">
											<label for="discount displayed">X-Ray Film </label>  
											<input type="radio" id="xray_filmYes"  value="1" <?php if($xray_film=='1' ) echo "checked"  ;?>  name="xray_film" /> Yes
											<input type="radio" id="xray_filmNo" value="0"  <?php if($xray_film=='0' ) echo "checked"  ;?> name="xray_film" /> No
										</div>
								   </div>
							</div>
							
							
							</div>
							
							<div class="row">
							
                             
							 		<div class="col-md-6">
							       <div class="form-group">
										<div class="check_div">
											<label for="discount displayed">Discount Availableon Package</label>  
											<input type="radio" id="discount_availableon_packageYes"  value="1"  <?php if($discount_availableon_package=='1' ) echo "checked"  ;?> name="discount_availableon_package" /> Yes
											<input type="radio" id="discount_availableon_packageNo" value="0"   <?php if($discount_availableon_package=='0' ) echo "checked"  ;?> name="discount_availableon_package" /> No
										</div>
								    </div>
							</div>
							
							
                             
							 <div class="col-md-6">
							 <div class="form-group">
								<div class="check_div">
									<label for="discount displayed">Discount Available On Individual Test</label>  
									<input type="radio" id="centretime"  value="1" <?php if($discountavailableonindividualtests=='1' ) echo "checked"  ;?>  name="discountavailableonindividualtests" /> Yes
									<input type="radio" id="centretime" value="0"  <?php if($discountavailableonindividualtests=='0' ) echo "checked"  ;?> name="discountavailableonindividualtests" /> No
								</div>
							</div></div>
							
							
							</div>
                    
                                  							
							 	<div class="row">
							
                             
							 		<div class="col-md-6">
							       <div class="form-group">
										 <div class="check_div">
												<label for="discount displayed">Report Upload </label>  
												<input type="radio" id="reportsuploadYes"  value="1" name="reportsupload" /> Yes
												<input type="radio" id="reportsuploadNo" value="0" checked="checked" name="reportsupload" /> No

										 </div>
									</div>
							</div>
							
							
							</div>
							
							
							
							
							
							
							
							
							 	<div class="row">
                                <div class="col-md-6">
                                   <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea class="form-control" id="address" name="address" ><?php echo $address; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                    <input type="text" class="form-control  equalTo" id="text" id="location" name="location" value="<?php  echo $location; ?>" maxlength="50">
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <select class="form-control " id="state" name="state">
                                            <option value="0">Select State</option>
											 <?php
										$state = $this->hospital_model->getState();
										$district = $this->hospital_model->fetch_district($stateid);
									
										$city = $this->hospital_model->fetch_city($districtid);
										$pincode = $this->hospital_model->fetch_pincode($cityid);
									
							
                                        foreach($state as $row)
                                          {
											  ?>
											
                                       <option value="<?php echo $row->id; ?>" <?php  if($row->id==$stateid) echo "selected";?> ><?php echo $row->state;?></option>
                                          <?php }
                                         ?>
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="District">District</label>
                                       <select class="form-control " id="district" name="district">
                                            <option value="0">Select District</option>
											<?php 
											  foreach($district as $row)
                                          { ?>
											  <option value="<?php echo $row->id; ?>" <?php  if ($row->id==$districtid) echo "selected"; ?>><?php echo $row->district;?></option>
										  <?php } ?>
											</select>
                                    </div>
                                </div>
                            </div>
							
							
							 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <select class="form-control " id="city" name="city">
                                            <option value="0">Select City</option>
											
											<?php foreach($city as $row){
												?>
											 <option value="<?php echo $row->id; ?>" <?php  if ($row->id==$cityid) echo "selected"; ?>><?php echo $row->city;?></option>
										  <?php } ?>
											</select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pincode">pincode</label>
                                       <select class="form-control " id="pincode" name="pincode">
									   
                                            <option value="0">Select Pincode</option> 
												<?php foreach($pincode as $row){
												
												?>
												 <option value="<?phpecho $row->id; ?>" <?php  if ($row->pincode==$pincodeid) echo "selected"; ?>><?php echo $row->pincode;?></option>
										  <?php } ?>
											
											</select>OR  <input type="text" class="form-control " id="pincodeother" value= "<?php if($pincodeid=='') echo $pincode1;?>" name="pincode1" maxlength="50">
                                    </div>
                                </div>
                            </div>
							
							
							
							
							
							
							
							
							
							
							
							 <div class="row">
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph </label>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                         <input type="hidden" class="form-control " id="hidphotograph" name="hidphotograph" value="<?php echo $photograph; ?>" >
										 
                                    </div>
									<img src="<?php echo $photograph; ?>" width="50" height="50"/>
                                </div>
								
								</div>	
								
								
							
							
							
							
							
					</div>
						<?php  
					if(!sizeof($HospitalDiaganosticcontactInfo))
					{
					?>
					<div id='TextBoxesGroup'>	
					<div class="TextBoxDiv1">
								
								<div class="col-md-3">
								<label for="contact-person">Contact Person</label>
								<input type="text" id="contact-person" name="contact[]" class="form-control"> 
								</div>
								<div class="col-md-3">
									<label for="landline">Land Line</label> 
									<input type="text" id="landline" name="landline[]" class="form-control">
								</div>
								<div class="col-md-3">
									<label for="email">Email</label>
									<input type="email" name="email[]" id="email" class="form-control">
								</div>
								<div class="col-md-3">
									<label for="mobile">Mobile</label>
									<input type="text" name="mobile[]" id="mobile" class="form-control">
								</div>
					</div>
				</div>
					<?php 
					}else{
					
					
					?>
					
					<?php for($i=0;$i<sizeof($HospitalDiaganosticcontactInfo);$i++){ ?>
					
					<div id='TextBoxesGroup'>	
							<div class="TextBoxDiv1">

								<div class="col-md-3">
								<label for="contact-person">Contact Persion</label>
								
								
								<input type="text" id="contact-person" name="contact[]" value="<?php echo  $HospitalDiaganosticcontactInfo[$i]->	contactname; ?>" class="form-control"> 
								
								</div>
								<div class="col-md-3">
									<label for="landline">Land Line</label> 
									
									<input type="text" id="landline" value="<?php echo $HospitalDiaganosticcontactInfo[$i]->landline; ?>" name="landline[]" class="form-control">
									
								</div>
								<div class="col-md-3">
									<label for="email">Email</label>
									
									<input type="text" name="email[]" value="<?php echo  $HospitalDiaganosticcontactInfo[$i]->email; ?>" id="email" class="form-control">
									
								</div>
								<div class="col-md-3">
									<label for="mobile">Mobile</label>
									
									<input type="text" name="mobile[]" value="<?php echo  $HospitalDiaganosticcontactInfo[$i]->mobile; ?>" id="mobile" class="form-control">
									
									<input type="hidden" name="oldid[]" value="<?php echo $HospitalDiaganosticcontactInfo[$i]->id; ?>" id="oldid" class="form-control">
									
									<input type="hidden" name="cmastercode[]" value="<?php echo  $HospitalDiaganosticcontactInfo[$i]->mastercode; ?>"  class="form-control">
									
									<input type="hidden" name="cdiaganosticSubCode[]" value="<?php echo  $HospitalDiaganosticcontactInfo[$i]->diaganosticSubCode; ?>" class="form-control">
									
									
									<input type="hidden" name="contactid[]" value="<?php echo  $HospitalDiaganosticcontactInfo[$i]->id; ?>"   class="form-control">
									
								
									
						
									
								</div>
							
			
					<?php } } ?>						
                           		
  							
						
							
				</div>
							
					<div>
							<input type='button' value='Add More' id='addButton'>
							<input type='button' value='Remove' id='removeButton'>
							</div>
					
					
					
					
					
					
			    </div>		
							
							
                            <div class="box-footer">
							
                            <input type="submit"  id="Submit" class="btn btn-primary" value="Submit" />
							     <!-- <input type="submit"  id="Submit"  value="Submit" />-->
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
					
					
					
			
					
				</div>		
				
						



						
		</div>	





			 
		
                </div>
				
				

			
				
				
          
 
		 </div>
		 

		 
		 
        </div>    
    </section>
</div>
<script>
$(document).ready(function(){

    var counter = 2;		
    $("#addButton").click(function () {				
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<div class="col-md-3"><label for="contact-person">Contact Person</label><input type="text" id="contact-person" name="contact[]" class="form-control"> </div><div class="col-md-3"><label for="landline">Land Line</label> <input type="text" id="landline" name="landline[]" class="form-control"></div><div class="col-md-3"><label for="email">Email</label><input type="email" name="email[]" id="email" class="form-control"></div><div class="col-md-3"><label for="mobile">Mobile</label><input type="text" name="mobile[]" id="mobile" class="form-control"></div>');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");				
	counter++;
    });
	
	
	
	

     
// Code to remove more fields for doctor	 
	 $("#removeButton").click(function () {
	if(counter==2){
          alert("At least one entry should be there");
          return false;
       }   
        
	counter--;
			
        $("#TextBoxDiv" + counter).remove();
			
     });
	 
	 
	 // add by santosh 
	 
	$('#timefrom').timepicker();
	$('#timeto').timepicker();
		
    
  });



</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
