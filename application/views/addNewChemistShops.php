
<?php 


$mastercode=$this->hospital_model->getHospitalMasterCode();

 ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Chemist Shops 
        <small>Add Chemist Shops</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Chemist Shops Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form");
					 ?>
				
                    <form role="form" id="addHospitalDoctors" action="<?php echo base_url().'admin/' ?>addHospitalChemistShops" method="post" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
							
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                        <select class="form-control required" id="mastercode" name="mastercode">
                                            <option value="0">Select Mastercode*</option>
                                           
                                               <?php foreach($mastercode   as $value){?>
                                                  <option value="<?php echo $value->mastercode;?>"><?php echo $value->mastercode;?></option>  
											   <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="doctor">Registration No*</label>
                                        <input type="text" class="form-control required" value="" id="registrationn_no" name="registrationn_no" maxlength="128">
                                    </div>
                                    
                                </div>
                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Shop SubCode*</label>
                                        <input type="text" class="form-control required" id="shopsubcode" name="shopsubcode" >
                                    </div>
                                </div>
                                
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Days</label>
                                        <input type="text" class="form-control required " id="days" name="days" >
                                    </div>
                                </div>
							</div>
							
							<div class="row">
							  <div class="col-md-6">
                                   <div class="form-group">
                                     
										<label for="discount displayed">Ayurvedic Medicines*</label>  
										Yes<input type="radio" id="chk4Yes" name="ayurvedic_medicines"  value="1"    />
										 No<input type="radio" id="chkNo" name="ayurvedic_medicines" value="0"   />
										   
										<div id="dvayurvedic" style="display: none">
										Offered on Medicines<input type="text" class="form-control required" value="" id="" name="offeron_ayurvedic_medicines" maxlength="128">
										
										Displayed on Medicines<input type="text" class="form-control required" value="" id="" name="displayon_ayurvedic_medicines" maxlength="128">
											
										</div>
									   
                                    </div>
									
                                </div>
                              
								
								  <div class="col-md-6">
                                   
									 <div class="form-group">
                                        <label for="discount displayed">Home Delivery*</label>  
										Yes <input type="radio" id="chk5Yes" name="home_delivery" />
										 No<input type="radio" id="chkNo" name="home_delivery" />
										   
										<div id="dvdelivery" style="display: none">
								<input type="text" class="form-control required" value="" id="" name="home_delivery" maxlength="128">
							   
							   </div>
									   
                                    </div>
                                </div>
							</div>
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                     
										<label for="discount displayed">Homeopathy   Medicines*</label>  
										Yes <input type="radio" id="chkYes" name="homeopathy_medicines" />
										 No<input type="radio" id="chkNo" name="homeopathy_medicines" />
										   
										<div id="dvhomeopathy" style="display: none">
											Offered on Medicines<input type="text" class="form-control required" value="" id="offeredon_homeopathy_medicines" name="offeron_homeopathy_medicines" maxlength="128">
											
											Displayed on Medicines<input type="text" class="form-control required" value="" id="homeopathy_medicines" name="displayon_homeopathy_medicines" maxlength="128">
										</div>
									   
                                    </div>
                                </div>
                           </div>
                           <div class="row">
						   
						      <div class="col-md-6">
							  
							   <div class="form-group">
                                     
										<label for="discount displayed">Allopathy Medicines*</label>  
										Yes <input type="radio" id="chk2Yes" name="allopathy_medicines" />
										 No<input type="radio" id="chkNo" name="allopathy_medicines" />
										   
										<div id="dvallopathy" style="display: none">
										Offered on Medicines	<input type="text" class="form-control required" value="" id="" name="offeredon_allopathy_medicines" maxlength="128">
										Displayed on Medicines		<input type="text" class="form-control required" value="" id="" name="displayedon_allopathy_medicines" maxlength="128">
										</div>
									   
                                    </div>
                                    
                                </div>
                               
								
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount displayed">Timings </label>
                                        <input type="text" class="form-control required " id="timings" name="timings" >
                                    </div>
                                </div>
							 </div>
							
							   <div class="row">
                              <div class="col-md-6">
                                  
									
									   <div class="form-group">
                                     
										<label for="discount displayed">Unani Medicines*</label>  
										Yes <input type="radio" id="chk3Yes" name="unani_medicines" />
										 No<input type="radio" id="chkNo" name="unani_medicines" />
										   
										<div id="dvunani" style="display: none">
											Offered on Medicines <input type="text" class="form-control required" value="" id="" name="offeredon_unani_medicines" maxlength="128">
											Dispalay on Medicines <input type="text" class="form-control required" value="" id="" name="displayon_unani_medicines" maxlength="128">
											
										</div>
									   
                                    </div>
									
                                </div>
								
                            </div>
							
							
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount offered on Consumables*</label>
                                        <input type="text" class="form-control required" id="dis_off_onconsumables" name="dis_off_onconsumables" >
                                    </div>
                                </div>
                                
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount offered on FMCG*</label>
                                        <input type="text" class="form-control required " id="dis_off_onfmcg" name="dis_off_onfmcg" >
                                    </div>
                                </div>
							</div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount offered on Specific Brand*</label>
                                        <input type="text" class="form-control required" id="dis_off_onspecific_brand" name="dis_off_onspecific_brand" >
                                    </div>
                                </div>
                                
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount to be displayed on Consumables*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_diplay_onconsumables" 
										name="dis_tobe_diplay_onconsumables" >
                                    </div>
                                </div>
							</div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount to be displayed on FMCG*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_display_onemcg" 
										name="dis_tobe_display_onemcg" >
                                    </div>
                                </div>
                                
							 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount to be displayed on Specific Brand*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_display_onspecific_brand" 
										name="dis_tobe_display_onspecific_brand"  >
                                    </div>
                                </div>
							</div>
							
							  <div class="row">
							  
                              
								
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph </label>
										 <input type="file" class="form-control " id="photograph" name="photograph" >
                                        
                                    </div>
                                </div>
                               
                            </div>
								 
				      </div>	
                      </div>		
					  </div>			
                     </div>
				 
					 
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script>
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><div class="col-md-4" style="margin-top:15px"><input type="text" name="contact[]" class="form-control"/></div><div class="col-md-4" style="margin-top:15px"><input type="text" name="landline[]" class="form-control"/></div><div class="col-md-4" style="margin-top:15px"><input type="text" name="email[]" class="form-control"/></div></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>