<?php 
$permission=explode (',',$this->session->permission);

$controller=$this->uri->segment(2);



?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $pageTitle; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/dist/css/custom.css" rel="stylesheet" type="text/css" />
    <style>
    	
		
		
		
    </style>
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
  <script src="<?php echo base_url(); ?>assets/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
  
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo green_bg">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>MW</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Mediwheel</b>&nbsp; Admin</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top reb_bg" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="fa fa-history"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : $last_login; ?></li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $name; ?>
                      <small><?php echo $role_text; ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url().'admin/'; ?>loadChangePass" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url().'admin/'; ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="<?php echo base_url().'admin/'; ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            
           <?php
            if($role == ROLE_ADMIN)
            {
            ?>
            <li class="treeview">
              <a href="<?php echo base_url().'admin/'; ?>userListing">
                <i class="fa fa-users"></i>
                <span>Users</span>
              </a>
            </li>
          
            <?php
            }
            ?>
			
			
			
			
			
			
			
			
			
		<li class="treeview  <?php if(in_array($controller,array('customerListing','orderListing','cartListing','customerDetail')))echo  activate_menu($controller); ?>">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>1MG </span>
				
				
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>customerListing"><i class="fa fa-medkit" aria-hidden="true"></i> Manage Customer</a></li>
					
				</ul>
				<ul class="treeview-menu">
					
					
					<li><a href="#"><i class="fa fa-medkit" aria-hidden="true"></i>Manage Order </a></li>
					
				</ul>
				
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>cartListing"><i class="fa fa-medkit" aria-hidden="true"></i>Manage Cart</a></li>
					
				</ul>
			
             </a>
            </li>
				
              </a>
            </li>
			
			
		
			
			<li class="treeview"><a href="<?php echo base_url().'admin/'; ?>hospitalListing"><i class="fa fa-user-md" aria-hidden="true"></i> Master Code Generation </a></li>
			<li class="treeview <?php if(in_array($controller,array('hospitalDoctorsListing','hospitalChemistShopsListing','hospitalDiaganosticListing','editHospitalDoctor','addNewHospitalDoctor','addNewChemistShop','addNewHospitalDiaganosticform','editHospitalDiaganostic','edithospitalChemistShop','scheduleListing','editDoctorSchedule')))echo  activate_menu($controller); ?>">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Hospital </span>
				
				
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>hospitalDoctorsListing"><i class="fa fa-medkit" aria-hidden="true"></i> Doctors</a></li>
					<!--<li><a href="<?php echo base_url().'admin/'; ?>hospitalDiaganosticListing"><i class="fa fa-medkit" aria-hidden="true"></i> Diaganostic Center</a></li>-->
					
				</ul>
				
				
				
				  <ul class="treeview-menu">
					<li><a href="<?php echo base_url().'admin/hospitalChemistShopsListing'; ?>"><i class="fa fa-medkit" aria-hidden="true"></i> Chemist Shops</a>
					</li>
					<li><a href="<?php echo base_url().'admin/hospitalDiaganosticListing'; ?>"><i class="fa fa-medkit" aria-hidden="true"></i> Hospital Diaganostic</a>
					</li>
				  </ul>
				
				
              </a>
            </li>
			
			
			
			<li class="treeview <?php if(in_array($controller,array('medicalPractitioner','editMedicalPractitionerDoctor','addNewmedicalPractitioner')))echo  activate_menu($controller); ?>">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Medical Practitioner  </span>
				
				
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>medicalPractitioner"><i class="fa fa-medkit" aria-hidden="true"></i> Doctors</a></li>
					
				</ul>
				
				
              </a>
            </li>
			
			<li class="treeview  <?php if(in_array($controller,array('healthChekupPackageListing','healthCheckupTest','healthChekupPackageLinking','addHealthCheckupPackage','editHealthChekupPackage','addHealthCheckupTest','editHealthChekupTest','testParameter','addHealthCheckupTestParameter','editHealthChekupTestParameter','addHealthPackageLinking','editHealthChekupPackageLinking','healthPackageSchedule','addHealthPackageSchedule','editHealthPackageSchedule')))echo  activate_menu($controller); ?>">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Health Checkup Package </span>
				
				
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>healthChekupPackageListing"><i class="fa fa-medkit" aria-hidden="true"></i> Health Checkup Package</a></li>
					
				</ul>
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>healthCheckupTest"><i class="fa fa-medkit" aria-hidden="true"></i> Health Checkup Test</a></li>
					
				</ul>
				
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>healthChekupPackageLinking"><i class="fa fa-medkit" aria-hidden="true"></i> Health Checkup Linking</a></li>
					
				</ul>
				
				
				<li class="treeview  <?php if(in_array($controller,array('diaganosticListing','addNewDiaganosticform','editDiaganostic')))echo  activate_menu($controller); ?>">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Diaganostic Center </span>
				
				
				<ul class="treeview-menu">
					
					
					<li><a href="<?php echo base_url().'admin/'; ?>diaganosticListing"><i class="fa fa-medkit" aria-hidden="true"></i> Diaganostic</a></li>
					
				</ul>
				
				
              </a>
            </li>
				
              </a>
            </li>
			
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>