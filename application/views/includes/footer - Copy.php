

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Mediwheel</b> Admin System
        </div>
        <!--<strong>Copyright &copy; 2014-2015 <a href="<?php echo base_url(); ?>">CodeInsect</a>.</strong> All rights reserved.-->
    </footer>
    
    <!-- jQuery UI 1.11.2 -->
     <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script> 
    <!--Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	
	 
<script>

</script>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css" />
	
	

		


    <script type="text/javascript">
        var windowURL = window.location.href;
        pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
        var x= $('a[href="'+pageURL+'"]');
            x.addClass('active');
            x.parent().addClass('active');
        var y= $('a[href="'+windowURL+'"]');
            y.addClass('active');
            y.parent().addClass('active');
			
			
		
	 
	 
	 $('#test').multiselect({
		includeSelectAllOption: false,
	  nonSelectedText: 'Select Test ',
	  enableFiltering: true,

	
       showCheckbox       : true, 
	  enableCaseInsensitiveFiltering: true,
	  buttonWidth:'100%'
	 });
	 
	 
	 $('#module').multiselect({
		includeSelectAllOption: false,
	  nonSelectedText: 'Select Module ',
	  enableFiltering: true,

	
       showCheckbox       : true, 
	  enableCaseInsensitiveFiltering: true,
	  buttonWidth:'100%'
	 });
	 
	 
	 
	 /**
		* This  Function use for admin chemist shop  add Texbox  on redio button click 
		*created by santosh Kumar  28 Aug 2018
		*/
			
	 $(function () 
	 {
		 
			$("input[name='homeopathy_medicines']").click(function () {
				if ($("#chkYes").is(":checked")) {
					$("#dvhomeopathy").show();
				} else {
					$("#dvhomeopathy").hide();
				}
			});
			
		
			$("input[name='allopathy_medicines']").click(function () {
				if ($("#chk2Yes").is(":checked")) {
					$("#dvallopathy").show();
				} else {
					$("#dvallopathy").hide();
				}
			});
			
			$("input[name='unani_medicines']").click(function () {
				if ($("#chk3Yes").is(":checked")) {
					$("#dvunani").show();
				} else {
					$("#dvunani").hide();
				}
			});
			
			
			$("input[name='ayurvedic_medicines']").click(function () {
				if ($("#chk4Yes").is(":checked")) {
					$("#dvayurvedic").show();
				} else {
					$("#dvayurvedic").hide();
				}
			});
			
			
			$("input[name='home_delivery']").click(function () {
				if ($("#chk5Yes").is(":checked")) {
					$("#dvdelivery").show();
				} else {
					$("#dvdelivery").hide();
				}
			});
			
			
		/**
		* This  Function use for admin  Hospital  chemist shop  add Texbox  on radio button click 
		*created by Santosh Kumar Verma 28 Aug 2018
		*/	
			
		$("input[name='imaging']").click(function () {
			if ($("#imgchkYes").is(":checked")) {
				$("#divimaging").show();
			} else {
				$("#divimaging").hide();
			}
		});
		
			
    });
	
	
	/**
		* This  Function use for  admin  editHospitalDiaganostic Page  if Imaging  not select then hide  HospitalDiaganostic div 
		*created by Santosh Kumar Verma 4 September 2018
		*/
	
    $(document).ready(function () {
        

            if ($("#imgchkYes").is(":checked")) {
                $("#divimaging").show();
            }else{
				$("#divimaging").hide();
			}
          
    });
	 
	 
    </script>
  </body>
</html>