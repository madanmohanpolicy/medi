

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Mediwheel</b> Admin System
        </div>
        <!--<strong>Copyright &copy; 2014-2015 <a href="<?php echo base_url(); ?>">CodeInsect</a>.</strong> All rights reserved.-->
    </footer>
    
    <!-- jQuery UI 1.11.2 -->
     <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script> 
    <!--Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.timepicker.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	
	 


<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css" />
<script type="text/javascript">
        var windowURL = window.location.href;
        pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
        var x= $('a[href="'+pageURL+'"]');
            x.addClass('active');
            x.parent().addClass('active');
        var y= $('a[href="'+windowURL+'"]');
            y.addClass('active');
            y.parent().addClass('active');
			
	 
	 /**
		* This  Function use for admin chemist shop  add Texbox  on redio button click 
		*created by santosh Kumar  28 Aug 2018
		*/
			
	 $(function () 
	 {
		 
			$("input[name='homeopathy_medicines']").click(function () {
				if ($("#chkYes").is(":checked")) {
					$("#dvhomeopathy").show();
				} else {
					$("#dvhomeopathy").hide();
				}
			});
			
		
			$("input[name='allopathy_medicines']").click(function () {
				if ($("#chk2Yes").is(":checked")) {
					$("#dvallopathy").show();
				} else {
					$("#dvallopathy").hide();
				}
			});
			
			$("input[name='unani_medicines']").click(function () {
				if ($("#chk3Yes").is(":checked")) {
					$("#dvunani").show();
				} else {
					$("#dvunani").hide();
				}
			});
			
			
			$("input[name='ayurvedic_medicines']").click(function () {
				if ($("#chk4Yes").is(":checked")) {
					$("#dvayurvedic").show();
				} else {
					$("#dvayurvedic").hide();
				}
			});
			
			
			$("input[name='home_delivery']").click(function () {
				if ($("#chk5Yes").is(":checked")) {
					$("#dvdelivery").show();
				} else {
					$("#dvdelivery").hide();
				}
			});
			
		
		/**
		* This  Function use for admin  master  code  dropdown select
		*created by Santosh Kumar Verma 7 September  2018
		*/
		
			$(function () {
				$("#btnSubmit").click(function () {
              
				var mastercode = $("#mastercode").find("option:selected").val();
				   
				if (mastercode == '0') {
				$('span.mcodeSpan').html('').append("Please Select the Master code");
				   
					return false;
				}else{
				$('span.mcodeSpan').hide();
				return true;
				}
				return true;
				});
			});

		
		/**
		* This  Function use for admin  Hospital  chemist shop  add Texbox  on radio button click 
		*created by Santosh Kumar Verma 28 Aug 2018
		*/	
			
		$("input[name='imaging']").click(function () {
			if ($("#imgchkYes").is(":checked")) {
				$("#divimaging").show();
			} else {
				$("#divimaging").hide();
			}
		});
		
			
    });
	
	
	/**
		* This  Function use for  admin  editHospitalDiaganostic Page  if Imaging  not select then hide  HospitalDiaganostic div 
		*created by Santosh Kumar Verma 4 September 2018
		*/
	
    $(document).ready(function () {
       

            if ($("#imgchkYes").is(":checked")) {
                $("#divimaging").show();
            }else{
				$("#divimaging").hide();
			}
		
          
    });
	
	
	
    
	/**
	* This Function Check use if 24X7 is select then  fromtotime and toTime hide  div 
	*created by Santosh Kumar Verma 13 September 2018
	*start 
	*/
		
			
				if ($("#chktimeYes").is(":checked")) {
					$("#divtime").hide();
					$("#divcharge").show();
				
				} else {
					$("#divtime").show();
					$("#divcharge").hide();
					
				}
		
	  $(function () 
	 {
		 
			$("input[name='chk_time']").click(function () {
				if ($("#chktimeYes").is(":checked")) {
					$("#divtime").hide();
					$("#divcharge").show();
				} else {
					$("#divtime").show();
					$("#divcharge").hide();
				}
			});
	});	
	 
   //end
   
   
   
   
   	/**
		* This Function Check use if 24X7 is select then  fromtotime and toTime hide  div 
		*created by Santosh Kumar Verma 13 September 2018
		*start 
		*/
		
			    
				if ($("#homecollection").is(":checked")) {
					$("#divhomecollectioncharge").show();
				    $("#divhomecollectiontime").hide();
				} else {
					$("#divhomecollectioncharge").hide();
					$("#divhomecollectiontime").show();
					
				}
		
		
		
		
	  $(function () 
	 {
		 
			$("input[name='homecollection']").click(function () {
				if ($("#hcollectYes").is(":checked")) {
					$("#divhomecollectiontime").hide();
					 $("#divhomecollectioncharge").show();
				} else {
					$("#divhomecollectiontime").show();
					 $("#divhomecollectioncharge").hide();
				}
			});
	});	
	 
   //end
   
   
   
      	/**
		* This Function  use if Opinion Via Mail  is check Then Show  "opinion via mail charges" div  if unchecked  hide "opinion 
		  via mail charges" div at  "addNewHospitalDoctor"
		*created by Santosh Kumar Verma 13 September 2018
		*start 
		*/
		if ($("#opinionmailYes").is(":checked")) {
					$("#optionviamaildiv").show();
						
					} else {
						
						 $("#optionviamaildiv").hide();
					}
		 $(function () 
		 {
			 
				$("input[name='opinionmail']").click(function () {
					if ($("#opinionmailYes").is(":checked")) {
						$("#optionviamaildiv").show();
						
					} else {
						
						 $("#optionviamaildiv").hide();
					}
				});
		});	
			 
   //end
   
   
        /**
		* This Function  use if Opinion Via Mail  is check Then Show  "Video Conferencing" div  if unchecked  hide "Video  *Conferencing" div at  "addNewHospitalDoctor  and EditNewHospitalDoctor" 
		*created by Santosh Kumar Verma 13 September 2018
		*start 
		*/
		
		if ($("#opinionvideoYes").is(":checked")) {
						$("#videoConferdiv").show();
						
					} else {
						
						 $("#videoConferdiv").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='opinionvideo']").click(function () {
					if ($("#opinionvideoYes").is(":checked")) {
						$("#videoConferdiv").show();
						
					} else {
						
						 $("#videoConferdiv").hide();
					}
				});
		});	
			 
   //end
 
 //############################This  function Use for  "Hospital Diaganostic and Center"   add , edit form Only ##############
 
         /**
		* This Function  use  for  Digital X-ray  
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#xrayYes").is(":checked")) {
						$("#divoxrayffer").show();
						
					} else {
						
						 $("#divoxrayffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='xray']").click(function () {
					if ($("#xrayYes").is(":checked")) {
						$("#divoxrayffer").show();
						
					} else {
						
						 $("#divoxrayffer").hide();
					}
				});
		});	
			 
			 
		//end	 
			 
		/**
		* This Function  use  for  Ecg 
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#ecgYes").is(":checked")) {
						$("#divecgoffer").show();
						
					} else {
						
						 $("#divecgoffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='ecg']").click(function () {
					if ($("#ecgYes").is(":checked")) {
						$("#divecgoffer").show();
						
					} else {
						
						 $("#divecgoffer").hide();
					}
				});
		});
		
		//end
		
		/**
		* This Function  use  for  ultrasound 
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#ultrasoundYes").is(":checked")) {
						$("#divultrasoundoffer").show();
						
					} else {
						
						 $("#divultrasoundoffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='ultrasound']").click(function () {
					if ($("#ultrasoundYes").is(":checked")) {
						$("#divultrasoundoffer").show();
						
					} else {
						
						 $("#divultrasoundoffer").hide();
					}
				});
		});	
		//end
		
		
		
		/**
		* This Function  use  for  CT 
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#ctYes").is(":checked")) {
						$("#divctoffer").show();
						
					} else {
						
						 $("#divctoffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='ct']").click(function () {
					if ($("#ctYes").is(":checked")) {
						$("#divctoffer").show();
						
					} else {
						
						 $("#divctoffer").hide();
					}
				});
		});	
		//end
		
		
		/**
		* This Function  use  for  MRI 
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#mriYes").is(":checked")) {
						$("#divmrioffer").show();
						
					} else {
						
						 $("#divmrioffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='mri']").click(function () {
					if ($("#mriYes").is(":checked")) {
						$("#divmrioffer").show();
						
					} else {
						
						 $("#divmrioffer").hide();
					}
				});
		});	
		//end
		
		
		/**
		* This Function  use  for  PETCT 
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#petctYes").is(":checked")) {
						$("#divpetctoffer").show();
						
					} else {
						
						 $("#divpetctoffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='petct']").click(function () {
					if ($("#petctYes").is(":checked")) {
						$("#divpetctoffer").show();
						
					} else {
						
						 $("#divpetctoffer").hide();
					}
				});
		});	
		//end
		
		
		/**
		* This Function  use  for  2D Eco
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#ecoYes").is(":checked")) {
						$("#divecooffer").show();
						
					} else {
						
						 $("#divecooffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='eco']").click(function () {
					if ($("#ecoYes").is(":checked")) {
						$("#divecooffer").show();
						
					} else {
						
						 $("#divecooffer").hide();
					}
				});
		});	
		//end
		
		
		/**
		* This Function  use  for  Trademill
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#trademillYes").is(":checked")) {
						$("#divtrademilloffer").show();
						
					} else {
						
						 $("#divtrademilloffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='trademill']").click(function () {
					if ($("#trademillYes").is(":checked")) {
						$("#divtrademilloffer").show();
						
					} else {
						
						 $("#divtrademilloffer").hide();
					}
				});
		});	
		//end
		
		
		/**
		* This Function  use  Waiting Area
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#waitingareaYes").is(":checked")) {
						$("#divwaitingareaoffer").show();
						
					} else {
						
						 $("#divwaitingareaoffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='waitingarea']").click(function () {
					if ($("#waitingareaYes").is(":checked")) {
						$("#divwaitingareaoffer").show();
						
					} else {
						
						 $("#divwaitingareaoffer").hide();
					}
				});
		});	
		//end
		
		
		/**
		* This Function  use Reception Area
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#receptionareaYes").is(":checked")) {
						$("#divreceptionareaoffer").show();
						
					} else {
						
						 $("#divreceptionareaoffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='receptionarea']").click(function () {
					if ($("#receptionareaYes").is(":checked")) {
						$("#divreceptionareaoffer").show();
						
					} else {
						
						 $("#divreceptionareaoffer").hide();
					}
				});
		});	
		//end
		
		
		/**
		* This Function  use Parking Space
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#parkingspaceYes").is(":checked")) {
						$("#divparkingspaceoffer").show();
						
					} else {
						
						 $("#divparkingspaceoffer").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='parkingspace']").click(function () {
					if ($("#parkingspaceYes").is(":checked")) {
						$("#divparkingspaceoffer").show();
						
					} else {
						
						 $("#divparkingspaceoffer").hide();
					}
				});
		});	
		//end
		
		
		
		
		/**
		* This Function  use Sunday Open
		*created by Santosh Kumar Verma 18 September 2018
		*start 
		*/
		
		if ($("#sundayopenYes").is(":checked")) {
						$("#divsundayopen").show();
						
					} else {
						
						 $("#divsundayopen").hide();
					}			
		
		 $(function () 
		 {
			 
				$("input[name='sundayopen']").click(function () {
					if ($("#sundayopenYes").is(":checked")) {
						$("#divsundayopen").show();
						
					} else {
						
						 $("#divsundayopen").hide();
					}
				});
		});	
		//end
		

   //####################End  Hospital Diaganostic  add , edit form#####################
   
   
   
   
   
   
 
  //################################################End Santosh Script ###############################################  
	
	 
	  $('#test').multiselect({
		includeSelectAllOption: false,
	  nonSelectedText: 'Select Test',
	  enableFiltering: true,

	
       showCheckbox       : true, 
	  enableCaseInsensitiveFiltering: true,
	  buttonWidth:'100%'
	 });
	 
	 
	 $('#module').multiselect({
		includeSelectAllOption: false,
	  nonSelectedText: 'Select Module ',
	  enableFiltering: true,

	
       showCheckbox       : true, 
	  enableCaseInsensitiveFiltering: true,
	  buttonWidth:'100%'
	 });
	 
	 
    </script>
  </body>
</html>