<script type="text/javascript">
    $(document).ready(function () {
       
//######for edithospitalChemistShop 
		
			if($("#chk4Yes").is(":checked"))
				{
					$("#dvayurvedic").show();
				}else{
					$("#dvayurvedic").hide();
			         }
					 
					 
				if($("#chkYes").is(":checked"))
				{
					$("#dvhomeopathy").show();
				}else{
					$("#dvhomeopathy").hide();
			         }
					 
					 
				if($("#chk2Yes").is(":checked"))
				{
					$("#dvallopathy").show();
				}else{
					$("#dvallopathy").hide();
			         }
					 
					 
				if($("#chk3Yes").is(":checked"))
				{
					$("#dvunani").show();
				}else{
					$("#dvunani").hide();
			         }
		//###end
          
    });

	
	
	 $(function () 
	 {
		 
			$("input[name='ayurvedic_medicines']").click(function () {
				if ($("#chk4Yes").is(":checked")) {
					$("#dvayurvedic").show();
				} else {
					$("#dvayurvedic").hide();
				}
			});
			
		
			$("input[name='homeopathy_medicines']").click(function () {
				if ($("#chkhomeopathyYes").is(":checked")) {
					$("#dvhomeopathy").show();
				} else {
					$("#dvhomeopathy").hide();
				}
			});
			
		
			
			
			$("input[name='allopathy_medicines']").click(function () {
				if ($("#chkAllopathyYes").is(":checked")) {
					$("#dvallopathy").show();
				} else {
					$("#dvallopathy").hide();
				}
			});
			
			
			$("input[name='unani_medicines']").click(function () {
				if ($("#chkUnaniYes").is(":checked")) {
					$("#dvunani").show();
				} else {
					$("#dvunani").hide();
				}
			});
			
			
    });	
	
</script>
<?php
if(!empty($HospitalChemistShopInfo))
{
    foreach ($HospitalChemistShopInfo as $uf)
    {
        $id 							= $uf->id;
        $mastercode 					= $uf->mastercode;
        $shopsubcode 	       		    = $uf->shopsubcode;
		$registrationn_no			    = $uf->registrationn_no;
        $days			 		        = $uf->days;
		$ayurvedic_medicines            = $uf->ayurvedic_medicines;
		$offeron_ayurvedic_medicines    = $uf->offeron_ayurvedic_medicines;
		$displayon_ayurvedic_medicines	= $uf->displayon_ayurvedic_medicines;
		$home_delivery			        = $uf->home_delivery;
		$homeopathy_medicines			= $uf->homeopathy_medicines;
		$offeredon_homeopathy_medicines	= $uf->offeredon_homeopathy_medicines;
		$displayon_homeopathy_medicines	= $uf->displayon_homeopathy_medicines;
		$allopathy_medicines			= $uf->allopathy_medicines;
		$offeredon_allopathy_medicines	= $uf->offeredon_allopathy_medicines;
		$displayedon_allopathy_medicines= $uf->displayedon_allopathy_medicines;
		$unani_medicines			    = $uf->unani_medicines;
		$dis_tobe_diplay_onconsumables  = $uf->dis_tobe_diplay_onconsumables;
		$timings                        = $uf->timings;
		$offeredon_unani_medicines		= $uf->offeredon_unani_medicines;
		$displayon_unani_medicines		= $uf->displayon_unani_medicines;
		$dis_off_onconsumables			= $uf->dis_off_onconsumables;
		$dis_off_onspecific_brand		= $uf->dis_off_onspecific_brand;
		$dis_off_onfmcg		            = $uf->dis_off_onfmcg;
		$dis_tobe_display_onemcg		= $uf->dis_tobe_display_onemcg;
		$dis_tobe_display_onspecific_brand= $uf->dis_tobe_display_onspecific_brand;
		
		$timefrom					= $uf->timefrom;
		$timeto					    = $uf->timeto;
		$time_24_hours				= $uf->time_24_hours;
		$photograph			            = $uf->photograph;
		$createdBy                      = $uf->createdBy;
    	
    }
}

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>  Hospital Chemist Shop
        <small> Edit Hospital Chemist Shop</small>
		
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Hospital Chemist Shop Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                   <form role="form" action="<?php echo base_url().'admin/';?>updateHospitalChemistShop/<?php echo $id;?>" method="post"  name="editHospitalChemistShopForm"  id="editHospitalChemistShopForm" role="form"  enctype="multipart/form-data" >
                        <div class="box-body">
						
						       <div class="row">
							 <input type="hidden" value="<?php echo $id; ?>" name="id" id="id" />  
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Hospital Mastercode</label>
                                         <input type="text" class="form-control" value="<?php echo $mastercode;?>" id="mastercode" name="mastercode" maxlength="128" readonly>
										 	
                                    </div>
                                </div>
                                
								
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="doctor">Registration No*</label>
                                        <input type="text" class="form-control" value="<?php echo $registrationn_no;?>" 
										id="registrationn_no" name="registrationn_no" maxlength="128">
                                    </div>
                                    
                                </div>
                             
                            </div>
                           
						        <div class="row">
							 <input type="hidden" value="<?php echo $id; ?>" name="id" id="id" />  
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mastercode">Shop Subcode</label>
                                        <input type="text" class="form-control" id="shopsubcode" name="shopsubcode" value="<?php echo $shopsubcode;?>" readonly>
										 	
                                    </div>
                                </div>
                                
								
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="doctor">Days*</label>
                                        <input type="text" class="form-control required " id="days" name="days"  value="<?php echo $days; ?>">
                                    </div>
                                    
                                </div>
                             
                            </div>
						   
						  <div class="row">
							  <div class="col-md-6">                                
										<div class="form-group">
										   <label for="discount displayed">Ayurvedic Medicines*</label>  
											Yes <input type="radio" id="chk4Yes" name="ayurvedic_medicines" value="1"  <?php if($ayurvedic_medicines=='1' ) echo "checked"  ;?> />
											 No<input type="radio" id="chkNo" value="0" <?php if($ayurvedic_medicines=='0' ) echo "checked";?>  name="ayurvedic_medicines" />
							     </div>
							  </div>
						  </div>
							
							<div class="row" id="dvayurvedic" style="display: none">
							  <div class="col-md-6">
                                   <div class="form-group">
                                     
										
										   
										
										<label for="discount displayed">Offered on Medicines</label> 
										<input type="text" class="form-control required" value="<?php echo $offeron_ayurvedic_medicines; ?>" id="" name="offeron_ayurvedic_medicines" maxlength="128">
										</div></div>
								     <div class="col-md-6">
                                   <div class="form-group">
										<label for="discount displayed">Displayed on Medicines</label> 
										<input type="text" class="form-control required" value="<?php echo $displayon_ayurvedic_medicines; ?>" id="" name="displayon_ayurvedic_medicines" maxlength="128">
											
										</div></div>
								 
							</div>
							<div class="row">
                                  <div class="col-md-6">                                
										<div class="form-group">							
								<label for="discount displayed">Homeopathy   Medicines*</label>  
										Yes <input type="radio" id="chkYes" name="homeopathy_medicines"
                                       value="1" <?php if($homeopathy_medicines=='1' ) echo "checked"  ;?>   
										/>
										 No<input type="radio" id="chkNo" name="homeopathy_medicines" value="0"   <?php if($homeopathy_medicines=='0' ) echo "checked"  ;?> />
										 
										 </div> </div>
							</div>
							<div class="row"  id="dvhomeopathy" style="display: none">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="discount displayed"> Offered on Medicines</label> 
									<input type="text" class="form-control required" value="<?php echo $offeredon_homeopathy_medicines; ?>" id="offeredon_homeopathy_medicines" name="offeredon_homeopathy_medicines" maxlength="128">
										
									   
                                    </div> </div>
									<div class="col-md-6">
                                    <div class="form-group">
											 <label for="discount displayed">Displayed on Medicines</label>
											<input type="text" class="form-control required" value="<?php echo $displayon_homeopathy_medicines; ?>" id="homeopathy_medicines" name="displayon_homeopathy_medicines" maxlength="128">
											</div>
									   
                                    </div>
									
                                </div>
                           </div>
                          
						   
						      
							  
							   <div class="row"> 
                                     <div class="col-md-6">
										 <div class="form-group">
								         <label for="discount displayed"> Allopathy Medicines*</label>  
										Yes <input type="radio" id="chk2Yes" name="allopathy_medicines"  <?php if($allopathy_medicines=='1' ) echo "checked"  ;?> />
										 No<input type="radio" id="chkNo" name="allopathy_medicines" <?php if($allopathy_medicines=='0' ) echo "checked"  ;?> />
									</div></div>
									
									</div>
								<div class="row" id="dvallopathy" style="display: none">
									<div class="col-md-6">
										 <div class="form-group">
										 <label for="discount displayed">Offered on Medicines</label>
										<input type="text" class="form-control required" value="<?php echo $offeredon_allopathy_medicines; ?>" id="" name="offeredon_allopathy_medicines" maxlength="128">
										</div>
									</div>
									<div class="col-md-6">
										 <div class="form-group">
										 <label for="discount displayed">Displayed on Medicines	</label>	
										 <input type="text" class="form-control required" value="<?php echo $displayedon_allopathy_medicines; ?>" id="" name="displayedon_allopathy_medicines" maxlength="128">
										</div>
									</div>
								</div>
									  
                                  
                               
	
							 
							 <div class="row">
							  <div class="col-md-6">
                              <div class="form-group">
							 <label for="discount displayed">  Unani Medicines*</label>  
										Yes <input type="radio" id="chk3Yes" name="unani_medicines" value="1"  <?php if($unani_medicines=='1' ) echo "checked"  ;?> /> 
										 No<input type="radio" id="chkNo" name="unani_medicines" value="0"  <?php if($unani_medicines=='0' ) echo "checked"  ;?> /> 
							 </div> </div> 
							 </div>
							
						<div class="row" id="dvunani" style="display: none" >
                              <div class="col-md-6">
                                   <div class="form-group">
                                     
										
										   
									
										 <label for="discount displayed">	Offered on Medicines </label>  <input type="text" class="form-control required" value="<?php echo $offeredon_unani_medicines; ?>" id="" name="offeredon_unani_medicines" maxlength="128">
											  </div>
									
                                </div>
									<div class="col-md-6">
                                   <div class="form-group">
                                     	
											 <label for="discount displayed">Dispalay on Medicines </label>  <input type="text" class="form-control required" value="<?php echo $displayon_unani_medicines; ?>" id="" name="displayon_unani_medicines" maxlength="128">
											
									
									   
                                    </div>
									
                                </div>
								
                    </div>
							
						<div class="row">
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount displayed">Timings </label>
                                        <input type="text" class="form-control required " id="timings" name="timings"  value="<?php echo $timings; ?>">
                                    </div>
                                </div>
								
								 <div class="col-md-6">
                                   
									 <div class="form-group">
                                        <label for="discount displayed">Home Delivery*</label>  
										Yes <input type="radio" id="chk5Yes" name="home_delivery" value="1"  <?php if($home_delivery=='1' ) echo "checked"  ;?> />
										 No<input type="radio" id="chkNo" value="0"  <?php if($home_delivery=='0' ) echo "checked"  ;?> name="home_delivery" />
										   
								
									   
                                    </div>
                                </div>
							 </div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount offered on Consumables*</label>
                                        <input type="text" class="form-control required" id="dis_off_onconsumables" name="dis_off_onconsumables"  value="<?php echo $dis_off_onconsumables; ?>" >
                                    </div>
                                </div>
                                 
								 
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount to be displayed on Consumables*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_diplay_onconsumables" 
										name="dis_tobe_diplay_onconsumables" value="<?php echo $dis_tobe_diplay_onconsumables; ?>" >
                                    </div>
                                </div>
								

								
							 
							</div>
							
							
							<div class="row">
							<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount offered on FMCG*</label>
                                        <input type="text" class="form-control required " id="dis_off_onfmcg" 
										name="dis_off_onfmcg"  value="<?php echo $dis_off_onfmcg; ?>" >
                                    </div>
                                </div>
							
							  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount to be displayed on FMCG*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_display_onemcg" 
										name="dis_tobe_display_onemcg"  value="<?php echo $dis_tobe_display_onemcg; ?>">
                                    </div>
                                </div>
                                
							</div>
							
							   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Discount offered on Specific Brand*</label>
                                        <input type="text" class="form-control required" id="dis_off_onspecific_brand" name="dis_off_onspecific_brand" value="<?php echo $dis_off_onspecific_brand; ?>" >
                                    </div>
                                </div>
                                
									 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount offered">Discount to be displayed on Specific Brand*</label>
                                        <input type="text" class="form-control required " id="dis_tobe_display_onspecific_brand" 
										name="dis_tobe_display_onspecific_brand"  value="<?php echo $dis_tobe_display_onspecific_brand; ?>" >
                                    </div>
                                </div>
								
							</div>
							
							
							
							
							
							<label for="discount displayed">24X7(Availability)</label>  
										Yes <input type="radio" id="chktimeYes"  value="1" name="chk_time"  <?php if($time_24_hours=='1' ) echo "checked"  ;?> />
										 No<input type="radio" id="chktimeNo" value="0"  name="chk_time"  <?php if($time_24_hours=='0' ) echo "checked"  ;?>/>
										 
							
                    
                                  							
							  <div class="row"  id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom" id="timefrom" value="<?php echo $timefrom;?>" class="form-control">
									
								</div>
								
										<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto" value="<?php echo $timeto;?>" class="form-control">
								</div>
							 
							</div>	
							
							  <div class="row">
							  
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="photograph">Photograph </label>
										 <input type="file" class="form-control " id="photograph" name="photograph"  >
                                         <input type="hidden" class="form-control " id="hidphotograph" name="hidphotograph" value="<?php echo $photograph; ?>" >  <img src="<?php echo $photograph; ?>" width="50" height="50">
                                    </div>
                                </div>
							
							
                            <div class="box-footer">
							
                            <input type="submit"  id="Submit" class="btn btn-primary" value="Submit" />
							     <!-- <input type="submit"  id="Submit"  value="Submit" />-->
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
					
					
					
			
					
				</div>		
				
						



						
		</div>	


		
		
                </div>
				
				
			<div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php  } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				
				
						<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>

						<div class="row">


						<?php echo validation_errors('<div  class="alert alert-danger alert-dismissable">', ' <button  type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
						</div> 
				
				
                
             </div>	
 
		 </div>
		 

		 
		 
        </div>    
    </section>
</div>
<script>
$(document).ready(function(){

	 
	$('#timefrom').timepicker();
	$('#timeto').timepicker();
		
    
  });
</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>