<?php

$id = '';
$packagename = '';
$cost = '';
$discount = '';

$tests='';
$healthTests=$this->hospital_model->getHealthTestsName();
if(!empty($healthCheckupInfo))
{
	
	
    foreach ($healthCheckupInfo as $uf)
    {
        $id = $uf->id;
        $packagename = $uf->name;
        $cost = $uf->cost;
        $discount = $uf->discount;
       $tests=$uf->tests;
	 $tests=explode(',',$tests);

	 $referred_by=$uf->referred_by;
	 $needto_visit_hospital=$uf->needto_visit_hospital;
	 $home_samplecollection=$uf->home_samplecollection;
	 $fasting=$uf->fasting;
	 $center_visit=$uf->center_visit;
	 $time_24_hours=$uf->time_24_hours;
	 $pkg_charge=$uf->pkg_charge;
	 $timefrom=$uf->timefrom;
	 $timeto=$uf->timeto;
	 
	$description=$uf->description;
    }
}



?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Health Package
        <small> Edit HealthPackage</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Health Package Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url().'admin/';?>editHealthPackages/<?php echo $id;?>" method="post"  id="healthcheckuppackage" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Package Name</label>
                                        <input type="text" class="form-control" id="packagename" placeholder="Package Name" name="packagename" value="<?php echo $packagename; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $id; ?>" name="id" id="id" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Cost</label>
                                        <input type="text" class="form-control" id="cost" placeholder="Enter cost" name="cost" value="<?php echo $cost; ?>" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <!--<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" maxlength="20">
                                    </div>
                                </div>
                            </div>-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Discount</label>
                                        <input type="text" class="form-control" id="discount" placeholder="discount" name="discount" value="<?php echo $discount; ?>" maxlength="10">
                                    </div>
                               
                            </div>
							
							  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename">Tests</label>
											 <select id="test" name="test[]" multiple class="form-control"  >
										 
										 <?php 
										
									
										foreach($healthTests as $value){
											
										$parametercount=$this->hospital_model->gethealthCheckupTestParametersCount($value->id);
										
											?>
											<option value="<?php echo $value->id;?>" <?php if(in_array($value->id,$tests)) echo "selected";?>><?php echo $value->name.'&nbsp;('.$parametercount.' parameter)';
									  
										}
									   ?></option>
                                     
										 
										 
   
  
                                    </select>
										
									   
                                    </div>
                                    
                               
                            </div>
                        </div>
						
						
						
								<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename">  Referred By</label>
                                        <input type="text" class="form-control " value="<?php echo $referred_by; ?>"  id="referred_by" name="referred_by" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
									<br/>
                                        <label for="cost">Need To Visit Hospital</label>
                                          <input type="radio" value="1" id="needvisithospitalYes"   <?php if($needto_visit_hospital=='1' ) echo "checked"  ;?>  name="needto_visit_hospital">Yes
										 <input type="radio" id="needvisithospitalNo" value="0"   <?php if($needto_visit_hospital=='0' ) echo "checked"  ;?> name="needto_visit_hospital">No
                                    </div>
                                </div>
                            </div>
							
								<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename">  Home Sample Collection</label>
                                        <input type="radio" value="1" id="homecollectionYes"  <?php if($home_samplecollection=='1' ) echo "checked"  ;?>   name="home_samplecollection">Yes
										 <input type="radio" id="homecollectionNo" value="0" <?php if($home_samplecollection=='0' ) echo "checked"  ;?>  name="home_samplecollection">No
                                    </div>
                                    
                                </div>
								
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cost">Fasting</label>
                                          <input type="radio" value="1" id="fastingYes"  <?php if($fasting=='1' ) echo "checked"  ;?>  name="fasting">Yes
										 <input type="radio" id="fastingNo" value="0"  <?php if($fasting=='0' ) echo "checked"  ;?> name="fasting">No
                                    </div>
                                </div>
                            </div>
								
								
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="packagename"> Center Visit</label>
                                        <input type="radio" value="1" id="centervisitYes" <?php if($center_visit=='1' ) echo "checked"  ;?>   name="center_visit">Yes
										 <input type="radio" id="centervisitNo" value="0"  <?php if($center_visit=='0' ) echo "checked"  ;?> name="center_visit">No
                                    </div>
                                    
                                </div>
                          
                            </div>	
								
								   
								    
									  <label for="discount displayed">24X7(Availability)</label>  
										Yes <input type="radio" id="chktimeYes"  value="1"  <?php if($time_24_hours=='1' ) echo "checked";?>  name="chk_time" />
										 No<input type="radio" id="chktimeNo" value="0" <?php if($time_24_hours=='0' ) echo "checked"  ;?>  name="chk_time" />
										 
							
                    
                                  							
							  <div class="row"  id="divtime">
                             
							 		<div class="col-md-4">
								
									<label for="timefrom">Time From</label> 
									<input type="text" name="timefrom" id="timefrom" value="<?php echo $timefrom;?>" class="form-control required">
									
								</div>
									
								
							
									<div class="col-md-4">
									<label for="timeto">Time To</label>
									<input type="text" name="timeto" id="timeto"  value="<?php echo $timeto;?>" class="form-control required">
								
								</div>
							 
							 
                            </div>
							<div class="row"  id="divcharge">
							<div class="col-md-6">
                             <div class="form-group">
							<label for="timeto">Charge</label>
							 <input type="text" class="form-control " value="<?php echo $pkg_charge;?>"  id="pkg_charge" name="pkg_charge" maxlength="128">
							</div></div>
							
							</div>
							<br/>
							
						
						
						 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" id="descreption" placeholder="descreption" name="descreption" ><?php echo $description; ?> </textarea>
                                    </div>
                               
                            </div>
						
						</div>
						
						
						
						<!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" name="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
          
        </div> 
  <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>		
    </section>
</div>
<script>
$(document).ready(function(){

	 
	$('#timefrom').timepicker();
	$('#timeto').timepicker();
		
    
  });



</script>
<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>