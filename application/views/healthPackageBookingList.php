


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Health Packages Booking List 
        
      </h1>
	  <?php 


?>
    </section>
    <section class="content">
       
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Health Packages Booking List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url().'admin/' ?>healthPackageBookingList" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
				
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
					    <th>Sr. No.</th>
                        <th>Package Name</th>
						 <th>User Name</th>
						  <th>Hopital Name</th>
                        <th>Cost</th>
                       
                       <th> Cost After Discount</th>
					    <th> Appointment Date</th>
						   <th> Appointment Time</th>
						   <th>Corporate User</th>
						   <th>Corporate Name</th>
						   <th>Action</th>
					   
                        
                    </tr>
                    <?php
			         $i=0;
                    if(!empty($bookingRecords))
                    {
						
                        foreach($bookingRecords as $record)
                        {
							
							$i++;
							
						
					
					
							
							
                    ?>
					<form action="<?php echo base_url().'admin/' ?>healthPackageBookingList" id="bookform_<?php echo $i;?>" name="bookform_<?php echo $i;?>" method="post">
                    <tr>
					    <td><?php echo $i;?></td>
                        <td><?php echo $record->package_name ?></td>
						
						
                        <td><?php echo $record->user_name ?></td>
                        <td><?php echo $record->hospital_name ?></td>
                       
                        <td><?php echo $record->cost ?></td>
						
                        
						 <td><?php echo $record->cost_after_discount; ?></td>
						  <td><?php echo $record->appointment_date; ?></td>
						   <td><?php echo $record->appointment_time; ?></td>
                            <td><?php if ($record->is_corporate_user) {echo 'Yes'; } else { echo 'No';}?></td>
							<td><?php echo $record->corporate_name; ?></td>
							<td></td>
                    </tr>
					</form>
                    <?php
                        }
                    }
					
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
				
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "admin/healthPackageBookingList/" + value);
            jQuery("#searchList").submit();
        });
    });
	
	function processing(frm){
		
		frm.submit();
	}
</script>
