<?php

$id = '';
$packagename = '';
$cost = '';
$discount = '';

$tests='';
$healthTests=$this->hospital_model->getHealthTestsName();
$healthpackage=$this->hospital_model->getHealthCheckupPackage();
if(!empty($healthCheckupPackageLinkingInfo))
{
	
	
    foreach ($healthCheckupPackageLinkingInfo as $uf)
    {
        $id = $uf->id;
	
        $hospital_diagonastic_id = $uf->hospital_diagonastic_id;
	
        $type = $uf->type;
        $packageId = $uf->packageId;
		$time_slot = $uf->time_slot;
		$vendor_decided_name = $uf->vendor_decided_name;
       $cost=$uf->cost;
	 $memberDiscount=$uf->memberDiscount;
$member_discount_inpercent=$uf->member_discount_inpercent;
$member_discount_inpercent;


	$mediwheelDiscount=$uf->mediwheelDiscount;
	$mediwheel_discount_inpercent=$uf->mediwheel_discount_inpercent;
	$homecollection=$uf->home_collection;
    }
	
	$organisation=$this->hospital_model->getOrganisationName($hospital_diagonastic_id );
	

}



?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Health Package Linking
        <small> Edit HealthPackage Linking</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Health Package Linking Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url().'admin/';?>editHealthPackageLinking/<?php echo $id;?>" method="post"  id="healthcheckuppackage" role="form">
                <div class="box-body">
					 <div class="row">
							 <div class="col-md-6">                                
								<div class="form-group">
									<label for="healthpackage">Health Package</label>
									<select class="form-control "  id="healthpackage" name="healthpackage" >
									<option value="0">Select HealthPackage</option>
									<?php foreach($healthpackage as $value){
										?>
									<option value="<?php echo $value->id;?>"  <?php  if($value->id==$packageId) echo "selected";?>><?php echo $value->name;?></option>
									<?php } ?>
										<select>
								</div>
								
							</div>
                            
                               
								<div class="col-md-6">
                                    <div class="form-group">
                                         <label for="doctor">Enter Vendor Decided Name*</label>
                                        <input type="text" class="form-control" value="<?php echo $vendor_decided_name; ?>" id="vendor_decided_name" name="vendor_decided_name" maxlength="128" required>
                                       
                                    </div>
                                </div>
                                
                     </div>
				
				 <div class="row"> 
				 
			
						
								  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="facilitytype">Facility Type</label> 
                                        <input type="text" name="facilitytype" id="facilitytype" class="form-control " value="<?php $type;?><?php if($type=='1')echo 'Hospital'; if ($type=='2') echo 'Diaganotic Center' ;?>" readonly>
                                    </div>
                                    
                               
                                
                                    </div>
								 
							
							
						
								  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="facilityname">Facility Name</label>
                                       <input type="text" name="facilityname" id="facilityname" value="<?php   $hospital_diagonastic_id;?> <?php if(isset($organisation[0])){echo $organisation[0]->organisation;}?>" class="form-control "  readonly>
                                    </div>
                          
                            </div>
							
							</div>
                                    
                               
                                
                           <div class="row">
								
								 
								
								  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="cost">Cost</label>
                                        <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control " value="<?php echo $cost;?>"  id="cost" name="cost" maxlength="128">
                                    </div>
                                    
                               
                                
                            </div>
							</div>
                          
                           
                           
                           
							  <div class="row">
							
						
								  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="memberdiscount">Member Discount</label>
										<input type="radio" name="member_discount_inpercent" value="0" <?php if($member_discount_inpercent=='0') echo "checked";?>>In Rs.
										<input type="radio" name="member_discount_inpercent" value="1" <?php if($member_discount_inpercent=='1') echo "checked";?>>In %
										
                                        <input type="text" value="<?php echo  $memberDiscount ?>" class="form-control "  oninput="this.value=this.value.replace(/[^0-9]/g,'');"  id="memberdiscount" name="memberdiscount"  required>
                                    </div>
                                    
                               
                                
                            </div>
								
								  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="mediwheeldiscount">Mediwheel Discount</label>
										<input type="radio" name="mediwheel_discount_inpercent" value="0" <?php if($mediwheel_discount_inpercent=='0') echo "checked";?>>In Rs.
										<input type="radio" name="mediwheel_discount_inpercent" value="1" <?php if($mediwheel_discount_inpercent=='1') echo "checked";?>>In %
                                        <input type="text" class="form-control " oninput="this.value=this.value.replace(/[^0-9]/g,'');" value="<?php echo  $mediwheelDiscount;?>"  id="mediwheeldiscount" name="mediwheeldiscount" required>
                                    </div>
                                    
                               
                                
                            </div>
                          
                            </div>
                           
						
							
						<div class="row">
						  <div class="col-md-6">                                
                                    <div class="form-group">
                                        
                                    <label for="time_slot">Time Slot (In minutes)</label>
								     <select class="form-control "  id="time_slot" name="time_slot" >
									
                                        <option value="15" <?php if($time_slot=='15') echo "selected"; ?> >15</option>
										<option value="30" <?php if($time_slot=='30') echo "selected"; ?>>30</option>
										<option value="45" <?php if($time_slot=='45') echo "selected"; ?>>45</option>
										<option value="60"<?php if($time_slot=='60') echo "selected"; ?> >60</option>
                                    </div>
									
						
						 </div>
							
							
							
							<div class="col-md-6">                                
                                    <div class="form-group">
                                        
                                      <input type="hidden"   value="1" <?php  if($homecollection=='1') echo "checked";?> id="homecollection" name="homecollection">
									
                                    </div>
                                    	
                                
                            </div>
						
							
						
							
		
                        </div><!-- /.box-body -->
						
						
						<!-- /.box-body -->
    
                        <div class="box-footer">
						<br/>
                            <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" name="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
           
			
			
        </div> 

 <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>		
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>