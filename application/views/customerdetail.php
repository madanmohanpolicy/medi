<?php
if(!empty($customerInfo))
{
    foreach ($customerInfo as $uf)
    {
        $id 					= $uf->id;
        $name 			= $uf->name;
		$email 			= $uf->email;
		$mobile 		= $uf->mobile;
		$landmark 		= $uf->landmark;
		$city 		    = $uf->city;
	    $address 			= $uf->address;
     }
}

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>Customer Listing
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
				<?php 
			$module=$this->session->module;
			
				$permission= explode(',',$this->session->permission);
				if(in_array('1',$permission)&&$module=='1'|| $role == ROLE_ADMIN){
				?>
                    <a class="btn btn-primary" href="<?php echo base_url().'admin/'; ?>addNewDiaganosticform"><i class="fa fa-plus"></i> Add New</a>
					<?php 
			 }
				 ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Customer Details</h3>
        <?php // print_r($customerInfo);?>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
					<th col-xs-12> Customer  Details </th>
				    </tr>
                    
					<tr> <td>Name</td> <td><?php echo $name;?></td></tr>
					<tr> <td>Email</td> <td><?php echo $email;?></td></tr>
					<tr> <td>phone</td> <td><?php echo $mobile;?></td></tr>
					<tr> <td>city</td> <td><?php echo  $city;?></td></tr>
				    <tr> <td>landmark</td> <td><?php echo $landmark;?></td></tr>
				   <tr> <td>Address</td> <td><?php echo $address;?></td></tr>
				  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
               jQuery("#searchList").attr("action", baseURL + "admin/customer/" + value);
			
            jQuery("#searchList").submit();
        });
    });
</script>
