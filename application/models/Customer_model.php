<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	class Customer_model extends CI_Model
	{
		/**
		 * This function is used to get the  Customer listing count
		 * created By  Santosh Kumar Verma
		 */
		/* this function for 1MG Customer Listing */
			
		function customerListingCount($searchText = '')
		{
			$this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.diaganosticSubCode, BaseTbl.registrationNo, BaseTbl.nabl,BaseTbl.lab,BaseTbl.digitalXray,BaseTbl.ultrasound,BaseTbl.ecg,BaseTbl.	trademill');
			$this->db->from('tbl_hospital_diaganostic as BaseTbl');
			$this->db->where('BaseTbl.masterCode  LIKE ','%DIA' );
	   
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
								OR  BaseTbl.diaganosticSubCode  LIKE '%".$searchText."%'
								OR  BaseTbl.registrationNo  LIKE '%".$searchText."%'
								
								)";
				$this->db->where($likeCriteria);
			}
			$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
			$this->db->where('BaseTbl.isDeleted', 0);
		   
			$query = $this->db->get();
		   
			return $query->num_rows();
		}
		

		/**
		* This function is used to to  Customer Listing 
		*created by santosh 10 September 2018
		*/
		 function customerListing($searchText = '', $page, $segment)
		{
		 $this->db->select('BaseTbl.id,BaseTbl.name, BaseTbl.mobile,BaseTbl.email, buy_address.address, buy_address.	landmark,buy_address.status,buy_address.state,buy_address.city');
			 $this->db->from('tbl_buyers as BaseTbl');
			 $this->db->join('tbl_buyer_address as buy_address', 'buy_address.buyer_id = BaseTbl.id');
		
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
								OR  BaseTbl.diaganosticSubCode  LIKE '%".$searchText."%'
								OR  BaseTbl.registrationNo  LIKE '%".$searchText."%'
								OR BaseTbl.nabl  LIKE '%".$searchText."%'
								OR BaseTbl.homeCollection  LIKE '%".$searchText."%'
								OR BaseTbl.discountOfferedTest  LIKE '%".$searchText."%'
								OR BaseTbl.createdBy  LIKE '%".$searchText."%'
							
								)";
			}
	
			$this->db->where('buy_address.isDeleted', 0);
			 $this->db->where('BaseTbl.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
			$this->db->limit($page, $segment);
			$query = $this->db->get();
			//print_r($query );
			
			$result = $query->result();   
			
			return $result;
		}
		
		  
		/**
		* This function is used to Add new  Diaganostic
		* create by Santosh Kumar
		* Date : 5 September 2018
		* @return boolean $result : TRUE / FALSE
		*/
		  
		 function addNewDiaganostic($diaganosticsInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_hospital_diaganostic', $diaganosticsInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
	
		/**
		* This function is used to  Diaganostic Info
		* created by Santosh Kumar  
		* Date : 5 September 2018
		*/
		function getcustomerInfo($id)
		{
			
			
			$this->db->select('BaseTbl.id,BaseTbl.name,BaseTbl.status, BaseTbl.mobile,BaseTbl.email, buy_address.address, buy_address.	landmark,buy_address.landmark,buy_address.city');
			$this->db->from('tbl_buyers as BaseTbl');
			$this->db->join('tbl_buyer_address as buy_address', 'buy_address.buyer_id = BaseTbl.id');
			$this->db->where('buy_address.isDeleted', 0);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.id', $id);
			$query = $this->db->get();
			return $query->result();
		}
	
		
		/**
		* This function is used to update  Diaganostic
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
		
		public function updateDiaganostic($hospitalDiaganosticInfo ,$id)
		{
		$this->db->where('id', $id);
		$query= $this->db->update('tbl_hospital_diaganostic', $hospitalDiaganosticInfo);
		return TRUE;
			
		}
		
		
		/**
		* This function is used to add  new Diaganostics
		*created by santosh 5 September 2018
		*/
		function addNewDiaganostics($diaganosticsInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_hospital_diaganostic', $diaganosticsInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
		
		
		/**
		* This function is used to delete customer
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function deleteCustomer($id ,$customer)
		{
		  
		  
		     $this->db->where('id', $id);
             $this->db->update('tbl_buyers', $customer);
			 
			 $this->db->where('buyer_id', $id);
			 $this->db->update('tbl_buyer_address', $customer);
			 
			 return $this->db->affected_rows();
			 
		
		}
		
		
		/**
		* This function is used to change  customer status
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function CustomerStatus($id ,$customer)
		{
		  
		  
		     $this->db->where('id', $id);
             $this->db->update('tbl_buyers', $customer);
			 $this->db->where('buyer_id', $id);
             $this->db->update('tbl_buyer_address', $customer);
			 return $this->db->affected_rows();
			 
		
		}
		#########################cart ###################################################
		/**
		* This function is used to to  cart Listing 
		*created by santosh 10 September 2018
		*/
		 function cartListing($searchText = '', $page, $segment)
		{
		 $this->db->select('BaseTbl.product_id,BaseTbl.id, BaseTbl.quantity, BaseTbl.discounted_price, BaseTbl.	mrp,BaseTbl.product_name,BaseTbl.city');
			 $this->db->from('tbl_cart as BaseTbl');
			 $this->db->join('tbl_buyers as buyers', 'buyers.id = BaseTbl.user_id');
		
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
								OR  BaseTbl.diaganosticSubCode  LIKE '%".$searchText."%'
								OR  BaseTbl.registrationNo  LIKE '%".$searchText."%'
								OR BaseTbl.nabl  LIKE '%".$searchText."%'
								OR BaseTbl.homeCollection  LIKE '%".$searchText."%'
								OR BaseTbl.discountOfferedTest  LIKE '%".$searchText."%'
								OR BaseTbl.createdBy  LIKE '%".$searchText."%'
							
								)";
			}
	
			$this->db->where('buyers.isDeleted', 0);
			
			$this->db->order_by('BaseTbl.id', 'DESC');
			$this->db->limit($page, $segment);
			$query = $this->db->get();
			//print_r($query );
			
			$result = $query->result();   
			
			return $result;
		}
		
		
		function cartListingCount($searchText = '')
		{
			$this->db->select('BaseTbl.product_id, BaseTbl.quantity, BaseTbl.discounted_price, BaseTbl.	mrp,BaseTbl.product_name,BaseTbl.city');
			$this->db->from('tbl_cart as BaseTbl');
			 $this->db->join('tbl_buyers as buyers', 'buyers.id = BaseTbl.user_id');
			$this->db->where('BaseTbl.status'  , 1 );
	   
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.product_id  LIKE '%".$searchText."%'
								OR  BaseTbl.mrp  LIKE '%".$searchText."%'
								OR  BaseTbl.product_name  LIKE '%".$searchText."%'
								
								)";
				$this->db->where($likeCriteria);
			}
		
			$this->db->where('buyers.isDeleted', 0);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
		   
			$query = $this->db->get();
		   
			return $query->num_rows();
		}
		
	
    
    }

  