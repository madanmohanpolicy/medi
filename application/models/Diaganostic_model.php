<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	class Diaganostic_model extends CI_Model
	{
		/**
		 * This function is used to get the  diaganostic listing count
		 * 
		 */
		/* function for Hospital Diaganostic Listing */
			
		function diagnosticListingCount($searchText = '')
		{
			$this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.diaganosticSubCode, BaseTbl.registrationNo, BaseTbl.nabl,BaseTbl.lab,BaseTbl.digitalXray,BaseTbl.ultrasound,BaseTbl.ecg,BaseTbl.	trademill');
			$this->db->from('tbl_hospital_diaganostic as BaseTbl');
			 $this->db->where('BaseTbl.masterCode  LIKE ','%DIA' );
	   
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
								OR  BaseTbl.diaganosticSubCode  LIKE '%".$searchText."%'
								OR  BaseTbl.registrationNo  LIKE '%".$searchText."%'
								
								)";
				$this->db->where($likeCriteria);
			}
			$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
			$this->db->where('BaseTbl.isDeleted', 0);
		   
			$query = $this->db->get();
		   
			return $query->num_rows();
		}
		

		/**
		* This function is used to to hospital Diaganostic Listing 
		*created by santosh 28 Aug 2018
		*/
		 function diaganosticListing($searchText = '', $page, $segment)
		{
		 $this->db->select('BaseTbl.id,hospital.organisation as organisation, BaseTbl.masterCode, BaseTbl.diaganosticSubCode,BaseTbl.registrationNo, BaseTbl.nabl, BaseTbl.lab,BaseTbl.photograph,BaseTbl.homeCollection,BaseTbl.discountOfferedTest,BaseTbl.status,BaseTbl.discountDisplayedTest,BaseTbl.createdBy,BaseTbl.status');
			 $this->db->from('tbl_hospital_diaganostic as BaseTbl');
			 $this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.masterCode');
		
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
								OR  BaseTbl.diaganosticSubCode  LIKE '%".$searchText."%'
								OR  BaseTbl.registrationNo  LIKE '%".$searchText."%'
								OR BaseTbl.nabl  LIKE '%".$searchText."%'
								OR BaseTbl.homeCollection  LIKE '%".$searchText."%'
								OR BaseTbl.discountOfferedTest  LIKE '%".$searchText."%'
								OR BaseTbl.createdBy  LIKE '%".$searchText."%'
							
								)";
			}
			$this->db->where('BaseTbl.masterCode LIKE "DIA%"' );
			$this->db->where('hospital.isDeleted', 0);
			 $this->db->where('BaseTbl.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
			$this->db->limit($page, $segment);
			$query = $this->db->get();
			//print_r($query );
			
			$result = $query->result();   
			
			return $result;
		}
		
		
		
			function getHospitalMasterCode(){
										 $this->db->select('id,mastercode,organisation');
			
										 $this->db->from('tbl_hospital');
										 $this->db->where('type', 2);
										 $this->db->where('isDeleted', 0);
										 $this->db->order_by('mastercode', 'ASC');
										 $query = $this->db->get();
										 $result=$query->result();
										 return $result;
			   
			  
		}
	
		/**
		* This function is used to  get last insert Id
		* create by Santosh Kumar
		* Date : 5 September 2018
		* @return boolean $result : TRUE / FALSE
		*/
		 
			function lastdiaid()
			{
			
			  $this->db->select('max(id) as id');
			
							$this->db->from('tbl_hospital_diaganostic');
							$this->db->where('isDeleted', 0);
							$this->db->where('mastercode like "DIA%"');
							$query = $this->db->get();
							$result=$query->result();
							return $result;
			
			
		  }
		  
		/**
		* This function is used to Add new  Diaganostic
		* create by Santosh Kumar
		* Date : 5 September 2018
		* @return boolean $result : TRUE / FALSE
		*/
		  
		 function addNewDiaganostic($diaganosticsInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_hospital_diaganostic', $diaganosticsInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
	
		/**
		* This function is used to  Diaganostic Info
		* created by Santosh Kumar  
		* Date : 5 September 2018
		*/
		function getDiaganosticInfo($id)
		{
		$this->db->select('id,
		mastercode,
		diaganosticSubCode,
		registrationNo,
		imaging,
		nabl,
		lab,
		
		digitalXray,
		digitalxray_offer,
		digitalxray_display,
		
		ultrasound,
		ultrasound_offer,
		ultrasound_display,
		
		ecg,
		ecg_offer,
		ecg_display,
		
		trademill,
		trademill_offer,
		trademill_display,
		
		twoDeco,
		eco_offer,
		eco_display,
		
		ct,
		ct_offer,
		ct_display,
		
		mri,
		mri_offer,
		mri_display,
		
		petCt,
		petct_offer,
		petct_display,
		
		daysOperational,
		timings,
		
		receptionArea,
		receptionarea_offer,
		receptionarea_display,
		
		waitingArea,
		waitingarea_offer,
		waitingarea_display,
		
		parkingSpace,
		parkingspace_offer,
		parkingspace_display,
		
		photograph,
		
		homeCollection,
		hcollect_charge,
		hcollect_time,
		
		discountOfferedTest,
		discountDisplayedTest,
		discountOfferdHealthCheckup,
		discountDisplayedHealthCheckupe,
		timefrom,timeto,
		time_24_hours,
		hard_copy,
		softcopy_onmail,
		
		sundayopen,
		sun_timefrom,
		suntimeto,
		reportsupload,
		discount_availableon_package,
		discountavailableonindividualtests,
		xray_film,
		
		location,
		address,
		district,
		city,
		state,
		pincode,
		pincodeother,
		
		
		createdBy');
        $this->db->from('tbl_hospital_diaganostic');
        $this->db->where('isDeleted', 0);
		$this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
		}
	
		
		
		/**
		* This function is used to  Diaganostic Info
		* created by Santosh Kumar  
		* Date : 5 September 2018
		*/
		function getDiaganostic_contactInfo($subcode)
		{
		$this->db->select(
		'id,
		 dia_id,
		mastercode,
		diaganosticSubCode,
		contactname,
		landline,
		email,
		mobile,
		');
        $this->db->from('tbl_diaganosticcentre_contact_details');
        $this->db->where('diaganosticSubCode', $subcode);
        $query = $this->db->get();
        
        return $query->result();
		}
	
		/**
		* This function is used to update  Diaganostic
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
		
		public function updateDiaganostic($hospitalDiaganosticInfo ,$id)
		{
		$this->db->where('id', $id);
		$query= $this->db->update('tbl_hospital_diaganostic', $hospitalDiaganosticInfo);
		return TRUE;
			
		}
		
		
		/**
		* This function is used to update  Diaganostic contact info
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
		
		public function updateDiaganosticscontactdetails($hospitalDiaganosticInfo ,$cid)
		{
		$this->db->where('id', $cid);
		$query= $this->db->update('tbl_diaganosticcentre_contact_details', $hospitalDiaganosticInfo);
		return TRUE;
			
		}
		
		
		
		/**
		* This function is used to add  new Diaganostics
		*created by santosh 5 September 2018
		*/
		function addNewDiaganostics($diaganosticsInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_hospital_diaganostic', $diaganosticsInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
		
		
		/**
		* This function is used to delete Diaganostic
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function deleteDiaganostic($id ,$diaganostic)
		{
		  
		  
		     $this->db->where('id', $id);
             $this->db->update('tbl_hospital_diaganostic', $diaganostic);
			 
			 //$this->db->where('id', $id);
			// $this->db->delete('tbl_hospital_diaganostic'); 
			 return $this->db->affected_rows();
			 
		
		}
		
		
		
		
		
			
		function  getState()
		{
			 $this->db->select('id,state');
			 $this->db->from('tbl_state');

			 $query = $this->db->get();
			$result=$query->result();

			return $result;
		}


		function  getStateName($id)
		{
			 $this->db->select('id,state');
			 $this->db->from('tbl_state');
		 $this->db->where('id', $id);
			 $query = $this->db->get();
			$result=$query->result();

			return $result;
		}
		function  getDistrictName($id)
		{
			 $this->db->select('id,district');
			 $this->db->from('tbl_district');
		 $this->db->where('id', $id);
		   
			 $query = $this->db->get();
			$result=$query->result();

			return $result;
		}
		function  getCityName($id)
		{
			 $this->db->select('id,city');
			 $this->db->from('tbl_city');
		 $this->db->where('id', $id);
		 
			 $query = $this->db->get();
			$result=$query->result();

			return $result;
		}
		function  getPincode($id)
		{
			 $this->db->select('id,pincode');
			 $this->db->from('tbl_pincode');
		 $this->db->where('id', $id);
			 $query = $this->db->get();
			$result=$query->result();

			return $result;
		}
		function fetch_district($id)
		{
			

			
			 $this->db->select('id,district');
			 $this->db->from('tbl_district');

			$this->db->where('stateId', $id);

			$query = $this->db->get();

			$output=$query->result();

			return $output;
		 }
 
		 function getType()
		 {
			 
			 $this->db->select('id,organisation');
			 $this->db->from('tbl_organisation_type');

			 $query = $this->db->get();
			$result=$query->result();

			return $result;
			 
			 
		}
		 
		 function fetch_city($id)
		 {
			 
			

			
			$this->db->select('id,city');
			$this->db->from('tbl_city');

			$this->db->where('district_id', $id);

			$query = $this->db->get();

			$output=$query->result();

			return $output;
		}
 
 
		 function fetch_pincode($id)
		 {
			

			
			$this->db->select('id,pincode');
			$this->db->from('tbl_pincode');

			$this->db->where('city_id', $id);

			$query = $this->db->get();

			$output=$query->result();

			return $output;
		}
		
		
		
		
		
		
		/**
		* This function is used to get last subcode ID
		*created by santosh 1 oct 2018
		*/
		
		function lastid()
		{
		               $this->db->select('max(id) as id');
					   
                       
		                $this->db->from('tbl_hospital_diaganostic');
						
						$this->db->where('diaganosticSubCode LIKE "DIA%"' );
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		   
		  
	    }
		
	
		
		/**
		* This function is used to get last subcode Details
		*created by santosh 1 oct 2018
		*/
		
		function lastsubcode_details($lastid)
		{
		               $this->db->select('id,mastercode,diaganosticSubCode');
		
		                $this->db->from('tbl_hospital_diaganostic');
						$this->db->where('id', $lastid);
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		   
		  
	    }
		
		
		
		/**
		* This function is used to add  new Diaganostics contact details
		*created by santosh 5 September 2018
		*/
		function addNewDiaganosticscontactdetails($diaganosticscontactInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_diaganosticcentre_contact_details', $diaganosticscontactInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
		
				
	    
		
		
		/**
		* This function is used to add  new Diaganostics Center contact details
		*created by santosh 5 September 2018
		*/
		function addNewDiaganosticscontactdetails2($diaganosticscontactInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_diaganosticcentre_contact_details', $diaganosticscontactInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
		
	
	
	    /**
		* This function is used to Get User Information details
		*created by santosh 1 Oct 2018
		*/
		
		function getUserName($userId)
		{
			
			$this->db->select('userId, name, email, mobile, permission,module');
			$this->db->from('tbl_users');
			$this->db->where('isDeleted', 0);
			//$this->db->where('roleId !=', 1);
			$this->db->where('userId', $userId);
			$query = $this->db->get();
			
			return $query->result();
		}
		
		
			/**
		* This function is used to change Master Code status
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function DiaganosticCenterSubcodeStatus($id ,$status)
		{
		  
		  
		     $this->db->where('id', $id);
             $this->db->update('tbl_hospital_diaganostic', $status);
			 $this->db->where('id', $id);
             $this->db->update('tbl_hospital', $status);
			 return $this->db->affected_rows();
			 
		
		}
		
		
	
    
    }

  