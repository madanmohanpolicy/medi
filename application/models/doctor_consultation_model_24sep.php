<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

			class Doctor_consultation_model extends CI_Model
			{
    
			/*** function for number of rows used in pagination */
			function doctorsListingCount()
			{
			$this->db->select( 'BaseTbl.id,hospital.organisation,hospital.id as hospitalId,hospital.location,hospital.timefrom,hospital.timeto,hospital.time_24_hours,BaseTbl.masterCode,BaseTbl.docSubCode,City.city,BaseTbl.doctorName, BaseTbl.specialization, BaseTbl.remarks, BaseTbl.qualification, BaseTbl.license,BaseTbl.experience,BaseTbl.opdConsultationDays,BaseTbl.timeSlot,BaseTbl.consultationCharges,BaseTbl.discountOffered,BaseTbl.discountDisplayed,BaseTbl.opinionViaMail,BaseTbl.opinionViaMailCharges,BaseTbl.opinionViaVideoConferencingCharges,BaseTbl.relship_manager,BaseTbl.photograph,BaseTbl.opinionmail,BaseTbl.opinionvideo,BaseTbl.time_24_hours,BaseTbl.timefrom,BaseTbl.timeto,BaseTbl.addressof_visitclinic,BaseTbl.availableonphone,BaseTbl.contactname,BaseTbl.landline,BaseTbl.email,BaseTbl.mobile');
			$this->db->from('tbl_doctors as BaseTbl');
			$this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.mastercode');
			$this->db->join('tbl_city as City', 'City.id = hospital.city'); 
			
            $this->db->where('hospital.city', $search_city);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('hospital.isDeleted', 0);
			$query = $this->db->get();
       
			return $query->num_rows();
			}
	
	
			/***** Function for Doctors Listing **/
			function doctorsListing($search_city,$specialisation)
			{
				
				
			$this->db->select( 'BaseTbl.id,hospital.organisation,hospital.id as hospitalId,hospital.location,hospital.timefrom,hospital.timeto,hospital.time_24_hours,BaseTbl.masterCode,BaseTbl.docSubCode,City.city,BaseTbl.doctorName, BaseTbl.specialization, BaseTbl.remarks, BaseTbl.qualification, BaseTbl.license,BaseTbl.experience,BaseTbl.opdConsultationDays,BaseTbl.timeSlot,BaseTbl.consultationCharges,BaseTbl.discountOffered,BaseTbl.discountDisplayed,BaseTbl.opinionViaMail,BaseTbl.opinionViaMailCharges,BaseTbl.opinionViaVideoConferencingCharges,BaseTbl.relship_manager,BaseTbl.photograph,BaseTbl.opinionmail,BaseTbl.opinionvideo,BaseTbl.time_24_hours,BaseTbl.timefrom,BaseTbl.timeto,BaseTbl.addressof_visitclinic,BaseTbl.availableonphone,BaseTbl.contactname,BaseTbl.landline,BaseTbl.email,BaseTbl.mobile');
		$this->db->from('tbl_doctors as BaseTbl');
			$this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.mastercode');
			$this->db->join('tbl_city as City', 'City.id = hospital.city'); 
			if($search_city){
            $this->db->where('hospital.city', $search_city);
			}
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('hospital.isDeleted', 0);
			if($specialisation){
			$this->db->where('BaseTbl.specialization',$specialisation);
			}
			
			//$this->db->where('Packageschedule.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
			// $this->db->limit($page, $segment);
			$query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
			}
	
	
			/*** Function For Getting Health Package Schedule by pasing package id of That package ***/
			function doctorsSchedule($subcode)
			{
			$this->db->select( 'id,subcode,type,day,timefrom,timeto');
			$this->db->from('tbl_admin_doctor_schedule ');
			
		    $this->db->where('isDeleted', 0);
			
		    $this->db->where('subcode', $subcode);
			$query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
		
			}	
          function doctorsQualification($qid){
			  $this->db->select( 'qualification.qualification');
			$this->db->from('tbl_doctors_qualification as qualification');
			$this->db->where('qualification.isDeleted', 0);
			$this->db->where('qualification.id', $qid);
			  $query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
		  }
		  
		   function doctorsSpeialization($spid){
			  $this->db->select( 'specialization.specialization');
			$this->db->from('tbl_doctors_specialization as specialization');
			$this->db->where('specialization.isDeleted', 0);
			$this->db->where('specialization.id', $spid);
			  $query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
		  }
		  
		  
		  
		  
		  	   function docType(){
			  $this->db->select( 'specialization.id,specialization.specialization');
			$this->db->from('tbl_doctors_specialization as specialization');
			$this->db->where('specialization.isDeleted', 0);
			
			  $query = $this->db->get();
      
			$result = $query->result();   
		
			return $result;
		  }
		  
		  
		  
		   function doctorsCity(){
			 $this->db->select('tbl_hospital.city as id,tbl_city.id,tbl_city.city as city');
        $this->db->from('tbl_hospital');
		$this->db->join('tbl_city ', 'tbl_hospital.city = tbl_city.id');
        $this->db->where('tbl_hospital.isDeleted', 0);
		
		//$this->db->where('roleId !=', 1);
       $this->db->group_by('tbl_city.id');
        $query = $this->db->get();
        
        return $query->result();
		  }
	      
		  
		  /*** function to check if time slot is already booked ***/
		  function ifBooked($docid,$mastercode,$appointmentdate,$appointmenttime)
		  {
			  
			  $this->db->select('id,booking_id,user_id,hospital_mastercode,doctor_id,appointment_date,appointment_time');
			$this->db->from('tbl_doctor_booking');
			

			$this->db->where('isDeleted', 0);
			$this->db->where('doctor_id',$docid);
			$this->db->where('hospital_mastercode',$mastercode);
			$this->db->where('appointment_date',$appointmentdate);
			$this->db->where('appointment_time',$appointmenttime);
			$query = $this->db->get();
       
			return $query->num_rows();
			  
			  
			  
			  
			}	
			
			  function ifBookedTime($docid,$mastercode,$subcode,$appointmentdate)
		  {
			  
			  $this->db->select('id,booking_id,user_id,hospital_mastercode,doctor_id,docSubCode,appointment_date,appointment_time');
			$this->db->from('tbl_doctor_booking');
			

			$this->db->where('isDeleted', 0);
			$this->db->where('doctor_id',$docid);
			$this->db->where('hospital_mastercode',$mastercode);
			$this->db->where('appointment_date',$appointmentdate);
			
			$query = $this->db->get();
       
				$result = $query->result();   
		
			return $result;
			  
			  
			  
			  
			}


            function getHospitalName($id)
    {
		
        $this->db->select('id, mastercode, organisation, type');
		   $this->db->from('tbl_hospital');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('mastercode', $id);
		 
        $query = $this->db->get();
        
        return $query->result();
    }			


              /*** function to get last booking id ***/
          	function lastbookid()
			{
	
		
		                 $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_doctor_booking');
						$this->db->where('isDeleted', 0);
						
					
		                $query = $this->db->get();
		                $result=$query->result();
					
		                return $result;
		
		
		
		
			}		  
	         
			 /*** Function for health package booking ***/
			function addNewBooking($bookinginfo)
			{

		
			$this->db->trans_start();
			$this->db->insert('tbl_doctor_booking', $bookinginfo);
        
			$insert_id = $this->db->insert_id();
        
			$this->db->trans_complete();
        
			return $insert_id;
			}
	         
			 
			 
			 /*** Function for uploading report ***/
			 function addNewReport($reportInfo)
			{

		
			$this->db->trans_start();
			$this->db->insert('tbl_healthcheckup_report', $reportInfo);
        
			$insert_id = $this->db->insert_id();
        
			$this->db->trans_complete();
        
			return $insert_id;
			}
			   /*** function for report information ***/
			   
			   function reportInfo($user_id)
			   {
				    $this->db->select('id,user_id,user_name,user_email,report_path,comment,report_upload_date');
		
		                $this->db->from('tbl_healthcheckup_report');
						$this->db->where('isDeleted', 0);
						$this->db->where('user_id',$user_id);
						
					
		                $query = $this->db->get();
		                $result=$query->result();
					
		                return $result;
		
				   
			   }   
			   
			
			 
			}	
	