<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hospital_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function hospitalListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.organisation, BaseTbl.type, BaseTbl.address,BaseTbl.location,BaseTbl.state,BaseTbl.state,BaseTbl.district,BaseTbl.city,BaseTbl.pincode,BaseTbl.nabh_accredited,BaseTbl.nabl_accredited,BaseTbl.description,BaseTbl.contactname,BaseTbl.email,BaseTbl.landline,BaseTbl.mobile');
        $this->db->from('tbl_hospital as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.organisation  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%'
							OR BaseTbl.mastercode  LIKE '%".$searchText."%'
							OR BaseTbl.type  LIKE '%".$searchText."%'
							OR BaseTbl.state  LIKE '%".$searchText."%'
							OR BaseTbl.district  LIKE '%".$searchText."%'
							OR BaseTbl.city  LIKE '%".$searchText."%'
							OR BaseTbl.location  LIKE '%".$searchText."%'
							OR BaseTbl.pincode  LIKE '%".$searchText."%'
							OR BaseTbl.contactname  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	 function hospitalDoctorsListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
        $this->db->from('tbl_doctors as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	
	
	 function medicalPractitionerCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
        $this->db->from('tbl_doctors as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.masterCode LIKE "MP%"' );
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	
	
	
	
	
	function editHospital($hospitalInfo, $id)
    {

        $this->db->where('id', $id);
       $query= $this->db->update('tbl_hospital', $hospitalInfo);
     
        return TRUE;
    }
      function editHospitalDoctors($doctorsInfo, $id)
	  {
		  $this->db->where('id', $id);
       $query= $this->db->update('tbl_doctors', $doctorsInfo);
     
        return TRUE;
		    
	  }
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function hospitalListing($searchText = '', $page, $segment)
    {
            $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.organisation, BaseTbl.type, BaseTbl.address,BaseTbl.location,BaseTbl.state,BaseTbl.state,BaseTbl.district,BaseTbl.city,BaseTbl.pincode,BaseTbl.nabh_accredited,BaseTbl.nabl_accredited,BaseTbl.description,BaseTbl.contactname,BaseTbl.email,BaseTbl.landline,BaseTbl.mobile');
        $this->db->from('tbl_hospital as BaseTbl');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
			
           $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.organisation  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%'
							OR BaseTbl.mastercode  LIKE '%".$searchText."%'
							OR BaseTbl.type  LIKE '%".$searchText."%'
							OR BaseTbl.state  LIKE '%".$searchText."%'
							OR BaseTbl.district  LIKE '%".$searchText."%'
							OR BaseTbl.city  LIKE '%".$searchText."%'
							OR BaseTbl.location  LIKE '%".$searchText."%'
							OR BaseTbl.pincode  LIKE '%".$searchText."%'
							OR BaseTbl.contactname  LIKE '%".$searchText."%')";
							 $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();   
		
        return $result;
    }
    
	
	 function hospitalDoctorsListing($searchText = '', $page, $segment)
    {
           $this->db->select('BaseTbl.id, BaseTbl.masterCode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
        $this->db->from('tbl_doctors as BaseTbl');
   $this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.masterCode');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('hospital.isDeleted', 0);
         $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();   
		
        return $result;
    }
    
   
   function medicalPractitioner($searchText = '', $page, $segment){
	   
	   
	                          $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
  $this->db->from('tbl_doctors as BaseTbl');
	$this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.masterCode');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
        }
		$this->db->where('BaseTbl.masterCode LIKE "MP%"' );
     $this->db->where('hospital.isDeleted', 0);
        $this->db->where('BaseTbl.isDeleted', 0);
    $this->db->order_by('BaseTbl.id', 'DESC');
   $this->db->limit($page, $segment);
	   
        $query = $this->db->get();
    
        $result = $query->result();   
		
        return $result;
								  
   }

  
  
    
    
    /**
     * This function is used to add new Hospital information to system
     * @return number $insert_id : This is last inserted id
     */
   
function addNewHospital($hospitalInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_hospital', $hospitalInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
	function getHospitalInfo($id)
    {
		
        $this->db->select('id, mastercode, organisation, type, address,location,state,district,city,pincode,nabh_accredited,nabl_accredited,description,contactname,email,landline,mobile');
        $this->db->from('tbl_hospital');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	function lastid(){
		               $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_hospital');
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		   
		  
	}
	
	
	
	
	
	function lastmpid()
	{
		                 $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_doctors');
						$this->db->where('isDeleted', 0);
						$this->db->where('mastercode like "MP%"');
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		
		
		
		
	}
	
	function lastmastercodeid($type =NULL)
	{
	
		
		                 $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_hospital');
						$this->db->where('isDeleted', 0);
						$this->db->where('mastercode like'.' "'.$type.'%"');
					
		                $query = $this->db->get();
		                $result=$query->result();
					
		                return $result;
		
		
		
		
	}
	
	

	
	
	
	
	function getHospitalMasterCode(){
		                             $this->db->select('id,mastercode');
		
		                             $this->db->from('tbl_hospital');
		                             $this->db->where('type', 4);
		                             $this->db->where('isDeleted', 0);
		                             $this->db->order_by('mastercode', 'ASC');
		                             $query = $this->db->get();
		                             $result=$query->result();
		                             return $result;
		   
		  
	}
	function getMedicalPractitionerMasterCode(){
		                             $this->db->select('id,mastercode');
		
		                             $this->db->from('tbl_hospital');
		                             $this->db->where('type', 5);
		                             $this->db->where('isDeleted', 0);
		                             $this->db->order_by('mastercode', 'ASC');
		                             $query = $this->db->get();
		                             $result=$query->result();
		                             return $result;
		   
		  
	}
	
	
	
	
	
	
	 function getHospitalInfoById($id)
    {
		
        $this->db->select('id, mastercode, organisation, type, address,location,state,district,city,pincode,nabh_accredited,nabl_accredited,description,contactname,email,landline,mobile');
        $this->db->from('tbl_hospital');
        $this->db->where('isDeleted', 0);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->row();
    }
	
	function deleteHospital($id, $hospitalInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_hospital', $hospitalInfo);
        
        return $this->db->affected_rows();
    }
	function deleteHospitalDoctor($id, $hospitalDoctorInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_doctors', $hospitalDoctorInfo);
        
        return $this->db->affected_rows();
    }
	
	function deleteMedicalPractitionerDoctor
	($id, $medicalPractitionerInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_doctors', $medicalPractitionerInfo);
        
        return $this->db->affected_rows();
    }
	
	
	
	
function  getState(){
	 $this->db->select('id,state');
	 $this->db->from('tbl_state');

	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}


function  getStateName($id){
	 $this->db->select('id,state');
	 $this->db->from('tbl_state');
 $this->db->where('id', $id);
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function  getDistrictName($id){
	 $this->db->select('id,district');
	 $this->db->from('tbl_district');
 $this->db->where('id', $id);
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function  getCityName($id){
	 $this->db->select('id,city');
	 $this->db->from('tbl_city');
 $this->db->where('id', $id);
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function  getPincode($id){
	 $this->db->select('id,pincode');
	 $this->db->from('tbl_pincode');
 $this->db->where('id', $id);
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function fetch_district($id){
	

	
	 $this->db->select('id,district');
	 $this->db->from('tbl_district');

  $this->db->where('stateId', $id);

  $query = $this->db->get();
 
$output=$query->result();

  return $output;
 }
 
 function getType(){
	 
	 $this->db->select('id,organisation');
	 $this->db->from('tbl_organisation_type');

	 $query = $this->db->get();
	$result=$query->result();

	return $result;
	 
	 
 }
 
 function fetch_city($id){
	

	
	 $this->db->select('id,city');
	 $this->db->from('tbl_city');

  $this->db->where('district_id', $id);

  $query = $this->db->get();
 
$output=$query->result();

  return $output;
 }
 
 
 function fetch_pincode($id){
	

	
	 $this->db->select('id,pincode');
	 $this->db->from('tbl_pincode');

  $this->db->where('city_id', $id);

  $query = $this->db->get();
 
$output=$query->result();

  return $output;
 }

	
	function addNewHospitalDoctors($doctorsInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_doctors', $doctorsInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
	function getHospitalDoctorInfo($id)
    {
		
        $this->db->select('id, masterCode, docSubCode, doctorName, specialization,remarks,qualification,license,experience,consultationCharges,discountOffered,discountDisplayed,opinionViaMail,opinionViaMailCharges,opinionViaVideoConferencing,opinionViaVideoConferencingCharges,photograph');
        $this->db->from('tbl_doctors');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	
    
}

  