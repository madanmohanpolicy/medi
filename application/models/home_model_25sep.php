<?php 
class Home_model extends CI_Model 
{
   function __construct()
    {
	parent::__construct();
    $this->load->database();	
	
	}
	
	public function getCity()
	{
		$this->db->select('id,city');
		$this->db->from('tbl_city');
		$this->db->order_by('city', 'asc');
		$query = $this->db->get();
		$result=$query->result();
		
		return $result;
	}
	
	function  getCityName($id){
	 $this->db->select('id,city');
	 $this->db->from('tbl_city');
	$this->db->where('id', $id);
 
	$query = $this->db->get();
	$result=$query->result();

		return $result;
	}
}