<?php

/**
 * This function used to   buyer model
 * Created by Santosh on 23st Aug, 2018
 * @param comments goes here
 */
class Buyer_model extends CI_model
{

    function __construct()
    {
        parent::__construct();
    }

    public function register_buier($user)
    {
        $this->db->insert('user', $user);
    }

    /**
     * This function used to check if any email id exist or not
     * Created by Madan on 26st Aug, 2018
     * @param comments goes here
     */
    public function is_email_exist($email)
    {
        if ($email != '') {
            $this->db->select('email');
            $this->db->from('tbl_buyers');
            $this->db->where('email', $email);
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }

    }

    /**
     * This function used to   buyer login
     * Created by Santosh on 26st Aug, 2018
     * Updated by Madan on 1st Sept, 2018
     * @param comments goes here
     */

    public function buyer_login($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        $this->db->where('status', 1);
        $query = $this->db->get('tbl_buyers');
        // Let's check if there are any results

        if ($query->num_rows() == 1) {
            // If there is a user, then create session data
            $row = $query->row();
            $data = array(
                'front_id' => $row->id,
                'front_name' => $row->name,
                'front_email' => $row->email,
                'front_mobile' => $row->mobile,
                'front_is_logged_in' => true
            );
            //$this->session->set_userdata($data);
            return $data;
        } else {
            // If the previous process did not validate
            // then return false.
            return false;
        }
    }

    public function getBuyer($id)
    {
        $this->db->select('id,name,mobile,email,password');
        $this->db->from('tbl_buyers');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updatePassword($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('tbl_buyers', $data);

        return true;
    }

    public function updateBuyer($id, $data)
    {


        $this->db->where('id', $id);
        $query = $this->db->update('tbl_buyers', $data);

        //readdir(buyer_profile);
        return true;
    }

    public function insertBuyer($buyerInfo)
    {
        if ($this->db->insert('tbl_buyers', $buyerInfo))
            return true;
        else
            return false;
    }

    public function verifyMobile($mobile, $data)
    {
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('tbl_buyers', $data);
        return true;
    }

    public function checkUserEmail($email)
    {
        $this->db->select('id,email');
        $this->db->from('tbl_buyers');
        $this->db->where('email', $email);
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function checkUserEmailExist($email)
    {
        $this->db->select('id,email');
        $this->db->from('tbl_buyers');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function corporateClientListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.name, BaseTbl.mobile, BaseTbl.email, BaseTbl.added_date');
        $this->db->from('tbl_buyers as BaseTbl');
        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.name !="System Administrator"');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function corporateClientListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.name, BaseTbl.mobile, BaseTbl.email, BaseTbl.added_date');
        $this->db->from('tbl_buyers as BaseTbl');
        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.name !="System Administrator"');
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function packageCorporateClientListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.name, BaseTbl.mobile, BaseTbl.email, BaseTbl.added_date');
        $this->db->from('tbl_buyers as BaseTbl');
        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.name !="System Administrator"');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function packageCorporateClientListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.service_start_date, BaseTbl.service_end_date, BaseTbl.assigned_by, 
        BaseTbl.client_id, client.client_name, healthPackage.name');
        $this->db->from('tbl_client_assigned_package as BaseTbl');
        $this->db->join('tbl_client as client', 'client.id = BaseTbl.client_id');
        $this->db->join('tbl_healthcheckup_packages as healthPackage', 'healthPackage.id = BaseTbl.package_id');
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    // This function will be called when any user will enetr his email for forgot password.
    function update_password_url_val($url_val, $email)
    {
        $result_user = $this->db->query("Update tbl_buyers set is_forgot_password = '1', forgot_url_val = '" . $url_val . "' WHERE email='" . $email . "' ");
    }

    function check_valid_forgot_password($url_val)
    {
        $result_user = $this->db->query("SELECT id,email FROM tbl_buyers WHERE forgot_url_val='" . $url_val . "'  AND is_forgot_password='1'");
        if ($result_user->num_rows() > 0) {
            return $result_user->result();
        } else {
            return false;
        }
        $result_user->free_result();
    }

    // This function will be called to update the new password .
    function update_password_val($password, $id)
    {
        if ($password != '' and $id > 0)
            $result_user = $this->db->query("Update tbl_buyers set is_forgot_password = '0', forgot_url_val = '', password = '" . md5($password) . "' WHERE id='" . $id . "' ");
    }

}


?>
