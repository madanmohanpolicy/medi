<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Corporatepackage_model extends CI_Model
{
    /***function for displaying health checkup packages that are linked  with hospital or diagnostic center**/
    function getHealthPackageList()
    {
        $this->db->select('BaseTbl.id, healthPackage.name');
        $this->db->from('tbl_healthcheckup_linking as BaseTbl');
        $this->db->join('tbl_healthcheckup_packages as healthPackage', 'healthPackage.id = BaseTbl.packageId');
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->group_by('BaseTbl.packageId');
        $this->db->order_by('BaseTbl.id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    /***function for displaying corporate client list**/
    function getCorporateClientList()
    {
        $this->db->select('id,client_name');
        $this->db->from('tbl_client');
        $this->db->where('isDeleted', 0);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    /***function for displaying client details**/
    public function getBuyer($field, $id)
    {
        $this->db->select('id,name,mobile,email,password');
        $this->db->from('tbl_buyers');
        $this->db->where($field, $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    /***function for displaying client count**/
    public function getClientCount($packageId, $clientId)
    {
        $this->db->select('id,client_id');
        $this->db->from('tbl_buyer_assigned_packages');
        $this->db->where('packageid', $packageId);
        $this->db->where('client_id', $clientId);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /***function for checking user exist with email/mobile**/
    public function checkUserEmailExist($email, $mobile)
    {
        $this->db->select('id,email');
        $this->db->from('tbl_buyers');
        $this->db->where('email', $email);
        $this->db->where('mobile', $mobile);
        $query = $this->db->get();
        return $query->row_array();
    }

    /***function for displaying corporate client list count**/
    public function corporateClientListingCount($clientId = '', $searchText = '')
    {
        $this->db->select('buyerAssignedPackage.id');
        $this->db->from('tbl_buyer_assigned_packages as buyerAssignedPackage');
        $this->db->join('tbl_buyers as BaseTbl', 'BaseTbl.id = buyerAssignedPackage.buyer_id');
        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($clientId) && $clientId != "") {
            $this->db->where('buyerAssignedPackage.client_id', $clientId);
        }
        $this->db->where('BaseTbl.is_corporate_user', 1);
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.name !="System Administrator"');
        $query = $this->db->get();
        return $query->num_rows();
    }

    /***function for displaying corporate client list details**/
    public function corporateClientListing($clientId = '', $searchText = '', $page, $segment)
    {
        $this->db->select('healthPackage.name as packagename, buyerAssignedPackage.id, buyerAssignedPackage.packageid, buyerAssignedPackage.client_id ,BaseTbl.id, BaseTbl.name, BaseTbl.mobile, BaseTbl.email, BaseTbl.added_date');
        $this->db->from('tbl_buyer_assigned_packages as buyerAssignedPackage');
        $this->db->join('tbl_buyers as BaseTbl', 'BaseTbl.id = buyerAssignedPackage.buyer_id');
        $this->db->join('tbl_healthcheckup_packages as healthPackage', 'healthPackage.id = buyerAssignedPackage.packageId');
        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($clientId) && $clientId != "") {
            $this->db->where('buyerAssignedPackage.client_id', $clientId);
        }
        $this->db->where('BaseTbl.is_corporate_user', 1);
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.name !="System Administrator"');
        $this->db->order_by('buyerAssignedPackage.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        //echo $this->db->last_query();
        //die;

        $result = $query->result();
        return $result;
    }

    /***function for displaying health checkup package with corporate client count**/
    public function packageCorporateClientListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id');
        $this->db->from('tbl_client_assigned_package as BaseTbl');
        $this->db->join('tbl_client as client', 'client.id = BaseTbl.client_id');
        $this->db->join('tbl_healthcheckup_packages as healthPackage', 'healthPackage.id = BaseTbl.package_id');
        if (!empty($searchText)) {
            $likeCriteria = "(healthPackage.name  LIKE '%" . $searchText . "%' OR  client.client_name  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /***function for displaying health checkup package with corporate client details**/
    public function packageCorporateClientListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.service_start_date, BaseTbl.service_end_date, BaseTbl.assigned_by, 
        BaseTbl.package_id, BaseTbl.client_id, client.client_name, healthPackage.name');
        $this->db->from('tbl_client_assigned_package as BaseTbl');
        $this->db->join('tbl_client as client', 'client.id = BaseTbl.client_id');
        $this->db->join('tbl_healthcheckup_packages as healthPackage', 'healthPackage.id = BaseTbl.package_id');
        if (!empty($searchText)) {
            $likeCriteria = "(healthPackage.name  LIKE '%" . $searchText . "%' OR  client.client_name  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

}
	