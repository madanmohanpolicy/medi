<?php
class Dynamic_dependent_model extends CI_Model
{
 function fetch_state()
 {
  $this->db->order_by("state", "ASC");
  	 $this->db->from('tbl_state');
  $query = $this->db->get();
  return $query->result();
 }

 function fetch_district($stateId)
 {
  $this->db->where('stateId', $stateId);
  $this->db->order_by('district', 'ASC');
   $this->db->from('tbl_district');
  $query = $this->db->get();
  $output = '<option value="">Select District</option>';
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->id.'">'.$row->district.'</option>';
  }
  return $output;
 }

 function fetch_city($districtId)
 {
  $this->db->where('districtId', $districtId);
  $this->db->order_by('city', 'ASC');
   $this->db->from('tbl_city'
  $query = $this->db->get();
  $output = '<option value="">Select City</option>';
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->id.'">'.$row->city.'</option>';
  }
  return $output;
 }
}

?>
