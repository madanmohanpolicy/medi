<?php 
class Home_model extends CI_Model 
{
   function __construct()
    {
	parent::__construct();
    $this->load->database();	
	
	}
	
	public function getMetroCity()
	{
		$this->db->select('id,city');
		$this->db->from('tbl_city');
		$this->db->order_by('city', 'asc');
		$this->db->where('metro_city', '1');
		$query = $this->db->get();
		$result=$query->result();
		
		return $result;
	}
	
	public function getCity()
	{
		$this->db->select('id,city');
		$this->db->from('tbl_city');
		$this->db->order_by('city', 'asc');
		$query = $this->db->get();
		$result=$query->result();
		
		return $result;
	}
	
	function  getCityName($id){
	 $this->db->select('id,city');
	 $this->db->from('tbl_city');
	$this->db->where('id', $id);
 
	$query = $this->db->get();
	$result=$query->result();

		return $result;
	}
	
	  function docType(){
					
			  $this->db->select( 'specialization.id,specialization.specialization');
			$this->db->from('tbl_doctors_specialization as specialization');
			$this->db->where('specialization.isDeleted', 0);
			
			  $query = $this->db->get();
      
			$result = $query->result();   
		
			return $result;
		  }
		  
		  
	
	
}