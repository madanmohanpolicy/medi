<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model {

	function __construct()
	{
        parent::__construct();
    }

	/**
	* This function used to check if any cart is there or not for any user. 
	* Created by Madan on 1st Sept, 2018
	* it returns false if no cart value is there other wise returns cart.
	*/
	
	function getCart($uid) 
	{
 		$error = 0;
		$total_row = 0;
		$message = "";
		if($uid > 0)
		{
			$this->db->select('*');
			$this->db->from('tbl_cart');  
			$this->db->where('user_id', $uid); 
			$this->db->where('status', '1'); 
			$this->db->where('isDeleted', '0');   					
			$query = $this->db->get();
						
			if($query->num_rows() > 0)
			{			
				$total_row = $query->num_rows(); 
				$message = $query->result();   			
			}
			else
			{
				$total_row = 0;
				$message = "Cart is empty"; 
			}
		}
		else
		{
			$error = 1;
			$message = "User id not valid";
		}	
		
		$final_data = array();
		$final_data['error'] = $error;
		$final_data['total_row'] = $total_row;
		$final_data['message'] = $message;
		return $final_data;
		
		
	}
	
	
	/**
	* This function used to check if any user's product is there or not in the cart.
	* Created by Madan on 5th Sept, 2018
	* it returns false if no cart value is there other wise returns product quantity.
	*/
	
	function getQuantity($pid,$uid)
	{
 		$error = 0;
		$total_row = 0;
		$message = false;
		if($uid > 0)
		{
			$this->db->select('quantity,id');
			$this->db->from('tbl_cart');  
			$this->db->where('user_id', $uid); 
			$this->db->where('status', '1'); 
			$this->db->where('isDeleted', '0');
			$this->db->where('product_id', $pid); 
			$query = $this->db->get();
						
			if($query->num_rows() > 0)
			{			
				$total_row = $query->num_rows(); 
				$message = $query->result();   			
			}
			else
			{
				$total_row = 0;
				$message = false; 
			}
		}
		else
		{			
			$message = false; 
		}	
		
		
		return $message;
		
		
	}
	
	
	/**
	* This function used to create the new cart for any user. 
	* Created by Madan on 1st Sept, 2018
	* it returns false if no cart value is there other wise returns cart.
	*/
	
	public function addCart($cartInfo)
	{  	   
	   if($this->db->insert('tbl_cart', $cartInfo))
			return true;
		else
			return false;
	}
	
	
	/**
	* This function used to remove the product from the cart for any user. 
	* Created by Madan on 6th Sept, 2018
	* it gets the id of cart table and deletes that product.
	*/
	
	public function remCart($cartInfo,$id)
	{        
	   $this->db->where('id', $id);
       $query= $this->db->update('tbl_cart', $cartInfo);
	   if($query)
			return true;
		else
			return false;
	}
	
	
	/**
	* This function used to add/remove the quantity of product in the cart for any user. 
	* Created by Madan on 6th Sept, 2018
	* it gets the id of cart table and set the new quantity of that product.
	*/
	
	public function changeCart($id,$cartInfo)
	{        
	   $this->db->where('id', $id);
       $query= $this->db->update('tbl_cart', $cartInfo);
	   if($query)
			return true;
		else
			return false;
	}
	
	
	
	/**
	* This function used to check if any user is having any address or not. If yes, it will return the address details. 
	* Created by Madan on 6th Sept, 2018
	* it returns false if no address value is found.
	*/
	
	function getAddress($uid, $field)
	{
 		$error = 0;
		$total_row = 0;
		$message = "";
		if($uid > 0)
		{
			$this->db->select('*');
			$this->db->from('tbl_buyer_address');  
			$this->db->where($field, $uid);
			$this->db->where('status', '1'); 
			$this->db->where('isDeleted', '0');
            $this->db->order_by('last_modified_date', 'DESC');
            $this->db->order_by('added_date', 'DESC');
			$query = $this->db->get();
						
			if($query->num_rows() > 0)
			{			
				$total_row = $query->num_rows(); 
				$message = $query->result();   			
			}
			else
			{
				$total_row = 0;
				$message = "No Address Available"; 
			}
		}
		else
		{
			$error = 1;
			$message = "User id not valid";
		}	
		
		$final_data = array();
		$final_data['error'] = $error;
		$final_data['total_row'] = $total_row;
		$final_data['message'] = $message;
		return $final_data;
		
	}
	
	/**
	* This function used to add the address of buyer 
	* Created by Madan on 19th Sept, 2018
	*/
	
	public function addAddress($addressInfo)
	{  	   
	   if($this->db->insert('tbl_buyer_address', $addressInfo))
			return true;
		else
			return false;
	}	
	
}