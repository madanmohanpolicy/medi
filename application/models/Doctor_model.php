<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	class Doctor_model extends CI_Model
	{
		/**
		 * This function is used to get the  Doctor Specialization listing count
		 * created By  Santosh Kumar Verma Date:14 September
		/* this function for 1MG Doctor Specialization Listing */
			
		function doctorSpecializationCount($searchText = '')
		{
			$this->db->select('BaseTbl.id, BaseTbl.specialization,createdDate');
			$this->db->from('tbl_doctors_specialization as BaseTbl');
			
	   
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.specialization  LIKE '%".$searchText."%'
								)";
				$this->db->where($likeCriteria);
			}
		
			$this->db->where('BaseTbl.isDeleted', 0);
		   
			$query = $this->db->get();
		   
			return $query->num_rows();
		}
		

		/**
		* This function is used to to  Doctor Specialization Listing 
		*created by santosh 14 September 2018
		*/
		function docSpecializationListing($searchText = '', $page, $segment)
		{
			$this->db->select('BaseTbl.id,BaseTbl.specialization,createdDate');
			$this->db->from('tbl_doctors_specialization as BaseTbl');
			
		
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
								 OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							     )";
			}
	        $this->db->where('BaseTbl.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
			$this->db->limit($page, $segment);
			$query = $this->db->get();
			$result = $query->result();   
			
			return $result;
		}
		
		  
		/**
		* This function is used to Add new Doctor Specialization
		* create by Santosh Kumar
		* Date : 14 September 2018
		* @return boolean $result : TRUE / FALSE
		*/
		  
		 function addDocSpecializationdetail($docInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_doctors_specialization', $docInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
	
		/**
		* This function is used to  Doctor Specialization Info
		* created by Santosh Kumar  
		* Date : 5 September 2018
		*/
		function getdoctorSpecializationInfo($id)
		{
			
			$this->db->select('id,specialization');
			$this->db->from('tbl_doctors_specialization');
			$this->db->where('isDeleted', 0);
			$this->db->where('id', $id);
			$query = $this->db->get();
			
			return $query->result();
		}
		
	
		
		/**
		* This function is used to update  Doctor Specialization
		* create by Santosh Kumar  Date:14 September
		* @return boolean $result : TRUE / FALSE
		*/
		
		public function updatedocSpecializationInfo($doctorSpecializationInfo ,$id)
		{
		$this->db->where('id', $id);
		$query= $this->db->update('tbl_doctors_specialization', $doctorSpecializationInfo);
		return TRUE;
			
		}
		
		
		
		/**
		* This function is used to delete Doctor Specialization
		* create by Santosh Kumar 14 September
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function deleteDocSpecialization($id ,$Doctor)
		{
			
		
		    $this->db->where('id', $id);
			$this->db->update('tbl_doctors_specialization', $Doctor);
			return $this->db->affected_rows();
			 
		
		}
		
		/**
		 * This function is used to get the  Doctor Qualification listing count
		 * created By  Santosh Kumar Verma Date:14 September
		/* this function for 1MG Doctor Qualification Listing */
			
		function doctorQualificationCount($searchText = '')
		{
			$this->db->select('BaseTbl.id, BaseTbl.qualification,createdDate');
			$this->db->from('tbl_doctors_qualification as BaseTbl');
			
	   
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.qualification  LIKE '%".$searchText."%'
								)";
				$this->db->where($likeCriteria);
			}
		
			$this->db->where('BaseTbl.isDeleted', 0);
		   
			$query = $this->db->get();
		   
			return $query->num_rows();
		}
		

		/**
		* This function is used to to  Doctor Qualification Listing 
		*created by santosh 14 September 2018
		*/
		function docQualificationListing($searchText = '', $page, $segment)
		{
			$this->db->select('BaseTbl.id,BaseTbl.qualification,createdDate');
			$this->db->from('tbl_doctors_Qualification as BaseTbl');
			
		
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.qualification  LIKE '%".$searchText."%'
								 
							     )";
			}
	        $this->db->where('BaseTbl.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
			$this->db->limit($page, $segment);
			$query = $this->db->get();
			$result = $query->result();   
			
			return $result;
		}
		
		  
		/**
		* This function is used to Add new Doctor Qualification
		* create by Santosh Kumar
		* Date : 14 September 2018
		* @return boolean $result : TRUE / FALSE
		*/
		  
		 function addDocQualificationdetail($docInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_doctors_qualification', $docInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
	
		/**
		* This function is used to  Doctor Qualification Info
		* created by Santosh Kumar  
		* Date : 5 September 2018
		*/
		function getdoctorQualificationInfo($id)
		{
			
			$this->db->select('id,qualification');
			$this->db->from('tbl_doctors_qualification');
			$this->db->where('isDeleted', 0);
			$this->db->where('id', $id);
			$query = $this->db->get();
			
			return $query->result();
		}
		
	
		
		/**
		* This function is used to update  Doctor Qualification
		* create by Santosh Kumar  Date:14 September
		* @return boolean $result : TRUE / FALSE
		*/
		
		public function updatedocQualificationInfo($doctorQualificationInfo ,$id)
		{
		$this->db->where('id', $id);
		$query= $this->db->update('tbl_doctors_qualification', $doctorQualificationInfo);
		return TRUE;
			
		}
		
		
		
		/**
		* This function is used to delete Doctor Qualification
		* create by Santosh Kumar 14 September
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function deleteDocQualification($id ,$Doctor)
		{
			
		
		    $this->db->where('id', $id);
			$this->db->update('tbl_doctors_qualification', $Doctor);
			return $this->db->affected_rows();
			 
		
		}
		
		
		
		
		
	
    
    }

  