<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hospital_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function hospitalListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.organisation, BaseTbl.type, BaseTbl.address,BaseTbl.location,BaseTbl.state,BaseTbl.state,BaseTbl.district,BaseTbl.city,BaseTbl.pincode,BaseTbl.pincodeother,BaseTbl.nabh_accredited,BaseTbl.nabl_accredited,BaseTbl.description,BaseTbl.contactname,BaseTbl.email,BaseTbl.landline,BaseTbl.mobile');
        $this->db->from('tbl_hospital as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.organisation  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%'
							OR BaseTbl.mastercode  LIKE '%".$searchText."%'
							OR BaseTbl.type  LIKE '%".$searchText."%'
							OR BaseTbl.state  LIKE '%".$searchText."%'
							OR BaseTbl.district  LIKE '%".$searchText."%'
							OR BaseTbl.city  LIKE '%".$searchText."%'
							OR BaseTbl.location  LIKE '%".$searchText."%'
							OR BaseTbl.pincode  LIKE '%".$searchText."%'
							OR BaseTbl.contactname  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	
	
	
	
	
	
	
	 function hospitalDoctorsListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
        $this->db->from('tbl_doctors as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	function hospitalScheduleListingCount($searchText = '',$subcode)
    {
		
        $this->db->select('BaseTbl.id, BaseTbl.subcode, BaseTbl.type, BaseTbl.day, BaseTbl.timefrom,BaseTbl.timeto');
        $this->db->from('tbl_admin_doctor_schedule as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.subcode  LIKE '%".$searchText."%'
                            OR  BaseTbl.type  LIKE '%".$searchText."%'
                            OR  BaseTbl.day  LIKE '%".$searchText."%'
							OR BaseTbl.timefrom  LIKE '%".$searchText."%'
							OR BaseTbl.timeto  LIKE '%".$searchText."%'
						
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.type LIKE "doctor"' );
		$this->db->where('BaseTbl.subcode',$subcode );
        $this->db->where('BaseTbl.isDeleted',0 );
       
        $query = $this->db->get();
     
        return $query->num_rows();
    }
	
	
	
	
	
	function healthPackageScheduleCount($searchText = '',$id)
    {
		
        $this->db->select('BaseTbl.id,BaseTbl.linkingId,BaseTbl.hospital_diaganostic,BaseTbl.package, BaseTbl.day, BaseTbl.timefrom,BaseTbl.timeto');
        $this->db->from('tbl_healthpackage_schedule as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.package  LIKE '%".$searchText."%'
                            
                            OR  BaseTbl.day  LIKE '%".$searchText."%'
							OR BaseTbl.timefrom  LIKE '%".$searchText."%'
							OR BaseTbl.timeto  LIKE '%".$searchText."%'
						
							)";
            $this->db->where($likeCriteria);
        }
		
		
        
        $this->db->where('BaseTbl.linkingId',$id);
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	
	
	
	 function medicalPractitionerCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
        $this->db->from('tbl_doctors as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.masterCode LIKE "MP%"' );
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	
	
	
	
	
	function editHospital($hospitalInfo, $id)
    {

       $this->db->where('id', $id);
       $query= $this->db->update('tbl_hospital', $hospitalInfo);
     
        return TRUE;
    }
      function editHospitalDoctors($doctorsInfo, $id)
	  {
		  $this->db->where('id', $id);
       $query= $this->db->update('tbl_doctors', $doctorsInfo);
     
        return TRUE;
		    
	  }
	  
	  function editHealthPackage($healthchekupPackageInfo, $id)
    {

        $this->db->where('id', $id);
       $query= $this->db->update('tbl_healthcheckup_packages', $healthchekupPackageInfo);
     
        return TRUE;
    }
      
	  
	  
	    
	  function editHealthTest($healthchekupTestInfo, $id)
    {

        $this->db->where('id', $id);
       $query= $this->db->update('tbl_health_tests', $healthchekupTestInfo);
     
        return TRUE;
    }
      
	   function editHealthTestParameter($healthchekupTestParameterInfo, $id)
    {

        $this->db->where('id', $id);
       $query= $this->db->update('tbl_healthcheckup_test_parameters', $healthchekupTestParameterInfo);
     
        return TRUE;
    }
	  
	  
	  /**** edit health checkup package linking ***/
	     
		 
		   function editHealthPackageLinking($healthchekupPackageLinkingInfo, $id)
    {

        $this->db->where('id', $id);
       $query= $this->db->update('tbl_healthcheckup_linking', $healthchekupPackageLinkingInfo);
     
        return TRUE;
    }
		 
	  
	  
	  
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function hospitalListing($searchText = '', $page, $segment)
    {
            $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.organisation, BaseTbl.type, BaseTbl.address,BaseTbl.location,BaseTbl.state,BaseTbl.state,BaseTbl.district,BaseTbl.city,BaseTbl.pincode,BaseTbl.pincodeother,BaseTbl.nabh_accredited,BaseTbl.nabl_accredited,BaseTbl.description,BaseTbl.contactname,BaseTbl.email,BaseTbl.landline,BaseTbl.mobile,BaseTbl.status');
        $this->db->from('tbl_hospital as BaseTbl');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
			
           $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.organisation  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%'
							OR BaseTbl.mastercode  LIKE '%".$searchText."%'
							OR BaseTbl.type  LIKE '%".$searchText."%'
							OR BaseTbl.state  LIKE '%".$searchText."%'
							OR BaseTbl.district  LIKE '%".$searchText."%'
							OR BaseTbl.city  LIKE '%".$searchText."%'
							OR BaseTbl.location  LIKE '%".$searchText."%'
							OR BaseTbl.pincode  LIKE '%".$searchText."%'
							OR BaseTbl.contactname  LIKE '%".$searchText."%')";
							 $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();   
		
        return $result;
    }
    
	
	 function hospitalDoctorsListing($searchText = '', $page, $segment)
    {
           $this->db->select('BaseTbl.id, BaseTbl.masterCode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
        $this->db->from('tbl_doctors as BaseTbl');
   $this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.masterCode');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('hospital.isDeleted', 0);
         $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();   
		
        return $result;
    }
    
	
	
	 function scheduleListing($searchText = '',$subcode ,$page, $segment)
    {
		
		
              $this->db->select('BaseTbl.id, BaseTbl.subcode, BaseTbl.type, BaseTbl.day, BaseTbl.timefrom,BaseTbl.timeto');
        $this->db->from('tbl_admin_doctor_schedule as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.subcode  LIKE '%".$searchText."%'
                            OR  BaseTbl.type  LIKE '%".$searchText."%'
                            OR  BaseTbl.day  LIKE '%".$searchText."%'
							OR BaseTbl.timefrom  LIKE '%".$searchText."%'
							OR BaseTbl.timeto  LIKE '%".$searchText."%'
						
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.type LIKE "doctor"' );
        
       $this->db->where('BaseTbl.subcode' ,$subcode );
     $this->db->where('BaseTbl.isDeleted' ,0 );
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();   

        return $result;
    }
    
	
	 function healthPackageSchedule($searchText = '',$page, $segment,$id)
    {
		
		 $this->db->select('BaseTbl.id, BaseTbl.linkingId,BaseTbl.package,BaseTbl.hospital_diaganostic, BaseTbl.day, BaseTbl.timefrom,BaseTbl.timeto');
        $this->db->from('tbl_healthpackage_schedule as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.package  LIKE '%".$searchText."%'
                          
                            OR  BaseTbl.day  LIKE '%".$searchText."%'
							OR BaseTbl.timefrom  LIKE '%".$searchText."%'
							OR BaseTbl.timeto  LIKE '%".$searchText."%'
						
							)";
            $this->db->where($likeCriteria);
        }
		
		 $this->db->where('BaseTbl.linkingId',$id);
		 $this->db->where('BaseTbl.isDeleted', 0);
		 $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();   
	
        return $result;
		
		
	}
	
	
	
	
	
	
   
   function medicalPractitioner($searchText = '', $page, $segment){

        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.docSubCode, BaseTbl.doctorName, BaseTbl.specialization,BaseTbl.remarks,BaseTbl.qualification,BaseTbl.license,BaseTbl.experience,BaseTbl.consultationCharges');
  $this->db->from('tbl_doctors as BaseTbl');
	$this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.masterCode');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.docSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.specialization  LIKE '%".$searchText."%'
							OR BaseTbl.doctorName  LIKE '%".$searchText."%'
							OR BaseTbl.remarks  LIKE '%".$searchText."%'
							OR BaseTbl.qualification  LIKE '%".$searchText."%'
							OR BaseTbl.license  LIKE '%".$searchText."%'
							OR BaseTbl.experience  LIKE '%".$searchText."%'
							OR BaseTbl.consultationCharges  LIKE '%".$searchText."%'
							)";
        }
		$this->db->where('BaseTbl.masterCode LIKE "MP%"' );
     $this->db->where('hospital.isDeleted', 0);
        $this->db->where('BaseTbl.isDeleted', 0);
    $this->db->order_by('BaseTbl.id', 'DESC');
   $this->db->limit($page, $segment);
	   
        $query = $this->db->get();
    
        $result = $query->result();   
		
        return $result;
								  
   }

  
  
    
    
    /**
     * This function is used to add new Hospital information to system
     * @return number $insert_id : This is last inserted id
     */
   
function addNewHospital($hospitalInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_hospital', $hospitalInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
	function getHospitalInfo($id)
    {
		
        $this->db->select('id, mastercode, organisation, type, address,location,state,district,city,pincode,pincodeother,nabh_accredited,nabl_accredited,timefrom,timeto,time_24_hours,description,contactname,email,landline,mobile');
        $this->db->from('tbl_hospital');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
		
	function getOrganisationName($id)
    {
		
        $this->db->select('id, mastercode, organisation, type');
		   $this->db->from('tbl_hospital');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
		 
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	
	
	
	function getDoctorNameBySubcode($subcode)
    {
		
        $this->db->select('id, doctorName');
        $this->db->from('tbl_doctors');
        $this->db->where('isDeleted', 0);
		   $this->db->where('docSubCode', $subcode);
		//$this->db->where('roleId !=', 1);
      //  $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	function lastid(){
		               $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_hospital');
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		   
		  
	}
	
	
	
	
	
	function lastmpid()
	{
		                 $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_doctors');
						$this->db->where('isDeleted', 0);
						$this->db->where('mastercode like "MP%"');
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		
		
		
		
	}
	
	function lastdocid(){
		
		  $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_doctors');
						$this->db->where('isDeleted', 0);
						$this->db->where('mastercode like "CP%"');
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		
		
	}

	
	
	
	function lastmastercodeid($type =NULL)
	{
	
		
		                 $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_hospital');
						$this->db->where('isDeleted', 0);
						$this->db->where('mastercode like'.' "'.$type.'%"');
					
		                $query = $this->db->get();
		                $result=$query->result();
					
		                return $result;
		
		
		
		
	}
	
	

	
	
	
	
	function getHospitalMasterCode(){
		                             $this->db->select('id,mastercode,organisation');
		                             $this->db->from('tbl_hospital');
		                             $this->db->where('type', 4);
		                             $this->db->where('isDeleted', 0);
		                             $this->db->order_by('mastercode', 'ASC');
		                             $query = $this->db->get();
		                             $result=$query->result();
		                             return $result;
		   
		  
	}
	function getMedicalPractitionerMasterCode(){
		                             $this->db->select('id,mastercode,organisation');
		
		                             $this->db->from('tbl_hospital');
		                             $this->db->where('type', 5);
		                             $this->db->where('isDeleted', 0);
		                             $this->db->order_by('mastercode', 'ASC');
		                             $query = $this->db->get();
		                             $result=$query->result();
		                             return $result;
		   
		  
	}
	
	function getHealthCheckupPackage(){
		                             $this->db->select('id,name');
		
		                             $this->db->from('tbl_healthcheckup_packages');
		                            
		                             $this->db->where('isDeleted', 0);
		                             $this->db->order_by('name', 'ASC');
		                             $query = $this->db->get();
		                             $result=$query->result();
		                             return $result;
		   
		  
	}
	
	
	
	
	
	
	
	
	 function getHospitalInfoById($id)
    {
		
        $this->db->select('id, mastercode, organisation, type, address,location,state,district,city,pincode,nabh_accredited,nabl_accredited,description,contactname,email,landline,mobile');
        $this->db->from('tbl_hospital');
        $this->db->where('isDeleted', 0);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->row();
    }
	
	function deleteHospital($id, $hospitalInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_hospital', $hospitalInfo);
        
        return $this->db->affected_rows();
    }
	function deleteHospitalDoctor($id, $hospitalDoctorInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_doctors', $hospitalDoctorInfo);
        
        return $this->db->affected_rows();
    }
	function deleteHealthTestParameter($id, $healthCheckupTestParameterInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_healthcheckup_test_parameters', $healthCheckupTestParameterInfo);
     
        return $this->db->affected_rows();
    }
	
	
	
	
	
	function deleteMedicalPractitionerDoctor
	($id, $medicalPractitionerInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_doctors', $medicalPractitionerInfo);
        
        return $this->db->affected_rows();
    }
	
	
	function deleteHealthPackage
	($id, $healthPackage)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_healthcheckup_packages', $healthPackage);
        
        return $this->db->affected_rows();
    }
	
	function deleteHealthTest($id, $healthTest)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_health_tests', $healthTest);
        
        return $this->db->affected_rows();
    }
	
	
	
	
	
	
function  getState(){
	 $this->db->select('id,state');
	 $this->db->from('tbl_state');

	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}


function  getStateName($id){
	 $this->db->select('id,state');
	 $this->db->from('tbl_state');
 $this->db->where('id', $id);
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function  getDistrictName($id){
	 $this->db->select('id,district');
	 $this->db->from('tbl_district');
 $this->db->where('id', $id);
   
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function  getCityName($id){
	 $this->db->select('id,city');
	 $this->db->from('tbl_city');
 $this->db->where('id', $id);
 
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function  getPincode($id){
	 $this->db->select('id,pincode');
	 $this->db->from('tbl_pincode');
 $this->db->where('id', $id);
	 $query = $this->db->get();
	$result=$query->result();

	return $result;
}
function fetch_district($id){
	

	
	 $this->db->select('id,district');
	 $this->db->from('tbl_district');

  $this->db->where('stateId', $id);

  $query = $this->db->get();
 
$output=$query->result();

  return $output;
 }
 
 function getType(){
	 
	 $this->db->select('id,organisation');
	 $this->db->from('tbl_organisation_type');

	 $query = $this->db->get();
	$result=$query->result();

	return $result;
	 
	 
 }
 
 function fetch_city($id){
	 
	

	
	 $this->db->select('id,city');
	 $this->db->from('tbl_city');

  $this->db->where('district_id', $id);

  $query = $this->db->get();
 
$output=$query->result();

  return $output;
 }
 
 
 function fetch_pincode($id){
	

	
	 $this->db->select('id,pincode');
	 $this->db->from('tbl_pincode');

  $this->db->where('city_id', $id);

  $query = $this->db->get();
 
$output=$query->result();

  return $output;
 }

	
	function addNewHospitalDoctors($doctorsInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_doctors', $doctorsInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
	function addHealthCheckUpPackageLinking($packageInfo)
    {
   
	  
        $this->db->trans_start();
        $this->db->insert('tbl_healthcheckup_linking', $packageInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	function addScheduledoctor($scheduleInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_admin_doctor_schedule', $scheduleInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
	
	function addSchedulepackage($scheduleInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_healthpackage_schedule', $scheduleInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
	
	
	
	function getHospitalDoctorInfo($id)
    {
		
        $this->db->select('id, masterCode, docSubCode, doctorName, specialization,remarks,qualification,license,experience,consultationCharges,discountOffered,discountDisplayed,opinionViaMail,opinionViaMailCharges,opinionViaVideoConferencing,opinionViaVideoConferencingCharges,relship_manager,
		
		opinionmail,
		opinionvideo,
		time_24_hours,
		timefrom,
		timeto,
		addressof_visitclinic,
		availableonphone,
		contactname,
		mobile,
		landline,
		email,
		
		
		photograph');
        $this->db->from('tbl_doctors');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function getHospitalDoctorSubcode($id)
    {
		
        $this->db->select('id,docSubCode');
        $this->db->from('tbl_doctors');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	
	function getHealthCheckupInfo($id)
    {

		
        $this->db->select('id, name,tests, cost, discount,
		referred_by,
		needto_visit_hospital,
		home_samplecollection,
		fasting,
		center_visit,
		time_24_hours,
		pkg_charge,
		timefrom,
		timeto,
		description');
        $this->db->from('tbl_healthcheckup_packages');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
       
    }
	
	
	
	
	function getHealthCheckupTestInfo($id)
    {
		
        $this->db->select('id,name');
        $this->db->from('tbl_health_tests');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	
	function getHealthCheckupTestParameterInfo($id)
    { 
		
        $this->db->select('id,parameterName,testId');
        $this->db->from('tbl_healthcheckup_test_parameters');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	
	
	
	
	function getHealthCheckupPackageName($id)
    {
		
        $this->db->select(' name');
        $this->db->from('tbl_healthcheckup_packages');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	function getHealthTestsName()
    {
		
        $this->db->select('id,name');
        $this->db->from('tbl_health_tests');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        //$this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
		
	function getHealthTestsNameById($id)
    {
		
        $this->db->select('name');
        $this->db->from('tbl_health_tests');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	
	/*model  for healthchekup package listing */
	 
	   function healthCheckupPackageListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.name,BaseTbl.tests, BaseTbl.cost, BaseTbl.discount, BaseTbl.createdBy,BaseTbl.createdDate');
        $this->db->from('tbl_healthcheckup_packages as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.cost  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.discount  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function healthCheckupPackageListing($searchText = '', $page, $segment)
    {
         $this->db->select('BaseTbl.id, BaseTbl.name, BaseTbl.tests,BaseTbl.cost, BaseTbl.discount, BaseTbl.createdBy,BaseTbl.createdDate,BAseTbl.updatedDate');
        $this->db->from('tbl_healthcheckup_packages as BaseTbl');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.cost  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.discount  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	
	
	
	
	
	
	    
    function healthChekupTest($searchText = '', $page, $segment)
    {
         $this->db->select('BaseTbl.id, BaseTbl.packageid,BaseTbl.name, BaseTbl.cost, BaseTbl.discount, BaseTbl.createdBy,BaseTbl.createdDate,BAseTbl.updatedDate');
        $this->db->from('tbl_health_tests as BaseTbl');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.cost  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.discount  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	
	
	
	/***function for listing healthchekup linking  with hospital or diaganostic center  **/
	
	 function healthPackageLinking($searchText = '', $page, $segment)
    {
		
		
		
       $this->db->select('BaseTbl.id, BaseTbl.hospital_diagonastic_id, BaseTbl.type, BaseTbl.packageId, BaseTbl.cost,BaseTbl.memberDiscount,BaseTbl.mediwheelDiscount,BaseTbl.createdBy,BaseTbl.createdDate,BaseTbl.updatedDate');
		
       $this->db->from('tbl_healthcheckup_linking as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.hospital_diagonastic_id  LIKE '%".$searchText."%'
                            OR  BaseTbl.type  LIKE '%".$searchText."%'
                            OR  BaseTbl.packageId  LIKE '%".$searchText."%'
							OR BaseTbl.cost  LIKE '%".$searchText."%'
							OR BaseTbl.memberDiscount  LIKE '%".$searchText."%'
							OR BaseTbl.mediwheelDiscount  LIKE '%".$searchText."%'
							OR BaseTbl.createdBy  LIKE '%".$searchText."%'
							OR BaseTbl.createdDate  LIKE '%".$searchText."%'
							OR BaseTbl.updatedDate  LIKE '%".$searchText."%'
							)";
        $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
      $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
	
	
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	
	
	
	  function healthCheckupTestParametersCount($searchText = '',$id)
    {
           $this->db->select('BaseTbl.id, BaseTbl.parameterName, BaseTbl.testId, BaseTbl.updatedBy, BaseTbl.createdBy,BaseTbl.createdDate,BaseTbl.updatedDate');
        $this->db->from('tbl_healthcheckup_test_parameters as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.parameterName  LIKE '%".$searchText."%'
                            OR  BaseTbl.createdBy  LIKE '%".$searchText."%'
                            OR  BaseTbl.updatedBy  LIKE '%".$searchText."%'
							 OR  BaseTbl.createdDate  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
		  $this->db->where('BaseTbl.testId', $id);
       
       
         $query = $this->db->get();
        
        return $query->num_rows();
    }
	function gethealthCheckupTestParametersCount($id)	{
		
		 $this->db->select('BaseTbl.id, BaseTbl.parameterName, BaseTbl.testId');
        $this->db->from('tbl_healthcheckup_test_parameters as BaseTbl');
		
		
		   $this->db->where('BaseTbl.isDeleted', 0);
		  $this->db->where('BaseTbl.testId', $id);
       
       
         $query = $this->db->get();
        
        return $query->num_rows();
	}
	
	
	
	
	
		   function healthCheckupTetsCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.name, BaseTbl.cost, BaseTbl.discount, BaseTbl.createdBy,BaseTbl.createdDate');
        $this->db->from('tbl_health_tests as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.cost  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.discount  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
	
	
	
	 function healthPackageLinkingCount($searchText = '')
    {
		
        $this->db->select('BaseTbl.id, BaseTbl.hospital_diagonastic_id, BaseTbl.type, BaseTbl.packageId, BaseTbl.cost,BaseTbl.memberDiscount,BaseTbl.mediwheelDiscount,BaseTbl.createdBy,BaseTbl.createdDate,BaseTbl.updatedDate,BaseTbl.isDeleted');
        $this->db->from('tbl_healthcheckup_linking as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.hospital_diagonastic_id  LIKE '%".$searchText."%'
                            OR  BaseTbl.type  LIKE '%".$searchText."%'
                            OR  BaseTbl.packageId  LIKE '%".$searchText."%'
							OR BaseTbl.cost  LIKE '%".$searchText."%'
							OR BaseTbl.memberDiscount  LIKE '%".$searchText."%'
							OR BaseTbl.mediwheelDiscount  LIKE '%".$searchText."%'
							OR BaseTbl.createdBy  LIKE '%".$searchText."%'
							OR BaseTbl.createdDate  LIKE '%".$searchText."%'
							OR BaseTbl.updatedDate  LIKE '%".$searchText."%'
							)";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
    
        return $query->num_rows();
    }
	
	
	
	
	   function healthChekupTestParameters($searchText = '', $page, $segment,$id)
    {
		
        $this->db->select('BaseTbl.id, BaseTbl.parameterName, BaseTbl.testId, BaseTbl.updatedBy, BaseTbl.createdBy,BaseTbl.createdDate,BaseTbl.updatedDate');
        $this->db->from('tbl_healthcheckup_test_parameters as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.parameterName  LIKE '%".$searchText."%'
                            OR  BaseTbl.createdBy  LIKE '%".$searchText."%'
                            OR  BaseTbl.updatedBy  LIKE '%".$searchText."%'
							 OR  BaseTbl.createdDate  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
		  $this->db->where('BaseTbl.testId', $id);
       
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
	
	
	
	function addHealthCheckUpPackage($packageInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_healthcheckup_packages', $packageInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
		function addHealthCheckUpTest($testInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_health_tests', $testInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
	
		function addHealthCheckUpTestParameter($parameterInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_healthcheckup_test_parameters', $parameterInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
    
	
	function getUserName($userId)
    {
		
        $this->db->select('userId, name, email, mobile, permission,module');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function fetch_facility($id)
    {
		
		if($id=='1'){
			$facilitytype='4';
		}
		if($id=='2'){
			$facilitytype='2';
		}
        $this->db->select('id, mastercode, organisation, address, location,city');
        $this->db->from('tbl_hospital');
        $this->db->where('isDeleted', 0);
		$this->db->where('status=', 1);
        $this->db->where('type', $facilitytype);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	/**** Delete HealthPackage Linking ****/
		function deletehealthPackageLinking($id, $healthPackageLinkingInfo)
    {
	
        $this->db->where('id', $id);
        $this->db->update('tbl_healthcheckup_linking', $healthPackageLinkingInfo);
        
        return $this->db->affected_rows();
    }
	
	function getHealthCheckupPackageLinkingInfo($id)
    {

		
        $this->db->select('id, hospital_diagonastic_id,type, packageId, cost,memberDiscount,member_discount_inpercent,mediwheelDiscount,mediwheel_discount_inpercent,time_slot,home_collection,	vendor_decided_name');
        $this->db->from('tbl_healthcheckup_linking');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
       
    }
	function getHealthCheckupPackageScheduleInfo($id)
    {

		
        $this->db->select('id,linkingId, hospital_diaganostic, package, day,timefrom,timeto');
        $this->db->from('tbl_healthpackage_schedule');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
       
    }
	
	
	
	  
	
	  function editHealthPackageSchedule($editscheduleInfo, $id)
    {

        $this->db->where('id', $id);
       $query= $this->db->update('tbl_healthpackage_schedule', $editscheduleInfo);
     
        return TRUE;
    }
	
	
	/**** Delete HealthPackage Schedule ****/
		function deleteHealthPackageSchedule($id, $healthPackageScheduleInfo)
    {
	
        $this->db->where('id', $id);
        $this->db->update('tbl_healthpackage_schedule', $healthPackageScheduleInfo);
        
        return $this->db->affected_rows();
    }
	
		function deleteDoctorSchedule($id, $hospitalDoctorScheduleInfo)
    {
	
        $this->db->where('id', $id);
        $this->db->update('tbl_admin_doctor_schedule', $hospitalDoctorScheduleInfo);
        
        return $this->db->affected_rows();
    }
	
	
	
	function getDoctorScheduleInfo($id)
    {

		
        $this->db->select('id,subcode, type, day,timefrom,timeto');
        $this->db->from('tbl_admin_doctor_schedule');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
       
    }
	
	
	
	 function editDoctorSchedule($editscheduleInfo, $id)
    {

        $this->db->where('id', $id);
       $query= $this->db->update('tbl_admin_doctor_schedule', $editscheduleInfo);
     
        return TRUE;
    }
	
	
	
	
			  function healthPackageBookingCount($searchText = ''){
					
					$this->db->select( 'id,booking_id,user_id,user_name,user_email,user_mobile,hospital_id,hospital_name,cost,discount,cost_after_discount,package_id,package_name,appointment_date,appointment_time');
			$this->db->from('tbl_healthcheckup_package_booking ');
			if(!empty($searchText)) {
            $likeCriteria = "(user_name  LIKE '%".$searchText."%'
                            OR  hospital_name LIKE '%".$searchText."%'
                            OR  cost  LIKE '%".$searchText."%'
							OR cost_after_discount  LIKE '%".$searchText."%'
							OR package_name LIKE '%".$searchText."%'
							OR appointment_date LIKE '%".$searchText."%'
							OR appointment_time  LIKE '%".$searchText."%'
							
							)";
            $this->db->where($likeCriteria);
        }
            
			$this->db->where('isDeleted', 0);
			
			$query = $this->db->get();
       
			return $query->num_rows();
					
				}

					
			    function healthPackageBooking($searchText = '', $page, $segment){
					
					
					
					$this->db->select( 'id,booking_id,user_id,user_name,user_email,user_mobile,hospital_id,hospital_name,cost,discount,cost_after_discount,package_id,package_name,appointment_date,appointment_time,is_corporate_user,corporate_name');
			$this->db->from('tbl_healthcheckup_package_booking ');
			if(!empty($searchText)) {
            $likeCriteria = "(user_name  LIKE '%".$searchText."%'
                            OR  hospital_name LIKE '%".$searchText."%'
                            OR  cost  LIKE '%".$searchText."%'
							OR cost_after_discount  LIKE '%".$searchText."%'
							OR package_name LIKE '%".$searchText."%'
							OR appointment_date LIKE '%".$searchText."%'
							OR appointment_time  LIKE '%".$searchText."%'
							
							)";
            $this->db->where($likeCriteria);
        }
            
			$this->db->where('isDeleted', 0);
			$query = $this->db->get();
            $this->db->limit($page, $segment);
			$result = $query->result();   
		
			return $result;
			
				}
	
	
	
		
    /**
	*############################################################################
	* These  all function   created by Santosh Kumar
	*
	* ########################################################################
	*/
	
	//this  function count  hospital diagnostic  create by Santosh Kumar
       
	 function hospitaldiagnosticListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.diaganosticSubCode, BaseTbl.registrationNo, BaseTbl.nabl,BaseTbl.lab,BaseTbl.digitalXray,BaseTbl.ultrasound,BaseTbl.ecg,BaseTbl.	trademill');
        $this->db->from('tbl_hospital_diaganostic as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.diaganosticSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.registrationNo  LIKE '%".$searchText."%'
							
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	/**
	* This function is used to to hospital Diaganostic Listing 
	*created by santosh 28 Aug 2018
	*/
	 function hospitalDiaganosticListing($searchText = '', $page, $segment)
    {
     $this->db->select('BaseTbl.id,hospital.organisation as organisation, BaseTbl.masterCode, BaseTbl.diaganosticSubCode,BaseTbl.registrationNo, BaseTbl.nabl, BaseTbl.lab,BaseTbl.photograph,BaseTbl.homeCollection,BaseTbl.discountOfferedTest,BaseTbl.discountDisplayedTest,BaseTbl.createdBy,BaseTbl.status');
         $this->db->from('tbl_hospital_diaganostic as BaseTbl');
         $this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.masterCode');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.diaganosticSubCode  LIKE '%".$searchText."%'
                            OR  BaseTbl.registrationNo  LIKE '%".$searchText."%'
							OR BaseTbl.nabl  LIKE '%".$searchText."%'
							OR BaseTbl.homeCollection  LIKE '%".$searchText."%'
							OR BaseTbl.discountOfferedTest  LIKE '%".$searchText."%'
							OR BaseTbl.createdBy  LIKE '%".$searchText."%'
						
							)";
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('hospital.isDeleted', 0);
         $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
		//print_r($query );
        
        $result = $query->result();   
		
        return $result;
    }
	
    
	/**
	* This function is used to delete HospitalDiaganostic
	* create by Santosh Kumar
	* @return boolean $result : TRUE / FALSE
	*/
   
    function deleteHospitalDiaganostic($id ,$hospitalDiaganostic)
	{
	    # for soft delete
	  	$this->db->where('id', $id);
        $this->db->update('tbl_hospital_diaganostic', $hospitalDiaganostic);
		 
		//$this->db->where('id', $id);
        //$this->db->delete('tbl_hospital_diaganostic'); 
		 
		 
		 return $this->db->affected_rows();
		 
	
    }
	
	
	/**
	* This function is used to update Hospital Diaganostic
	* create by Santosh Kumar
	* @return boolean $result : TRUE / FALSE
	*/
	
	public function updateHospitalDiaganostic($hospitalDiaganosticInfo ,$id)
	{
	$this->db->where('id', $id);
    $query= $this->db->update('tbl_hospital_diaganostic', $hospitalDiaganosticInfo);
    return TRUE;
		
	}
	
	
	/**
	* This function is used to update Hospital Diaganostic
	* create by Santosh Kumar
	* @return boolean $result : TRUE / FALSE
	*/
	
	public function updateHospitalChemistShop($hospitalDiaganosticInfo ,$id)
	{
	$this->db->where('id', $id);
    $query= $this->db->update(' tbl_chemist_shop', $hospitalDiaganosticInfo);
    return TRUE;
		
	}
	
	
	/**
	* This function is used to Hospital Diaganostic Info
	*created by santosh 29 Aug 2018
	*/
	function getHospitalDiaganosticInfo($id)
    {
		$this->db->select('id,
		mastercode,
		diaganosticSubCode,
		registrationNo,
		imaging,
		nabl,
		lab,
		
		digitalXray,
		digitalxray_offer,
		digitalxray_display,
		
		ultrasound,
		ultrasound_offer,
		ultrasound_display,
		
		ecg,
		ecg_offer,
		ecg_display,
		
		trademill,
		trademill_offer,
		trademill_display,
		
		twoDeco,
		eco_offer,
		eco_display,
		
		ct,
		ct_offer,
		ct_display,
		
		mri,
		mri_offer,
		mri_display,
		
		petCt,
		petct_offer,
		petct_display,
		
		daysOperational,
		timings,
		
		receptionArea,
		receptionarea_offer,
		receptionarea_display,
		
		waitingArea,
		waitingarea_offer,
		waitingarea_display,
		
		parkingSpace,
		parkingspace_offer,
		parkingspace_display,
		
		photograph,
		
		homeCollection,
		hcollect_charge,
		hcollect_time,
		
		discountOfferedTest,
		discountDisplayedTest,
		discountOfferdHealthCheckup,
		discountDisplayedHealthCheckupe,
		timefrom,timeto,
		time_24_hours,
		hard_copy,
		softcopy_onmail,
		
		sundayopen,
		sun_timefrom,
		suntimeto,
		reportsupload,
		discount_availableon_package,
		discountavailableonindividualtests,
		xray_film,
		
		location,
		address,
		district,
		city,
		state,
		pincode,
		pincodeother,
		
		
		createdBy');
        $this->db->from('tbl_hospital_diaganostic');
        $this->db->where('isDeleted', 0);
		$this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	/**
	* This function is used to open form for add a shop to the system
	*created by santosh 28 Aug 2018
	*/
	function addNewHospitalChemistShops($chemistshopInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_chemist_shop', $chemistshopInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	/**
	* This function is used to add  new HospitalDiaganostics
	*created by santosh 28 Aug 2018
	*/
	function addNewHospitalDiaganostics($diaganosticsInfo)
    {

		
        $this->db->trans_start();
        $this->db->insert('tbl_hospital_diaganostic', $diaganosticsInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	
		
	/**
	* This function is used to get last insert ID  for  diagnostic
	*created by santosh 28 Aug 2018
	*/
	
	function lastdiaid()
	{
	
	  $this->db->select('max(id) as id');
	
					$this->db->from('tbl_hospital_diaganostic');
					$this->db->where('isDeleted', 0);
					$this->db->where('mastercode like "CP%"');
					$query = $this->db->get();
					$result=$query->result();
					return $result;
	
	
    }
	  
	  
	 /**
     * This function is used to add new Hospital chemist shop Listing to system
     * @return number $insert_id : This is last inserted id
	 *created by santosh 22 Aug 2018
     */
	 
	 function hospitalChemistShopsListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.masterCode,BaseTbl.registrationn_no, BaseTbl.shopsubcode,BaseTbl.home_delivery,BaseTbl.days,BaseTbl.dis_off_onfmcg');
        $this->db->from(' tbl_chemist_shop as BaseTbl');
        $this->db->join('tbl_hospital as hospital', 'hospital.mastercode = BaseTbl.masterCode');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.shopsubcode  LIKE '%".$searchText."%'
                            OR  BaseTbl.home_delivery  LIKE '%".$searchText."%'
							
							)";
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('hospital.isDeleted', 0);
         $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get(); 
  
        $result = $query->result();   
		
        return $result;
    }
	
	
	
   /**
	* This function is used to delete hospitalChemistShop 
	* create by Santosh Kumar
	* @return boolean $result : TRUE / FALSE
	*/
   
    function deletehospitalChemistShop($id ,$hospitalChemistShopinfo)
	{
	  
		 //$this->db->where('id', $id);
         //$this->db->delete('tbl_chemist_shop'); 
		 
		$this->db->where('id', $id);
        $this->db->update('tbl_chemist_shop', $hospitalChemistShopinfo);
		 return $this->db->affected_rows();
		 
	
    }
	
	
	/**
	* This function is used to Hospital Chemist Shop  Info
	*created by santosh 29 Aug 2018
	*/
	function getHospitalChemistShopInfo($id)
    {
		$this->db->select('id, mastercode, shopsubcode,registrationn_no,
		days,
		ayurvedic_medicines,
		offeron_ayurvedic_medicines,
		displayon_ayurvedic_medicines,
		home_delivery,
		homeopathy_medicines,
		offeredon_homeopathy_medicines,
		displayon_homeopathy_medicines,
		allopathy_medicines,
		timings,
		offeredon_allopathy_medicines,
		displayedon_allopathy_medicines,
		unani_medicines,
		offeredon_unani_medicines,
		displayon_unani_medicines,
		dis_off_onconsumables,
		dis_off_onfmcg,
		
		dis_off_onspecific_brand,
		dis_tobe_diplay_onconsumables,
		dis_tobe_display_onemcg,
		dis_tobe_display_onspecific_brand,
		time_24_hours,
		timefrom,
		timeto,
		photograph,
		createdBy');
        $this->db->from('tbl_chemist_shop');
        $this->db->where('isDeleted', 0);
		$this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
	/**
	* This function is used to Hospital Chemist Shop moster code
	*created by santosh 5 September  2018
	*/
		function getChemistShopMasterCode(){
		                             $this->db->select('id,mastercode,organisation');
		
		                             $this->db->from('tbl_hospital');
		                             $this->db->where('type', 4);
		                             $this->db->where('isDeleted', 0);
		                             $this->db->order_by('mastercode', 'ASC');
		                             $query = $this->db->get();
		                             $result=$query->result();
		                             return $result;
		   
		  
	}
	
	/**
	* This function is used to get Hospital Chemist Shop Last ID
	*created by santosh 5 September  2018
	*/
	function lastchid(){
		
		  $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_chemist_shop');
						$this->db->where('isDeleted', 0);
						$this->db->where('mastercode like "CP%"');
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		
		
	}
	
	
	
	
	//count chemist shop 
	
	
	function hospitalChemistShopCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.mastercode, BaseTbl.shopsubcode, BaseTbl.registrationn_no, BaseTbl.offeron_ayurvedic_medicines,BaseTbl.displayon_ayurvedic_medicines,BaseTbl.home_delivery,BaseTbl.offeredon_homeopathy_medicines,BaseTbl.displayon_homeopathy_medicines,BaseTbl.days');
        $this->db->from('tbl_chemist_shop as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.mastercode  LIKE '%".$searchText."%'
                            OR  BaseTbl.shopsubcode  LIKE '%".$searchText."%'
                            OR  BaseTbl.registrationn_no  LIKE '%".$searchText."%'
							OR BaseTbl.offeron_ayurvedic_medicines  LIKE '%".$searchText."%'
							OR BaseTbl.displayon_ayurvedic_medicines  LIKE '%".$searchText."%'
							OR BaseTbl.home_delivery  LIKE '%".$searchText."%'
							OR BaseTbl.offeredon_homeopathy_medicines  LIKE '%".$searchText."%'
							
							)";
            $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.masterCode LIKE "CP%"' );
        $this->db->where('BaseTbl.isDeleted', 0);
       
        $query = $this->db->get();
       
        return $query->num_rows();
    }
	
	
	
	/**
	* This function is used to get  last  Package ID 
	*created by santosh 12 September  2018
	*/
	function lastpkid(){
		
		  $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_healthcheckup_packages');
						$this->db->where('isDeleted', 0);
						$this->db->where('pkg_id like "PKG%"');
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		
		
	}
	
    
	
	
	/**
	* This function is used to get  Packages ID  
	*created by santosh 12 September  2018
	*/
	function getpkgid($healthpackageid){
		
		  $this->db->select('pkg_id');
		
		                $this->db->from('tbl_healthcheckup_packages');
						$this->db->where('isDeleted', 0);
						$this->db->where('id', $healthpackageid);
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		
		
	}
	
	
	
	
	/**
	* This function is used to get  Reffred Doctor  subcode 
	*created by santosh 21 September  2018
	*/
	
    function getReferredDoctorSubcode($id)
    {
		
        $this->db->select('id,docSubCode');
        $this->db->from('tbl_doctors');
        $this->db->where('isDeleted', 0);
		//$this->db->where('roleId !=', 1);
        $this->db->where('id', $id);
		$this->db->where('masterCode LIKE "MP%"' );
        $query = $this->db->get();
        
        return $query->result();
    }
	
	
		 function reffscheduleListing($searchText = '',$subcode ,$page, $segment)
    {
		
		
              $this->db->select('BaseTbl.id, BaseTbl.subcode, BaseTbl.type, BaseTbl.day, BaseTbl.timefrom,BaseTbl.timeto');
              $this->db->from('tbl_admin_doctor_schedule as BaseTbl');
   
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.subcode  LIKE '%".$searchText."%'
                            OR  BaseTbl.type  LIKE '%".$searchText."%'
                            OR  BaseTbl.day  LIKE '%".$searchText."%'
							OR BaseTbl.timefrom  LIKE '%".$searchText."%'
							OR BaseTbl.timeto  LIKE '%".$searchText."%'
						
							)";
        $this->db->where($likeCriteria);
        }
		$this->db->where('BaseTbl.type LIKE "referredDoctor"' );
        $this->db->where('BaseTbl.subcode' ,$subcode );
	  
	
       $this->db->where('BaseTbl.isDeleted' ,0 );
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();   

        return $result;
    }
	
	
	/**
	* This function is used to get  Reffred Doctor  count Search 
	*created by santosh 21 September  2018
	*/
	
		function hospitalReffredScheduleListingCount($searchText = '',$subcode)
		{
			
				$this->db->select('BaseTbl.id, BaseTbl.subcode, BaseTbl.type, BaseTbl.day, BaseTbl.timefrom,BaseTbl.timeto');
				$this->db->from('tbl_admin_doctor_schedule as BaseTbl');
		   
				if(!empty($searchText)) {
					$likeCriteria = "(BaseTbl.subcode  LIKE '%".$searchText."%'
									OR  BaseTbl.type  LIKE '%".$searchText."%'
									OR  BaseTbl.day  LIKE '%".$searchText."%'
									OR BaseTbl.timefrom  LIKE '%".$searchText."%'
									OR BaseTbl.timeto  LIKE '%".$searchText."%'
								
									)";
					$this->db->where($likeCriteria);
				}
				$this->db->where('BaseTbl.type LIKE "referredDoctor"' );
				$this->db->where('BaseTbl.subcode',$subcode );
				$this->db->where('BaseTbl.isDeleted',0 );
			   
				$query = $this->db->get();
			 
				return $query->num_rows();
		}
	

	
	
		function editReffredDoctorSchedule($editscheduleInfo, $id)
		{

			$this->db->where('id', $id);
		    $query= $this->db->update('tbl_admin_doctor_schedule', $editscheduleInfo);
     
            return TRUE;
		}
		
		
		//fetch SubCode
		function fetch_subCode($mastercode)
		{
			
			
			$this->db->select('id, mastercode, diaganosticSubCode,location,city');
			$this->db->from('tbl_hospital_diaganostic');
			$this->db->where('isDeleted', 0);
			//$this->db->where('roleId !=', 1);
			$this->db->where('mastercode', $mastercode);
			$query = $this->db->get();
			
			return $query->result();
		}
		
		
		
		//fetch masterCode
		function fetch_masterCode($id)
		{
			
			
			$this->db->select('id, mastercode, organisation');
			$this->db->from('tbl_hospital');
			$this->db->where('isDeleted', 0);
			//$this->db->where('roleId !=', 1);
			$this->db->where('id', $id);
			$query = $this->db->get();
			
			return $query->result();
		}
		
		
		//get last subcodeID
		
		
		
		function lastsubcodeid()
		{
		               $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_hospital_diaganostic');
						$this->db->where('diaganosticSubCode LIKE "CP%"' );
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		   
		  
	    }
		
		
		//get last subcode Details
		
		
		function lastsubcode_details($lastid)
		{
		               $this->db->select('id,mastercode,diaganosticSubCode');
		
		                $this->db->from('tbl_hospital_diaganostic');
						$this->db->where('id', $lastid);
		                $query = $this->db->get();
		                $result=$query->result();
		                return $result;
		   
		  
	    }
		
		
		/**
		* This function is used to add Hospital  new Diaganostics contact details
		*created by santosh 1 Oct 2018
		*/
		function addNewDiaganosticscontactdetails($diaganosticscontactInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_diaganosticcentre_contact_details', $diaganosticscontactInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
	
	
       	/**
		* This function is used to change Master Code status
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function mastercodeStatus($id ,$status)
		{
		  
		  
		   
			 $this->db->where('id', $id);
             $this->db->update('tbl_hospital', $status);
			 return $this->db->affected_rows();
			 
		
		}
	
	
	
		/**
		* This function is used to Chnage Hospital Diaganostic Subcode Status
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
	   
		function chnageHospitalDiaganosticSubcodeStatus($id ,$status)
		{
		  
		  
		     $this->db->where('id', $id);
             $this->db->update('tbl_hospital_diaganostic', $status);
			 return $this->db->affected_rows();
			 
		
		}
		
		
		/**
		* This function is used to Hospital  Diaganostic Info
		* created by Santosh Kumar  
		* Date : 3 octobar  2018
		*/
		function getHospitalDiaganostic_contactInfo($subcode)
		{
		$this->db->select(
		'id,
		 dia_id,
		mastercode,
		diaganosticSubCode,
		contactname,
		landline,
		email,
		mobile,
		');
        $this->db->from('tbl_diaganosticcentre_contact_details');
        $this->db->where('diaganosticSubCode', $subcode);
        $query = $this->db->get();
        
        return $query->result();
		}
		
		
		/**
		* This function is used to update Hospital  Diaganostic contact info
		* create by Santosh Kumar
		* @return boolean $result : TRUE / FALSE
		*/
		
		public function updateDiaganosticscontactdetails($hospitalDiaganosticInfo ,$cid)
		{
			$this->db->where('id', $cid);
			$query= $this->db->update('tbl_diaganosticcentre_contact_details', $hospitalDiaganosticInfo);
			return TRUE;
			
		}
		
		
		
		/**
		* This function is used to add  new Hospital  Diaganostics Center contact details
		*created by santosh 5 September 2018
		*/
		function addNewDiaganosticscontactdetails2($diaganosticscontactInfo)
		{

			
			$this->db->trans_start();
			$this->db->insert('tbl_diaganosticcentre_contact_details', $diaganosticscontactInfo);
			
			$insert_id = $this->db->insert_id();
			
			$this->db->trans_complete();
			
			return $insert_id;
		}
		
		
		
		/**
		* This function is check Duplicate records  from  Health Checkup Package Linking   
		*created by santosh 5 September 2018
		*/
		function checkDuplicateSubcode($packageInfo)
		{ 
		// echo "pp". $packageInfo['packageId'];
		// echo "<br/>";
			//print_r($packageInfo);
			 
			  $this->db->select('BaseTbl.id, BaseTbl.hospital_diagonastic_id, BaseTbl.type, BaseTbl.packageId, BaseTbl.vendor_decided_name');
				$this->db->from('tbl_healthcheckup_linking as BaseTbl');
		   
				
				
				$this->db->where('BaseTbl.diaganosticSubCodea',$packageInfo['diaganosticSubCode']);
				$this->db->where('BaseTbl.type',$packageInfo['type']);
				$this->db->where('BaseTbl.healthchk_id',$packageInfo['healthchk_id']);
				$this->db->where('BaseTbl.packageId',$packageInfo['packageId']);
				$this->db->where('BaseTbl.hospital_diagonastic_id',$packageInfo['hospital_diagonastic_id']);
				$this->db->where('BaseTbl.cost',$packageInfo['cost']);
				
				
				$this->db->where('BaseTbl.memberDiscount',$packageInfo['memberDiscount']);
				$this->db->where('BaseTbl.member_discount_inpercent',$packageInfo['member_discount_inpercent']);
				$this->db->where('BaseTbl.mediwheelDiscount',$packageInfo['mediwheelDiscount']);
				$this->db->where('BaseTbl.mediwheel_discount_inpercent',$packageInfo['mediwheel_discount_inpercent']);
				$this->db->where('BaseTbl.cost',$packageInfo['hospital_diagonastic_id']);
				
				
				
				$this->db->where('BaseTbl.isDeleted',0);
			   
				$query = $this->db->get();
			 
				return $query->num_rows();
		}
		
		//Get type info
		
		function getTypeInfo($id)
		{
			
			$this->db->select('type');
			$this->db->from('tbl_hospital');
			$this->db->where('id =', $id);
			$this->db->where('isDeleted', 0);
			$query = $this->db->get();
			return $query->result();
			
			
		}
		
		
		//Get  Location , city Name   for sub code
		function  getsubcodeCityName($id)
		{
				$this->db->select('id,city');
				$this->db->from('tbl_city');
				$this->db->where('id', $id);

				$query = $this->db->get();
				$result=$query->result();

				return $result;
		}


		
		
}

  