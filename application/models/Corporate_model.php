<?php

class Corporate_model extends CI_model
{
	
	function __construct()
	{
        parent::__construct();
    }
 
 
 
 function getMobile($skey){

	
	 if($skey != '')
		{
			$this->db->select('mobile');
			$this->db->from('tbl_buyers');
			$this->db->where('corporate_link',$skey);
			$this->db->where('status',0);
				$this->db->where('isDeleted',0);
			$query=$this->db->get();

			
				return $query->result();
				 
		}
		else
		{
			return false;
		}
	 
 }
 
 
 function createCorporatePassword($corporateuserinfo,$scrtkey){
	 
        $this->db->where('corporate_link', $scrtkey);
       $query= $this->db->update('tbl_buyers', $corporateuserinfo);
     
        return TRUE;
	 
	 
	 
	 
 }
 
 
 function getCorporateUserInfo($scrtkey){
	 
	 	
	 if($scrtkey != '')
		{
			$this->db->select('id,name,mobile,email,client_id');
			$this->db->from('tbl_buyers');
			$this->db->where('corporate_link',$scrtkey);
			$this->db->where('status',1);
			$this->db->where('is_mobile_verified',1);
				$this->db->where('isDeleted',0);
			$query=$this->db->get();

			
				return $query->result();
				 
		}
		else
		{
			return false;
		}
	 
 }
 
 }