<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

			class Healthpackage_model extends CI_Model
			{
    
			/*** function for number of rows used in pagination */
			function healthPackageListingCount()
			{
			$this->db->select( 'BaseTbl.id,hospital.organisation,hospital.id as hospitalId,hospital.location,hospital.timefrom,hospital.timeto,hospital.time_24_hours,BaseTbl.time_slot,BaseTbl.PackageId,City.city,hospital.landline,hospital.mobile,packages.name ,BaseTbl.id, BaseTbl.type, BaseTbl.cost, BaseTbl.memberDiscount, BaseTbl.member_discount_inpercent,BaseTbl.mediwheelDiscount,BaseTbl.mediwheel_discount_inpercent,BaseTbl.home_collection');
			$this->db->from('tbl_healthcheckup_linking as BaseTbl');
			$this->db->join('tbl_hospital as hospital', 'hospital.id = BaseTbl.hospital_diagonastic_id');
			$this->db->join('tbl_healthcheckup_packages as packages', 'packages.id = BaseTbl.PackageId');
			$this->db->join('tbl_city as City', 'City.id = hospital.city'); 
			
            $this->db->where('hospital.city', $search_city);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('packages.isDeleted', 0);
			$query = $this->db->get();
       
			return $query->num_rows();
			}
	
	
			/***** Function for HealthPackage Listing **/
			function healthPackageListing($search_city)
			{
			$this->db->select( 'BaseTbl.id,hospital.organisation,hospital.id as hospitalId,hospital.location,hospital.timefrom,hospital.timeto,hospital.time_24_hours,BaseTbl.time_slot,BaseTbl.PackageId,City.city,hospital.landline,hospital.mobile,packages.name ,BaseTbl.id, BaseTbl.type, BaseTbl.cost, BaseTbl.memberDiscount, BaseTbl.member_discount_inpercent,BaseTbl.mediwheelDiscount,BaseTbl.mediwheel_discount_inpercent,BaseTbl.home_collection');
			$this->db->from('tbl_healthcheckup_linking as BaseTbl');
			$this->db->join('tbl_hospital as hospital', 'hospital.id = BaseTbl.hospital_diagonastic_id');
			$this->db->join('tbl_healthcheckup_packages as packages', 'packages.id = BaseTbl.PackageId');
			$this->db->join('tbl_city as City', 'City.id = hospital.city'); 
			
            $this->db->where('hospital.city', $search_city);
			
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('hospital.isDeleted', 0);
			$this->db->where('packages.isDeleted', 0);
			//$this->db->where('Packageschedule.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
			// $this->db->limit($page, $segment);
			$query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
			}
	
	
			/*** Function For Getting Health Package Schedule by pasing package id of That package ***/
			function getHealthPackageSchedule($pkgId,$hosid)
			{
			$this->db->select( 'package.day,package.timefrom,package.timeto');
			$this->db->from('tbl_healthpackage_schedule as package');
			$this->db->join('tbl_hospital as hospital', 'package.hospital_diaganostic=hospital.id');
		    $this->db->where('package.isDeleted', 0);
			$this->db->where('package.package',$pkgId);
			$this->db->where('hospital.isDeleted', 0);
		    $this->db->where('hospital.id', $hosid);
			$query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
		
			}	
    
	      
		  
		  /*** function to check if time slot is already booked ***/
		  function ifBooked($packageid,$organisationid,$appointmentdate,$appointmenttime)
		  {
			  
			  $this->db->select('id,booking_id,user_id,hospital_id,package_id,appointment_date,appointment_time');
			$this->db->from('tbl_healthcheckup_package_booking');
			

			$this->db->where('isDeleted', 0);
			$this->db->where('package_id',$packageid);
			$this->db->where('hospital_id',$organisationid);
			$this->db->where('appointment_date',$appointmentdate);
			$this->db->where('appointment_time',$appointmenttime);
			$query = $this->db->get();
       
			return $query->num_rows();
			  
			  
			  
			  
			}	
			
			  function ifBookedTime($packageid,$organisationid,$appointmentdate)
		  {
			  
			  $this->db->select('id,booking_id,user_id,hospital_id,package_id,appointment_date,appointment_time');
			$this->db->from('tbl_healthcheckup_package_booking');
			

			$this->db->where('isDeleted', 0);
			$this->db->where('package_id',$packageid);
			$this->db->where('hospital_id',$organisationid);
			$this->db->where('appointment_date',$appointmentdate);
			
			$query = $this->db->get();
       
				$result = $query->result();   
		
			return $result;
			  
			  
			  
			  
			}	


              /*** function to get last booking id ***/
          	function lastbookid()
			{
	
		
		                 $this->db->select('max(id) as id');
		
		                $this->db->from('tbl_healthcheckup_package_booking');
						$this->db->where('isDeleted', 0);
						
					
		                $query = $this->db->get();
		                $result=$query->result();
					
		                return $result;
		
		
		
		
			}		  
	         
			 /*** Function for health package booking ***/
			function addNewBooking($bookinginfo)
			{

		
			$this->db->trans_start();
			$this->db->insert('tbl_healthcheckup_package_booking', $bookinginfo);
        
			$insert_id = $this->db->insert_id();
        
			$this->db->trans_complete();
        
			return $insert_id;
			}
	         
			 
			 
			 /*** Function for uploading report ***/
			 function addNewReport($reportInfo)
			{

		
			$this->db->trans_start();
			$this->db->insert('tbl_healthcheckup_report', $reportInfo);
        
			$insert_id = $this->db->insert_id();
        
			$this->db->trans_complete();
        
			return $insert_id;
			}
			   /*** function for report information ***/
			   
			   function reportInfo($user_id)
			   {
				    $this->db->select('id,user_id,user_name,user_email,report_path,comment,report_upload_date');
		
		                $this->db->from('tbl_healthcheckup_report');
						$this->db->where('isDeleted', 0);
						$this->db->where('user_id',$user_id);
						
					
		                $query = $this->db->get();
		                $result=$query->result();
					
		                return $result;
		
				   
			   }   
			   
			
			 
			}	
	