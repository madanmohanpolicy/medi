<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

			class Healthpackage_model extends CI_Model
			{
    
			/*** function for number of rows used in pagination */
			function healthPackageListingCount()
			{
			$this->db->select('hospital.organisation,hospital.location,BaseTbl.time_slot,City.city,hospital.landline,hospital.mobile,packages.name,BaseTbl.id, BaseTbl.type, BaseTbl.cost, BaseTbl.memberDiscount, BaseTbl.member_discount_inpercent,BaseTbl.mediwheelDiscount,BaseTbl.mediwheel_discount_inpercent,BaseTbl.home_collection');
			$this->db->from('tbl_healthcheckup_linking as BaseTbl');
			$this->db->join('tbl_hospital as hospital', 'hospital.id = BaseTbl.hospital_diagonastic_id');
			$this->db->join('tbl_healthcheckup_packages as packages', 'packages.id = BaseTbl.PackageId'); 
			$this->db->join('tbl_city as City', 'City.id = hospital.city'); 

			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('packages.isDeleted', 0);
			$query = $this->db->get();
       
			return $query->num_rows();
			}
	
	
			/***** Function for HealthPackage Listing **/
			function healthPackageListing()
			{
			$this->db->select( 'BaseTbl.id,hospital.organisation,hospital.id as hospitalId,hospital.location,BaseTbl.time_slot,BaseTbl.PackageId,City.city,hospital.landline,hospital.mobile,packages.name ,BaseTbl.id, BaseTbl.type, BaseTbl.cost, BaseTbl.memberDiscount, BaseTbl.member_discount_inpercent,BaseTbl.mediwheelDiscount,BaseTbl.mediwheel_discount_inpercent,BaseTbl.home_collection');
			$this->db->from('tbl_healthcheckup_linking as BaseTbl');
			$this->db->join('tbl_hospital as hospital', 'hospital.id = BaseTbl.hospital_diagonastic_id');
			$this->db->join('tbl_healthcheckup_packages as packages', 'packages.id = BaseTbl.PackageId');
			$this->db->join('tbl_city as City', 'City.id = hospital.city'); 

			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('hospital.isDeleted', 0);
			$this->db->where('packages.isDeleted', 0);
			//$this->db->where('Packageschedule.isDeleted', 0);
			$this->db->order_by('BaseTbl.id', 'DESC');
			// $this->db->limit($page, $segment);
			$query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
			}
	
	
			/*** Function For Getting Health Package Schedule by pasing package id of That package ***/
			function getHealthPackageSchedule($pkgId,$hosid)
			{
			$this->db->select( 'package.day,package.timefrom,package.timeto');
			$this->db->from('tbl_healthpackage_schedule as package');
			$this->db->join('tbl_hospital as hospital', 'package.hospital_diaganostic=hospital.id');
		    $this->db->where('package.isDeleted', 0);
			$this->db->where('package.package',$pkgId);
			$this->db->where('hospital.isDeleted', 0);
		    $this->db->where('hospital.id', $hosid);
			$query = $this->db->get();
        
			$result = $query->result();   
		
			return $result;
		
			}	
    
	
	
	
}	
	